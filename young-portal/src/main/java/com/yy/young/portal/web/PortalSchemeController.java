package com.yy.young.portal.web;

import com.yy.young.common.util.CommonUtil;
import com.yy.young.common.util.Result;
import com.yy.young.common.util.StringUtils;
import com.yy.young.dal.util.Page;
import com.yy.young.interfaces.log.annotation.Log;
import com.yy.young.interfaces.model.User;
import com.yy.young.portal.model.Menu;
import com.yy.young.portal.model.Scheme;
import com.yy.young.portal.model.SchemeMenu;
import com.yy.young.portal.service.IPortalSchemeService;
import com.yy.young.portal.service.ISchemeMenuService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * 方案服务
 * Created by rookie on 2017-11-18.
 */
@Controller
@RequestMapping("/portal/scheme")
public class PortalSchemeController {

    @Resource(name="schemeService")
    IPortalSchemeService service;

    @Resource(name="schemeMenuService")
    ISchemeMenuService schemeMenuService;

    /**
     * 获取数据列表
     * @param scheme
     * @param request
     * @return
     * @throws Exception
     */
    @Log("查询方案列表")
    @RequestMapping("/getSchemeList")
    @ResponseBody
    public Object getSchemeList(Scheme scheme, HttpServletRequest request) throws Exception{
        List<Scheme> list = service.getSchemeList(scheme);
        return new Result(list);
    }

    /**
     * 获取分页数据
     * @param scheme
     * @param request
     * @return
     * @throws Exception
     */
    @Log("分页查询方案列表")
    @RequestMapping("/getSchemePage")
    @ResponseBody
    public Object getSchemePage(Scheme scheme, HttpServletRequest request) throws Exception{
        Page page = CommonUtil.getPageFromRequest(request);
        List<Scheme> list = service.getSchemePage(scheme, page);
        page.setData(list);
        return page;
    }

    /**
     * 获取单条数据
     * @param id
     * @param request
     * @return
     * @throws Exception
     */
    @Log("查询方案")
    @RequestMapping("/getScheme")
    @ResponseBody
    public Object getScheme(String id, HttpServletRequest request) throws Exception{
        Scheme scheme = service.getScheme(id);
        return new Result(scheme);
    }

    /**
     * 新增
     * @param scheme
     * @param request
     * @return
     * @throws Exception
     */
    @Log("新增方案")
    @RequestMapping("/insert")
    @ResponseBody
    public Object insert(Scheme scheme, HttpServletRequest request) throws Exception{
        int num = service.insertScheme(scheme);
        return new Result();
    }

    /**
     * 修改
     * @param scheme
     * @param request
     * @return
     * @throws Exception
     */
    @Log("修改方案")
    @RequestMapping("/update")
    @ResponseBody
    public Object update(Scheme scheme, HttpServletRequest request) throws Exception{
        int num = service.updateScheme(scheme);
        return new Result();
    }

    /**
     * 删除
     * @param ids
     * @param id
     * @param request
     * @return
     * @throws Exception
     */
    @Log("删除方案")
    @RequestMapping("/delete")
    @ResponseBody
    public Object delete(String ids, String id, HttpServletRequest request) throws Exception{
        Result result = new Result();
        if(StringUtils.isNotBlank(ids)) {
            String[] idArr = ids.split(",");
            int num = service.deleteScheme(idArr);
        }else if(StringUtils.isNotBlank(id)){
            int num = service.deleteScheme(id);
        }else{
            result.setCode(-1);
            result.setInfo("删除失败:待删除编号无效!");
        }
        return result;
    }

    /**
     * 查询当前用户方案菜单
     * @param request
     * @return
     * @throws Exception
     */
    @Log("查询当前用户方案菜单")
    @RequestMapping("/getMenu")
    @ResponseBody
    public Object getMenu(HttpServletRequest request) throws Exception{
        Result result = new Result();
        User user = CommonUtil.getLoginUser(request);
        if (user != null){
            Scheme scheme = service.getSchemeByUser(user);//获取优先级最高的方案
            List<SchemeMenu> list = schemeMenuService.getMenuListBySchemeId(scheme.getId());
            //定义菜单集合,用于存放格式化后的menu
            List<Menu> menuList = new LinkedList<Menu>();
            //菜单映射,方便查找
            Map<String, Menu> map = new HashMap<String, Menu>();

            for(SchemeMenu schemeMenu : list){
                Menu menu = new Menu(schemeMenu);
                menuList.add(menu);
                map.put(menu.getId(), menu);
            }

            if (menuList != null && menuList.iterator() != null){
                Iterator<Menu> it = menuList.iterator();
                while (it.hasNext()){
                    Menu menu = it.next();
                    if (map.containsKey(menu.getParentId()) && map.get(menu.getParentId()) != null){//如果能找到此父id,则添加到子菜单中
                        map.get(menu.getParentId()).appendChild(menu);
                        it.remove();//成功匹配到父菜单的移出集合
                    }
                }
            }

            //到这树形结构就ok了
            result.setData(menuList);
            return result;
        }else{
            result.setCode(-101);
            result.setInfo("登录超时!");
        }
        return result;
    }

}