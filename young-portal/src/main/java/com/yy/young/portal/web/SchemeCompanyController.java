package com.yy.young.portal.web;

import com.yy.young.common.util.CommonUtil;
import com.yy.young.common.util.Result;
import com.yy.young.common.util.StringUtils;
import com.yy.young.dal.util.Page;
import com.yy.young.interfaces.log.annotation.Log;
import com.yy.young.portal.model.SchemeCompany;
import com.yy.young.portal.service.ISchemeCompanyService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 方案单位服务
 * Created by rookie on 2017-12-17.
 */
@Controller
@RequestMapping("/portal/schemeCompany")
public class SchemeCompanyController {

    @Resource(name="schemeCompanyService")
    ISchemeCompanyService service;

    /**
     * 获取数据列表
     * @param schemeCompany
     * @param request
     * @return
     * @throws Exception
     */
    @Log("查询方案单位列表")
    @RequestMapping("/getSchemeCompanyList")
    @ResponseBody
    public Object getSchemeCompanyList(SchemeCompany schemeCompany, HttpServletRequest request) throws Exception{
        List<SchemeCompany> list = service.getSchemeCompanyList(schemeCompany);
        return new Result(list);
    }

    /**
     * 获取分页数据
     * @param schemeCompany
     * @param request
     * @return
     * @throws Exception
     */
    @Log("分页查询方案单位列表")
    @RequestMapping("/getSchemeCompanyPage")
    @ResponseBody
    public Object getSchemeCompanyPage(SchemeCompany schemeCompany, HttpServletRequest request) throws Exception{
        Page page = CommonUtil.getPageFromRequest(request);
        List<SchemeCompany> list = service.getSchemeCompanyPage(schemeCompany, page);
        page.setData(list);
        return page;
    }

    /**
     * 获取单条数据
     * @param id
     * @param request
     * @return
     * @throws Exception
     */
    @Log("查询方案单位")
    @RequestMapping("/getSchemeCompany")
    @ResponseBody
    public Object getSchemeCompany(String id, HttpServletRequest request) throws Exception{
        SchemeCompany schemeCompany = service.getSchemeCompany(id);
        return new Result(schemeCompany);
    }

    /**
     * 新增
     * @param schemeCompany
     * @param request
     * @return
     * @throws Exception
     */
    @Log("新增方案单位")
    @RequestMapping("/insert")
    @ResponseBody
    public Object insert(SchemeCompany schemeCompany, HttpServletRequest request) throws Exception{
        int num = service.insertSchemeCompany(schemeCompany);
        return new Result();
    }

    /**
     * 修改
     * @param schemeCompany
     * @param request
     * @return
     * @throws Exception
     */
    @Log("修改方案单位")
    @RequestMapping("/update")
    @ResponseBody
    public Object update(SchemeCompany schemeCompany, HttpServletRequest request) throws Exception{
        int num = service.updateSchemeCompany(schemeCompany);
        return new Result();
    }

    /**
     * 删除
     * @param ids
     * @param id
     * @param request
     * @return
     * @throws Exception
     */
    @Log("删除方案单位")
    @RequestMapping("/delete")
    @ResponseBody
    public Object delete(String ids, String id, HttpServletRequest request) throws Exception{
        Result result = new Result();
        if(StringUtils.isNotBlank(ids)) {
            String[] idArr = ids.split(",");
            int num = service.deleteSchemeCompany(idArr);
        }else if(StringUtils.isNotBlank(id)){
            int num = service.deleteSchemeCompany(id);
        }else{
            result.setCode(-1);
            result.setInfo("删除失败:待删除编号无效!");
        }
        return result;
    }

    /**
     * 批量关联方案单位
     * @param schemeId 方案编号
     * @param companyIds 单位编号,逗号分隔
     * @param request
     * @return
     * @throws Exception
     */
    @Log("批量关联方案单位(1-N)")
    @RequestMapping("/batchInsertSchemeCompany")
    @ResponseBody
    public Object batchInsertSchemeCompany(String schemeId, String companyIds, HttpServletRequest request) throws Exception{
        Result result = new Result();
        if (StringUtils.isBlank(schemeId) || StringUtils.isBlank(companyIds)){
            result.setCode(-1);
            result.setInfo("关联失败:方案编号或单位编号为空!");
        }else{
            service.batchInsertSchemeCompany(schemeId, companyIds.split(","));
        }
        return result;
    }

}