package com.yy.young.portal.service.impl;

import com.yy.young.common.util.StringUtils;
import com.yy.young.interfaces.model.User;
import com.yy.young.interfaces.ums.service.IUmsOutService;
import com.yy.young.portal.service.ISchemeUserService;
import com.yy.young.common.util.CommonUtil;
import com.yy.young.dal.service.IDataAccessService;
import com.yy.young.dal.util.Page;
import com.yy.young.portal.model.SchemeUser;
import com.yy.young.portal.util.PortalConstants;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * 方案用户服务实现
 * Created by rookie on 2017-12-08.
 */
@Service("schemeUserService")
public class SchemeUserServiceImpl implements ISchemeUserService {

    @Resource(name = "dataAccessService")
    private IDataAccessService dataAccessService;//数据层服务

    @Resource(name = "umsOutService")
    private IUmsOutService umsOutService;

    private static final Logger logger = LoggerFactory.getLogger(SchemeUserServiceImpl.class);

    //获取数据列表
    @Override
    public List<SchemeUser> getSchemeUserList(SchemeUser schemeUser) throws Exception {
        return dataAccessService.getList(PortalConstants.MAPPER.TB_PORTAL_SCHEME_USER + ".getSchemeUserList", schemeUser);
    }

    //获取数据列表(分页)
    @Override
    public List<SchemeUser> getSchemeUserPage(SchemeUser schemeUser, Page page) throws Exception {
        return dataAccessService.getList(PortalConstants.MAPPER.TB_PORTAL_SCHEME_USER + ".getSchemeUserList", schemeUser, page);
    }

    //查询单条
    @Override
    public SchemeUser getSchemeUser(String id) throws Exception {
        return (SchemeUser)dataAccessService.getObject(PortalConstants.MAPPER.TB_PORTAL_SCHEME_USER + ".getSchemeUserById", id);
    }

    //修改
    @Override
    public int updateSchemeUser(SchemeUser schemeUser) throws Exception {
        return dataAccessService.update(PortalConstants.MAPPER.TB_PORTAL_SCHEME_USER + ".update", schemeUser);
    }

    //批量删除
    @Override
    public int deleteSchemeUser(String[] idArr) throws Exception {
        int i = 0;
        for(String id : idArr){
            i += this.deleteSchemeUser(id);
        }
        return i;
    }

    //删除单条
    @Override
    public int deleteSchemeUser(String id) throws Exception {
        return dataAccessService.delete(PortalConstants.MAPPER.TB_PORTAL_SCHEME_USER + ".delete", id);
    }

    //插入
    @Override
    public int insertSchemeUser(SchemeUser schemeUser) throws Exception {
        if (hasSchemeUser(schemeUser)){//存在此方案用户关联关系
            logger.info("[插入方案用户] 待插入方案用户关系已存在:方案编号为{},用户编号为{}", schemeUser.getSchemeId(), schemeUser.getUserId());
            return 0;
        }
        User user = umsOutService.getLoginUserById(schemeUser.getUserId());
        if (user != null){
            schemeUser.setUserName(user.getName());//设置用户名称
        }else{
            logger.warn("[插入方案用户] *警告 根据用户编号{}查找用户信息失败!", schemeUser.getUserId());
            return 0;
        }
        if (schemeUser.getCreateTime() == null){
            schemeUser.setCreateTime(new Date());
        }
        if (StringUtils.isBlank(schemeUser.getId())){
            schemeUser.setId(CommonUtil.getUUID());
        }

        return dataAccessService.insert(PortalConstants.MAPPER.TB_PORTAL_SCHEME_USER + ".insert", schemeUser);
    }

    /**
     * 判断方案用户是否存在,也就是判断方案编号和用户编号是否已存在关联关系
     * @return
     * @throws Exception
     */
    private boolean hasSchemeUser(SchemeUser schemeUser) throws Exception{
        Integer count = (Integer)dataAccessService.getObject(PortalConstants.MAPPER.TB_PORTAL_SCHEME_USER + ".countBySchemeIdAndUserId", schemeUser);
        if (count > 0){
            return true;
        }
        return false;
    }

    @Override
    public int batchInsertSchemeUser(String schemeId, String[] userIds) throws Exception {
        int count = 0;
        for (String userId : userIds){
            SchemeUser schemeUser = new SchemeUser();
            schemeUser.setSchemeId(schemeId);
            schemeUser.setUserId(userId);
            count += this.insertSchemeUser(schemeUser);
        }
        return count;
    }

}