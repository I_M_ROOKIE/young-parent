package com.yy.young.portal.service.impl;

import com.yy.young.common.util.StringUtils;
import com.yy.young.interfaces.ums.model.Dept;
import com.yy.young.interfaces.ums.service.IUmsOutService;
import com.yy.young.portal.model.SchemeUser;
import com.yy.young.portal.service.ISchemeCompanyService;
import com.yy.young.common.util.CommonUtil;
import com.yy.young.dal.service.IDataAccessService;
import com.yy.young.dal.util.Page;
import com.yy.young.portal.model.SchemeCompany;
import com.yy.young.portal.util.PortalConstants;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * 方案单位服务实现
 * Created by rookie on 2017-12-17.
 */
@Service("schemeCompanyService")
public class SchemeCompanyServiceImpl implements ISchemeCompanyService {

    @Resource(name = "dataAccessService")
    IDataAccessService dataAccessService;//数据层服务

    @Resource(name = "umsOutService")
    IUmsOutService umsOutService;

    private static final Logger logger = LoggerFactory.getLogger(SchemeCompanyServiceImpl.class);

    //获取数据列表
    @Override
    public List<SchemeCompany> getSchemeCompanyList(SchemeCompany schemeCompany) throws Exception {
        return dataAccessService.getList(PortalConstants.MAPPER.TB_PORTAL_SCHEME_COMPANY + ".getSchemeCompanyList", schemeCompany);
    }

    //获取数据列表(分页)
    @Override
    public List<SchemeCompany> getSchemeCompanyPage(SchemeCompany schemeCompany, Page page) throws Exception {
        return dataAccessService.getList(PortalConstants.MAPPER.TB_PORTAL_SCHEME_COMPANY + ".getSchemeCompanyList", schemeCompany, page);
    }

    //查询单条
    @Override
    public SchemeCompany getSchemeCompany(String id) throws Exception {
        return (SchemeCompany)dataAccessService.getObject(PortalConstants.MAPPER.TB_PORTAL_SCHEME_COMPANY + ".getSchemeCompanyById", id);
    }

    //修改
    @Override
    public int updateSchemeCompany(SchemeCompany schemeCompany) throws Exception {
        return dataAccessService.update(PortalConstants.MAPPER.TB_PORTAL_SCHEME_COMPANY + ".update", schemeCompany);
    }

    //批量删除
    @Override
    public int deleteSchemeCompany(String[] idArr) throws Exception {
        int i = 0;
        for(String id : idArr){
            i += this.deleteSchemeCompany(id);
        }
        return i;
    }

    //删除单条
    @Override
    public int deleteSchemeCompany(String id) throws Exception {
        return dataAccessService.delete(PortalConstants.MAPPER.TB_PORTAL_SCHEME_COMPANY + ".delete", id);
    }

    //插入
    @Override
    public int insertSchemeCompany(SchemeCompany schemeCompany) throws Exception {
        if (hasSchemeCompany(schemeCompany)){//存在此方案单位关联关系
            logger.info("[插入方案单位] 待插入方案单位关系已存在:方案编号为{},单位编号为{}", schemeCompany.getSchemeId(), schemeCompany.getCompanyId());
            return 0;
        }
        Dept dept = umsOutService.getDeptById(schemeCompany.getCompanyId());
        if (dept != null){
            schemeCompany.setCompanyName(dept.getName());//设置单位名称
        }else{
            logger.warn("[插入方案单位] *警告 根据单位编号{}查找单位信息失败!", schemeCompany.getCompanyId());
            return 0;
        }
        if(StringUtils.isBlank(schemeCompany.getId())){
            schemeCompany.setId(CommonUtil.getUUID());
        }
        if (schemeCompany.getCreateTime() == null){
            schemeCompany.setCreateTime(new Date());
        }
        return dataAccessService.insert(PortalConstants.MAPPER.TB_PORTAL_SCHEME_COMPANY + ".insert", schemeCompany);
    }

    /**
     * 判断是否存在方案单位
     * @param schemeCompany
     * @return
     * @throws Exception
     */
    private boolean hasSchemeCompany(SchemeCompany schemeCompany) throws Exception{
        Integer count = (Integer)dataAccessService.getObject(PortalConstants.MAPPER.TB_PORTAL_SCHEME_COMPANY + ".countBySchemeIdAndCompanyId", schemeCompany);
        if (count > 0){
            return true;
        }
        return false;
    }

    @Override
    public int batchInsertSchemeCompany(String schemeId, String[] companyIds) throws Exception {
        int count = 0;
        for (String companyId : companyIds){
            SchemeCompany item = new SchemeCompany();
            item.setSchemeId(schemeId);
            item.setCompanyId(companyId);
            count += this.insertSchemeCompany(item);
        }
        return count;
    }

}