package com.yy.young.portal.service.impl;

import com.yy.young.common.util.StringUtils;
import com.yy.young.portal.service.ISchemeMenuService;
import com.yy.young.common.util.CommonUtil;
import com.yy.young.dal.service.IDataAccessService;
import com.yy.young.dal.util.Page;
import com.yy.young.portal.model.SchemeMenu;
import com.yy.young.portal.util.PortalConstants;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.annotation.Resource;
import javax.naming.ldap.HasControls;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 方案菜单服务实现
 * Created by rookie on 2017-11-27.
 */
@Service("schemeMenuService")
public class SchemeMenuServiceImpl implements ISchemeMenuService {

    @Resource(name = "dataAccessService")
    IDataAccessService dataAccessService;//数据层服务

    private static final Logger logger = LoggerFactory.getLogger(SchemeMenuServiceImpl.class);

    //获取数据列表
    @Override
    public List<SchemeMenu> getSchemeMenuList(SchemeMenu schemeMenu) throws Exception {
        return dataAccessService.getList(PortalConstants.MAPPER.TB_PORTAL_SCHEME_MENU + ".getSchemeMenuList", schemeMenu);
    }

    //获取数据列表(分页)
    @Override
    public List<SchemeMenu> getSchemeMenuPage(SchemeMenu schemeMenu, Page page) throws Exception {
        return dataAccessService.getList(PortalConstants.MAPPER.TB_PORTAL_SCHEME_MENU + ".getSchemeMenuList", schemeMenu, page);
    }

    //查询单条
    @Override
    public SchemeMenu getSchemeMenu(String id) throws Exception {
        return (SchemeMenu)dataAccessService.getObject(PortalConstants.MAPPER.TB_PORTAL_SCHEME_MENU + ".getSchemeMenuById", id);
    }

    @Override
    public List<SchemeMenu> getMenuListBySchemeId(String schemeId) throws Exception {
        return dataAccessService.getList(PortalConstants.MAPPER.TB_PORTAL_SCHEME_MENU + ".getMenuListBySchemeId", schemeId);
    }

    //修改
    @Override
    public int updateSchemeMenu(SchemeMenu schemeMenu) throws Exception {
        return dataAccessService.update(PortalConstants.MAPPER.TB_PORTAL_SCHEME_MENU + ".update", schemeMenu);
    }

    //批量删除
    @Override
    public int deleteSchemeMenu(String[] idArr) throws Exception {
        int i = 0;
        for(String id : idArr){
            i += this.deleteSchemeMenu(id);
        }
        return i;
    }

    //删除单条
    @Override
    public int deleteSchemeMenu(String id) throws Exception {
        return dataAccessService.delete(PortalConstants.MAPPER.TB_PORTAL_SCHEME_MENU + ".delete", id);
    }

    //插入
    @Override
    public int insertSchemeMenu(SchemeMenu schemeMenu) throws Exception {
        if (StringUtils.isBlank(schemeMenu.getId())){
            schemeMenu.setId(CommonUtil.getUUID());
        }
        return dataAccessService.insert(PortalConstants.MAPPER.TB_PORTAL_SCHEME_MENU + ".insert", schemeMenu);
    }

    @Override
    public int batchInsertMenu(List<SchemeMenu> menus, String menuId) throws Exception {
        //检查父子关系
        Map<String, SchemeMenu> map = new HashMap();
        //将list转为map,方便后面筛选
        for (SchemeMenu menu : menus){
            map.put(menu.getId(), menu);//菜单id作为key
            menu.setId(CommonUtil.getUUID());//使用uuid作为菜单的新id
        }
        //循环生成父子关联
        for (SchemeMenu menu : menus){
            if (menu.getParentId() != null && map.containsKey(menu.getParentId())){//存在该菜单的父亲
                menu.setParentId(map.get(menu.getParentId()).getId());//设置经过重新赋值的父菜单id
            }else{//没有父亲的话,设置默认父id
                menu.setParentId(menuId);
            }
        }

        int i = 0;
        for (SchemeMenu menu : menus){
            i += this.insertSchemeMenu(menu);
        }
        return i;
    }

}