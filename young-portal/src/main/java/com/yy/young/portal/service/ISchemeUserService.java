package com.yy.young.portal.service;

import com.yy.young.dal.util.Page;
import com.yy.young.portal.model.SchemeUser;
import java.util.List;

/**
 * 方案用户服务接口
 * Created by rookie on 2017-12-08.
 */
public interface ISchemeUserService {

    /**
     * 获取数据列表
     * @param schemeUser
     * @return
     * @throws Exception
     */
    List<SchemeUser> getSchemeUserList(SchemeUser schemeUser) throws Exception;

    /**
     * 获取数据列表(分页)
     * @param schemeUser
     * @param page 分页参数
     * @return
     * @throws Exception
     */
    List<SchemeUser> getSchemeUserPage(SchemeUser schemeUser, Page page) throws Exception;

    /**
     * 根据id获取信息
     * @param id
     * @return
     * @throws Exception
     */
    SchemeUser getSchemeUser(String id) throws Exception;

    /**
     * 修改
     * @param schemeUser
     * @return
     * @throws Exception
     */
    int updateSchemeUser(SchemeUser schemeUser) throws Exception;

    /**
     * 批量删除
     * @param idArr id数组
     * @return
     * @throws Exception
     */
    int deleteSchemeUser(String[] idArr) throws Exception;

    /**
     * 删除单条
     * @param id
     * @return
     * @throws Exception
     */
    int deleteSchemeUser(String id) throws Exception;

    /**
     * 插入
     * @param schemeUser
     * @return
     * @throws Exception
     */
    int insertSchemeUser(SchemeUser schemeUser) throws Exception;

    /**
     * 批量关联方案用户
     * @param schemeId 方案编号
     * @param userIds 用户编号数组
     * @return
     * @throws Exception
     */
    int batchInsertSchemeUser(String schemeId, String[] userIds) throws Exception;

}