package com.yy.young.portal.model;

import java.util.Date;

/**
 * 方案单位实体类
 * Created by rookie on 2017-12-17.
 */
public class SchemeCompany{

    private String id;//编号
    private String schemeId;//方案编号
    private String companyId;//单位编号
    private String companyName;//单位名称
    private Date createTime;//创建时间
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getSchemeId() {
        return schemeId;
    }
    public void setSchemeId(String schemeId) {
        this.schemeId = schemeId;
    }
    public String getCompanyId() {
        return companyId;
    }
    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }
    public String getCompanyName() {
        return companyName;
    }
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
    public Date getCreateTime() {
        return createTime;
    }
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}