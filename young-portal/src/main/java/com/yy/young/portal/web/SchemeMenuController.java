package com.yy.young.portal.web;

import com.yy.young.common.util.CommonJsonUtil;
import com.yy.young.common.util.CommonUtil;
import com.yy.young.common.util.Result;
import com.yy.young.common.util.StringUtils;
import com.yy.young.dal.util.Page;
import com.yy.young.interfaces.log.annotation.Log;
import com.yy.young.interfaces.model.User;
import com.yy.young.interfaces.ums.model.Navigate;
import com.yy.young.interfaces.ums.service.IUmsOutService;
import com.yy.young.portal.model.SchemeMenu;
import com.yy.young.portal.service.ISchemeMenuService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 方案菜单服务
 * Created by rookie on 2017-11-27.
 */
@Controller
@RequestMapping("/portal/schemeMenu")
public class SchemeMenuController {

    @Resource(name="schemeMenuService")
    ISchemeMenuService service;

    @Resource(name="umsOutService")
    IUmsOutService umsOutService;

    /**
     * 查询当前用户权限范围内的资源列表,用于选择资源
     * @param request
     * @return
     * @throws Exception
     */
    @Log("查询当前用户权限范围内的资源列表")
    @RequestMapping("/getCurUserNavigate")
    @ResponseBody
    public Object getCurUserNavigate(HttpServletRequest request) throws Exception{
        Result result = new Result();
        User user = CommonUtil.getLoginUser(request);
        if (user == null || StringUtils.isBlank(user.getId())){
            result.setCode(-1);
            result.setInfo("当前用户信息获取失败!");
            return result;
        }
        result.setData(umsOutService.getNavigateByUserId(user.getId()));
        return result;
    }

    /**
     * 获取数据列表
     * @param schemeMenu
     * @param request
     * @return
     * @throws Exception
     */
    @Log("查询方案菜单列表")
    @RequestMapping("/getSchemeMenuList")
    @ResponseBody
    public Object getSchemeMenuList(SchemeMenu schemeMenu, HttpServletRequest request) throws Exception{
        List<SchemeMenu> list = service.getSchemeMenuList(schemeMenu);
        return new Result(list);
    }

    /**
     * 获取分页数据
     * @param schemeMenu
     * @param request
     * @return
     * @throws Exception
     */
    @Log("分页查询方案菜单列表")
    @RequestMapping("/getSchemeMenuPage")
    @ResponseBody
    public Object getSchemeMenuPage(SchemeMenu schemeMenu, HttpServletRequest request) throws Exception{
        Page page = CommonUtil.getPageFromRequest(request);
        List<SchemeMenu> list = service.getSchemeMenuPage(schemeMenu, page);
        page.setData(list);
        return page;
    }

    /**
     * 获取单条数据
     * @param id
     * @param request
     * @return
     * @throws Exception
     */
    @Log("查询方案菜单")
    @RequestMapping("/getSchemeMenu")
    @ResponseBody
    public Object getSchemeMenu(String id, HttpServletRequest request) throws Exception{
        SchemeMenu schemeMenu = service.getSchemeMenu(id);
        return new Result(schemeMenu);
    }

    /**
     * 根据方案查询菜单列表
     * @param schemeId
     * @param request
     * @return
     * @throws Exception
     */
    @Log("根据方案查询菜单列表")
    @RequestMapping("/getMenuListBySchemeId")
    @ResponseBody
    public Object getMenuListBySchemeId(String schemeId, HttpServletRequest request) throws Exception{
        List<SchemeMenu> list = service.getMenuListBySchemeId(schemeId);
        return new Result(list);
    }

    /**
     * 新增
     * @param schemeMenu
     * @param request
     * @return
     * @throws Exception
     */
    @Log("新增方案菜单")
    @RequestMapping("/insert")
    @ResponseBody
    public Object insert(SchemeMenu schemeMenu, HttpServletRequest request) throws Exception{
        int num = service.insertSchemeMenu(schemeMenu);
        return new Result();
    }

    /**
     * 修改
     * @param schemeMenu
     * @param request
     * @return
     * @throws Exception
     */
    @Log("修改方案菜单")
    @RequestMapping("/update")
    @ResponseBody
    public Object update(SchemeMenu schemeMenu, HttpServletRequest request) throws Exception{
        int num = service.updateSchemeMenu(schemeMenu);
        return new Result();
    }

    /**
     * 删除
     * @param ids
     * @param id
     * @param request
     * @return
     * @throws Exception
     */
    @Log("删除方案菜单")
    @RequestMapping("/delete")
    @ResponseBody
    public Object delete(String ids, String id, HttpServletRequest request) throws Exception{
        Result result = new Result();
        if(StringUtils.isNotBlank(ids)) {
            String[] idArr = ids.split(",");
            int num = service.deleteSchemeMenu(idArr);
        }else if(StringUtils.isNotBlank(id)){
            int num = service.deleteSchemeMenu(id);
        }else{
            result.setCode(-1);
            result.setInfo("删除失败:待删除编号无效!");
        }
        return result;
    }

    /**
     * 批量关联方案菜单
     * @param menuId 父菜单id
     * @param menus 待导入菜单
     * @param schemeId 方案id
     * @param request
     * @return
     * @throws Exception
     */
    @Log("批量关联方案菜单")
    @RequestMapping("/batchInsertMenu")
    @ResponseBody
    public Object batchInsertMenu(String menuId, String menus, String schemeId, HttpServletRequest request) throws Exception{
        Result result = new Result();
        if (StringUtils.isNotBlank(menus) && StringUtils.isNotBlank(schemeId)){
            List<SchemeMenu> list = CommonJsonUtil.jsonStrArrToBeanList(menus, SchemeMenu.class);
            for (SchemeMenu menu : list){
                menu.setSchemeId(schemeId);
            }
            service.batchInsertMenu(list, menuId);
        }else{
            result.setCode(-1);
            result.setInfo("关联菜单信息或方案无效!");
        }
        return result;
    }

}