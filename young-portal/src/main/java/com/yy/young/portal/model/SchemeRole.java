package com.yy.young.portal.model;

import java.util.Date;

/**
 * 方案角色实体类
 * Created by rookie on 2017-12-17.
 */
public class SchemeRole{

    private String id;//编号
    private String schemeId;//方案编号
    private String roleId;//角色编号
    private String roleName;//角色名称
    private Date createTime;//创建时间
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getSchemeId() {
        return schemeId;
    }
    public void setSchemeId(String schemeId) {
        this.schemeId = schemeId;
    }
    public String getRoleId() {
        return roleId;
    }
    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }
    public String getRoleName() {
        return roleName;
    }
    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
    public Date getCreateTime() {
        return createTime;
    }
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}