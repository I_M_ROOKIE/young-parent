package com.yy.young.portal.service.impl;

import com.yy.young.common.util.StringUtils;
import com.yy.young.interfaces.model.User;
import com.yy.young.interfaces.ums.model.Role;
import com.yy.young.interfaces.ums.service.IUmsOutService;
import com.yy.young.portal.model.SchemeCompany;
import com.yy.young.portal.service.ISchemeRoleService;
import com.yy.young.common.util.CommonUtil;
import com.yy.young.dal.service.IDataAccessService;
import com.yy.young.dal.util.Page;
import com.yy.young.portal.model.SchemeRole;
import com.yy.young.portal.util.PortalConstants;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * 方案角色服务实现
 * Created by rookie on 2017-12-17.
 */
@Service("schemeRoleService")
public class SchemeRoleServiceImpl implements ISchemeRoleService {

    @Resource(name = "dataAccessService")
    IDataAccessService dataAccessService;//数据层服务

    @Resource(name = "umsOutService")
    IUmsOutService umsOutService;

    private static final Logger logger = LoggerFactory.getLogger(SchemeRoleServiceImpl.class);

    //获取数据列表
    @Override
    public List<SchemeRole> getSchemeRoleList(SchemeRole schemeRole) throws Exception {
        return dataAccessService.getList(PortalConstants.MAPPER.TB_PORTAL_SCHEME_ROLE + ".getSchemeRoleList", schemeRole);
    }

    //获取数据列表(分页)
    @Override
    public List<SchemeRole> getSchemeRolePage(SchemeRole schemeRole, Page page) throws Exception {
        return dataAccessService.getList(PortalConstants.MAPPER.TB_PORTAL_SCHEME_ROLE + ".getSchemeRoleList", schemeRole, page);
    }

    //查询单条
    @Override
    public SchemeRole getSchemeRole(String id) throws Exception {
        return (SchemeRole)dataAccessService.getObject(PortalConstants.MAPPER.TB_PORTAL_SCHEME_ROLE + ".getSchemeRoleById", id);
    }

    //修改
    @Override
    public int updateSchemeRole(SchemeRole schemeRole) throws Exception {
        return dataAccessService.update(PortalConstants.MAPPER.TB_PORTAL_SCHEME_ROLE + ".update", schemeRole);
    }

    //批量删除
    @Override
    public int deleteSchemeRole(String[] idArr) throws Exception {
        int i = 0;
        for(String id : idArr){
            i += this.deleteSchemeRole(id);
        }
        return i;
    }

    //删除单条
    @Override
    public int deleteSchemeRole(String id) throws Exception {
        return dataAccessService.delete(PortalConstants.MAPPER.TB_PORTAL_SCHEME_ROLE + ".delete", id);
    }

    //插入
    @Override
    public int insertSchemeRole(SchemeRole schemeRole) throws Exception {
        if (hasSchemeRole(schemeRole)) {//存在此方案角色关联关系
            logger.info("[插入方案角色] 待插入方案角色关系已存在:方案编号为{},角色编号为{}", schemeRole.getSchemeId(), schemeRole.getRoleId());
            return 0;
        }
        Role role = umsOutService.getRoleById(schemeRole.getRoleId());
        if (role != null){
            schemeRole.setRoleName(role.getName());//设置角色名称
        }else{
            logger.warn("[插入方案角色] *警告 根据角色编号{}查找角色信息失败!", schemeRole.getRoleId());
            return 0;
        }
        if (StringUtils.isBlank(schemeRole.getId())){
            schemeRole.setId(CommonUtil.getUUID());
        }
        if (schemeRole.getCreateTime() == null){
            schemeRole.setCreateTime(new Date());
        }
        return dataAccessService.insert(PortalConstants.MAPPER.TB_PORTAL_SCHEME_ROLE + ".insert", schemeRole);
    }

    /**
     * 判断方案角色是否已存在
     * @param schemeRole
     * @return
     * @throws Exception
     */
    private boolean hasSchemeRole(SchemeRole schemeRole) throws Exception{
        Integer count = (Integer)dataAccessService.getObject(PortalConstants.MAPPER.TB_PORTAL_SCHEME_ROLE + ".countBySchemeIdAndRoleId", schemeRole);
        if (count > 0){
            return true;
        }
        return false;
    }

    @Override
    public int batchInsertSchemeRole(String schemeId, String[] roleIds) throws Exception {
        int count = 0;
        for (String roleId : roleIds){
            SchemeRole item = new SchemeRole();
            item.setSchemeId(schemeId);
            item.setRoleId(roleId);
            count += this.insertSchemeRole(item);
        }
        return count;
    }

}