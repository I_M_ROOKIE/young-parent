package com.yy.young.portal.model;

import com.yy.young.interfaces.ums.model.Navigate;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rookie on 2017/5/19.
 */
public class Menu {
    private String id;//编号
    private String name;//资源名称
    private String href;//链接
    private Menu parent;//父节点,不推荐使用
    private String parentId;//父节点id
    private String parentName;//父节点名称
    private String icon;//图标
    private String type;//资源类型
    private int level;//层级
    private int num;//显示顺序
    private List<Menu> children;//子菜单

    public Menu(){
        super();
    }

    /**
     * 将接口中的navigate对象转为menu对象
     * @param nav
     */
    public Menu(Navigate nav){
        super();
        this.id = nav.getId();
        this.name = nav.getName();
        this.href = nav.getHref();
        this.parentId = nav.getParentId();
        this.parentName = nav.getParentName();
        this.icon = nav.getIcon();
        this.type = nav.getType();
        this.level = nav.getLevel();
        this.num = nav.getNum();
    }

    /**
     * 方案菜单转菜单
     * @param nav
     */
    public Menu(SchemeMenu nav){
        super();
        this.id = nav.getId();
        this.name = nav.getName();
        this.href = nav.getNavHref();
        this.parentId = nav.getParentId();
        this.icon = nav.getIcon();
        this.num = nav.getNum();
    }

    /**
     * 添加子菜单
     * @param menu
     */
    public void appendChild(Menu menu){
        if(this.children == null){
            this.children = new ArrayList<Menu>();
        }
        this.children.add(menu);
    }

    /**
     * 判断节点是否根节点
     * 判断规则: parent属性为null的即为根节点
     * @return
     */
    public boolean isRoot(){
        return this.parent == null;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public List<Menu> getChildren() {
        return children;
    }

    public void setChildren(List<Menu> children) {
        this.children = children;
    }

    public Menu getParent() {
        return parent;
    }

    public void setParent(Menu parent) {
        this.parent = parent;
    }
}
