package com.yy.young.portal.util;

/**
 * 门户常量类
 * Created by rookie on 2017/11/18.
 */
public class PortalConstants {

    /**
     * sql映射常量
     */
    public interface MAPPER{
        String TB_PORTAL_SCHEME = "com.yy.young.portal.mapper.scheme";//方案
        String TB_PORTAL_SCHEME_MENU = "com.yy.young.portal.mapper.schemeMenu";//方案菜单
        String TB_PORTAL_SCHEME_USER = "com.yy.young.portal.mapper.schemeUser";//方案用户
        String TB_PORTAL_SCHEME_COMPANY = "com.yy.young.portal.mapper.schemeCompany";//方案单位
        String TB_PORTAL_SCHEME_ROLE = "com.yy.young.portal.mapper.schemeRole";//方案角色
    }
}
