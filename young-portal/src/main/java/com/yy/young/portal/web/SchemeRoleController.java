package com.yy.young.portal.web;

import com.yy.young.common.util.CommonUtil;
import com.yy.young.common.util.Result;
import com.yy.young.common.util.StringUtils;
import com.yy.young.dal.util.Page;
import com.yy.young.interfaces.log.annotation.Log;
import com.yy.young.portal.model.SchemeRole;
import com.yy.young.portal.service.ISchemeRoleService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 方案角色服务
 * Created by rookie on 2017-12-17.
 */
@Controller
@RequestMapping("/portal/schemeRole")
public class SchemeRoleController {

    @Resource(name="schemeRoleService")
    ISchemeRoleService service;

    /**
     * 获取数据列表
     * @param schemeRole
     * @param request
     * @return
     * @throws Exception
     */
    @Log("查询方案角色列表")
    @RequestMapping("/getSchemeRoleList")
    @ResponseBody
    public Object getSchemeRoleList(SchemeRole schemeRole, HttpServletRequest request) throws Exception{
        List<SchemeRole> list = service.getSchemeRoleList(schemeRole);
        return new Result(list);
    }

    /**
     * 获取分页数据
     * @param schemeRole
     * @param request
     * @return
     * @throws Exception
     */
    @Log("分页查询方案角色列表")
    @RequestMapping("/getSchemeRolePage")
    @ResponseBody
    public Object getSchemeRolePage(SchemeRole schemeRole, HttpServletRequest request) throws Exception{
        Page page = CommonUtil.getPageFromRequest(request);
        List<SchemeRole> list = service.getSchemeRolePage(schemeRole, page);
        page.setData(list);
        return page;
    }

    /**
     * 获取单条数据
     * @param id
     * @param request
     * @return
     * @throws Exception
     */
    @Log("查询方案角色")
    @RequestMapping("/getSchemeRole")
    @ResponseBody
    public Object getSchemeRole(String id, HttpServletRequest request) throws Exception{
        SchemeRole schemeRole = service.getSchemeRole(id);
        return new Result(schemeRole);
    }

    /**
     * 新增
     * @param schemeRole
     * @param request
     * @return
     * @throws Exception
     */
    @Log("新增方案角色")
    @RequestMapping("/insert")
    @ResponseBody
    public Object insert(SchemeRole schemeRole, HttpServletRequest request) throws Exception{
        int num = service.insertSchemeRole(schemeRole);
        return new Result();
    }

    /**
     * 修改
     * @param schemeRole
     * @param request
     * @return
     * @throws Exception
     */
    @Log("修改方案角色")
    @RequestMapping("/update")
    @ResponseBody
    public Object update(SchemeRole schemeRole, HttpServletRequest request) throws Exception{
        int num = service.updateSchemeRole(schemeRole);
        return new Result();
    }

    /**
     * 删除
     * @param ids
     * @param id
     * @param request
     * @return
     * @throws Exception
     */
    @Log("删除方案角色")
    @RequestMapping("/delete")
    @ResponseBody
    public Object delete(String ids, String id, HttpServletRequest request) throws Exception{
        Result result = new Result();
        if(StringUtils.isNotBlank(ids)) {
            String[] idArr = ids.split(",");
            int num = service.deleteSchemeRole(idArr);
        }else if(StringUtils.isNotBlank(id)){
            int num = service.deleteSchemeRole(id);
        }else{
            result.setCode(-1);
            result.setInfo("删除失败:待删除编号无效!");
        }
        return result;
    }

    /**
     * 批量关联方案角色
     * @param schemeId 方案编号
     * @param roleIds 角色编号,逗号分隔
     * @param request
     * @return
     * @throws Exception
     */
    @Log("批量关联方案角色(1-N)")
    @RequestMapping("/batchInsertSchemeRole")
    @ResponseBody
    public Object batchInsertSchemeRole(String schemeId, String roleIds, HttpServletRequest request) throws Exception{
        Result result = new Result();
        if (StringUtils.isBlank(schemeId) || StringUtils.isBlank(roleIds)){
            result.setCode(-1);
            result.setInfo("关联失败:方案编号或单位编号为空!");
        }else{
            service.batchInsertSchemeRole(schemeId, roleIds.split(","));
        }
        return result;
    }
}