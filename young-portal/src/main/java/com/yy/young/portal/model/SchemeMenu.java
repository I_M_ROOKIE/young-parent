package com.yy.young.portal.model;

import java.util.ArrayList;
import java.util.List;

/**
 * 方案菜单实体类
 * Created by rookie on 2017-11-27.
 */
public class SchemeMenu{

    private String id;//菜单编号
    private String name;//菜单名称
    private String parentId;//上级编号
    private String navId;//资源ID,关联ums中nav表
    private String navHref;//菜单链接
    private String icon;//图标
    private int num;//显示顺序
    private String schemeId;//方案编号
    private List<SchemeMenu> childList;//子菜单集合

    /**
     * 添加子菜单
     * @param menu
     */
    public void appendChild(SchemeMenu menu){
        if (this.childList == null){
            childList = new ArrayList<SchemeMenu>();
        }
        childList.add(menu);
    }

    /**
     * 添加多个子菜单
     * @param menu
     */
    public void appendChild(List<SchemeMenu> menu){
        if (this.childList == null){
            childList = menu;
        }
        childList.addAll(menu);
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getParentId() {
        return parentId;
    }
    public void setParentId(String parentId) {
        this.parentId = parentId;
    }
    public String getNavId() {
        return navId;
    }
    public void setNavId(String navId) {
        this.navId = navId;
    }
    public String getNavHref() {
        return navHref;
    }
    public void setNavHref(String navHref) {
        this.navHref = navHref;
    }
    public String getIcon() {
        return icon;
    }
    public void setIcon(String icon) {
        this.icon = icon;
    }
    public int getNum() {
        return num;
    }
    public void setNum(int num) {
        this.num = num;
    }
    public String getSchemeId() {
        return schemeId;
    }
    public void setSchemeId(String schemeId) {
        this.schemeId = schemeId;
    }

    public List<SchemeMenu> getChildList() {
        return childList;
    }

    public void setChildList(List<SchemeMenu> childList) {
        this.childList = childList;
    }
}