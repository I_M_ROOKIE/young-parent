package com.yy.young.portal.service;

import com.yy.young.dal.util.Page;
import com.yy.young.portal.model.SchemeMenu;
import java.util.List;

/**
 * 方案菜单服务接口
 * Created by rookie on 2017-11-27.
 */
public interface ISchemeMenuService {

    /**
     * 获取数据列表
     * @param schemeMenu
     * @return
     * @throws Exception
     */
    List<SchemeMenu> getSchemeMenuList(SchemeMenu schemeMenu) throws Exception;

    /**
     * 获取数据列表(分页)
     * @param schemeMenu
     * @param page 分页参数
     * @return
     * @throws Exception
     */
    List<SchemeMenu> getSchemeMenuPage(SchemeMenu schemeMenu, Page page) throws Exception;

    /**
     * 根据id获取信息
     * @param id
     * @return
     * @throws Exception
     */
    SchemeMenu getSchemeMenu(String id) throws Exception;

    /**
     * 根据方案编号查询菜单
     * @param schemeId
     * @return
     * @throws Exception
     */
    List<SchemeMenu> getMenuListBySchemeId(String schemeId) throws Exception;

    /**
     * 修改
     * @param schemeMenu
     * @return
     * @throws Exception
     */
    int updateSchemeMenu(SchemeMenu schemeMenu) throws Exception;

    /**
     * 批量删除
     * @param idArr id数组
     * @return
     * @throws Exception
     */
    int deleteSchemeMenu(String[] idArr) throws Exception;

    /**
     * 删除单条
     * @param id
     * @return
     * @throws Exception
     */
    int deleteSchemeMenu(String id) throws Exception;

    /**
     * 插入
     * @param schemeMenu
     * @return
     * @throws Exception
     */
    int insertSchemeMenu(SchemeMenu schemeMenu) throws Exception;

    /**
     * 批量关联方案菜单
     * @param menus
     * @param menuId
     * @return
     * @throws Exception
     */
    int batchInsertMenu(List<SchemeMenu> menus, String menuId) throws Exception;

}