package com.yy.young.portal.model;

import java.util.Date;

/**
 * 方案用户实体类
 * Created by rookie on 2017-12-08.
 */
public class SchemeUser{

    private String id;//主键
    private String schemeId;//方案id
    private String userId;//用户id
    private String userName;//用户名称
    private Date createTime;//创建时间
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getSchemeId() {
        return schemeId;
    }
    public void setSchemeId(String schemeId) {
        this.schemeId = schemeId;
    }
    public String getUserId() {
        return userId;
    }
    public void setUserId(String userId) {
        this.userId = userId;
    }
    public Date getCreateTime() {
        return createTime;
    }
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}