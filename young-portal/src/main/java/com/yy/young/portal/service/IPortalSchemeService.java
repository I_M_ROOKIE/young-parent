package com.yy.young.portal.service;

import com.yy.young.dal.util.Page;
import com.yy.young.interfaces.model.User;
import com.yy.young.portal.model.Scheme;
import java.util.List;

/**
 * 方案服务接口
 * Created by rookie on 2017-11-18.
 */
public interface IPortalSchemeService {

    /**
     * 获取数据列表
     * @param scheme
     * @return
     * @throws Exception
     */
    List<Scheme> getSchemeList(Scheme scheme) throws Exception;

    /**
     * 根据用户获取方案,根据用户id/角色/单位查询方案配置,并返回优先级最高的方案
     * @param user 用户信息
     * @return
     * @throws Exception
     */
    Scheme getSchemeByUser(User user) throws Exception;

    /**
     * 根据用户id查询方案
     * @param userId
     * @return
     * @throws Exception
     */
    List<Scheme> getSchemeByUserId(String userId) throws Exception;

    /**
     * 根据角色id查询方案
     * @param roleId
     * @return
     * @throws Exception
     */
    List<Scheme> getSchemeByRoleId(String roleId) throws Exception;

    /**
     * 根据单位id查询方案
     * @param companyId
     * @return
     * @throws Exception
     */
    List<Scheme> getSchemeByCompanyId(String companyId) throws Exception;

    /**
     * 获取数据列表(分页)
     * @param scheme
     * @param page 分页参数
     * @return
     * @throws Exception
     */
    List<Scheme> getSchemePage(Scheme scheme, Page page) throws Exception;

    /**
     * 根据id获取信息
     * @param id
     * @return
     * @throws Exception
     */
    Scheme getScheme(String id) throws Exception;

    /**
     * 修改
     * @param scheme
     * @return
     * @throws Exception
     */
    int updateScheme(Scheme scheme) throws Exception;

    /**
     * 批量删除
     * @param idArr id数组
     * @return
     * @throws Exception
     */
    int deleteScheme(String[] idArr) throws Exception;

    /**
     * 删除单条
     * @param id
     * @return
     * @throws Exception
     */
    int deleteScheme(String id) throws Exception;

    /**
     * 插入
     * @param scheme
     * @return
     * @throws Exception
     */
    int insertScheme(Scheme scheme) throws Exception;

}