package com.yy.young.portal.service;

import com.yy.young.dal.util.Page;
import com.yy.young.portal.model.SchemeCompany;
import java.util.List;

/**
 * 方案单位服务接口
 * Created by rookie on 2017-12-17.
 */
public interface ISchemeCompanyService {

    /**
     * 获取数据列表
     * @param schemeCompany
     * @return
     * @throws Exception
     */
    List<SchemeCompany> getSchemeCompanyList(SchemeCompany schemeCompany) throws Exception;

    /**
     * 获取数据列表(分页)
     * @param schemeCompany
     * @param page 分页参数
     * @return
     * @throws Exception
     */
    List<SchemeCompany> getSchemeCompanyPage(SchemeCompany schemeCompany, Page page) throws Exception;

    /**
     * 根据id获取信息
     * @param id
     * @return
     * @throws Exception
     */
    SchemeCompany getSchemeCompany(String id) throws Exception;

    /**
     * 修改
     * @param schemeCompany
     * @return
     * @throws Exception
     */
    int updateSchemeCompany(SchemeCompany schemeCompany) throws Exception;

    /**
     * 批量删除
     * @param idArr id数组
     * @return
     * @throws Exception
     */
    int deleteSchemeCompany(String[] idArr) throws Exception;

    /**
     * 删除单条
     * @param id
     * @return
     * @throws Exception
     */
    int deleteSchemeCompany(String id) throws Exception;

    /**
     * 插入
     * @param schemeCompany
     * @return
     * @throws Exception
     */
    int insertSchemeCompany(SchemeCompany schemeCompany) throws Exception;

    /**
     * 批量关联方案单位
     * @param schemeId
     * @param companyIds
     * @return
     * @throws Exception
     */
    int batchInsertSchemeCompany(String schemeId, String[] companyIds) throws Exception;

}