package com.yy.young.portal.web;

import com.yy.young.common.util.CommonUtil;
import com.yy.young.common.util.Result;
import com.yy.young.common.util.StringUtils;
import com.yy.young.dal.util.Page;
import com.yy.young.interfaces.log.annotation.Log;
import com.yy.young.portal.model.SchemeUser;
import com.yy.young.portal.service.ISchemeUserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 方案用户服务
 * Created by rookie on 2017-12-08.
 */
@Controller
@RequestMapping("/portal/schemeUser")
public class SchemeUserController {

    @Resource(name="schemeUserService")
    ISchemeUserService service;

    /**
     * 获取数据列表
     * @param schemeUser
     * @param request
     * @return
     * @throws Exception
     */
    @Log("查询方案用户列表")
    @RequestMapping("/getSchemeUserList")
    @ResponseBody
    public Object getSchemeUserList(SchemeUser schemeUser, HttpServletRequest request) throws Exception{
        List<SchemeUser> list = service.getSchemeUserList(schemeUser);
        return new Result(list);
    }

    /**
     * 获取分页数据
     * @param schemeUser
     * @param request
     * @return
     * @throws Exception
     */
    @Log("分页查询方案用户列表")
    @RequestMapping("/getSchemeUserPage")
    @ResponseBody
    public Object getSchemeUserPage(SchemeUser schemeUser, HttpServletRequest request) throws Exception{
        Page page = CommonUtil.getPageFromRequest(request);
        List<SchemeUser> list = service.getSchemeUserPage(schemeUser, page);
        page.setData(list);
        return page;
    }

    /**
     * 获取单条数据
     * @param id
     * @param request
     * @return
     * @throws Exception
     */
    @Log("查询方案用户")
    @RequestMapping("/getSchemeUser")
    @ResponseBody
    public Object getSchemeUser(String id, HttpServletRequest request) throws Exception{
        SchemeUser schemeUser = service.getSchemeUser(id);
        return new Result(schemeUser);
    }

    /**
     * 新增
     * @param schemeUser
     * @param request
     * @return
     * @throws Exception
     */
    @Log("新增方案用户")
    @RequestMapping("/insert")
    @ResponseBody
    public Object insert(SchemeUser schemeUser, HttpServletRequest request) throws Exception{
        int num = service.insertSchemeUser(schemeUser);
        return new Result();
    }

    /**
     * 修改
     * @param schemeUser
     * @param request
     * @return
     * @throws Exception
     */
    @Log("修改方案用户")
    @RequestMapping("/update")
    @ResponseBody
    public Object update(SchemeUser schemeUser, HttpServletRequest request) throws Exception{
        int num = service.updateSchemeUser(schemeUser);
        return new Result();
    }

    /**
     * 删除
     * @param ids
     * @param id
     * @param request
     * @return
     * @throws Exception
     */
    @Log("删除方案用户")
    @RequestMapping("/delete")
    @ResponseBody
    public Object delete(String ids, String id, HttpServletRequest request) throws Exception{
        Result result = new Result();
        if(StringUtils.isNotBlank(ids)) {
            String[] idArr = ids.split(",");
            int num = service.deleteSchemeUser(idArr);
        }else if(StringUtils.isNotBlank(id)){
            int num = service.deleteSchemeUser(id);
        }else{
            result.setCode(-1);
            result.setInfo("删除失败:待删除编号无效!");
        }
        return result;
    }

    /**
     * 批量关联方案用户
     * @param schemeId 方案编号
     * @param userIds 关联用户编号集
     * @param request
     * @return
     */
    @Log("批量关联方案用户(1-N)")
    @RequestMapping("/batchInsertSchemeUser")
    @ResponseBody
    public Object batchInsertSchemeUser(String schemeId, String userIds, HttpServletRequest request) throws Exception{
        Result result = new Result();
        if (StringUtils.isBlank(schemeId) || StringUtils.isBlank(userIds)){
            result.setCode(-1);
            result.setInfo("关联失败:方案编号或用户编号为空!");
        }else{
            service.batchInsertSchemeUser(schemeId, userIds.split(","));
        }
        return result;
    }

}