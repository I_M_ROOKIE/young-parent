package com.yy.young.portal.web;

import com.yy.young.common.util.StringUtils;
import com.yy.young.interfaces.log.annotation.Log;
import com.yy.young.interfaces.model.User;
import com.yy.young.common.util.CommonUtil;
import com.yy.young.common.util.Result;
import com.yy.young.interfaces.ums.model.Navigate;
import com.yy.young.interfaces.ums.service.IUmsOutService;
import com.yy.young.portal.model.Menu;
import com.yy.young.portal.model.Scheme;
import com.yy.young.portal.service.IPortalSchemeService;
import com.yy.young.portal.service.ISchemeUserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by rookie on 2017/5/11.
 */
@Controller
@RequestMapping("/portal")
public class PortalCoreController {

    @Resource(name="umsOutService")
    IUmsOutService umsOutService;

    @Resource(name="schemeService")
    IPortalSchemeService schemeService;

    /**
     * 门户首页入口
     * @param request
     * @return
     * @throws Exception
     */
    @Log("门户首页入口")
    @RequestMapping("")
    public Object index(HttpServletRequest request) throws Exception{
        User user = CommonUtil.getLoginUser(request);
        if (user != null){
            Scheme scheme = schemeService.getSchemeByUser(user);//获取用户最高优先级的方案设置
            if (scheme != null && StringUtils.isNotBlank(scheme.getIndexUrl())){
                return scheme.getIndexUrl();
            }
        }
        return "/page/portal/index.jsp";
    }

    /**
     * 获取菜单(资源)
     * @param request
     * @return
     * @throws Exception
     */
    @Log("获取菜单(资源)")
    @RequestMapping("/getMenu")
    @ResponseBody
    public Object getMenu(HttpServletRequest request) throws Exception{
        User user = CommonUtil.getLoginUser(request);
        List<Navigate> navList = umsOutService.getNavigateByUserId(user.getId());
        List<Menu> menuList = new LinkedList<Menu>();
        //navigate转menu
        for(Navigate n : navList){
            menuList.add(new Menu(n));
        }
        //遍历父子关系
        for(Menu m : menuList){
            if(m != null){
                for(Menu m2 : menuList){
                    if(m2 != null && m2.getParentId() != null && m2.getParentId().equals(m.getId())){
                        m.appendChild(m2);
                        m2.setParent(m);
                    }
                }
            }
        }
        //找出根节点即是树
        List<Menu> menu = new LinkedList<Menu>();
        for(Menu m : menuList){
            if(m.isRoot()){
                menu.add(m);
                m.setParent(null);
            }else{
                m.setParent(null);//清除parent,否则springmvc返回json时会报异常
            }
        }
        return new Result(menu);
    }
    /**
     * 查询用户上一次登录时间
     * @param request
     * @return
     * @throws Exception
     */
    @Log("查询上次登录时间")
    @RequestMapping("/getLastLoginTime")
    @ResponseBody
    public Object getLastLoginTime(HttpServletRequest request) throws Exception{
        User user = CommonUtil.getLoginUser(request);
        if (user != null){
            return new Result(umsOutService.getLastLoginTime(user.getId()));
        }
        return new Result();
    }


}
