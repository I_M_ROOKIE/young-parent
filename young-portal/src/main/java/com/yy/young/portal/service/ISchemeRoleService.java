package com.yy.young.portal.service;

import com.yy.young.dal.util.Page;
import com.yy.young.portal.model.SchemeRole;
import java.util.List;

/**
 * 方案角色服务接口
 * Created by rookie on 2017-12-17.
 */
public interface ISchemeRoleService {

    /**
     * 获取数据列表
     * @param schemeRole
     * @return
     * @throws Exception
     */
    List<SchemeRole> getSchemeRoleList(SchemeRole schemeRole) throws Exception;

    /**
     * 获取数据列表(分页)
     * @param schemeRole
     * @param page 分页参数
     * @return
     * @throws Exception
     */
    List<SchemeRole> getSchemeRolePage(SchemeRole schemeRole, Page page) throws Exception;

    /**
     * 根据id获取信息
     * @param id
     * @return
     * @throws Exception
     */
    SchemeRole getSchemeRole(String id) throws Exception;

    /**
     * 修改
     * @param schemeRole
     * @return
     * @throws Exception
     */
    int updateSchemeRole(SchemeRole schemeRole) throws Exception;

    /**
     * 批量删除
     * @param idArr id数组
     * @return
     * @throws Exception
     */
    int deleteSchemeRole(String[] idArr) throws Exception;

    /**
     * 删除单条
     * @param id
     * @return
     * @throws Exception
     */
    int deleteSchemeRole(String id) throws Exception;

    /**
     * 插入
     * @param schemeRole
     * @return
     * @throws Exception
     */
    int insertSchemeRole(SchemeRole schemeRole) throws Exception;

    /**
     * 批量插入方案角色
     * @param schemeId
     * @param roleIds
     * @return
     * @throws Exception
     */
    int batchInsertSchemeRole(String schemeId, String[] roleIds) throws Exception;

}