package com.yy.young.portal.service.impl;

import com.yy.young.common.util.StringUtils;
import com.yy.young.interfaces.model.User;
import com.yy.young.portal.service.IPortalSchemeService;
import com.yy.young.dal.service.IDataAccessService;
import com.yy.young.dal.util.Page;
import com.yy.young.portal.model.Scheme;
import com.yy.young.portal.util.PortalConstants;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * 方案服务实现
 * Created by rookie on 2017-11-18.
 */
@Service("schemeService")
public class PortalSchemeServiceImpl implements IPortalSchemeService {

    @Resource(name = "dataAccessService")
    IDataAccessService dataAccessService;//数据层服务

    private static final Logger logger = LoggerFactory.getLogger(PortalSchemeServiceImpl.class);

    //获取数据列表
    @Override
    public List<Scheme> getSchemeList(Scheme scheme) throws Exception {
        return dataAccessService.getList(PortalConstants.MAPPER.TB_PORTAL_SCHEME + ".getSchemeList", scheme);
    }

    //返回用户的最高优先级方案
    @Override
    public Scheme getSchemeByUser(User user) throws Exception {
        if (user != null){
            //根据用户id查找方案
            List<Scheme> list = this.getSchemeByUserId(user.getId());

            //根据用户角色查找方案
            if ((list == null || list.size() == 0) && StringUtils.isNotBlank(user.getRoleId())){
                //由于用户可以具有多角色,在此处要对个角色进行筛选
                String[] roleArr = user.getRoleId().split(",");
                for(String roleId : roleArr){
                    if (list != null && list.size() > 0){//当方案list有效时,停止循环
                        break;
                    }
                    list = this.getSchemeByRoleId(roleId);
                }
            }

            //根据用户所属单位查找方案
            if ((list == null || list.size() == 0) && StringUtils.isNotBlank(user.getCompanyId())){
                list = this.getSchemeByCompanyId(user.getCompanyId());
            }

            //如果查找到多套方案,筛选出一套使用:时间最邻近的方案
            if (list != null && list.size() > 0){
                return list.get(0);//默认使用第一套
            }
        }

        return null;
    }

    @Override
    public List<Scheme> getSchemeByUserId(String userId) throws Exception {
        return dataAccessService.getList(PortalConstants.MAPPER.TB_PORTAL_SCHEME + ".getSchemeByUserId", userId);
    }

    @Override
    public List<Scheme> getSchemeByRoleId(String roleId) throws Exception {
        return dataAccessService.getList(PortalConstants.MAPPER.TB_PORTAL_SCHEME + ".getSchemeByRoleId", roleId);
    }

    @Override
    public List<Scheme> getSchemeByCompanyId(String companyId) throws Exception {
        return dataAccessService.getList(PortalConstants.MAPPER.TB_PORTAL_SCHEME + ".getSchemeByCompanyId", companyId);
    }

    //获取数据列表(分页)
    @Override
    public List<Scheme> getSchemePage(Scheme scheme, Page page) throws Exception {
        return dataAccessService.getList(PortalConstants.MAPPER.TB_PORTAL_SCHEME + ".getSchemeList", scheme, page);
    }

    //查询单条
    @Override
    public Scheme getScheme(String id) throws Exception {
        return (Scheme)dataAccessService.getObject(PortalConstants.MAPPER.TB_PORTAL_SCHEME + ".getSchemeById", id);
    }

    //修改
    @Override
    public int updateScheme(Scheme scheme) throws Exception {
        scheme.setUpdateTime(new Date());//更新时间
        return dataAccessService.update(PortalConstants.MAPPER.TB_PORTAL_SCHEME + ".update", scheme);
    }

    //批量删除
    @Override
    public int deleteScheme(String[] idArr) throws Exception {
        int i = 0;
        for(String id : idArr){
            i += this.deleteScheme(id);
        }
        return i;
    }

    //删除单条
    @Override
    public int deleteScheme(String id) throws Exception {
        return dataAccessService.delete(PortalConstants.MAPPER.TB_PORTAL_SCHEME + ".delete", id);
    }

    //插入
    @Override
    public int insertScheme(Scheme scheme) throws Exception {
        scheme.setCreateTime(new Date());//创建时间
        scheme.setUpdateTime(new Date());//更新时间
        return dataAccessService.insert(PortalConstants.MAPPER.TB_PORTAL_SCHEME + ".insert", scheme);
    }

}