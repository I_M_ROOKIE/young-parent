package com.yy.young.portal.model;

import java.util.Date;

/**
 * 方案实体类
 * Created by rookie on 2017-11-18.
 */
public class Scheme{

    private String id;//方案编号
    private String name;//方案名称
    private String indexUrl;//首页地址
    private String remark;//备注
    private Date createTime;//创建时间
    private Date updateTime;//更新时间
    private int num;//显示顺序
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getRemark() {
        return remark;
    }
    public void setRemark(String remark) {
        this.remark = remark;
    }
    public Date getCreateTime() {
        return createTime;
    }
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    public Date getUpdateTime() {
        return updateTime;
    }
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
    public int getNum() {
        return num;
    }
    public void setNum(int num) {
        this.num = num;
    }

    public String getIndexUrl() {
        return indexUrl;
    }

    public void setIndexUrl(String indexUrl) {
        this.indexUrl = indexUrl;
    }
}