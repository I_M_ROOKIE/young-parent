package com.yy.young.dal.util;
import org.apache.ibatis.session.RowBounds;
public class DalRowBounds extends RowBounds {
	private Page<?> page;
	
	public Page<?>  getPage() {
		return page;
	}

	public void setPage(Page<?>  page) {
		this.page = page;
	}

	public DalRowBounds(Page<?> page){
		/*
		 * super参数1设为(page.getCurrentPage()-1)*page.getPageSize()会导致分页出现问题
		 * 原因:虽然分页插件中合成了分页sql,但是最后还是会用父类里的这2个参数继续查询,相当于在原来的第N页基础上又取了一个第N页集合,导致查询为空
		 * 解决办法:1.在此将参数1设为0;2.分页插件中不对查询sql作处理.
		 */
		super(0,page.getPageSize());
		this.page = page;
	}
	public DalRowBounds(){
		super();
	}
}
