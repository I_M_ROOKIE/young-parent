package com.yy.young.dal.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.yy.young.dal.EDBType;
import org.springframework.stereotype.Service;

import com.yy.young.dal.util.DalRowBounds;
import com.yy.young.dal.util.Page;
import com.yy.young.dal.dao.IDataAccessDao;
import com.yy.young.dal.service.IDataAccessService;

@Service("dataAccessService")
public class DataAccessServiceImpl implements IDataAccessService<Object> {

	@Resource(name="dataAccessDao")
	IDataAccessDao dataAccessDao;
	
	
	public IDataAccessDao getDataSourceDao() {
		return dataAccessDao;
	}

	public void setDataSourceDao(IDataAccessDao dataAccessDao) {
		this.dataAccessDao = dataAccessDao;
	}

	@Override
	public EDBType getDBType() {
		return dataAccessDao.getDBType();
	}

	@Override
	public int insert(String method,Object parameter) throws Exception {
		return dataAccessDao.insert(method, parameter);
	}

	@Override
	public int delete(String method, Object parameter) throws Exception {
		return dataAccessDao.delete(method, parameter);
	}

	@Override
	public List<?> getList(String method,
						   Object parameter) throws Exception {
		return (List<?>)dataAccessDao.getList(method, parameter);
	}

	@Override
	public List<?> getList(String method,
						   Object parameter,Page page) throws Exception {
		if (page == null){//分页参数为null,调用常规查询
			return getList(method, parameter);
		}
		return dataAccessDao.getList(method, parameter, new DalRowBounds(page));
	}

	@Override
	public int update(String method, Object parameter) throws Exception {
		return dataAccessDao.update(method, parameter);
	}

	@Override
	public Object getObject(String method, Object parameter)
			throws Exception {

		return dataAccessDao.getObject(method, parameter);
	}
	
}
