package com.yy.young.dal.util;

import java.io.Serializable;
import java.util.List;

public class Page<T> implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	private int pageNumber = 1;//页码，默认是第一页
    private int pageSize = 10;//每页显示的记录数，默认是10
    private int total;//总记录数
    private List<?> data;//记录数据
	private List<?> rows;//记录数据,bootstrap-table使用该字段
    private String callback;//处理跨域调用jsonp格式处理标识
    private String data_id = "";
	private String data_desc = "";
	private int code = 0;
	private String info = "成功";
 
    public int getPageNumber() {
		return pageNumber<1?1:pageNumber;
	}

	public void setPageNumber(int currentPage) {
		this.pageNumber = currentPage;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int totalCount) {
		this.total = totalCount;
	}


	public int getPageSize() {
       return pageSize;
    }
 
    public void setPageSize(int pageSize) {
       this.pageSize = pageSize;
    }
 
 
    public void setTotalRecord(int totalCount) {
       this.total = totalCount;

    }
	 
	public List<?> getData() {
        return data;
    }

    public void setData(List<?> data) {
        this.data = data;
		this.rows = data;//供bootstrap-table使用
    }

	public List<?> getRows() {
		return rows;
	}

	public void setRows(List<?> rows) {
		this.rows = rows;
	}

	public String getCallback() {
        return callback;
    }

    public void setCallback(String callback) {
        this.callback = callback;
    }

    public String getData_id() {
		return data_id;
	}

	public void setData_id(String data_id) {
		this.data_id = data_id;
	}

	public String getData_desc() {
		return data_desc;
	}

	public void setData_desc(String data_desc) {
		this.data_desc = data_desc;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int errorCode) {
		this.code = errorCode;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String errorInfo) {
		this.info = errorInfo;
	}

	public Page(int currentPage, int pageSize) {
		super();
		this.pageNumber = currentPage;
		this.pageSize = pageSize;
	}

	public Page(){
		super();
	}

}
