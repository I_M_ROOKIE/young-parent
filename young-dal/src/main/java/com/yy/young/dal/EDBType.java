package com.yy.young.dal;

/**
 * 数据库类型枚举
 * Created by rookie on 2017/9/13.
 */
public enum EDBType {
    ORACLE,MYSQL,POSTGRESQL,OTHER;
}
