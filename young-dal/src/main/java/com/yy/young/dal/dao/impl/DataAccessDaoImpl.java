package com.yy.young.dal.dao.impl;

import java.util.List;
import java.util.Map;

import com.yy.young.dal.EDBType;
import org.apache.ibatis.session.RowBounds;
import org.mybatis.spring.SqlSessionTemplate;

import com.yy.young.dal.dao.IDataAccessDao;

public class DataAccessDaoImpl implements IDataAccessDao {

	private SqlSessionTemplate sqlSessionTemplate;
	
	
	public SqlSessionTemplate getSqlSessionTemplate() {
		return sqlSessionTemplate;
	}

	public void setSqlSessionTemplate(SqlSessionTemplate sqlSessionTemplate) {
		this.sqlSessionTemplate = sqlSessionTemplate;
	}

	@Override
	public EDBType getDBType() {
		String dbId = sqlSessionTemplate.getConfiguration().getDatabaseId();
		if ("MySQL".equalsIgnoreCase(dbId)){
			return EDBType.MYSQL;
		}else if ("Oracle".equalsIgnoreCase(dbId)){
			return EDBType.ORACLE;
		}else if ("PostgreSQL".equalsIgnoreCase(dbId)){
			return EDBType.POSTGRESQL;
		}else{
			return EDBType.OTHER;
		}
	}

	@Override
	public int insert(String method, Object parameter)
			throws Exception {
		// TODO Auto-generated method stub
		return sqlSessionTemplate.insert(method, parameter);
	}

	@Override
	public int delete(String method, Object parameter)
			throws Exception {
		// TODO Auto-generated method stub
		return sqlSessionTemplate.delete(method, parameter);
	}

	@Override
	public List<?> getList(String method,
						   Object parameter) throws Exception {
		// TODO Auto-generated method stub
		return sqlSessionTemplate.selectList(method, parameter);
	}

	@Override
	public List<?> getList(String method,
						   Object parameter, RowBounds rowBounds) throws Exception {
		
		return sqlSessionTemplate.selectList(method, parameter, rowBounds);
	}

	@Override
	public int update(String method, Object parameter)
			throws Exception {
		// TODO Auto-generated method stub
		return sqlSessionTemplate.update(method, parameter);
	}

	@Override
	public Object getObject(String method, Object parameter)
			throws Exception {
		// TODO Auto-generated method stub
		return sqlSessionTemplate.selectOne(method, parameter);
	}


}
