package com.yy.young.dal.service;

import java.util.List;
import java.util.Map;

import com.yy.young.dal.EDBType;
import com.yy.young.dal.util.Page;

/**
 * 数据库操作接口
 * @author wjy
 *
 */
public interface IDataAccessService<T> {
	/**
	 * 获取数据库类型
	 * @return
	 */
	EDBType getDBType();
	/**
	 * 插入
	 * @param method
	 * @param parameter
	 * @return
	 * @throws Exception
	 */
	int insert(String method, Object parameter) throws Exception;
	/**
	 * 删除
	 * @param method
	 * @param parameter
	 * @return
	 * @throws Exception
	 */
	int delete(String method, Object parameter) throws Exception;
	/**
	 * 查询
	 * @param method
	 * @param parameter
	 * @return
	 * @throws Exception
	 */
	List<?> getList(String method, Object parameter) throws Exception;
	/**
	 * 分页查询
	 * @param method
	 * @param parameter
	 * @param page
	 * @return
	 * @throws Exception
	 */
	List<?> getList(String method, Object parameter, Page page) throws Exception;
	/**
	 * 修改
	 * @param method
	 * @param parameter
	 * @return
	 * @throws Exception
	 */
	int update(String method, Object parameter) throws Exception;
	/**
	 * 查询一条数据
	 * @param method
	 * @param parameter
	 * @return
	 * @throws Exception
	 */
	Object getObject(String method, Object parameter) throws Exception;
}
