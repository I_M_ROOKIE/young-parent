package com.yy.young.log.core;

import com.yy.young.interfaces.log.annotation.Log;
import com.yy.young.interfaces.log.model.LogDTO;
import com.yy.young.interfaces.log.service.ILogHandler;

import java.util.Date;

/**
 * 日志消费任务
 * Created by rookie on 2017/11/10.
 */
public class LogConsumeTask implements Runnable {

    private LogDTO logDTO;//待销费的日志

    /**
     * 无参构造函数
     */
    public LogConsumeTask(){
        super();
    }

    /**
     * 带参构造函数
     * @param logDTO
     */
    public LogConsumeTask(LogDTO logDTO){
        super();
        this.logDTO = logDTO;
    }

    @Override
    public void run() {
        logDTO.setLogTime(new Date());//设置日志处理时间
        Log log = logDTO.getLog();
        ILogHandler logHandler = null;
        if (log != null && !"".equals(log.handler())){//获取指定的日志处理器
            logHandler = LogHandlerFactory.getHandler(log.handler());
        }
        if (logHandler == null){
            logHandler = LogHandlerFactory.getDefaultHandler();//默认的日志处理器
        }
        //调用处理器的日志处理
        logHandler.handle(logDTO);
    }
}
