package com.yy.young.log.util;

import com.yy.young.base.util.GlobalConstants;
import com.yy.young.interfaces.model.User;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

/**
 * 日志服务用到的工具方法
 * Created by rookie on 2017/11/10.
 */
public class LogUtil {

    //本机ip缓存,linux获取网卡配置缓慢(5S左右)
    private static String LOCAL_IP = null;

    /**
     * 获取本地IP地址
     * @return
     */
    public static String getLocalIp(){
        if(LOCAL_IP != null){
            return LOCAL_IP;
        }
        String ipAddress = "";
        InetAddress inet = null;
        try {
            inet = InetAddress.getLocalHost();
        } catch (UnknownHostException var4) {
            var4.printStackTrace();
        }
        if (inet != null){
            ipAddress = inet.getHostAddress();
            if(ipAddress != null && ipAddress.length() > 15 && ipAddress.indexOf(",") > 0) {
                ipAddress = ipAddress.substring(0, ipAddress.indexOf(","));
            }
            LOCAL_IP = ipAddress;//计入缓存,方便下次使用
        }
        return ipAddress;
    }

    /**
     * !
     *  获取当前登录用户信息
     * @param request
     * @return
     */
    public static User getLoginUser(HttpServletRequest request){
        if (request.getSession() != null){
            return (User) request.getSession().getAttribute(GlobalConstants.SESSION.KEY_LOGINUSER);
        }
        return null;
    }

    /**
     * 将request中的参数封装为map对象返回
     * @Title: getParameterFromRequest
     * @Description: 将request中的参数封装为map对象返回
     * @param @param request
     * @param @return    设定文件
     * @return Map<String,Object>    返回类型
     * @throws
     */
    public static Map<String,Object> getParameterFromRequest(HttpServletRequest request){
        Map<String,Object> result = new HashMap<String,Object>();
        Enumeration<String> enums = request.getParameterNames();
        while (enums.hasMoreElements()) {
            String key = enums.nextElement();
            String val = request.getParameter(key);
            result.put(key, val);
        }
        //result.put(GlobalConstants.SESSION.KEY_LOGINUSER, getLoginUser(request));
        return result;
    }
    /**
     * 获取客户端IP地址，考虑到含有代理的情况
     * @return
     */
    public static String getClientIpAddr(HttpServletRequest request){
        String ipAddress = request.getHeader("x-forwarded-for");
        if(ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getHeader("Proxy-Client-IP");
        }

        if(ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getHeader("WL-Proxy-Client-IP");
        }

        if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getHeader("HTTP_CLIENT_IP");
        }

        if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getHeader("HTTP_X_FORWARDED_FOR");
        }
        if(ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getRemoteAddr();
            if("127.0.0.1".equals(ipAddress) || "0:0:0:0:0:0:0:1".equals(ipAddress)) {
                /*InetAddress inet = null;
                try {
                    //根据网卡取本机配置的IP
                    inet = InetAddress.getLocalHost();
                } catch (UnknownHostException var4) {
                    var4.printStackTrace();
                }
                ipAddress = inet.getHostAddress();*/
                ipAddress = getLocalIp();//默认取本机ip地址
            }
        }
        //对于通过多个代理的情况，第一个IP为客户端真实IP,多个IP按照','分割
        if(ipAddress != null && ipAddress.length() > 15 && ipAddress.indexOf(",") > 0) {
            ipAddress = ipAddress.substring(0, ipAddress.indexOf(","));
        }
        return ipAddress;
    }
    /**
     * 获取操作系统的版本
     * @return
     */
    public static String getClientOS(HttpServletRequest request){
        String Agent = request.getHeader("User-Agent");
        StringTokenizer st = new StringTokenizer(Agent,";");
        if (st != null){
            if (st.hasMoreTokens()){
                st.nextToken();
            }
            if (st.hasMoreTokens()){
                st.nextToken();
            }
            if (st.hasMoreTokens()){
                return st.nextToken();
            }
        }
        return "";
    }

    /**
     * 获取浏览器系统的版本
     * @return
     */
    public static String getClientBrowser(HttpServletRequest request){
        String Agent = request.getHeader("User-Agent");
        if(Agent==null) return null;
        StringTokenizer st = new StringTokenizer(Agent,";");
        if(st==null) return null;
        if (st.hasMoreTokens()){
            st.nextToken();
        }
        if (st.hasMoreTokens()){
            return st.nextToken();
        }
        return  "";
    }
}
