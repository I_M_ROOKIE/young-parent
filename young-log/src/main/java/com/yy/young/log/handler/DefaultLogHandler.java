package com.yy.young.log.handler;

import com.yy.young.interfaces.log.model.LogDTO;
import com.yy.young.interfaces.log.service.ILogHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 默认的日志处理器
 * Created by rookie on 2017/11/10.
 */
public class DefaultLogHandler implements ILogHandler {

    private static final Logger logger = LoggerFactory.getLogger(DefaultLogHandler.class);

    @Override
    public void handleBefore(LogDTO logDTO) {

    }

    @Override
    public void handle(LogDTO logDTO) {
        logger.info("[{}] -请求时间{} -类{} -方法{} -响应时间{}ms", logDTO.getLog().value(), logDTO.getBeginTime(), logDTO.getClassName(), logDTO.getMethodName(), logDTO.getMs());
    }

    @Override
    public void handleAfter(LogDTO logDTO) {

    }
}
