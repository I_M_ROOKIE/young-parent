package com.yy.young.log.core;

import com.yy.young.interfaces.log.annotation.Log;
import com.yy.young.interfaces.log.model.LogDTO;
import com.yy.young.log.util.LogUtil;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;

import java.lang.reflect.Method;

/**
 * 日志抓捕任务,用于异步格式化日志信息(将请求信息封装为LogDTO)
 * Created by rookie on 2017/11/10.
 */
public class LogCatchTask implements Runnable {

    private ProceedingJoinPoint joinPoint;//方法切入点对象
    private LogDTO logDTO;//日志

    /**
     * 无参构造器
     */
    public LogCatchTask(){
        super();
    }

    /**
     * 带参构造器
     * @param joinPoint
     * @param logDTO
     */
    public LogCatchTask(ProceedingJoinPoint joinPoint, LogDTO logDTO){
        super();
        this.joinPoint = joinPoint;
        this.logDTO = logDTO;
    }
    @Override
    public void run() {
        logDTO.setMs(logDTO.getEndTime().getTime() - logDTO.getBeginTime().getTime());//方法耗时
        logDTO.setServerIp(LogUtil.getLocalIp());//服务器ip
        //获取目标方法的标记
        MethodSignature ms = (MethodSignature)joinPoint.getSignature();
        Method method = ms.getMethod();
        logDTO.setLog(method.getAnnotation(Log.class));//注解信息
        logDTO.setClassName(joinPoint.getTarget().getClass().getName().toString());//类名
        logDTO.setMethodName(method.getName());//方法名

        //放入日志队列
        LogControl.put(logDTO);
    }

    public ProceedingJoinPoint getJoinPoint() {
        return joinPoint;
    }

    public void setJoinPoint(ProceedingJoinPoint joinPoint) {
        this.joinPoint = joinPoint;
    }

    public LogDTO getLogDTO() {
        return logDTO;
    }

    public void setLogDTO(LogDTO logDTO) {
        this.logDTO = logDTO;
    }
}
