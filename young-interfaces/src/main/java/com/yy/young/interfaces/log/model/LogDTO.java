package com.yy.young.interfaces.log.model;

import com.yy.young.interfaces.log.annotation.Log;
import com.yy.young.interfaces.model.User;

import java.util.Date;
import java.util.Map;

/**
 * 日志传输对象,用于日志控制器和日志处理器之间传递参数
 * Created by rookie on 2017/11/10.
 */
public class LogDTO implements java.io.Serializable {
    private static final long serialVersionUID = 1L;
    private Log log;//注解信息
    private User loginUser;//操作用户
    private Date beginTime;//请求开始时间
    private Date endTime;//请求结束时间
    private Date logTime;//日志处理时间
    private String serverIp;//服务器ip
    private String clientIp;//用户ip
    private String os;//用户操作系统信息
    private String browser;//用户浏览器信息
    private String scheme;//请求的协议,http/https等
    private String host;//请求的host
    private Integer port;//请求的端口号
    private String uri;//请求的uri
    private String url;//请求的url
    private String className;//类名
    private String methodName;//方法名
    private long ms;//方法执行耗时(毫秒)
    private Exception exception;//请求执行后产生的异常(正常执行则为null)
    private Map<String, Object> requestParam;//请求参数,LogUtil.getParameterFromRequest(request)
    private Map<String, Object> extParam;//扩展参数,暂时没有使用此字段
    private Object result;//方法返回结果

    @Override
    public String toString() {
        return "LogDTO{" +
                "log=" + log +
                ", loginUser=" + loginUser +
                ", beginTime=" + beginTime +
                ", endTime=" + endTime +
                ", logTime=" + logTime +
                ", serverIp='" + serverIp + '\'' +
                ", clientIp='" + clientIp + '\'' +
                ", os='" + os + '\'' +
                ", browser='" + browser + '\'' +
                ", scheme='" + scheme + '\'' +
                ", host='" + host + '\'' +
                ", port=" + port +
                ", uri='" + uri + '\'' +
                ", url='" + url + '\'' +
                ", className='" + className + '\'' +
                ", methodName='" + methodName + '\'' +
                ", ms=" + ms +
                ", exception=" + exception +
                ", requestParam=" + requestParam +
                ", extParam=" + extParam +
                ", result=" + result +
                '}';
    }

    public String getServerIp() {
        return serverIp;
    }

    public void setServerIp(String serverIp) {
        this.serverIp = serverIp;
    }

    public long getMs() {
        return ms;
    }

    public void setMs(long ms) {
        this.ms = ms;
    }

    public Log getLog() {
        return log;
    }

    public void setLog(Log log) {
        this.log = log;
    }

    public User getLoginUser() {
        return loginUser;
    }

    public void setLoginUser(User loginUser) {
        this.loginUser = loginUser;
    }

    public Date getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }


    public String getClientIp() {
        return clientIp;
    }

    public void setClientIp(String clientIp) {
        this.clientIp = clientIp;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Date getLogTime() {
        return logTime;
    }

    public void setLogTime(Date logTime) {
        this.logTime = logTime;
    }

    public Exception getException() {
        return exception;
    }

    public void setException(Exception exception) {
        this.exception = exception;
    }

    public Map<String, Object> getRequestParam() {
        return requestParam;
    }

    public void setRequestParam(Map<String, Object> requestParam) {
        this.requestParam = requestParam;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public Map<String, Object> getExtParam() {
        return extParam;
    }

    public void setExtParam(Map<String, Object> extParam) {
        this.extParam = extParam;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getScheme() {
        return scheme;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }
}
