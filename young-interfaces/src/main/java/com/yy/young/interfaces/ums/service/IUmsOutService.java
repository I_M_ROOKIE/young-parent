package com.yy.young.interfaces.ums.service;

import com.yy.young.interfaces.model.User;
import com.yy.young.interfaces.ums.model.Dept;
import com.yy.young.interfaces.ums.model.Navigate;
import com.yy.young.interfaces.ums.model.Role;
import com.yy.young.interfaces.ums.model.SsoVerifyDTO;

import java.util.Date;
import java.util.List;

/**
 * Created by rookie on 2017/5/11.
 * 统一用户对外接口
 */
public interface IUmsOutService {
    /**
     * 根据用户ID获取用户包含的资源列表
     * @param userId
     * @return
     * @throws Exception
     */
    List<Navigate> getNavigateByUserId(String userId) throws Exception;

    /**
     * 根据用户id获取资源树(获取菜单),暂未实现
     * @param userId
     * @return
     * @throws Exception
     */
    List<Navigate> getNavigateAsTreeByUserId(String userId) throws Exception;

    /**
     * 根据用户id获取用户登录信息
     * 用于单点客户端获取用户信息
     * @param userId
     * @return
     * @throws Exception
     */
    User getLoginUserById(String userId) throws Exception;

    /**
     * 验证单点,即验证token合法性
     * @param token
     * @return
     * @throws Exception
     */
    SsoVerifyDTO verifySSO(String token) throws Exception;

    /**
     * 查询用户上一次登录的时间
     * @param userId
     * @return
     * @throws Exception
     */
    Date getLastLoginTime(String userId) throws Exception;

    /**
     * 根据组织id获取组织信息
     * @param deptId
     * @return
     * @throws Exception
     */
    Dept getDeptById(String deptId) throws Exception;

    /**
     * 根据角色id获取角色信息
     * @param roleId
     * @return
     * @throws Exception
     */
    Role getRoleById(String roleId) throws Exception;

    /**
     * 判断是否存在指定id/account的用户
     * 在创建用户之前做唯一性验证
     * @param userIdOrAccount 用户id或者登陆账号
     * @return
     * @throws Exception
     */
    boolean existUserIdOrAccount(String userIdOrAccount) throws Exception;

    /**
     * 创建用户,供外部调用,id/account/companyId不允许为空
     * @param user
     * @return
     * @throws Exception
     */
    boolean insertUser(User user) throws Exception;

    /**
     * 更新用户信息
     * @param user
     * @return int 改动的数据条数,-1表示操作失败
     * @throws Exception
     */
    int updateUser(User user) throws Exception;

    /**
     * 删除用户
     * @param userId 用户id
     * @return int 改动的数据条数,-1表示操作失败
     * @throws Exception
     */
    int deleteUser(String userId) throws Exception;
}
