package com.yy.young.interfaces.ums.model;


/**
 * Created by rookie on 2017/5/11.
 * 资源实体类
 */
public class Navigate implements java.io.Serializable {
    private static final long serialVersionUID = 1L;
    private String id;//编号
    private String name;//资源名称
    private String href;//链接
    private String parentId;//父节点id
    private String parentName;//父节点名称
    private String icon;//图标
    private String type;//资源类型
    private int level;//层级
    private int num;//显示顺序

    public Navigate() {
    }

    @Override
    public String toString() {
        return "Navigate{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", href='" + href + '\'' +
                ", parentId='" + parentId + '\'' +
                ", parentName='" + parentName + '\'' +
                ", icon='" + icon + '\'' +
                ", type='" + type + '\'' +
                ", level=" + level +
                ", num=" + num +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }
}
