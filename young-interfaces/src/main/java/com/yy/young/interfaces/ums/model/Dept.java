package com.yy.young.interfaces.ums.model;

import java.util.Date;

/**
 * 组织机构实体类
 * Created by rookie on 2017/7/3.
 */
public class Dept implements java.io.Serializable {
    private static final long serialVersionUID = 1L;
    private String id;//编号
    private String name;//名称
    private String shortName;//简称
    private String parentId;//父节点id
    private int level;//层级
    private int num;//排序
    private String wholeId;//id全路径
    private String wholeName;//name全路径
    private String type;//类型,
    private Date createTime;//创建时间

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getWholeId() {
        return wholeId;
    }

    public void setWholeId(String wholeId) {
        this.wholeId = wholeId;
    }

    public String getWholeName() {
        return wholeName;
    }

    public void setWholeName(String wholeName) {
        this.wholeName = wholeName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
