package com.yy.young.interfaces.log.annotation;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 日志标记注解类
 * Created by rookie on 2017/11/8.
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Log {
    String value() default "";//日志信息
    String handler() default "";//日志处理器名称
    String type() default "";//日志类型,用于自定义处理器对特殊日志的识别
}
