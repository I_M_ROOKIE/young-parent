package com.yy.young.interfaces.ums.model;

import com.yy.young.interfaces.model.User;

/**
 * 单点登录验证数据传输对象
 * 用于向单点客户端返回验证信息
 * Created by rookie on 2017/8/2.
 */
public class SsoVerifyDTO implements java.io.Serializable {
    private static final long serialVersionUID = 1L;
    private String token;//令牌信息
    private int code;//结果代码,1表示成功,0表示失败
    private String info;//结果信息
    private String userId;//验证成功时返回的用户id
    private User user;//验证成功时返回用户信息

    public static SsoVerifyDTO getSuccessInstance(String token, User user){
        return new SsoVerifyDTO(token, 1, "成功", user.getId(), user);
    }

    public static SsoVerifyDTO getSuccessInstance(String token, String userId){
        return new SsoVerifyDTO(token, 1, "成功", userId, null);
    }

    public static SsoVerifyDTO getFailInstance(String token, String info){
        return new SsoVerifyDTO(token, 0, info, null, null);
    }

    public SsoVerifyDTO(){
        super();
    }

    public SsoVerifyDTO(String token, int code, String info, String userId) {
        this.token = token;
        this.code = code;
        this.info = info;
        this.userId = userId;
    }

    public SsoVerifyDTO(String token, int code, String info, String userId, User user) {
        this.token = token;
        this.code = code;
        this.info = info;
        this.userId = userId;
        this.user = user;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
