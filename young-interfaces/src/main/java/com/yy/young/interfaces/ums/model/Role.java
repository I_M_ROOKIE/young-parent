package com.yy.young.interfaces.ums.model;

/**
 * 角色实体类
 * Created by rookie on 2017/12/17.
 */
public class Role implements java.io.Serializable {
    private static final long serialVersionUID = 1L;
    private String id;//编号
    private String name;//角色名称
    private String rangeId;//管理范围
    private int num;//显示顺序
    private String remark;//描述
    private String type;//角色类型
    private String companyId;//所属单位

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getRangeId() {
        return rangeId;
    }
    public void setRangeId(String rangeId) {
        this.rangeId = rangeId;
    }
    public int getNum() {
        return num;
    }
    public void setNum(int num) {
        this.num = num;
    }
    public String getRemark() {
        return remark;
    }
    public void setRemark(String remark) {
        this.remark = remark;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getCompanyId() {
        return companyId;
    }
    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }
}
