/*
 * 二龙山下的小钢炮公司源代码，版权归中北大学公司所有。
 * 
 * 项目名称 : StudentStudy
 * 创建日期 : 2016年1月6日
 * 修改历史 : 
 *     1. [2016年1月6日]创建文件 by yuge
 */
package com.yy.young.interfaces.model;

/**!
 * //TODO 用户表实体类
 * 二次开发如果需要自定义用户实体类,可以对该类进行继承
 * @author yuge
 */
public class User implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

    /*
     * 编号
     */
    private String id;
    /*
     * 用户名字
     */
    private String name;
    /*
     * 年龄
     */
    private int age;
    /*
     * 登录名
     */
    private String account;
    /*
     * 密码
     */
    private String password;
    /*
     * 生日
     */
    private String birthday;
    /*
     * 性别
     */
    private String sex;
    /*
     * 电话
     */
    private String tel;
    /*
     * 邮箱
     */
    private String email;
    /*
     * 用户状态
     */
    private String state;
	/*
	 * 头像
	 */
	private String header;
	/*
	 * 所属单位id
	 */
	private String companyId;
	/*
	 * 所属单位名称
	 */
	private String companyName;
	/*
	 * 显示顺序
	 */
	private Integer num;
	/*
	 * 备注
	 */
	private String remark;


    /*
     * 角色id
     */
    private String roleId;
    /*
     * 角色名称
     */
    private String roleName;
    /*
     * 部门id
     */
    private String deptId;
    /*
     * 部门名称
     */
    private String deptName;
    /*
     * 管理范围,部门号
     */
    private String rangeId;

	/*
	 * 用户登录时的发放的令牌
	 */
	private String token;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getRangeId() {
		return rangeId;
	}

	public void setRangeId(String rangeId) {
		this.rangeId = rangeId;
	}
    
	public int getAge() {
		return age;
	}

	@Override
	public String toString() {
		return "User{" +
				"id='" + id + '\'' +
				", name='" + name + '\'' +
				", age=" + age +
				", account='" + account + '\'' +
				", password='" + password + '\'' +
				", birthday='" + birthday + '\'' +
				", sex='" + sex + '\'' +
				", tel='" + tel + '\'' +
				", email='" + email + '\'' +
				", state='" + state + '\'' +
				", header='" + header + '\'' +
				", companyId='" + companyId + '\'' +
				", companyName='" + companyName + '\'' +
				", num=" + num +
				", remark='" + remark + '\'' +
				", roleId='" + roleId + '\'' +
				", roleName='" + roleName + '\'' +
				", deptId='" + deptId + '\'' +
				", deptName='" + deptName + '\'' +
				", rangeId='" + rangeId + '\'' +
				", token='" + token + '\'' +
				'}';
	}

	public void setAge(int age) {
		this.age = age;
	}

	/**
     * @return Returns the password.
     */
    public String getPassword() {
        return password;
    }
    /**
     * @param password The password to set.
     */
    public void setPassword(String password) {
        this.password = password;
    }
    
    public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getDeptId() {
		return deptId;
	}
	public void setDeptId(String deptId) {
		this.deptId = deptId;
	}
	public String getDeptName() {
		return deptName;
	}
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
	/**
     * @return Returns the roleId.
     */
    public String getRoleId() {
        return roleId;
    }
    /**
     * @param roleId The roleId to set.
     */
    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }
    /**
     * @return Returns the roleName.
     */
    public String getRoleName() {
        return roleName;
    }
    /**
     * @param roleName The roleName to set.
     */
    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
    /**
     * @return Returns the id.
     */
    public String getId() {
        return id;
    }
    /**
     * @param id The id to set.
     */
    public void setId(String id) {
        this.id = id;
    }

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
}
