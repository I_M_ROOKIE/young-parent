package com.yy.young.interfaces.log.service;

import com.yy.young.interfaces.log.model.LogDTO;

/**
 * 日志处理器接口
 * Created by rookie on 2017/11/8.
 */
public interface ILogHandler {

    /**
     * 日志处理前置操作
     * @param logDTO 待处理的日志传输对象
     */
    void handleBefore(LogDTO logDTO);

    /**
     * 日志处理
     * @param logDTO
     */
    void handle(LogDTO logDTO);

    /**
     * 日志处理后置操作
     * @param logDTO
     */
    void handleAfter(LogDTO logDTO);
}
