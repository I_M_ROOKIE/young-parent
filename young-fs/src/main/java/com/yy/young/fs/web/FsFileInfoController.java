package com.yy.young.fs.web;

import com.yy.young.common.util.CommonUtil;
import com.yy.young.common.util.Result;
import com.yy.young.common.util.StringUtils;
import com.yy.young.dal.util.Page;
import com.yy.young.fs.model.FileInfo;
import com.yy.young.fs.model.EFileType;
import com.yy.young.fs.service.IFileInfoService;
import com.yy.young.fs.util.FsConstants;
import com.yy.young.interfaces.log.annotation.Log;
import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.lang.reflect.Field;
import java.util.*;

/**
 * cms文件信息请求处理
 * Created by Administrator on 2017/5/8.
 */
@Controller
@RequestMapping("/fs/fileInfo")
public class FsFileInfoController {

    @Resource(name="fsFileInfoService")
    IFileInfoService fileInfoService;

    /**
     * 获取文件信息列表
     * @param fileInfo
     * @param request
     * @return
     * @throws Exception
     */
    @Log("查询文件信息列表")
    @RequestMapping("/getFileInfoList")
    @ResponseBody
    public Object getFileInfoList(FileInfo fileInfo, HttpServletRequest request) throws Exception{
        List<FileInfo> list = fileInfoService.getFileInfoList(fileInfo);
        return new Result(list);
    }

    /**
     * 获取文件夹下的文件信息,包含文件夹
     * @param fileInfo
     * @param request
     * @return
     * @throws Exception
     */
    @Log("查询文件夹下的文件信息(含文件夹)")
    @RequestMapping("/getFileInfoByFolder")
    @ResponseBody
    public Object getFileInfoByFolder(FileInfo fileInfo, HttpServletRequest request) throws Exception{
        List<FileInfo> list = fileInfoService.getFileInfoByFolder(fileInfo.getFolderId());
        return new Result(list);
    }

    /**
     * 获取文件信息列表(分页)
     * @param fileInfo
     * @param request
     * @return
     * @throws Exception
     */
    @Log("分页查询文件信息列表")
    @RequestMapping("/getFileInfoPage")
    @ResponseBody
    public Object getFileInfoPage(FileInfo fileInfo, HttpServletRequest request) throws Exception{
        Page page = CommonUtil.getPageFromRequest(request);
        List<FileInfo> list = fileInfoService.getFileInfoPage(fileInfo, page);
        page.setData(list);
        return page;
    }

    /**
     * 添加文件信息
     * @param fileInfo
     * @param request
     * @return
     * @throws Exception
     */
    @Log("添加文件信息")
    @RequestMapping("/insertFileInfo")
    @ResponseBody
    public Object insertFileInfo(FileInfo fileInfo, HttpServletRequest request) throws Exception{
        Result result = new Result();
        int num = fileInfoService.insertFileInfo(fileInfo);
        if(num < 0){
            result.setCode(-1);
            result.setInfo("添加文件信息失败!");
        }
        return result;
    }

    /**
     * 修改文件信息
     * @param fileInfo
     * @param request
     * @return
     * @throws Exception
     */
    @Log("修改文件信息")
    @RequestMapping("/updateFileInfo")
    @ResponseBody
    public Object updateFileInfo(FileInfo fileInfo, HttpServletRequest request) throws Exception{
        int num = fileInfoService.updateFileInfo(fileInfo);
        Result result = new Result();
        if(num < 0){
            result.setCode(-1);
            result.setInfo("修改文件信息失败!");
        }
        return result;
    }

    /**
     * 删除文件信息
     * @param request
     * @return
     * @throws Exception
     */
    @Log("删除文件信息")
    @RequestMapping("/deleteFileInfo")
    @ResponseBody
    public Object deleteFileInfo(HttpServletRequest request) throws Exception{
        Map<String,Object> parameter = CommonUtil.getParameterFromRequest(request);
        String idsStr = parameter.get("ids")+"";
        String[] idArr;
        Result result = new Result();
        if(StringUtils.isNotBlank(idsStr)) {
            idArr = idsStr.split(",");
            int num = fileInfoService.deleteFileInfo(idArr);
            if(num < 0){
                result.setCode(-1);
                result.setInfo("删除文件信息失败!");
            }
        }else{
            result.setCode(-1);
            result.setInfo("删除失败:待删除文件ID无效!");
        }

        return result;
    }

    /**
     * 获取文件夹树
     * @param fileInfo
     * @param request
     * @return
     * @throws Exception
     */
    @Log("获取文件夹树")
    @RequestMapping("/getFolderTree")
    @ResponseBody
    public Object getFolderTree(FileInfo fileInfo, HttpServletRequest request) throws Exception{
        List<FileInfo> list = fileInfoService.getFolderTree(fileInfo);
        return new Result(list);
    }

    /**
     * 获取文件类型列表
     * @param request
     * @return
     * @throws Exception
     */
    @Log("查询文件类型枚举项列表")
    @RequestMapping("/getFileTypeList")
    @ResponseBody
    public Object getFileTypeList(HttpServletRequest request) throws Exception{
        /*List<Map<String, String>> list = new ArrayList<Map<String, String>>();
        Class cls = EFileType.class;
        Field[] fields = cls.getDeclaredFields();
        for(Field field : fields){
            Map<String,String> map = new HashMap<String, String>();
            EFileType.Remark remark = field.getAnnotation(EFileType.Remark.class);
            if(remark != null){
                map.put("key", field.getName());
                map.put("value", remark.value());
                list.add(map);
            }
        }*/
        return new Result(fileInfoService.getFileTypeList());
    }

    /**
     * 统计文件数量
     * @param request
     * @return
     * @throws Exception
     */
    @Log("文件数量统计")
    @RequestMapping("/statisFileNum")
    @ResponseBody
    public Object statisFileNum(HttpServletRequest request) throws Exception{

        return new Result(fileInfoService.statisFileNum());
    }

    /**
     * 获取一级目录,即ROOT下的直接子目录
     * @param request
     * @return
     * @throws Exception
     */
    @Log("查询一级目录")
    @RequestMapping("/getOneLevelFolder")
    @ResponseBody
    public Object getOneLevelFolder(HttpServletRequest request) throws Exception{
        List<FileInfo> list = fileInfoService.getSubFolderList(FsConstants.ROOT_FOLDER_ID);
        if (list != null && list.size() > 0){
            for (FileInfo fileInfo : list){
                //查询子文件数量
                fileInfo.setChildFileNum(fileInfoService.countFileNumByFolderId(fileInfo.getId()));
                if (fileInfo.getAddr() != null){
                    File file = new File(fileInfo.getAddr());
                    if (file.exists() && file.isDirectory()){
                        //设置文件夹大小
                        fileInfo.setSize((int)(FileUtils.sizeOfDirectory(file) / 1024));
                    }
                }
            }
        }
        return new Result(list);
    }

    /**
     * 查询根目录信息
     * @param request
     * @return
     * @throws Exception
     */
    @Log("查询根目录信息")
    @RequestMapping("/getRootFolderInfo")
    @ResponseBody
    public Object getRootFolderInfo(HttpServletRequest request) throws Exception{
        FileInfo fileInfo = fileInfoService.getFolderInfo(FsConstants.ROOT_FOLDER_ID);
        if (fileInfo != null){
            //根目录下的文件信息(含文件夹)
            List<FileInfo> list = fileInfoService.getFileInfoByFolder(FsConstants.ROOT_FOLDER_ID);
            //剔除文件夹并计算文件大小
            if (list != null && list.size() > 0){
                /*Iterator<FileInfo> it = list.iterator();
                int size = 0;
                while (it.hasNext()){
                    FileInfo one = it.next();
                    if (one != null && one.isFile()){
                        size += one.getSize();
                    }
                }*/
                fileInfo.setChildFileNum(list.size());
                File file = new File(fileInfo.getAddr());
                if (file.exists() && file.isDirectory()){
                    //设置文件夹大小
                    fileInfo.setSize((int)(FileUtils.sizeOfDirectory(file) / 1024));
                }
            }
        }
        return new Result(fileInfo);
    }

}
