package com.yy.young.fs.model;

/**
 * 文件统计实体类,按照类型进行统计
 * Created by rookie on 2017/9/23.
 */
public class FileStatisticsOnType {
    private String type;//文件类型
    private String typeRemark;//类型注释
    private int num;//数量

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTypeRemark() {
        return typeRemark;
    }

    public void setTypeRemark(String typeRemark) {
        this.typeRemark = typeRemark;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }
}
