package com.yy.young.fs.service;

import com.yy.young.fs.model.FileInfo;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 文件服务接口
 * Created by rookie on 2017/9/5.
 */
public interface IFileService {

    /**
     * 保存文件,默认存放到根目录
     * @param file 文件信息
     * @return
     * @throws Exception
     */
    FileInfo saveFile(MultipartFile file) throws Exception;

    /**
     * 上传文件到指定文件夹
     * @param file 文件信息
     * @param folderId 文件夹id
     * @return
     * @throws Exception
     */
    FileInfo saveFile(MultipartFile file, String folderId) throws Exception;

    /**
     * 批量上传文件到指定文件夹
     * @param files
     * @param folderId
     * @return
     * @throws Exception
     */
    List<FileInfo> saveFiles(List<MultipartFile> files, String folderId) throws Exception;

    /**
     * 删除文件
     * @param fileId 文件编号
     * @return
     * @throws Exception
     */
    boolean dropFile(String fileId) throws Exception;

    /**
     * 删除文件夹
     * @param folderId 文件夹编号
     * @return
     * @throws Exception
     */
    boolean dropFolder(String folderId) throws Exception;

    /**
     * 创建文件夹
     * @param folder 文件夹信息
     * @return 文件夹信息
     * @throws Exception
     */
    FileInfo createFolder(FileInfo folder) throws Exception;
}
