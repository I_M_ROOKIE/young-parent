package com.yy.young.fs.service;

import com.yy.young.dal.util.Page;
import com.yy.young.fs.model.FileInfo;
import com.yy.young.fs.model.FileStatisticsOnType;

import java.util.List;
import java.util.Map;


/**
 * 文件信息服务,主要是对数据库存储的文件信息作处理
 * Created by rookie on 2017/9/5.
 */
public interface IFileInfoService {
    /**
     * 获取文件信息列表
     * @param fileInfo
     * @return
     * @throws Exception
     */
    List<FileInfo> getFileInfoList(FileInfo fileInfo) throws Exception;

    /**
     * 查询子文件夹信息列表
     * @param folderId
     * @return
     * @throws Exception
     */
    List<FileInfo> getSubFolderList(String folderId) throws Exception;

    /**
     * 根据文件编号查询文件信息
     * @param id
     * @return
     * @throws Exception
     */
    FileInfo getFileInfo(String id) throws Exception;

    /**
     * 获取文件夹树
     * @param fileInfo
     * @return
     * @throws Exception
     */
    List<FileInfo> getFolderTree(FileInfo fileInfo) throws Exception;

    /**
     * 获取文件信息(分页)
     * @param fileInfo
     * @param page
     * @return
     * @throws Exception
     */
    List<FileInfo> getFileInfoPage(FileInfo fileInfo, Page page) throws Exception;

    /**
     * 查询文件夹下的文件信息
     * @param folderId
     * @return
     * @throws Exception
     */
    List<FileInfo> getFileInfoByFolder(String folderId) throws Exception;

    /**
     * 获取文件类型列表,key=类型,value=描述
     * @return
     */
    List<Map<String, String>> getFileTypeList();

    /**
     * 查询文件夹信息
     * @param id 文件夹编号
     * @return
     * @throws Exception
     */
    FileInfo getFolderInfo(String id) throws Exception;

    /**
     * 修改文件信息信息
     * @param fileInfo
     * @return
     * @throws Exception
     */
    int updateFileInfo(FileInfo fileInfo) throws Exception;

    /**
     * 删除文件信息信息
     * @param idsArr 编号数组
     * @return
     * @throws Exception
     */
    int deleteFileInfo(String[] idsArr) throws Exception;

    /**
     * 删除文件夹下的文件信息
     * @param folderId
     * @return
     * @throws Exception
     */
    int deleteFileInfoByFolder(String folderId) throws Exception;

    /**
     * 插入文件信息信息
     * @param fileInfo
     * @return
     * @throws Exception
     */
    int insertFileInfo(FileInfo fileInfo) throws Exception;

    /**
     * 文件数量统计
     * @return
     * @throws Exception
     */
    List<FileStatisticsOnType> statisFileNum() throws Exception;

    /**
     * 统计文件夹下的文件数量(文件夹算1个文件)
     * @param folderId
     * @return
     * @throws Exception
     */
    int countFileNumByFolderId(String folderId) throws Exception;
}
