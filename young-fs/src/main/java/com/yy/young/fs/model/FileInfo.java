package com.yy.young.fs.model;

import java.util.Date;

/**
 * 文件信息实体类
 * Created by rookie on 2017/9/5.
 */
public class FileInfo {

    private String id;  //编号
    private String name;    //名称
    private String folderId;    //所属文件夹编号
    private String status;  //状态,正常1/只读2/隐藏3
    private EFileType type;    //类型
    private int size;   //大小
    private String remark;  //备注
    private String isFile;  //文件标识,1标识文件,0标识文件夹
    private String addr;    //实际存储地址
    private Date createTime;  //创建时间
    private Date updateTime;  //更新时间
    private int num; //排序

    private int childFileNum;//子文件数量

    public FileInfo(){
        super();
        this.status = "1";
    }

    /**
     * 判断当前文件是否为文件类型
     * @return
     */
    public boolean isFile(){
        return "1".equals(this.getIsFile());
    }

    /**
     * 判断当前文件是否为文件夹类型
     * @return
     */
    public boolean isFolder(){
        return "0".equals(this.getIsFile());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public EFileType getType() {
        return type;
    }

    public void setType(EFileType type) {
        this.type = type;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getFolderId() {
        return folderId;
    }

    public void setFolderId(String folderId) {
        this.folderId = folderId;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getIsFile() {
        return isFile;
    }

    public void setIsFile(String isFile) {
        this.isFile = isFile;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public int getChildFileNum() {
        return childFileNum;
    }

    public void setChildFileNum(int childFileNum) {
        this.childFileNum = childFileNum;
    }
}
