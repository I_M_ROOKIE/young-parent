package com.yy.young.fs.util;

/**
 * 文件系统常量
 * Created by rookie on 2017/9/5.
 */
public class FsConstants {

    //根文件夹编号
    public static final String ROOT_FOLDER_ID = "ROOT";

    /*
     * sql映射
     */
    public interface MAPPER{
        String FS_FILEINFO = "com.yy.young.fs.mapper.fileInfo";//文件信息
    }


}
