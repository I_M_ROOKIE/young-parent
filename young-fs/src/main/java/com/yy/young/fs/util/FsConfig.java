package com.yy.young.fs.util;

/**
 * 文件系统配置类
 * 在xml里面配置
 * Created by rookie on 2017/9/5.
 */
public class FsConfig {

    public static String ROOT_PATH_LINUX;//linux下的根路径

    public static String ROOT_PATH_WINDOWS;//windows下的根路径

    public static String getRootPathLinux() {
        return ROOT_PATH_LINUX;
    }

    public static void setRootPathAtLinux(String rootPathLinux) {
        ROOT_PATH_LINUX = rootPathLinux;
    }

    public static String getRootPathWindows() {
        return ROOT_PATH_WINDOWS;
    }

    public static void setRootPathAtWindows(String rootPathWindows) {
        ROOT_PATH_WINDOWS = rootPathWindows;
    }
}
