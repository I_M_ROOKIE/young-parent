package com.yy.young.fs.web;

import com.yy.young.base.exception.ParameterException;
import com.yy.young.base.exception.YoungBaseException;
import com.yy.young.common.util.CommonUtil;
import com.yy.young.common.util.FileDownloadUtil;
import com.yy.young.common.util.Result;
import com.yy.young.common.util.StringUtils;
import com.yy.young.fs.model.FileInfo;
import com.yy.young.fs.service.IFileInfoService;
import com.yy.young.fs.service.IFileService;
import com.yy.young.fs.util.FsConstants;
import com.yy.young.interfaces.log.annotation.Log;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.DefaultMultipartHttpServletRequest;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * cms文件请求处理
 * Created by Administrator on 2017/5/8.
 */
@Controller
@RequestMapping("/fs/file")
public class FsFileController {

    @Resource(name="fsFileService")
    IFileService fileService;

    @Resource(name="fsFileInfoService")
    IFileInfoService fileInfoService;

    private static final Logger logger = LoggerFactory.getLogger(FsFileController.class);


    /**
     * 文件上传
     * @param file
     * @param request
     * @return
     * @throws Exception
     */
    @Log("上传文件")
    @RequestMapping("/upload")
    @ResponseBody
    public Object upload(MultipartFile file, String folderId, HttpServletRequest request) throws Exception{
        Result result = new Result();
        if (file != null && !file.isEmpty()){
            FileInfo fileInfo = null;
            if (StringUtils.isNotBlank(folderId)){
                fileInfo = fileService.saveFile(file, folderId);//保存文件
            }else{
                fileInfo = fileService.saveFile(file);//保存文件
            }
            List<FileInfo> list = new ArrayList<FileInfo>();
            list.add(fileInfo);
            result.setData(list);
        }else{
            result.setCode(-1);
            result.setInfo("文件不存在!");
        }
        return result;
    }

    /**
     * 文件下载
     * @param fileId
     * @param request
     * @throws Exception
     */
    @Log("下载文件")
    @RequestMapping("/download")
    public void download(String fileId, HttpServletRequest request, HttpServletResponse response) throws Exception{
        logger.info("[文件下载] 待下载文件编号为:{}", fileId);
        FileInfo fileInfo = fileInfoService.getFileInfo(fileId);
        if(fileInfo != null){
            String fileName = fileInfo.getName();
            response.setContentType("application/x-msdownload;");//文件下载类型
            FileDownloadUtil.setFileNameOfCN(request, response, fileName);//设置中文文件名
            /*response.setHeader("Content-disposition", "attachment; filename="
                    + new String( fileName.getBytes("utf-8"), "ISO8859-1" ));//设置文件名,转码是为了解决中文乱码问题*/
            File file = new File(fileInfo.getAddr());
            response.setHeader("Content-Length", String.valueOf(file.length()));//设置内容长度
            OutputStream os = response.getOutputStream();
            IOUtils.copy(new FileInputStream(file), os);//拷贝到输出流
        }else{
            response.getWriter().write("文件信息无效!");
        }

    }

    /**
     * 删除文件
     * @param fileId 文件编号
     * @param request
     * @return
     * @throws Exception
     */
    @Log("删除文件")
    @RequestMapping("/dropFile")
    @ResponseBody
    public Object dropFile(String fileId, HttpServletRequest request) throws Exception{
        logger.info("[文件删除] 待删除文件编号为:{}", fileId);
        Result result = new Result();
        if (!fileService.dropFile(fileId)){
            result.setCode(-1);
            result.setInfo("文件删除失败!");
        }
        return result;
    }

    /**
     * 删除文件夹
     * @param folderId
     * @param request
     * @return
     * @throws Exception
     */
    @Log("删除文件夹")
    @RequestMapping("/dropFolder")
    @ResponseBody
    public Object dropFolder(String folderId, HttpServletRequest request) throws Exception{
        logger.info("[文件夹删除] 待删除文件夹编号为:{}", folderId);
        Result result = new Result();
        if (!fileService.dropFolder(folderId)){
            result.setCode(-1);
            result.setInfo("文件夹删除失败!");
        }
        return result;
    }

    /**
     * 创建文件夹
     * @param folder
     * @param request
     * @return
     * @throws Exception
     */
    @Log("创建文件夹")
    @RequestMapping("/createFolder")
    @ResponseBody
    public Object createFolder(FileInfo folder, HttpServletRequest request) throws Exception{
        Result result = new Result();
        if (folder == null || folder.getName() == null || folder.getFolderId() == null){
            result.setCode(-1);
            result.setInfo("新建文件夹失败:所属文件夹或文件夹名称为空!");
        }else{
            List<FileInfo> list = new ArrayList<FileInfo>();
            list.add(fileService.createFolder(folder));
            result.setData(list);
        }
        return result;
    }

    /**
     * 获取图片流
     * @param id
     * @param request
     * @param response
     * @throws Exception
     */
    @Log("获取图片流")
    @RequestMapping("/image")
    public void getImage(String id, HttpServletRequest request, HttpServletResponse response) throws Exception{
        if (StringUtils.isNotBlank(id)){
            FileInfo image = fileInfoService.getFileInfo(id);
            if (image != null){
                //图片文件
                File file = new File(image.getAddr());
                if (file.exists()){
                    InputStream is = new FileInputStream(file);//图片流
                    response.setContentType("image/*");//设置响应类型
                    //写入到响应
                    OutputStream os = response.getOutputStream();
                    IOUtils.copy(is, os);//拷贝到输出流
                }
            }
        }
    }

    /**
     * 获取图片流(restful形式)
     * @param id
     * @param request
     * @param response
     * @throws Exception
     */
    @Log("获取图片流")
    @RequestMapping("/image/{id}")
    public void getImage2(@PathVariable("id")String id, HttpServletRequest request, HttpServletResponse response) throws Exception{
        if (StringUtils.isNotBlank(id)){
            FileInfo image = fileInfoService.getFileInfo(id);
            if (image != null){
                //图片文件
                File file = new File(image.getAddr());
                if (file.exists()){
                    InputStream is = new FileInputStream(file);//图片流
                    response.setContentType("image/*");//设置响应类型
                    //写入到响应
                    OutputStream os = response.getOutputStream();
                    IOUtils.copy(is, os);//拷贝到输出流
                }
            }
        }
    }

    /**
     * 文件传输(接收端)
     * @param folderId 指定存储文件夹编号,默认为根目录
     * @param request
     * @return
     * @throws Exception
     */
    @Log("接收传输文件")
    @RequestMapping("/transferFile")
    @ResponseBody
    public Object transferFile(String folderId, DefaultMultipartHttpServletRequest request) throws Exception{
        Result result = new Result();
        List<MultipartFile> list = CommonUtil.getMultipartFileFromRequest(request);
        //若没有指定存储文件夹,使用根目录
        folderId = StringUtils.isNotBlank(folderId) ? folderId : FsConstants.ROOT_FOLDER_ID;
        if (list != null && list.size() > 0){
            //批量保存文件
            try {
                List<FileInfo> fsList = fileService.saveFiles(list, folderId);
                result.setData(fsList);
            }catch (YoungBaseException pe){//对于业务异常,做相关处理,给调用端提供详细错误信息
                pe.printStackTrace();
                result.setCode(-1);
                result.setInfo(pe.getErrorInfo());
            }
        }else{
            result.setCode(-1);
            result.setInfo("文件不存在!");
        }
        return result;
    }
}
