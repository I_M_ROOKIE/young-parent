package com.yy.young.fs.model;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 文件类型枚举类
 * Created by rookie on 2017/9/5.
 */
public enum EFileType {
    @Remark("文件夹")
    FOLDER,
    @Remark("图片")
    IMG,
    @Remark("视频")
    VIDEO,
    @Remark("音频")
    VOICE,
    @Remark("文本")
    TXT,
    @Remark("文档")
    DOC,
    @Remark("EXCEL表格")
    EXCEL,
    @Remark("PPT")
    PPT,
    @Remark("压缩文件")
    ZIP,
    @Remark("其他")
    OTHER;

    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.FIELD)
    public @interface Remark{
        String value() default "";
    }

    /**
     * 根据文件后缀名获取文件类型
     * @param suffix
     * @return
     */
    public static EFileType getTypeBySuffix(String suffix){
        if (".jpg".equals(suffix) || ".jpeg".equals(suffix) || ".png".equals(suffix) || ".gif".equals(suffix)){
            return IMG;
        }else if (".doc".equals(suffix) || ".docx".equals(suffix)){
            return DOC;
        }else if (".txt".equals(suffix)){
            return TXT;
        }else if (".xls".equals(suffix) || ".xlsx".equals(suffix)){
            return EXCEL;
        }else if (".ppt".equals(suffix) || ".pptx".equals(suffix)){
            return PPT;
        }else if (".mp4".equals(suffix) || ".avi".equals(suffix) || ".wmv".equals(suffix) || ".navi".equals(suffix) || ".3gp".equals(suffix) || ".flv".equals(suffix)){
            return VIDEO;
        }else if (".mp3".equals(suffix) || ".pptx".equals(suffix)){
            return VOICE;
        }else if (".zip".equals(suffix) || ".rar".equals(suffix)){
            return ZIP;
        }else{
            return OTHER;
        }
    }

}

