package com.yy.young.fs.service.impl;

import com.yy.young.common.util.CommonUtil;
import com.yy.young.dal.service.IDataAccessService;
import com.yy.young.dal.util.Page;
import com.yy.young.fs.model.EFileType;
import com.yy.young.fs.model.FileInfo;
import com.yy.young.fs.model.FileStatisticsOnType;
import com.yy.young.fs.service.IFileInfoService;
import com.yy.young.fs.util.FsConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.lang.reflect.Field;
import java.util.*;

/**
 * 文件信息服务
 * Created by rookie on 2017/9/5.
 */
@Service("fsFileInfoService")
public class FileInfoServiceImpl implements IFileInfoService {

    @Resource(name = "dataAccessService")
    IDataAccessService dataAccessService;//数据层服务

    private static final Logger logger = LoggerFactory.getLogger(FileInfoServiceImpl.class);

    @Override
    public List<FileInfo> getFileInfoList(FileInfo fileInfo) throws Exception {
        return dataAccessService.getList(FsConstants.MAPPER.FS_FILEINFO + ".getFileInfo", fileInfo);
    }

    //查询子文件夹信息
    @Override
    public List<FileInfo> getSubFolderList(String folderId) throws Exception {
        return dataAccessService.getList(FsConstants.MAPPER.FS_FILEINFO + ".getFolderListByFolderId", folderId);
    }

    @Override
    public FileInfo getFileInfo(String id) throws Exception {
        return (FileInfo)dataAccessService.getObject(FsConstants.MAPPER.FS_FILEINFO + ".getFileInfoById", id);
    }

    @Override
    public List<FileInfo> getFolderTree(FileInfo fileInfo) throws Exception {
        if(fileInfo == null){
            fileInfo = new FileInfo();
        }
        fileInfo.setIsFile("0");//0表示文件夹
        return this.getFileInfoList(fileInfo);
    }

    @Override
    public List<FileInfo> getFileInfoPage(FileInfo fileInfo, Page page) throws Exception {
        return dataAccessService.getList(FsConstants.MAPPER.FS_FILEINFO + ".getFileInfo", fileInfo, page);
    }

    @Override
    public int updateFileInfo(FileInfo fileInfo) throws Exception {
        if (fileInfo.getUpdateTime() == null){//设置更新时间
            fileInfo.setUpdateTime(new Date());
        }
        return dataAccessService.update(FsConstants.MAPPER.FS_FILEINFO + ".updateFileInfo", fileInfo);
    }

    @Override
    public int deleteFileInfo(String[] idsArr) throws Exception {
        return dataAccessService.delete(FsConstants.MAPPER.FS_FILEINFO + ".deleteFileInfo", idsArr);
    }

    @Override
    public int deleteFileInfoByFolder(String folderId) throws Exception {
        int count = 0;
        //查询文件夹下的所有文件夹
        List<FileInfo> childList = this.getSubFolderList(folderId);//dataAccessService.getList(FsConstants.MAPPER.FS_FILEINFO + ".getFolderListByFolderId", folderId);
        if (childList != null && childList.size() > 0){
            for (FileInfo file : childList){
                //计算该子文件夹下的文件数
                int num = this.countFileNumByFolderId(file.getId());//(Integer)dataAccessService.getObject(FsConstants.MAPPER.FS_FILEINFO + ".countFileNumByFolderId", file.getId());
                if (num > 0){
                    count += deleteFileInfoByFolder(file.getId());//清空它,递归
                }
            }
        }
        //子文件夹全部清空后,清空文件夹下的直属文件信息
        count += dataAccessService.delete(FsConstants.MAPPER.FS_FILEINFO + ".deleteFileInfoByFolder", folderId);
        logger.info("[清空文件夹信息] 文件夹[{}]下的信息已清空,总计删除文件信息(含文件夹){}个!", folderId, count);
        return count;
    }

    @Override
    public int insertFileInfo(FileInfo fileInfo) throws Exception {
        if (fileInfo.getCreateTime() == null){//设置插入时间
            fileInfo.setCreateTime(new Date());
            fileInfo.setUpdateTime(new Date());
        }
        if (fileInfo.getId() == null){//设置编号
            fileInfo.setId(CommonUtil.getUUID());
        }
        return dataAccessService.insert(FsConstants.MAPPER.FS_FILEINFO + ".insertFileInfo", fileInfo);
    }

    @Override
    public List<FileInfo> getFileInfoByFolder(String folderId) throws Exception {
        return dataAccessService.getList(FsConstants.MAPPER.FS_FILEINFO + ".getFileInfoByFolder", folderId);
    }

    @Override
    public List<FileStatisticsOnType> statisFileNum() throws Exception {
        List<Map<String, Object>> list = dataAccessService.getList(FsConstants.MAPPER.FS_FILEINFO + ".countFileNumByType", null);
        //list转map
        Map<String, Integer> map = new HashMap<String, Integer>();
        for (Map<String, Object> m : list){
            map.put(m.get("STYPE")+"", Integer.parseInt(m.get("N")+""));
        }
        //文件类型集合
        List<Map<String, String>> list2 = this.getFileTypeList();
        List<FileStatisticsOnType> resList = new ArrayList<FileStatisticsOnType>();
        for (Map<String, String> m : list2){
            FileStatisticsOnType one = new FileStatisticsOnType();
            one.setType(m.get("key"));
            one.setTypeRemark(m.get("value"));
            one.setNum(map.containsKey(m.get("key")) ? map.get(m.get("key")) : 0);
            resList.add(one);
        }
        Collections.sort(resList, new Comparator<FileStatisticsOnType>() {
            @Override
            public int compare(FileStatisticsOnType o1, FileStatisticsOnType o2) {
                if (o1.getNum() > o2.getNum()){
                    return 1;
                }
                return 0;
            }
        });
        return resList;
    }

    @Override
    public int countFileNumByFolderId(String folderId) throws Exception {
        return (Integer)dataAccessService.getObject(FsConstants.MAPPER.FS_FILEINFO + ".countFileNumByFolderId", folderId);
    }

    @Override
    public List<Map<String, String>> getFileTypeList() {
        List<Map<String, String>> list = new ArrayList<Map<String, String>>();
        Class cls = EFileType.class;
        Field[] fields = cls.getDeclaredFields();
        for(Field field : fields){
            Map<String,String> map = new HashMap<String, String>();
            EFileType.Remark remark = field.getAnnotation(EFileType.Remark.class);
            if(remark != null){
                map.put("key", field.getName());
                map.put("value", remark.value());
                list.add(map);
            }
        }
        return list;
    }

    @Override
    public FileInfo getFolderInfo(String id) throws Exception {
        return (FileInfo)dataAccessService.getObject(FsConstants.MAPPER.FS_FILEINFO + ".getFolderInfoById", id);
    }


}
