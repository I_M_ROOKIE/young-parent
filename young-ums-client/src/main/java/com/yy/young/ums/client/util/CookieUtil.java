package com.yy.young.ums.client.util;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by rookie on 2017/8/1.
 */
public class CookieUtil {

    /**
     * 获取所有cookie
     * @param request
     * @return
     */
    public static Cookie[] getCookies(HttpServletRequest request){
        if (request != null){
            return request.getCookies();
        }
        return null;
    }

    /**
     * 获取指定key的cookie
     * @param request
     * @param key
     * @return
     */
    public static Cookie getCookie(HttpServletRequest request, String key){
        Cookie[] cookies = getCookies(request);
        if (cookies != null && key != null){
           for(Cookie c : cookies){
               if (key.equals(c.getName())){
                   return c;
               }
           }
        }
        return null;
    }
}
