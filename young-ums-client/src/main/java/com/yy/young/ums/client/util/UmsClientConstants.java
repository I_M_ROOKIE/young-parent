package com.yy.young.ums.client.util;

import com.yy.young.base.util.GlobalConstants;
import com.yy.young.common.config.DomainHandler;
import com.yy.young.common.util.StringUtils;

/**
 * 统一用户客户端常量
 * Created by Administrator on 2017/5/8.
 */
public class UmsClientConstants {

    public static final String UMS_SERVER_CONTEXT = DomainHandler.get(GlobalConstants.URL_CONFIG_KEY.URL_UMS);//UMS服务端域名

    public interface SSO_CLIENT{
        String TOKEN_KEY = "_token";//令牌key
        String SSO_DEFAULT_REDIRECT_URL = "/young/page/login.jsp";//默认重定向页面
        String SSO_VERIFY_URL = UMS_SERVER_CONTEXT + "verifySSO.action";//单点验证地址
    }
}
