import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

/**
 * redis客户端测试（jedis）
 * Created by Administrator on 2018/6/19.
 */
public class RedisTest {

    public static void main(String[] args) {
        //链接redis
        //Jedis jedis = new Jedis("192.168.1.72", 6379);//直接创建redis实例
        JedisPool jedisPool = new JedisPool("192.168.1.72", 6379);//jedis连接池
        Jedis jedis = jedisPool.getResource();
        System.out.println("查看服务是否运行："+jedis.ping());
        //设置字符串类型


        System.out.println("使用getSet设置并返回旧值："+jedis.getSet("sex", "男"));
        //存储list
        jedis.lpush("students", "张三");
        jedis.lpush("students", "李四");
        jedis.lpush("students", "王五");
        System.out.println("存储list测试："+jedis.lrange("students", 0, 2));
        //存储hash
        jedis.hset("user", "name", "张三");
        jedis.hset("user", "age", "14");
        System.out.println("存储hash测试："+jedis.hgetAll("user"));
        //存储set
        jedis.sadd("nameSet", "tom");
        jedis.sadd("nameSet", "jek");
        jedis.sadd("nameSet", "rookie");
        jedis.sadd("nameSet", "jek");
        System.out.println("存储set测试："+jedis.smembers("nameSet"));
        //存储zset
        jedis.zadd("rank", 8, "赵六");
        jedis.zadd("rank", 10, "王五");
        jedis.zadd("rank", 11, "张三");
        jedis.zadd("rank", 12, "李四");
        System.out.println("存储zset测试："+jedis.zrange("rank", 0, 2));//索引0-2的值

        jedis.close();//资源释放，returnResource方法被废弃，在Jedis类中有一个protected属性pool，在使用jedisPool获取jedis时会对pool进行set，调用close其实和之前的jedisPool.returnResource(jedis)差不多
        //jedisPool.returnResource(jedis);


    }
}
