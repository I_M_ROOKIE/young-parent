<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <%--常量--%>
    <%@ include file="/common/constHead.jsp"%>
    <%--jQuery--%>
    <%@ include file="/common/jqueryHead.jsp"%>
    <%--jo--%>
    <%@ include file="/common/joHead.jsp"%>
    <%--bootstrap和字体--%>
    <%@ include file="/common/bootstrapHead.jsp"%>
    <%--layer--%>
    <%@ include file="/common/layerHead.jsp"%>
    <%--common公共方法--%>
    <%@ include file="/common/commonHead.jsp"%>
    <%--H+首页相关css--%>
    <link href="<%=URL_STATIC%>static/plugin/index_hplus/css/animate.min.css" rel="stylesheet">
    <link href="<%=URL_STATIC%>static/plugin/index_hplus/css/style.min862f.css" rel="stylesheet">
    <title>首页</title>
    <script type="text/javascript">
        //注销登录
        function logout(){
            jo.confirm("您确定要注销登录吗?", {title:"退出"}, function(){
                window.location.href = URL_UMS + "logout.action";
            });
        }
    </script>
    <style>
        /*菜单字体*/
        .nav>li>a{
            color: rgba(163,175,183,.9);
            padding: 11px 20px 11px 20px;
            font-size: 14px;
            font-family: "Helvetica Neue",Helvetica,Tahoma,Arial,"Microsoft Yahei","Hiragino Sans GB","WenQuanYi Micro Hei",sans-serif;
            font-weight: normal;
        }
        /*二级菜单*/
        .nav-second-level li a{
            padding-left: 42px;
        }
        /*三级菜单*/
        .nav-third-level li a{
            padding-left: 62px;
        }
        /*四级菜单*/
        .nav-fourth-level li{
            border-bottom: none!important
        }
        .nav-fourth-level li a {
            padding-left: 72px;
        }
        .nav.nav-third-level>li.active{
            border: none;/*左边高亮防止重复*/
        }
        /*五级菜单*/
        .nav-fifth-level li{
            border-bottom: none!important
        }
        .nav-fifth-level li a {
            padding-left: 82px;/*缩进*/
        }
        .nav.nav-fourth-level>li.active{
            border: none;/*左边高亮防止重复*/
        }
        /*菜单栏活动状态*/
        .nav>li.active{
            background-color: rgb(38, 50, 56);
        }
        /*右侧活动页 页签样式*/
        .page-tabs a.active{
            color: rgba(163,175,183,.9);
            background-color: rgb(38, 50, 56);
        }

        /*顶部*/
        .head-bar{ /*顶部div*/
            width: 100%;
            /*height: 100%;*/
            display: block;
            height: 60px; /*顶部为60px*/
            line-height: 60px;
            color: white;
            overflow: hidden;
        }
        .bg-head{ /*顶部背景色*/
            background: #4E97D9;
        }
        .head-bar::after{
            content: '';
            display: block;
            clear: both;
        }
        .logo-bar{ /*网站logo和标题区域*/
            width: auto;
            height: 100%;
            padding-left: 20px;
            float: left;
        }
        .head-tools-bar{
            float: right;
        }
        .tools-ul{
            list-style: none;
            margin: 0px;
            padding: 0px;
        }
        .tools-ul li{
            padding: 0px;
            margin: 0px;
            float: left;
            width: 44px;
            height: 100%;
            text-align: center;
            cursor: pointer;
        }
        .tools-ul li:hover{
            background-color: #4688C3;
        }


    </style>
</head>
<body class="fixed-sidebar full-height-layout gray-bg" style="overflow:hidden">
<div id="wrapper">
    <!--左侧导航开始-->
    <nav class="navbar-default navbar-static-side" role="navigation" style="background-color: rgb(38, 50, 56);">
        <div class="nav-close"><i class="fa fa-times-circle"></i>
        </div>
        <div class="sidebar-collapse">
            <ul class="nav" id="side-menu">
                <li style="height: 42px;line-height: 42px;padding-left: 22px;color: #76838f;font-size: 12px;">菜单栏</li>
            </ul>
            <script type="text/javascript">
                var URL_MENU = "{URL_PORTAL}portal/getMenu.action";//菜单地址
                //根据菜单层级获取类样式
                function getClassByLevel(level){
                    if(level == 1){
                        console.warn("[菜单初始化] 菜单层级存在异常(通常不存在1级子菜单): "+level);
                        return "nav-second-level";
                    }else if(level == 2){
                        return "nav-second-level";
                    }else if(level == 3){
                        return "nav-third-level";
                    }else if(level == 4){
                        return "nav-fourth-level";
                    }else if(level == 5){
                        return "nav-fifth-level";
                    }
                    console.warn("[菜单初始化] 菜单层级存在异常(通常在2~5): "+level);
                    //默认返回五级菜单样式
                    return "nav-fifth-level";
                }
                //将平台带函数的链接转为纯url
                function parseHref(href){
                    if(href){
                        href = href.trim();//去除两边空格
                        if(href.indexOf("openPageOnMain") == 0){//右侧打开方法
                            href = href.replace("openPageOnMain('", "");
                            href = href.replace("')", "");
                        }
                        if(href.indexOf("newWindow") == 0){//新窗口方法
                            href = href.replace("newWindow('", "");
                            href = href.replace("')", "");
                        }
                        return href;
                    }else{
                        return "";
                    }
                }
                //生成菜单元html,参数1:菜单对象;参数2:菜单层级
                function getNavUnitHtml(menu, level){
                    var _html = '';
                    if(menu){
                        _html += '<li>';
                        if(menu.href){//链接有效,则使用J_menuItem,可点击状态
                            if(menu.href.indexOf("newWindow") == 0){//新窗口打开
                                _html += '<a target="_blank" href="'+parseHref(menu.href)+'">';
                            }else{
                                _html += '<a class="J_menuItem" href="'+parseHref(menu.href)+'">';
                            }
                        }else{//链接无效
                            _html += '<a href="javascript:;">';
                        }
                        _html += '<i class="fa '+menu.icon+' fa-fw"></i> ' +
                                '<span class="nav-label">'+menu.name+'</span>';
                        if(menu.children && menu.children.length > 0){//存在子菜单
                            _html += '<span class="fa arrow"></span>' +
                                    '</a>';
                            _html += '<ul class="nav '+getClassByLevel(level+1)+'">';
                            for(var i=0;i<menu.children.length;i++){
                                _html += getNavUnitHtml(menu.children[i], level+1);
                            }
                            _html += '</ul>';
                        }else{//没有子菜单
                            _html += '</a>';
                        }
                        _html += '</li>';
                    }
                    return _html;
                }
                $(function(){
                    //加载菜单
                    jo.postAjax(URL_MENU,{},function(json){
                        if(json && json.code == 0){
                            var menus = json.data;
                            if(menus && menus.length > 0){
                                var _html = '';
                                for(var i=0;i<menus.length;i++){
                                    var menu = menus[i];
                                    _html += getNavUnitHtml(menu, 1);
                                }
                                $("#side-menu").append(_html);//插入菜单
                                //当有且只有1个根节点时,自动展开
                                /*if(menus.length == 1){
                                 var one = $(".young-nav-container>.young-nav-unit")[0];
                                 var child = $(one).find(">.young-nav-child");
                                 childNavOpen(child);//展开节点
                                 }*/
                            }
                        }else{
                            jo.alert(jo.getDefVal(json.info, "获取菜单异常!"));
                        }
                    });
                });
            </script>
        </div>
    </nav>
    <!--左侧导航结束-->
    <!--右侧部分开始-->
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0;height: 60px;">
                <div class="head-bar bg-head" style="background-image: url('<%=URL_STATIC%>static/images/header.png');background-size: 100% 100%;overflow: hidden;">
                    <%--logo区域--%>
                    <div class="logo-bar">
                        <i class="fa fa-2x fa-diamond" aria-hidden="true"></i>&nbsp;
                        <%--<img src="<%=URL_STATIC%>static/images/bg.png" style="width: 45px;height: 45px;margin: 0px;padding: 0px;"/>--%>
                        <span style="font-size: 18px;">Young快速开发平台</span>
                    </div>
                    <%--/logo区域--%>
                    <%--顶部工具栏--%>
                    <div class="head-tools-bar">
                        <ul class="tools-ul" style="padding-right: 12px;">
                            <li style="font-size: 20px;" data-toggle="popover" title="" data-container="body" data-trigger="hover"
                                data-toggle="popover" data-placement="bottom" data-content="个人信息" onclick="openPageOnTabs('{URL_UMS}page/ums/personInfo.jsp','个人信息')">
                                <%--<a class="J_menuItem" href="{URL_UMS}page/ums/personInfo.jsp">--%><i class="fa fa-1x fa-user" aria-hidden="true"></i>
                                <%--</a>--%>
                            </li>
                            <li style="font-size: 20px;" data-toggle="popover" title="" data-container="body" data-trigger="hover"
                                data-toggle="popover" data-placement="bottom" data-content="注销" onclick="logout()">
                                <i class="fa fa-1x fa-power-off" aria-hidden="true"></i>
                            </li>
                        </ul>
                    </div>
                    <%--/顶部工具栏--%>
                </div>
            </nav>
        </div>
        <div class="row content-tabs">
            <button class="roll-nav roll-left J_tabLeft"><i class="fa fa-backward"></i>
            </button>
            <nav class="page-tabs J_menuTabs">
                <%--tab页标题,与content-main里面的iframe要一一对应(data-id)--%>
                <div class="page-tabs-content">
                    <a href="javascript:;" class="active J_menuTab" data-id="page/portal/home.jsp">首页</a>
                </div>
            </nav>
            <button class="roll-nav roll-right J_tabRight"><i class="fa fa-forward"></i>
            </button>
            <div class="btn-group roll-nav roll-right">
                <button class="dropdown J_tabClose" data-toggle="dropdown">关闭操作<span class="caret"></span>
                </button>
                <ul role="menu" class="dropdown-menu dropdown-menu-right">
                    <li class="J_tabShowActive"><a>定位当前选项卡</a>
                    </li>
                    <li class="divider"></li>
                    <li class="J_tabCloseAll"><a>关闭全部选项卡</a>
                    </li>
                    <li class="J_tabCloseOther"><a>关闭其他选项卡</a>
                    </li>
                </ul>
            </div>
            <a href="javascript:logout();" class="roll-nav roll-right J_tabExit"><i class="fa fa fa-sign-out"></i> 退出</a>
        </div>
        <%--iframe容器--%>
        <div class="row J_mainContent" id="content-main" style="">
            <iframe class="J_iframe" name="iframe0" width="100%" height="100%" src="page/portal/home.jsp" frameborder="0" data-id="page/portal/home.jsp" seamless></iframe>
        </div>
        <div class="footer">
            <div class="pull-right">&copy; 2018 <a href="http://www.imrookie.cn" target="_blank">imrookie's blog</a>
            </div>
        </div>
    </div>
    <!--右侧部分结束-->
    </div>
    <!--右侧边栏结束-->
</div>


<script src="<%=URL_STATIC%>static/plugin/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
<%--H+首页相关js--%>
<script src="<%=URL_STATIC%>static/plugin/index_hplus/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<%=URL_STATIC%>static/plugin/index_hplus/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<%=URL_STATIC%>static/plugin/index_hplus/js/hplus.js"></script>
<%--tab选项卡形式展示网页,要在所有相关的菜单(.J_menuItem)生产后加载--%>
<script type="text/javascript" src="<%=URL_STATIC%>static/plugin/index_hplus/js/contabs.js"></script>
<script src="<%=URL_STATIC%>static/plugin/index_hplus/js/plugins/pace/pace.min.js"></script>
<script type="text/javascript">
    $(function(){
        //固定顶部
        $(".navbar-static-top").removeClass("navbar-static-top").addClass("navbar-fixed-top");
        $("body").addClass("fixed-nav");
    });
    //tab方式打开导航
    function openPageOnTabs(sUrl, sName, sIndex) {
        // 获取标识数据
        var dataUrl = sUrl,
                dataIndex = sIndex || new Date().getTime(),
                menuName = sName,
                flag = true;
        if (dataUrl == undefined || $.trim(dataUrl).length == 0)return false;

        // 选项卡菜单已存在,则将其显示
        $('.J_menuTab').each(function () {
            if ($(this).data('id') == dataUrl) {
                if (!$(this).hasClass('active')) {
                    $(this).addClass('active').siblings('.J_menuTab').removeClass('active');
                    scrollToTab(this);
                    // 显示tab对应的内容区
                    $('.J_mainContent .J_iframe').each(function () {
                        if ($(this).data('id') == dataUrl) {
                            $(this).show().siblings('.J_iframe').hide();
                            return false;
                        }
                    });
                }
                flag = false;
                return false;
            }
        });

        // 选项卡菜单不存在,则创建iframe
        if (flag) {
            var str = '<a href="javascript:;" class="active J_menuTab" data-id="' + dataUrl + '">' + menuName + ' <i class="fa fa-times-circle"></i></a>';
            $('.J_menuTab').removeClass('active');
            var height = $(window).height() - 110;
            //为了兼容平台,在这里对url做解析处理,因为url用来作为data-id,所以不能提前进行解析,在生产iframe时使用解析后的url
            var dataUrl2 = dataUrl;
            if(typeof (replaceUrlConstants) == "function"){
                dataUrl2 = replaceUrlConstants(dataUrl);
            }
            // 添加选项卡对应的iframe
            height = '100%';
            var str1 = '<iframe class="J_iframe" name="iframe' + dataIndex + '" width="100%" height="'+height+'" src="' + dataUrl2 + '" frameborder="0" data-id="' + dataUrl + '" seamless></iframe>';
            $('.J_mainContent').find('iframe.J_iframe').hide().parents('.J_mainContent').append(str1);

            // 添加选项卡
            $('.J_menuTabs .page-tabs-content').append(str);
            scrollToTab($('.J_menuTab.active'));
        }
        return false;
    }
    //滚动到当前页
    function scrollToTab(element) {
        refreshMainDom();
        var marginLeftVal = calSumWidth($(element).prevAll()), marginRightVal = calSumWidth($(element).nextAll());
        // 可视区域非tab宽度
        var tabOuterWidth = calSumWidth($(".content-tabs").children().not(".J_menuTabs"));
        //可视区域tab宽度
        var visibleWidth = $(".content-tabs").outerWidth(true) - tabOuterWidth;
        //实际滚动宽度
        var scrollVal = 0;
        if ($(".page-tabs-content").outerWidth() < visibleWidth) {
            scrollVal = 0;
        } else if (marginRightVal <= (visibleWidth - $(element).outerWidth(true) - $(element).next().outerWidth(true))) {
            if ((visibleWidth - $(element).next().outerWidth(true)) > marginRightVal) {
                scrollVal = marginLeftVal;
                var tabElement = element;
                while ((scrollVal - $(tabElement).outerWidth()) > ($(".page-tabs-content").outerWidth() - visibleWidth)) {
                    scrollVal -= $(tabElement).prev().outerWidth();
                    tabElement = $(tabElement).prev();
                }
            }
        } else if (marginLeftVal > (visibleWidth - $(element).outerWidth(true) - $(element).prev().outerWidth(true))) {
            scrollVal = marginLeftVal - $(element).prev().outerWidth(true);
        }
        $('.page-tabs-content').animate({
            marginLeft: 0 - scrollVal + 'px'
        }, "fast");
    }
    //刷新主dom,即更新当前活动页为main,兼容单iframe首页
    function refreshMainDom(){
        //当前活动的窗口id
        var activeId = $('.J_menuTab.active').data('id');
        $('.J_mainContent .J_iframe').each(function () {
            if ($(this).data('id') == activeId) {//匹配到该iframe
                window.main = this.contentWindow;//将main指向当前活动窗口dom
                return false;
            }
        });
    }
    //计算元素集合的总宽度
    function calSumWidth(elements) {
        var width = 0;
        $(elements).each(function () {
            width += $(this).outerWidth(true);
        });
        return width;
    }
</script>
</body>
</html>