<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
    <%--常量--%>
    <%@ include file="/common/constHead.jsp"%>
    <%--jQuery--%>
    <%@ include file="/common/jqueryHead.jsp"%>
    <%--jo--%>
    <%@ include file="/common/joHead.jsp"%>
    <%--bootstrap和字体--%>
    <%@ include file="/common/bootstrapHead.jsp"%>
    <%--layer--%>
    <%@ include file="/common/layerHead.jsp"%>
    <%--公共--%>
    <%@ include file="/common/commonHead.jsp"%>
    <%--日期选择--%>
    <%@ include file="/common/dateHead.jsp"%>
    <title>代码生成主体信息表单</title>
    <script type="text/javascript">
        $(function(){
            var jParams = {
                "PKName" : "id",//主键属性名
                "saveAfter" : "toEdit",
                "addUrl" : "cms/genMain/insert.action",//新增
                "deleteUrl" : "cms/genMain/delete.action",//删除
                "updateUrl" : "cms/genMain/update.action",//修改
                "formDataUrl" : "cms/genMain/get.action"	//查询
            };
            joForm.initFormPage(jParams);//初始化

            //表格悬停处理
            $("#colList").on('mouseover', 'td', function(event){//td-hover
                var idx = $(this).index();
                $("#colList tr").find("td").removeClass("td-hover");
                $("#colList tr").find("td:eq("+idx+")").addClass("td-hover");
            });

        });
        //新增和修改前对数据处理
        joForm.dealDataAtSaveBefore = joForm.dealDataAtUpdateBefore = function(obj){
            var trs = $("#colList>tbody").find("tr");
            var cols = [];
            for(var i=0;i<trs.length;i++){
                cols.push(parseInput2Json($(trs[i])));
            }
            obj.cols = jo.obj2JsonStr(cols);
        };
        //解析控件
        function parseInput2Json(obj){
            var res = {};
            var arr = $(obj).find('input');
            for(var i=0;i<arr.length;i++){
                if(arr[i].type == "checkbox"){//勾选框,通过判断是否选中设置值
                    if(arr[i].checked){
                        res[arr[i].name] = '1';
                    }else{
                        res[arr[i].name] = '0';
                    }
                }else{
                    res[arr[i].name] = arr[i].value;
                }
            }
            arr = $(obj).find('textarea');
            for(var i=0;i<arr.length;i++){
                res[arr[i].name] = arr[i].value;
            }
            arr = $(obj).find('select');
            for(var i=0;i<arr.length;i++){
                res[arr[i].name] = arr[i].value;
            }
            arr = $(obj).find('span');
            for(var i=0;i<arr.length;i++){
                res[$(arr[i]).attr("name")] = $(arr[i]).attr("value");
            }
            return res;
        }
        //新增初始化
        joForm.initFormPageOfAdd = function(){
            var m = '<%=path%>';
            if(m){
                $("#moduleName").val(m.substring(1));
            }
        };
        //初始化表单值前处理
        joForm.initFormValueBefore = function(oJson){
        };
        //初始化表单后
        joForm.initFormValueAfter = function(oJson){
            //加载字段配置
            if(joForm.params.PKValue){
                jo.postAjax("cms/genCol/getList", {"genId":joForm.params.PKValue}, function(json){
                    if(json && json.code == "0"){
                        jo.sort(json.data, function(a, b){
                            return a.viewNum < b.viewNum;
                        });
                        Grid("colList").loadData(json.data);
                    }
                });
            }
            $("#tableName").attr("disabled", true);//禁止改变数据表
        };
        //数据表改变事件
        function tableChange(tableName){
            if(joForm.isAdd && tableName){//新增页时才允许切换数据表
                if(tableName.indexOf('tb_') == 0 || tableName.indexOf('TB_') == 0){
                    $("#className").val(wordOne2Up(up2Tuo(tableName.substring(3))));
                }else{
                    $("#className").val(wordOne2Up(up2Tuo(tableName)));
                }
                jo.postAjax("cms/db/getColumsByTableName.action", {"tableName":tableName}, function(json){
                    if(json && json.code == "0"){
                        for(var i=0;i<json.data.length;i++){
                            var item = json.data[i];
                            item.fieldName = item.field;
                            item.fieldRemark = item.remark;
                            item.fieldType = item.type;
                            item.fieldLength = item.length;
                            item.fieldNotNull = item.notNull;
                            item.formNum = (i+1)*10;
                            item.viewNum = (i+1)*10;
                            item.attrName = up2Tuo(item.fieldName);
                            if(item.fieldLength && item.fieldLength > 2000){//字段长度较长的默认不在视图显示,默认使用大文本框
                                item.viewShow = '0';
                                item.controlType = 'textarea';
                            }else{
                                item.viewShow = 1;
                            }
                            item.formShow = 1;
                        }
                        jo.sort(json.data, function(a, b){
                            return a.viewNum < b.viewNum;
                        });
                        Grid("colList").loadData(json.data);
                    }
                });
            }else{
                $("#className").val('');
                Grid("colList").loadData([]);
            }
        }
        //控件类型选项
        var options_controlType = [
            {value:'input', text:'输入框(input)'}
            ,{value:'date', text:'日期选择'}
            ,{value:'select', text:'下拉选择(select)'}
            ,{value:'joSelect', text:'jo下拉选择'}
            ,{value:'textarea', text:'大文本框(textarea)'}
            ,{value:'radio', text:'单选(radio)'}
            ,{value:'checkbox', text:'多选(checkbox)'}
            ,{value:'tree', text:'树选择(单选)'}
            ,{value:'trees', text:'树选择(多选)'}
            ,{value:'list', text:'列表选择(单选)'}
            ,{value:'lists', text:'列表选择(多选)'}
            ,{value:'label', text:'文字'}
        ];
        //控件只读处理选项
        var options_controlReadonly = [
            {value:'none', text:'无'}
            ,{value:'add', text:'新增时只读'}
            ,{value:'edit', text:'编辑时只读'}
            ,{value:'all', text:'总是只读'}
        ];
        //控件样式选项
        var options_controlClass = [
            {value:'1', text:'普通样式'}
        ];
        //表单验证选项
        var options_controlCheck = [
            {value:'', text:''}
            ,{value:'ErrEmpty', text:'非空验证'}
            ,{value:'ErrLength', text:'长度验证'}
            ,{value:'ErrNumber', text:'数字类型验证'}
            ,{value:'ErrMail', text:'邮箱验证'}
            ,{value:'ErrPhone', text:'手机号验证'}
            ,{value:'ErrReg', text:'自定义正则验证'}
        ];
        //检索条件
        var options_asCondition = [
            {value:'1', text:'否'}
            ,{value:'2', text:'=查询'}
            ,{value:'3', text:'like查询'}
        ];
        //格式化控件,将数据转为表单元素
        function formatControls(item, idx){

            item.attrName = val2Control(item.attrName, "attrName", "input");

            item.viewShow = val2Control(item.viewShow, "viewShow", "checkbox");
            item.supportSort = val2Control(jo.getDefVal(item.supportSort, '0'), "supportSort", "checkbox");
            item.viewNum = val2Control(item.viewNum, "viewNum", "input");
            //主键默认作为检索条件
            if(item.fieldName.toUpperCase() == "ID"){
                item.asCondition = val2Control(jo.getDefVal(item.asCondition, '2'), "asCondition", "select", options_asCondition);
            }else{
                item.asCondition = val2Control(item.asCondition, "asCondition", "select", options_asCondition);
            }

            item.formShow = val2Control(item.formShow, "formShow", "checkbox");
            item.controlMust = val2Control(jo.getDefVal(item.controlMust, jo.getDefVal(item.fieldNotNull, '0')), "controlMust", "checkbox", null, "mustChecked");
            item.formNum = val2Control(item.formNum, "formNum", "input");
            if(item.controlType == 'joSelect' || item.controlType == 'tree' || item.controlType == 'trees' || item.controlType == 'list' || item.controlType == 'lists'){
                item.controlParam = val2Control(item.controlParam, "controlParam", "input", '控件参数(URL等)');
            }else{
                item.controlParam = '<input type="text" name="controlParam" placeholder="控件参数(URL等)" value="'+jo.getDefVal(item.controlParam, '')+'" class="form-control input-sm" style="padding-left:4px;padding-right:4px;display:none;">';
            }
            item.controlDefault = val2Control(item.controlDefault, "controlDefault", "input");
            //时间类型则默认使用日期控件
            if(item.fieldType.indexOf("date") > -1 || item.fieldType.indexOf("DATE") > -1
                    || item.fieldType.indexOf("time") > -1 || item.fieldType.indexOf("TIME") > -1){
                item.controlType = val2Control(jo.getDefVal(item.controlType, 'date'), "controlType", "select", options_controlType, 'controlTypeChange');
            }else if(item.fieldType.indexOf("text") > -1 || item.fieldType.indexOf("TEXT") > -1
                    || item.fieldType.indexOf("clob") > -1 || item.fieldType.indexOf("CLOB") > -1 ){
                item.controlType = val2Control(jo.getDefVal(item.controlType, 'textarea'), "controlType", "select", options_controlType, 'controlTypeChange');
            }else{
                item.controlType = val2Control(item.controlType, "controlType", "select", options_controlType, 'controlTypeChange');
            }
            item.controlReadonly = val2Control(jo.getDefVal(item.controlReadonly, ((item.fieldName == 'id' || item.fieldName == 'ID') ? 'edit' : '')), "controlReadonly", "select", options_controlReadonly);
            item.controlClass = val2Control(item.controlClass, "controlClass", "select", options_controlClass);

            //数字类型默认进行数字验证
            if(item.fieldType.indexOf("number") > -1 || item.fieldType.indexOf("NUMBER") > -1
                    || item.fieldType.indexOf("int") > -1 || item.fieldType.indexOf("INT") > -1
                    || item.fieldType.indexOf("float") > -1 || item.fieldType.indexOf("FLOAT") > -1
                    || item.fieldType.indexOf("double") > -1 || item.fieldType.indexOf("DOUBLE") > -1 ){
                if(joForm.isAdd){//新增时才进行验证默认值设置
                    item.controlCheck = jo.getDefVal(item.controlCheck, 'ErrNumber');
                }
            }

            //默认非空字段使用非空验证
            if(joForm.isAdd){//新增时才进行验证默认值设置
                item.controlCheck = jo.getDefVal(item.controlCheck, (item.fieldNotNull == '1' ? 'ErrEmpty' : ''));
            }
            item.controlCheckVal = jo.getDefVal(item.controlCheckVal, getTipByCheckType(item.controlCheck, item.fieldRemark));
            //表单验证有效,则显示验证值
            if(item.controlCheck){
                item.controlCheckVal = val2Control(item.controlCheckVal, "controlCheckVal", "input", '验证值(可以为空)');
            }else{
                item.controlCheckVal = '<input type="text" name="controlCheckVal" placeholder="验证值(可以为空)" value="'+jo.getDefVal(item.controlCheckVal, '')+'" class="form-control input-sm" style="padding-left:4px;padding-right:4px;display:none;">';
            }
            item.controlCheck = val2Control(item.controlCheck, "controlCheck", "select", options_controlCheck, 'controlCheckChange');

            item.fieldRemark = val2Control(item.fieldRemark, "fieldRemark", "input");
            //固定值格式化为span标签,方便数据收集
            item.fieldName = val2Control(item.fieldName, 'fieldName', 'span', ((item.fieldNotNull == '1') ? 'font-red' : '') )
                    + '<input type="hidden" name="id" value="'+item.id+'">' +
                    '<input type="hidden" name="genId" value="'+item.genId+'"><input type="hidden" name="fieldNotNull" value="'+item.fieldNotNull+'">';
            item.fieldType = val2Control(item.fieldType, 'fieldType', 'span');
            item.fieldLength = val2Control(item.fieldLength, 'fieldLength', 'span');

            item.fillExplain = val2Control(item.fillExplain, 'fillExplain', 'textarea');
            //item.fieldNotNull = val2Control(item.fieldNotNull, 'fieldNotNull', 'span');
        }
        //根据表单验证类型返回提示语
        function getTipByCheckType(controlCheck, remark){
            var tip = '';
            if(remark && remark.indexOf(',') > -1){
                remark = remark.split(',')[0];
            }
            if(controlCheck == 'ErrEmpty'){
                tip = jo.getDefVal(remark, '') + '不允许为空';
            }else if(controlCheck == 'ErrLength'){//使用默认提示
                //tip = jo.getDefVal(remark, '') + '长度过长';
            }else if(controlCheck == 'ErrNumber'){
                tip = jo.getDefVal(remark, '') + '需填写数字类型';
            }
            return tip;
        }
        /**
        * 值转表单控件
        * @param val 值
        * @param name 控件name属性
        * @param type 控件类型
        * @param options select控件的备选项,[{value:x,text:y}..]; 控件类型为input/textarea时,表示placeholder提示; 控件类型为span时,表示class样式;
        * @param onchange select/checkbox控件的onchange事件函数名
         */
        function val2Control(val, name, type, options, onchange){
            var _html = '';
            if(type == "input"){
                _html = '<input type="text" name="'+name+'" value="'+jo.getDefVal(val, '')+'" placeholder="'+jo.getDefVal(options, '')+'" class="form-control input-sm" style="padding-left:4px;padding-right:4px;">';
            }else if(type == "checkbox"){
                _html = '<input type="checkbox" name="'+name+'" value="1" '+(val != '0' ? 'checked' : '')+' '+(onchange ? 'onchange="'+onchange+'(this)"' : '')+' style="width:16px;height:16px;">';
            }else if(type == "select"){
                val = jo.getDefVal(val, '');
                _html = '<select name="'+name+'" class="form-control input-sm" value="'+val+'" '+(onchange ? 'onchange="'+onchange+'(this)"' : '')+' style="padding-left:2px;padding-right:2px;">';
                for(var i =0;i<options.length;i++){
                    if(val && options[i].value == val){
                        _html += '<option value="'+options[i].value+'" selected="selected">'+options[i].text+'</option>';
                    }else{
                        _html += '<option value="'+options[i].value+'">'+options[i].text+'</option>';
                    }
                }
                _html += '</select>';
            }else if(type == "span"){
                val = jo.getDefVal(val, '');
                _html = '<span name="'+name+'" value="'+val+'" class="'+jo.getDefVal(options, '')+'">'+val+'</span>';
            }else if(type == "textarea"){
                _html = '<textarea rows="1" name="'+name+'" placeholder="'+jo.getDefVal(options, '')+'" class="form-control input-sm" style="padding-left:4px;padding-right:4px;">'+jo.getDefVal(val, '')+'</textarea>';
            }
            return _html;
        }
        //控件类型选择
        function controlTypeChange(obj){
            if(obj && (obj.value == 'joSelect' || obj.value == 'tree' || obj.value == 'trees' || obj.value == 'list' || obj.value == 'lists')){
                $(obj).next("input[name='controlParam']").show();
            }else{
                $(obj).next("input[name='controlParam']").hide();
            }
        }
        //表单验证选择
        function controlCheckChange(obj){
            if(obj && obj.value){
                $(obj).next("input[name='controlCheckVal']").show();
                $(obj).next("input[name='controlCheckVal']").val(getTipByCheckType(obj.value, $(obj).parent().parent().find("input[name='fieldRemark']").val()));
            }else{
                $(obj).next("input[name='controlCheckVal']").hide();
            }
        }
        //是否必填复选框选择事件
        function mustChecked(obj){
            if(obj.checked){//必填项
                var o = $(obj).parent().parent().find("select[name='controlCheck']");
                if(o && o.val() == ""){//没有选择表单验证,自动改为非空验证
                    o.val("ErrEmpty");
                }
            }else{
                var o = $(obj).parent().parent().find("select[name='controlCheck']");
                if(o && o.val() == "ErrEmpty"){//已设置过非空验证,则清空
                    o.val("");
                }
            }
        }
        //大写格式转驼峰,首字母小写
        function up2Tuo(str){
            if(jo.isValid(str)){
                var arr = str.split("_");
                if(arr.length > 1){
                    var s = arr[0].toLowerCase();
                    for(var i=1;i<arr.length;i++){
                        s += arr[i].substring(0,1).toUpperCase() + arr[i].substring(1).toLowerCase();
                    }
                    return s;
                }else{
                    return str.toLowerCase();
                }
            }else{
                return "";
            }
        }
        //单词首字母大写
        function wordOne2Up(str){
            return str.substring(0,1).toUpperCase() + str.substring(1);
        }
        //单词首字母小写
        function wordOne2Low(str){
            return str.substring(0,1).toLowerCase() + str.substring(1);
        }
    </script>
    <style>
        /*#colList tr>td:nth-child(n+7){
            border-bottom: solid 2px #57C7D4;
        }
        #colList tr>td:nth-child(n+11){
            border-bottom: solid 2px #F96868;
        }*/
        #colList tr>th:nth-child(n+5){
            color: #62A8EA;
        }
        #colList tr>th:nth-child(n+9){
            color: #F96868;
        }
        .td-hover{
            background-color: #F4F4F4;
        }
    </style>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">
            <!--按钮栏-->
            <div class="form-group button-bar">
                <button type="button" isShow="joForm.isAdd" class="btn btn-sm btn-outline btn-default" onclick="joForm.save()">
                    <i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;新增
                </button>
                <button type="button" isShow="!joForm.isAdd" class="btn btn-sm btn-outline btn-default" onclick="joForm.update()">
                    <i class="fa fa-pencil-square" aria-hidden="true"></i>&nbsp;修改
                </button>
                <button type="button" isShow="!joForm.isAdd" class="btn btn-sm btn-outline btn-default" onclick="joForm.del()">
                    <i class="fa fa-trash" aria-hidden="true"></i>&nbsp;删除
                </button>
            </div>
            <!--按钮栏-->
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <form id="pageForm" name="pageForm" action="" method="post">
                <table class="table table-bordered  table-condensed dataTables-example dataTable no-footer">

                    <tr>
                        <td width="15%" class="active must" align="">数 据 表</td>
                        <td class="tnt">
                            <%--<input type="text"   name="tableName" class="form-control input-sm "  />--%>
                            <select ErrEmpty="数据表不允许为空" onchange="tableChange(this.value)" id="tableName" name="tableName" class="joSelect form-control input-sm" data="" dataurl="cms/db/getTableList.action" keyfield="id" valuefield="id" firstitem='{"id":"","id":""}'></select>
                        </td>
                        <td width="15%" class="active must" align="">
                            实体类名<span class="fa fa-info-circle" style="padding: 0px 3px;color: #57C7D4;" onmouseover="window.tipsIdx=jo.tips('建议将业务无关的前缀去掉',this,{tips:[2, '#3595CC'], time:10000});" onmouseout="jo.close(window.tipsIdx);"></span>
                        </td>
                        <td class="tnt">
                            <input type="text" id="className"  name="className" class="form-control input-sm " ErrEmpty="实体类名不允许为空" />
                        </td>
                    </tr>
                    <tr>
                        <td width="15%" class="active must" align="">表(类)注 释</td>
                        <td class="tnt">
                            <input type="text"   name="tableRemark" class="form-control input-sm "  ErrEmpty="表注释不允许为空" />
                        </td>
                        <td width="15%" class="active " align="">日志服务</td>
                        <td class="tnt">
                            <select  name="logService" class="form-control input-sm ">
                                <option value="1">启用</option>
                                <option value="0">不启用</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td width="15%" class="active " align="">包　　名</td>
                        <td class="tnt">
                            <input type="text"   name="packageName" class="form-control input-sm " value="com.yy.young" placeholder="例如: com.yy.young" />
                        </td>
                        <td width="15%" class="active " align="">模 块 名</td>
                        <td class="tnt">
                            <input type="text" id="moduleName"  name="moduleName" class="form-control input-sm " value="" placeholder="例如: cms"  />
                        </td>
                    </tr>
                    <tr>
                        <td width="15%" class="active " align="">
                            保存路径
                            <span class="fa fa-info-circle" style="padding: 0px 2px;color: #57C7D4;" onmouseover="window.tipsIdx=jo.tips('生成的代码文件存放目录,请务必输入正确的文件夹路径',this,{tips:[2, '#3595CC'], time:10000});" onmouseout="jo.close(window.tipsIdx);"></span>
                        </td>
                        <td class="tnt">
                            <input type="text"   name="savePath" class="form-control input-sm " value="C:\youngCodeGenerate" placeholder="输入代码保存路径" />
                        </td>
                        <td width="15%" class="active " align="">作　　者</td>
                        <td class="tnt">
                            <input type="text"   name="author" class="form-control input-sm "  placeholder="例如: imrookie" />
                        </td>
                    </tr>
                    <tr>
                        <td width="15%" class="active " align="">代码生成说明</td>
                        <td class="tnt" colspan="3">
                            <input type="text" name="name" class="form-control input-sm "  placeholder="输入当前代码生成配置的说明信息" />
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <%--字段配置信息--%>
            <div class="table-responsive">
                <table class="table table-bordered table-hover table-line-auto" id="colList" trHandle="formatControls">
                   <%-- <col field="id" title="主键" width="15%" align="" event="click"/>
                    <col field="genId" title="代码生成编号" width="15%" align=""/>--%>
                    <col field="fieldName" title="字段名" width="20%" align="left" headAlign="left"/>
                    <col field="attrName" title="java属性名" width="20%" align=""/>
                    <col field="fieldRemark" title="字段描述" width="20%" align=""/>
                    <col field="{fieldType} ( {fieldLength} )" title="字段类型" width="21%" align="left" headAlign="left"/>
                    <%--<col field="fieldLength" title="字段长度" width="10%" align="right" headAlign="right"/>--%>
                    <%--<col field="fieldNotNull" title="字段非空" width="10%" align=""/>--%>
                    <col field="viewShow" title="视图显示" width="10%" align=""/>
                    <col field="viewNum" title="视图顺序" width="10%" align=""/>
                    <col field="asCondition" title="作为检索条件" width="15%" align=""/>
                    <col field="supportSort" title="支持排序" width="10%" align=""/>
                    <col field="formShow" title="表单显示" width="10%" align=""/>
                    <col field="formNum" title="表单顺序" width="10%" align=""/>
                    <col field="{controlType}{controlParam}" title="控件类型" width="25%" align=""/>
                    <%--<col field="controlParam" title="控件参数" width="15%" align=""/>--%>
                    <col field="controlMust" title="是否必填" width="10%" align=""/>
                    <col field="controlDefault" title="默认值" width="10%" align=""/>
                    <col field="controlReadonly" title="只读属性" width="15%" align=""/>
                    <col field="{controlCheck}{controlCheckVal}" title="表单验证" width="20%" align=""/>
                    <%--<col field="controlCheckVal" title="验证值" width="10%" align=""/>--%>
                    <col field="fillExplain" title="填写说明" width="20%" align=""/>
                    <col field="controlClass" title="控件样式" width="10%" align=""/>
                </table>
            </div>
        </div>
    </div>
</div>

<script src="<%=URL_STATIC%>static/plugin/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
<script type="text/javascript">
    jo.formatUI();//格式化jo组件
</script>
</body>
</html>
