<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <%--常量--%>
    <%@ include file="/common/constHead.jsp"%>
    <%--jQuery--%>
    <%@ include file="/common/jqueryHead.jsp"%>
    <%--jo--%>
    <%@ include file="/common/joHead.jsp"%>
    <%--bootstrap和字体--%>
    <%@ include file="/common/bootstrapHead.jsp"%>
    <%--layer--%>
    <%@ include file="/common/layerHead.jsp"%>
    <%--zTree树--%>
    <%@ include file="/common/zTreeHead.jsp"%>
    <%--公共--%>
    <%@ include file="/common/commonHead.jsp"%>
    <title>代码生成主体信息视图</title>
    <script type="text/javascript">
        $(function(){
            joView.init({grid:$("#mainList"),PKName:"id",winRender:"top",formWidth:"85%"});//初始化页面
        });
        //行处理
        joView.handleItem = function(oItem,iIndex){
            oItem._opt = '<button type="button" class="btn btn-sm btn-outline btn-primary" onclick="codeGenerate(\''+oItem.id+'\')"> <i class="fa fa-magic" aria-hidden="true"></i>&nbsp;生成代码</button>';
        };
        function codeGenerate(genId){
            if(genId){
                jo.postAjax("cms/genMain/generate/" + genId, {}, function (json) {
                    if(json && json.code == 0){
                        jo.showMsg("操作成功!", {icon:1});
                    }
                });
            }
        }
    </script>
</head>
<body>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12">
            <%--检索栏--%>
            <form class="form-inline search-bar" id="pageForm">
                <div class="form-group">
                    <label class="control-label">表名：&nbsp;</label>
                    <input type="text" name="tableName" class="form-control input-sm" value="">
                </div>
                <div class="form-group">
                    <label class="control-label">作者：&nbsp;</label>
                    <input type="text" name="author" class="form-control input-sm" value="">
                </div>
                <button type="button" class="btn btn-primary btn-sm" onclick="joView.select()"> <i class="fa fa-search" aria-hidden="true"></i>&nbsp;查询</button>
            </form>
            <%--/检索栏--%>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12">
            <%--按钮栏--%>
            <div class="form-group button-bar">
                <button isShow="" type="button" class="btn btn-sm btn-outline btn-default" onclick="joView.add()">
                    <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;新建代码配置
                </button>
                <button type="button" class="btn btn-sm btn-outline btn-default" onclick="joView.del()">
                    <i class="fa fa-trash-o" aria-hidden="true"></i>&nbsp;删除
                </button>
                <button type="button" class="btn btn-sm btn-outline btn-default" onclick="window.location.reload()">
                    <i class="fa fa-refresh" aria-hidden="true"></i>&nbsp;刷新
                </button>
            </div>
            <%--/按钮栏--%>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12">
            <%--grid--%>
            <div class="table-responsive">
                <table class="table table-bordered table-hover table-striped table-condensed" id="mainList" dataUrl="cms/genMain/getPage.action" deleteUrl="cms/genMain/delete.action" formUrl="page/cms/gen/genMainForm.jsp">
                    <col field="tableName" title="表名" width="15%" align="" event="click"/>
                    <col field="tableRemark" title="表说明" width="15%" align=""/>
                    <col field="savePath" title="保存路径" width="30%" align="left"/>
                    <col field="author" title="作者" width="15%" align=""/>
                    <col field="[=jo.formatTime({createTime})]" title="创建时间" width="15%" align=""/>
                    <col field="_opt" title="操作" width="15%" align=""/>
                </table>
            </div>
            <%--/grid--%>

            <%--分页条--%>
            <div class="page-bar" gridId="mainList">

            </div>
            <%--/分页条--%>
        </div>
    </div>
</div>

<script src="<%=URL_STATIC%>static/plugin/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
<script type="text/javascript">
    jo.formatUI();//格式化jo组件
</script>
</body>
</html>
