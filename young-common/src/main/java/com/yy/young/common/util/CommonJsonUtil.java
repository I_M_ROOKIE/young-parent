package com.yy.young.common.util;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.core.JsonProcessingException;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Json通用工具类 
 * 类名：CommonJsonUtil
 * 创建人：rookie
 * 创建时间：2016-03-29 
 */
public class CommonJsonUtil {

	/** Commons Logging instance. */
	private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory
			.getLog(CommonJsonUtil.class);

	/**
	 * 将对象转换为Json字符串 objectToJsonString(这里用一句话描述这个方法的作用) (这里描述这个方法适用条件 – 可选)
	 * 
	 * @param obj
	 *            转换对象
	 * @return String
	 * @exception haoyl_2014
	 *                -2-22下午4:51:25
	 */
	public static String objectToJsonString(Object obj) {
		StringBuilder json = new StringBuilder();
		if (obj == null) {
			json.append("\"\"");
		} else if (obj instanceof String || obj instanceof Integer
				|| obj instanceof Float || obj instanceof Boolean
				|| obj instanceof Short || obj instanceof Double
				|| obj instanceof Long || obj instanceof BigDecimal
				|| obj instanceof BigInteger || obj instanceof Byte) {
			json.append("\"").append(string2json(obj.toString())).append("\"");
		} else if (obj instanceof Object[]) {
			json.append(array2json((Object[]) obj));
		} else if (obj instanceof List) {
			json.append(list2json((List<?>) obj));
		} else if (obj instanceof Map) {
			json.append(map2json((Map<?, ?>) obj));
		} else if (obj instanceof Set) {
			json.append(set2json((Set<?>) obj));
		} else {
			json.append(bean2json(obj));
		}
		return json.toString();
	}

	/**
	 * @param bean
	 *            bean对象
	 * @return String
	 */
	private static String bean2json(Object bean) {
		StringBuilder json = new StringBuilder();
		json.append("{");
		PropertyDescriptor[] props = null;
		try {
			props = Introspector.getBeanInfo(bean.getClass(), Object.class)
					.getPropertyDescriptors();
		} catch (IntrospectionException e) {
			e.printStackTrace();
			log.error(e);
		}
		if (props != null) {
			for (int i = 0; i < props.length; i++) {
				try {
					String name = objectToJsonString(props[i].getName());
					String value = objectToJsonString(props[i].getReadMethod()
							.invoke(bean));
					json.append(name);
					json.append(":");
					json.append(value);
					json.append(",");
				} catch (Exception e) {
					e.printStackTrace();
					log.error(e);
				}
			}
			json.setCharAt(json.length() - 1, '}');
		} else {
			json.append("}");
		}
		return json.toString();
	}

	/**
	 * @param list
	 *            list对象
	 * @return String
	 */
	private static String list2json(List<?> list) {
		StringBuilder json = new StringBuilder();
		json.append("[");
		if (list != null && list.size() > 0) {
			for (Object obj : list) {
				json.append(objectToJsonString(obj));
				json.append(",");
			}
			json.setCharAt(json.length() - 1, ']');
		} else {
			json.append("]");
		}
		return json.toString();
	}

	/**
	 * @param array
	 *            对象数组
	 * @return String
	 */
	private static String array2json(Object[] array) {
		StringBuilder json = new StringBuilder();
		json.append("[");
		if (array != null && array.length > 0) {
			for (Object obj : array) {
				json.append(objectToJsonString(obj));
				json.append(",");
			}
			json.setCharAt(json.length() - 1, ']');
		} else {
			json.append("]");
		}
		return json.toString();
	}

	/**
	 * @param map
	 *            map对象
	 * @return String
	 */
	private static String map2json(Map<?, ?> map) {
		StringBuilder json = new StringBuilder();
		json.append("{");
		if (map != null && map.size() > 0) {
			for (Object key : map.keySet()) {
				json.append(objectToJsonString(key));
				json.append(":");
				json.append(objectToJsonString(map.get(key)));
				json.append(",");
			}
			json.setCharAt(json.length() - 1, '}');
		} else {
			json.append("}");
		}
		return json.toString();
	}

	/**
	 * @param set
	 *            集合对象
	 * @return String
	 */
	private static String set2json(Set<?> set) {
		StringBuilder json = new StringBuilder();
		json.append("[");
		if (set != null && set.size() > 0) {
			for (Object obj : set) {
				json.append(objectToJsonString(obj));
				json.append(",");
			}
			json.setCharAt(json.length() - 1, ']');
		} else {
			json.append("]");
		}
		return json.toString();
	}

	/**
	 * @param s
	 *            参数
	 * @return String
	 */
	private static String string2json(String s) {
		if (null == s) {
			return "";
		}
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < s.length(); i++) {
			char ch = s.charAt(i);
			switch (ch) {
			case '"':
				sb.append("\\\"");
				break;
			case '\\':
				sb.append("\\\\");
				break;
			case '\b':
				sb.append("\\b");
				break;
			case '\f':
				sb.append("\\f");
				break;
			case '\n':
				sb.append("\\n");
				break;
			case '\r':
				sb.append("\\r");
				break;
			case '\t':
				sb.append("\\t");
				break;
			case '/':
				sb.append("\\/");
				break;
			default:
				if (ch >= '\u0000' && ch <= '\u001F') {
					String ss = Integer.toHexString(ch);
					sb.append("\\u");
					for (int k = 0; k < 4 - ss.length(); k++) {
						sb.append('0');
					}
					sb.append(ss.toUpperCase());
				} else {
					sb.append(ch);
				}
			}
		}
		return sb.toString();
	}

	/**
	 * 将json对象串转换为JavaBean对象 jsonToBean(这里用一句话描述这个方法的作用) (这里描述这个方法适用条件 – 可选)
	 * 
	 * @param jsonString
	 *            Json对象串 {'name':'zs','age':1}
	 * @param beanCalss
	 *            待转换对象Class
	 * @return T 封装后对象
	 * @exception haoyl_2014
	 *                -2-22上午10:51:58
	 */
	@SuppressWarnings("unchecked")
	public static <T> T jsonStrToBean(String jsonString, Class<T> beanCalss) {
		try {
			JSONObject jsonObject = JSONObject.fromObject(jsonString);
			T bean = (T) JSONObject.toBean(jsonObject, beanCalss);
			return bean;
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return null;

	}

	/**
	 * 
	 * jsonToBeanList(这里用一句话描述这个方法的作用) (这里描述这个方法适用条件 – 可选)
	 * 
	 * @param jsonString
	 * @param beanClass
	 * @return List<T>
	 * @exception haoyl_2014
	 *                -2-22下午5:01:27
	 */
	@SuppressWarnings("unchecked")
	public static <T> List<T> jsonStrArrToBeanList(String jsonString,
			Class<T> beanClass) {

		JSONArray jsonArray = JSONArray.fromObject(jsonString);
		JSONObject jsonObject;
		T bean;
		int size = jsonArray.size();
		List<T> list = new ArrayList<T>(size);

		for (int i = 0; i < size; i++) {

			jsonObject = jsonArray.getJSONObject(i);
			bean = (T) JSONObject.toBean(jsonObject, beanClass);
			list.add(bean);

		}

		return list;

	}

	/**
	 * 将符合JSON格式的字符串解析为MAP对象
	 * 
	 * @param jsonString
	 *            参数对象 "{'dwbm': '011011', 'jcSql': ' and JC_OBJ_ID is null'}";
	 * @return 返回解析后的Map对象
	 */
	public static Map<String, Object> jsonStrToMap(String jsonString) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		JSONObject jsonObject = JSONObject.fromObject(jsonString);
		for (Object key : jsonObject.keySet()) {
			Object obj = jsonObject.get(key);
			if (obj != null && obj instanceof JSONObject) {
				if (((JSONObject) obj).isNullObject()) {
					obj = null;
				}
			}
			if(obj instanceof  String){
				obj  = ((String) obj).trim();
			}
			resultMap.put((String) key, obj);
		}
		return resultMap;
	}

	/**
	 * 
	 * parseList(这里用一句话描述这个方法的作用) (这里描述这个方法适用条件 – 可选)
	 * 
	 * @param json
	 * @return List<Map<String,String>>
	 * @exception haoyl_2014
	 *                -2-22下午5:06:35
	 */
	public static List<Map<String, Object>> jsonStrArrToMapList(String json) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		if (StringUtils.isEmpty(json)) {
			return list;
		}
		JSONArray jsonArray = JSONArray.fromObject(json);
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject jsonObj = jsonArray.getJSONObject(i);
			Map<String, Object> map = new HashMap<String, Object>();

			for (Object key : jsonObj.keySet()) {
				String sKey = (String) key;
				Object value = jsonObj.get(sKey);
				if (value instanceof JSONObject) {
					JSONObject jsonValue = (JSONObject) value;
					if (jsonValue.isNullObject()) {
						value = null;
					}
				}

				map.put(sKey, value);
			}
			list.add(map);
		}
		return list;

	}

	/**
	 * 将JavaBean对象转换为Json对象串 beanToJson(这里用一句话描述这个方法的作用) (这里描述这个方法适用条件 – 可选)
	 * 
	 * @param bean
	 *            待转换javaBean对象
	 * @return String 生成的json对象串
	 * @exception haoyl_2014
	 *                -2-22上午10:53:47
	 */
	public static String beanToJsonStr(Object bean) {

		JSONObject json = JSONObject.fromObject(bean);

		return json.toString();

	}

	/**
	 * 将JavaBean对象转换为Json对象串，转换属性可以设置 beanToJson(这里用一句话描述这个方法的作用) (这里描述这个方法适用条件
	 * – 可选)
	 * 
	 * @param bean
	 *            javabean对象
	 * @param _nory_changes
	 *            对象属性名称集合 new String[]{"name","age"}
	 * @param nory
	 *            处理方式 true 转换指定的属性值；false 转换指定属性外的属性值
	 * @return String 生成的json对象串
	 * @exception haoyl_2014
	 *                -2-22下午2:39:45
	 */
	public static String beanToJsonStr(Object bean, String[] _nory_changes,
			boolean nory) {

		JSONObject json = null;

		if (nory) {// 转换_nory_changes里的属性

			Field[] fields = bean.getClass().getDeclaredFields();
			String str = "";
			for (Field field : fields) {
				// System.out.println(field.getName());
				str += (":" + field.getName());
			}
			fields = bean.getClass().getSuperclass().getDeclaredFields();
			for (Field field : fields) {
				// System.out.println(field.getName());
				str += (":" + field.getName());
			}
			str += ":";
			for (String s : _nory_changes) {
				str = str.replace(":" + s + ":", ":");
			}
			json = JSONObject.fromObject(bean, configJson(str.split(":")));

		} else {// 转换除了_nory_changes里的属性

			json = JSONObject.fromObject(bean, configJson(_nory_changes));
		}

		return json.toString();

	}

	private static JsonConfig configJson(String[] excludes) {

		JsonConfig jsonConfig = new JsonConfig();

		jsonConfig.setExcludes(excludes);
		//
		jsonConfig.setIgnoreDefaultExcludes(false);
		//
		// jsonConfig.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);

		// jsonConfig.registerJsonValueProcessor(Date.class,
		//
		// new DateJsonValueProcessor(datePattern));

		return jsonConfig;

	}

	/**
	 * 将javabean对象集合转换为json对象串数组 beanListToJson(这里用一句话描述这个方法的作用) (这里描述这个方法适用条件 –
	 * 可选)
	 * 
	 * @param beans
	 *            javabean对象集合
	 * @return String json对象串集合
	 * @exception haoyl_2014
	 *                -2-22下午2:53:12
	 */
	public static String beanListToJsonStrArr(List<?> beans) {

		StringBuffer rest = new StringBuffer();

		rest.append("[");

		int size = beans.size();

		for (int i = 0; i < size; i++) {

			rest.append(beanToJsonStr(beans.get(i))
					+ ((i < size - 1) ? "," : ""));

		}

		rest.append("]");

		return rest.toString();

	}

	/**
	 * 将java对象集合转换为json对象串集合 转换属性可以设置 beanListToJson(这里用一句话描述这个方法的作用)
	 * (这里描述这个方法适用条件 – 可选)
	 * 
	 * @param beans
	 *            javabean对象集合
	 * @param _nory_changes
	 *            属性数组
	 * @param nory
	 *            处理方式 ：true 转换指定属性 false 转换指定属性外属性
	 * @return String json对象串集合
	 * @exception haoyl_2014
	 *                -2-22下午2:54:41
	 */
	public static String beanListToJsonStrArr(List<?> beans,
			String[] _nory_changes, boolean nory) {

		StringBuffer rest = new StringBuffer();

		rest.append("[");

		int size = beans.size();

		for (int i = 0; i < size; i++) {
			try {
				rest.append(beanToJsonStr(beans.get(i), _nory_changes, nory));
				if (i < size - 1) {
					rest.append(",");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		rest.append("]");

		return rest.toString();

	}
	
	@SuppressWarnings("rawtypes")
	public static <T> Object strToObject(String str,Class clazz,Map map){
		return JSONObject.toBean(JSONObject.fromObject(str),clazz,map);
	}

	/**
	 * List 2 Json listToJsonStrArr(这里用一句话描述这个方法的作用) (这里描述这个方法适用条件 – 可选)
	 * 
	 * @param list
	 * @return String
	 * @exception haoyl_2014
	 *                -2-22下午5:21:56
	 */
	public static String listToJsonStrArr(List<?> list) {

		JSONArray jsonArray = JSONArray.fromObject(list);

		return jsonArray.toString();

	}
	
	private static ObjectMapper objectMapper;
	static{
	    objectMapper = new ObjectMapper();
	    objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES , true);
        objectMapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
//		objectMapper.setPropertyNamingStrategy(PropertyNamingStrategy.LOWER_CASE);
//		objectMapper.setPropertyNamingStrategy(PropertyNamingStrategy.CAMEL_CASE_TO_LOWER_CASE_WITH_UNDERSCORES);
	}
	/**
	 * 
	* 方法名称: jsonStr2Bean 
	* 方法描述: 将json字符串解析为bean对象 
	* @参数：
	*         @param json
	*         @param class1
	*         @return    
	* @返回值：
	*		  T    返回类型 
	* @作者    haoyl
	* @创建时间 2015-9-24上午8:42:55
	 */
	public static <T> T jsonStr2Bean(String json,Class class1){
	    try{
	        return (T) objectMapper.readValue(json, class1);
	    }catch(Exception e){
	        e.printStackTrace();
	    }
	    return null;
	}
	public static String bean2JsonStr(Object bean){
		try {
			SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			objectMapper.setDateFormat(fmt);
			return objectMapper.writeValueAsString(bean);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
	}

}
