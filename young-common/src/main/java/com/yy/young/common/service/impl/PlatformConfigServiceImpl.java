package com.yy.young.common.service.impl;

import com.yy.young.common.service.IPlatformConfigService;
import com.yy.young.common.util.CommonConstants;
import com.yy.young.common.util.CommonUtil;
import com.yy.young.common.util.StringUtils;
import com.yy.young.dal.service.IDataAccessService;
import com.yy.young.dal.util.Page;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

/**
 * Created by Administrator on 2017/5/8.
 */
@Service("platformConfigService")
public class PlatformConfigServiceImpl implements IPlatformConfigService {

    @Resource(name="dataAccessService")
    IDataAccessService dataService;

    @Override
    public List<Map<String, Object>> getConfigList(Map<String, Object> parameter) throws Exception {
        return dataService.getList(CommonConstants.MAPPER.PLATFORM_CONFIG+".getConfig",parameter);
    }

    @Override
    public List<Map<String, Object>> getConfigPage(Map<String, Object> parameter,Page page) throws Exception {
        return dataService.getList(CommonConstants.MAPPER.PLATFORM_CONFIG+".getConfig",parameter,page);
    }

    @Override
    public int updateConfig(Map<String, Object> parameter) throws Exception {
        return dataService.update(CommonConstants.MAPPER.PLATFORM_CONFIG+".updateConfig",parameter);
    }

    @Override
    public int deleteConfig(Map<String, Object> parameter) throws Exception {
        String idsStr = parameter.get("ids")+"";
        if(StringUtils.isNotBlank(idsStr)){
            String[] idsArr = idsStr.split(",");
            parameter.put("ids",idsArr);
        }
        return dataService.delete(CommonConstants.MAPPER.PLATFORM_CONFIG+".deleteConfig",parameter);
    }

    @Override
    public int insertConfig(Map<String, Object> parameter) throws Exception {
        if(StringUtils.isBlank(parameter.get("ID") + "")){
            parameter.put("ID", CommonUtil.getUUID());//默认ID为UUID
        }
        return dataService.insert(CommonConstants.MAPPER.PLATFORM_CONFIG+".insertConfig",parameter);
    }

    @Override
    public List<Map<String, Object>> getDomainConfigList() throws Exception {
        List<Map<String, Object>> list = this.getConfigList(null);
        if(list != null && list.size() > 0){
            Iterator<Map<String, Object>> it = list.iterator();
            while(it.hasNext()){
                Map<String, Object> map = it.next();
                //将非域名配置的配置项删掉
                if(map == null || StringUtils.isBlank(map.get("CKEY")+"") || !isDomainConfig(map.get("CKEY").toString())){
                    it.remove();
                }
            }
        }
        return list;
    }

    @Override
    public boolean isDomainConfig(String key) throws Exception {
        if(StringUtils.isNotBlank(key) && key.startsWith("URL_")){
            return true;
        }
        return false;
    }

    @Override
    public List<Map<String, Object>> getConfigByCategoryAndKeys(String category, String[] keys) throws Exception {
        Map<String,Object> map = new HashMap<String, Object>();
        map.put("CATEGORY", category);
        map.put("keys", keys);
        return dataService.getList(CommonConstants.MAPPER.PLATFORM_CONFIG+".getConfigByCategoryAndKeys", map);
    }

    @Override
    public void batchUpdateOrInsertConfig(List<Map<String, Object>> list) throws Exception {
        for (Map<String, Object> map : list){
            Map<String, Object> one = this.getConfigByCategoryAndKey(map.get("CATEGORY")+"", map.get("CKEY")+"");
            if (one != null){//存在则修改
                map.put("ID", one.get("ID"));
                //System.out.println("修改==old值="+one.get("CVALUE")+", new值="+map.get("CVALUE"));
                this.updateConfig(map);
            }else{//不存在则新增
                this.insertConfig(map);
            }
        }
    }

    @Override
    public Map<String, Object> getConfigByCategoryAndKey(String category, String key) throws Exception {
        Map<String,Object> map = new HashMap<String, Object>();
        map.put("CATEGORY", category);
        map.put("CKEY", key);
        return (Map<String, Object>)dataService.getObject(CommonConstants.MAPPER.PLATFORM_CONFIG+".getConfigByCategoryAndKey", map);
    }

}
