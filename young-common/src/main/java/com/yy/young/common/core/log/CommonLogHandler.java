package com.yy.young.common.core.log;

import com.yy.young.common.model.OperLog;
import com.yy.young.common.service.IOperLogService;
import com.yy.young.interfaces.log.model.LogDTO;
import com.yy.young.interfaces.log.service.ILogHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;

/**
 * 公共日志处理器(存储在日志表中)
 * Created by rookie on 2017/12/11.
 */
public class CommonLogHandler implements ILogHandler {

    @Resource(name="operLogService")
    IOperLogService service;

    private static final Logger logger = LoggerFactory.getLogger(CommonLogHandler.class);

    @Override
    public void handleBefore(LogDTO logDTO) {

    }

    @Override
    public void handle(LogDTO logDTO) {
        try {
            logger.info("[{}] -请求时间{} -类{} -方法{} -响应时间{}ms", new Object[]{logDTO.getLog().value(), logDTO.getBeginTime(), logDTO.getClassName(), logDTO.getMethodName(), logDTO.getMs()});
            service.insertOperLog(OperLog.fromLogDTO(logDTO));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void handleAfter(LogDTO logDTO) {

    }
}
