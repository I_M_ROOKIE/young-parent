package com.yy.young.common.web;

import com.yy.young.common.core.monit.NodeStatusFactory;
import com.yy.young.common.util.CommonJsonUtil;
import com.yy.young.common.util.Result;
import com.yy.young.common.util.StringUtils;
import com.yy.young.interfaces.log.annotation.Log;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * 监视器,返回服务器各项监控指标
 * Created by rookie on 2018/5/20.
 */
@Controller
@RequestMapping("/common/monitor")
public class MonitorController {


    /**
     * 查询服务器运行状况
     * @param request
     * @return
     */
    @Log("查询服务器运行状况")
    @RequestMapping("/getStatus")
    @ResponseBody
    public Object getStatus(HttpServletRequest request){
        Result result = new Result(NodeStatusFactory.getLocalNodeStatus());
        if(StringUtils.isNotBlank(request.getParameter("callback"))){//jsonp处理
            return request.getParameter("callback")+"("+ CommonJsonUtil.bean2JsonStr(result)+");";
        }
        return result;
    }

   /* public static void main(String[] args){
        System.out.println(new MonitorController().getStatus(null));
    }*/
}
