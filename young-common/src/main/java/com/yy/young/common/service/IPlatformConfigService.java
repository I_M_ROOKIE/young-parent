package com.yy.young.common.service;

import com.yy.young.dal.util.Page;

import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/5/8.
 */
public interface IPlatformConfigService {
    /**
     * 获取平台配置列表
     * @param parameter
     * @return
     * @throws Exception
     */
    List<Map<String,Object>> getConfigList(Map<String, Object> parameter) throws Exception;

    /**
     * 获取平台配置(分页)
     * @param parameter
     * @param page
     * @return
     * @throws Exception
     */
    List<Map<String,Object>> getConfigPage(Map<String, Object> parameter, Page page) throws Exception;

    /**
     * 修改平台配置信息
     * @param parameter
     * @return
     * @throws Exception
     */
    int updateConfig(Map<String, Object> parameter) throws Exception;

    /**
     * 删除平台配置信息
     * @param parameter
     * @return
     * @throws Exception
     */
    int deleteConfig(Map<String, Object> parameter) throws Exception;

    /**
     * 插入平台配置信息
     * @param parameter
     * @return
     * @throws Exception
     */
    int insertConfig(Map<String, Object> parameter) throws Exception;

    /**
     * 获取域名配置
     * @return
     * @throws Exception
     */
    List<Map<String,Object>> getDomainConfigList() throws Exception;

    /**
     * 是否为域名配置
     * @return
     * @throws Exception
     */
    boolean isDomainConfig(String key) throws Exception;

    /**
     * 根据分组和key查询配置信息
     * @param category
     * @param keys
     * @return
     * @throws Exception
     */
    List<Map<String,Object>> getConfigByCategoryAndKeys(String category, String[] keys) throws Exception;

    /**
     * 批量保存或插入配置信息
     * @param list
     * @throws Exception
     */
    void batchUpdateOrInsertConfig(List<Map<String, Object>> list) throws Exception;

    /**
     * 根据分组和key查询配置
     * @param category
     * @param key
     * @return
     * @throws Exception
     */
    Map<String,Object> getConfigByCategoryAndKey(String category, String key) throws Exception;
}
