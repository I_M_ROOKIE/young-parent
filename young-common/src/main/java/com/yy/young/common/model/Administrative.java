package com.yy.young.common.model;

/**
 * 行政区划实体类
 * Created by rookie on 2017-10-27.
 */
public class Administrative{

    private String id;//主键
    private String code;//行政编码
    private String name;//行政区划名称
    private String parentId;//上级id
    private String parentCode;//上级编码
    private String level;//级别
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getParentId() {
        return parentId;
    }
    public void setParentId(String parentId) {
        this.parentId = parentId;
    }
    public String getLevel() {
        return level;
    }
    public void setLevel(String level) {
        this.level = level;
    }

    public String getParentCode() {
        return parentCode;
    }

    public void setParentCode(String parentCode) {
        this.parentCode = parentCode;
    }
}