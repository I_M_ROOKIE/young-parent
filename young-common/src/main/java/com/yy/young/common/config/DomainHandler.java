package com.yy.young.common.config;

import com.yy.young.common.service.IPlatformConfigService;
import com.yy.young.common.util.CommonUtil;
import com.yy.young.common.util.SpringContextHolder;
import com.yy.young.common.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 域名处理器
 * 配置在web.xml监听中
 * 项目启动会扫描平台配置中的域名信息
 * Created by rookie on 2017/6/10.
 */
public class DomainHandler implements ServletContextListener {

    //容器
    private static final Map<String, String> domain = new HashMap<String, String>();

    //@Resource(name = "platformConfigService")
    private IPlatformConfigService configService;

    private static final Logger logger = LoggerFactory.getLogger(DomainHandler.class);

    /**
     * 获取域名地址
     * @param key 域名名称
     * @return
     */
    public static String get(String key){
        if(domain.containsKey(key)){
            return domain.get(key);
        }
        return "";
    }

    /**
     * 获取对应域名地址并且与uri合并
     * @param domainKey
     * @param uri
     * @return
     */
    public static String getAndFormat(String domainKey, String uri){
        if(domain.containsKey(domainKey)){
            return domain.get(domainKey) + (uri != null ? uri : "");
        }
        return uri;
    }

    /**
     * 设置域名
     * @param key
     * @param value
     */
    public static void put(String key, String value){
        if (StringUtils.isNotBlank(key)){
            logger.info("[域名处理器] 添加域名配置{}={}!", key, CommonUtil.getDefaultValue(value, ""));
            domain.put(key, CommonUtil.getDefaultValue(value, ""));
        }else{
            logger.info("[域名处理器] 添加域名时异常,key值无效!");
        }
    }

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        logger.info("[域名处理器] 初始化开始...");
        //获取配置服务的bean,使用注解的方式会获取失败
        configService = SpringContextHolder.getBean("platformConfigService");
        if(configService != null){
            try {
                List<Map<String,Object>> list = configService.getDomainConfigList();
                if(list != null && list.size() > 0){
                    for (Map<String,Object> map : list){
                        logger.info("[域名处理器] 初始化域名{}={}", map.get("CKEY")+"", CommonUtil.getDefaultValue(map.get("CVALUE")+"", ""));
                        domain.put(map.get("CKEY")+"", CommonUtil.getDefaultValue(map.get("CVALUE")+"", ""));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            logger.error("[域名处理器] 初始化配置服务失败!");
        }
        logger.info("[域名处理器] 初始化结束!");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        logger.info("[域名处理器] 销毁开始...");
        domain.clear();
        logger.info("[域名处理器] 销毁结束!");
    }
}
