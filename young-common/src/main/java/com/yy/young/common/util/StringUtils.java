package com.yy.young.common.util;

import javax.lang.model.element.NestingKind;

/**
 * 项目名称：
 * 类名称：StringUtils   
 * 类描述：   
 * 创建人：
 * 创建时间：2015-6-24 下午8:05:42      
 * @version    
 */
public class StringUtils {

    /** 
    * 方法名称: isNotBlank 
    * 方法描述: 判断字符串为非空
    * @参数：
    *         @param currentPage
    *         @return    
    * @返回值：
    *		  boolean    返回类型 
    * @作者    haoyl
    * @创建时间 2015-6-24下午8:05:54
    */ 
    public static boolean isNotBlank(String currentPage) {
       if(null==currentPage || "".equals(currentPage.trim()) || "null".equals(currentPage)||"undefined".equals(currentPage)){
           return false;
       }
       return true;
    }

    /** 
    * 方法名称: isBlank 
    * 方法描述: 判断字符串为空
    * @参数：
    *         @param value
    *         @return
    * @返回值：
    *		  boolean    返回类型 
    * @作者    haoyl
    * @创建时间 2015-7-13下午5:36:38
    */ 
    public static boolean isBlank(String value) {
        if(null==value || "".equals(value.trim()) || "null".equals(value)){
            return true;
        }
        return false;
    }
    /**
     * 方法名称：toCamel
     * 方法描述：将数据库的字段转换成驼峰式字符串(如：DISPLAY_ORDER转换后会变成displayOrder)
     * @param value
     * @return String 转换后的字符串
     * @author zkq
     * @date 2016-03-01 11:32
     */
    public static String toCamel(String value) {
    	StringBuilder sbResult = new StringBuilder();
    	String[] terms = value.split("_");
    	for (int i = 0; i < terms.length; i++) {
    		if(i==0){
    			sbResult.append(terms[i]);
    		}
			if(i!=0){
				sbResult.append(new String(new char[]{terms[i].charAt(0)}).toUpperCase());
				sbResult.append(terms[i].substring(1));
			}
		}
    	return sbResult.toString();
    }
    
    
	/**
	 * 方法名称：toSqlFormt 方法描述：将字符串转换为Sql的格式
	 * 
	 * @param value
	 *            例ID1,ID2,ID3
	 * @return String 转换后的字符串 'ID1','ID2','ID3'
	 * @author cqy
	 * @date 2016-03-16 11:32
	 */
	public static String toSqlFormt(String value) {
		StringBuilder sbResult = new StringBuilder();
		String[] valeList = value.split(",");
		for (int i = 0; i < valeList.length; i++) {
			if (i == 0) {
				sbResult.append("'");
				sbResult.append(valeList[i]);
				sbResult.append("'");
			} else {
				sbResult.append(",");
				sbResult.append("'");
				sbResult.append(valeList[i]);
				sbResult.append("'");
			}
		}
		return sbResult.toString();
	}

	/**
	 * 比较2个字符串值是否相等
	 * @param str1
	 * @param str2
	 * @return
	 */
	public static boolean equals(String str1, String str2){
		if (str1 == null && str2 == null){
			return true;
		}
		if (str1 != null ){
			return str1.equals(str2);
		}
		if (str2 != null){
			return str2.equals(str1);
		}
		return false;
	}
}
