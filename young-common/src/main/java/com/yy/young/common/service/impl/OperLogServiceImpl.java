package com.yy.young.common.service.impl;

import com.yy.young.common.service.IOperLogService;
import com.yy.young.common.util.CommonUtil;
import com.yy.young.dal.service.IDataAccessService;
import com.yy.young.dal.util.Page;
import com.yy.young.common.model.OperLog;
import com.yy.young.common.util.CommonConstants;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.annotation.Resource;
import java.util.List;

/**
 * 操作日志服务实现
 * Created by rookie on 2017-12-11.
 */
@Service("operLogService")
public class OperLogServiceImpl implements IOperLogService {

    @Resource(name = "dataAccessService")
    IDataAccessService dataAccessService;//数据层服务

    private static final Logger logger = LoggerFactory.getLogger(OperLogServiceImpl.class);

    //获取数据列表
    @Override
    public List<OperLog> getOperLogList(OperLog operLog) throws Exception {
        return dataAccessService.getList(CommonConstants.MAPPER.TB_COMMON_OPER_LOG + ".getOperLogList", operLog);
    }

    //获取数据列表(分页)
    @Override
    public List<OperLog> getOperLogPage(OperLog operLog, Page page) throws Exception {
        return dataAccessService.getList(CommonConstants.MAPPER.TB_COMMON_OPER_LOG + ".getOperLogList", operLog, page);
    }

    //查询单条
    @Override
    public OperLog getOperLog(String id) throws Exception {
        return (OperLog)dataAccessService.getObject(CommonConstants.MAPPER.TB_COMMON_OPER_LOG + ".getOperLogById", id);
    }

    //修改
    @Override
    public int updateOperLog(OperLog operLog) throws Exception {
        return dataAccessService.update(CommonConstants.MAPPER.TB_COMMON_OPER_LOG + ".update", operLog);
    }

    //批量删除
    @Override
    public int deleteOperLog(String[] idArr) throws Exception {
        int i = 0;
        for(String id : idArr){
            i += this.deleteOperLog(id);
        }
        return i;
    }

    //删除单条
    @Override
    public int deleteOperLog(String id) throws Exception {
        return dataAccessService.delete(CommonConstants.MAPPER.TB_COMMON_OPER_LOG + ".delete", id);
    }

    //插入
    @Override
    public int insertOperLog(OperLog operLog) throws Exception {
        if (operLog.getId() == null){
            operLog.setId(CommonUtil.getUUID());
        }
        return dataAccessService.insert(CommonConstants.MAPPER.TB_COMMON_OPER_LOG + ".insert", operLog);
    }

    @Override
    public int clear() throws Exception {
        return dataAccessService.delete(CommonConstants.MAPPER.TB_COMMON_OPER_LOG + ".clear", null);
    }

}