package com.yy.young.common.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Result{
	
	private String data_id = "";
	private String data_desc = "";
	private int code = 0;
	private String info = "成功";
	private List<?> data;
	private String callback = "";

	public Result(){
	}
	public Result(List<?> list){
		this.setData(list);
	}
	public Result(List<?> list,Map<String,Object> parameter){
		this.setData(list);
		this.setCallback(parameter);
	}

	public Result(Object obj) {
		List<Object> list = new ArrayList<Object>();
		list.add(obj);
		this.setData(list);
	}
	public Result(Object obj,Map<String,Object> parameter) {
		List<Object> list = new ArrayList<Object>();
		list.add(obj);
		this.setData(list);
		this.setCallback(parameter);
	}

	public List<?> getData() {
		return data;
	}
	public void setData(List<?> data) {
		this.data = data;
	}
	public String getData_id() {
		return data_id;
	}
	public void setData_id(String data_id) {
		this.data_id = data_id;
	}
	public String getData_desc() {
		return data_desc;
	}
	public void setData_desc(String data_desc) {
		this.data_desc = data_desc;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int errorCode) {
		this.code = errorCode;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String errorInfo) {
		this.info = errorInfo;
	}
    public String getCallback() {
        return callback;
    }
	/*public void setCallback(Map<String,Object> parameter) {
		this.callback = parameter.get("callback")==null?"":parameter.get("callback")+"";
	}
	public void setCallback(String callback) {
		this.callback = callback;
	}*/

	public void setCallback(Object callback) {
		if (callback != null && callback instanceof Map){
			Map<String, Object> map = (Map<String,Object>)callback;
			this.callback = map.get("callback")==null?"":map.get("callback")+"";
		}else if(callback != null && callback instanceof String){
			this.callback = callback.toString();
		}
		this.callback = "";
	}
	
	public String toString(){
		String rs =  CommonJsonUtil.bean2JsonStr(this);
		return rs;
	}
}
