package com.yy.young.common.core.excel;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * excel列解析相关的参数单元,反射得到的方法都是一样的
 * 包含字段,ExcelColumn注解,字段set/get方法等信息
 * Created by rookie on 2018/2/8.
 */
public class ExcelColumnUnit {
    Field field;//字段信息
    Method setMethod;//字段的set方法
    Method getMethod;//字段的get方法
    ExcelColumn excelColumn;//字段对应的注解信息

    public Field getField() {
        return field;
    }

    public void setField(Field field) {
        this.field = field;
    }

    public Method getSetMethod() {
        return setMethod;
    }

    public void setSetMethod(Method setMethod) {
        this.setMethod = setMethod;
    }

    public Method getGetMethod() {
        return getMethod;
    }

    public void setGetMethod(Method getMethod) {
        this.getMethod = getMethod;
    }

    public ExcelColumn getExcelColumn() {
        return excelColumn;
    }

    public void setExcelColumn(ExcelColumn excelColumn) {
        this.excelColumn = excelColumn;
    }
}
