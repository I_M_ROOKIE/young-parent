package com.yy.young.common.core.excel;

import java.util.Date;

/**
 * excel测试模型类
 * Created by rookie on 2018/2/8.
 */
public class TestDO {
    @ExcelColumn(value = "编号", order = 1)
    private String id;
    @ExcelColumn(value = "时间", order = 2)
    private Date time;
    @ExcelColumn(value = "顺序", order = 3)
    private Integer num;
    @ExcelColumn(value = "小数", order = 4)
    private Float num2;

    @Override
    public String toString() {
        return "TestDO{" +
                "id='" + id + '\'' +
                ", time=" + time +
                ", num=" + num +
                ", num2=" + num2 +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Float getNum2() {
        return num2;
    }

    public void setNum2(Float num2) {
        this.num2 = num2;
    }
}
