package com.yy.young.common.filter;

import com.yy.young.common.util.ClientInfoUtil;
import com.yy.young.common.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;

/**
 * xss跨站脚本攻击过滤器
 * Created by rookie on 2017/9/28.
 */
public class XSSFilter implements Filter {

    private String[] white;//白名单
    private String[] black;//黑名单

    private static final Logger logger = LoggerFactory.getLogger(XSSFilter.class);

    /**
     * 过滤器初始化
     * @param filterConfig
     * @throws ServletException
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        logger.info("[XSS过滤器] 初始化...");
        //白名单
        String whiteStr = filterConfig.getInitParameter("white");
        if (isNotBlank(whiteStr)){
            //当配置的白名单不为空时,初始化白名单
            white = whiteStr.split(";");
            logger.info("[XSS过滤器] 白名单:{}", whiteStr);
        }
        //黑名单
        String blankStr = filterConfig.getInitParameter("black");
        if (isNotBlank(blankStr)){
            black = blankStr.split(";");
            logger.info("[XSS过滤器] 黑名单:{}", blankStr);
        }
        logger.info("[XSS过滤器] 准备就绪!");
    }

    /**
     * 过滤器核心处理
     * @param request
     * @param response
     * @param chain
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        //请求的url
        String requestUrl = ((HttpServletRequest)request).getRequestURI();
        //logger.info("[XSS过滤器] URI:"+requestUrl);
        //当地址在白名单中时,直接通过
        PathMatcher matcher = new AntPathMatcher();
        if (white != null){
            for(String url : white){
                if(matcher.match(url, requestUrl)){//匹配
                    logger.debug("[XSS过滤器] 白名单URI[{}],通过...", requestUrl);
                    chain.doFilter(request, response);//通过,继续执行过滤链
                    return;
                }
            }
        }

        //使用无上下文匹配方式匹配白名单
        String context = ((HttpServletRequest) request).getContextPath();//获取当前上下文
        if(StringUtils.isNotBlank(context) && !"/".equals(context) && requestUrl.indexOf(context) == 0){
            String requestUrl2 = requestUrl.substring(context.length());//请求uri除去上下文后的地址
            for(String url : white){
                if(matcher.match(url, requestUrl2)){//匹配
                    logger.debug("[XSS过滤器] (无上下文匹配)白名单URI[{}],上下文为[{}],对应白名单项为[{}],通过...", requestUrl, context, url);
                    chain.doFilter(request, response);//通过,继续执行过滤链
                    return;
                }
            }
        }

        //校验请求参数中是否携带有黑名单中的敏感字符,只要存在黑名单中一项则返回失败
        if (black != null){
            if (checkRequestParameter(request)){
                chain.doFilter(request, response);//通过,继续执行过滤链
                logger.debug("[XSS过滤器] 参数合法[{}],通过...", requestUrl);
                return;
            }else{
                logger.warn("[XSS过滤器] *警告 该疑似非法请求的来源IP为:{},目的地址为:{}", ClientInfoUtil.getClientIpAddr((HttpServletRequest)request), requestUrl);
                try {
                    //对非法请求进行处理
                    redirect2ErrorPage((HttpServletRequest)request,(HttpServletResponse)response);
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        chain.doFilter(request, response);
    }

    /**
     * 过滤器销毁
     */
    @Override
    public void destroy() {
        logger.info("[XSS过滤器],销毁...");
        white = null;
        black = null;
    }

    /**
     * 验证请求中的参数是否有效:即验证请求中的参数是否全部合法(不存在黑名单中的敏感字即算合法)
     * @param request
     * @return
     */
    private boolean checkRequestParameter(ServletRequest request){
        //获得所有用户输入的参数
        Enumeration<String> enums = request.getParameterNames();
        while (enums.hasMoreElements()) {
            String key = enums.nextElement();//参数名
            String val = request.getParameter(key);//参数值
            if (strContainBlackEle(val)){//如果参数中含有黑名单中的值
                logger.warn("[XSS过滤器] *警告 发现异常参数,参数名:{},参数值:{}", key, val);
                return false;//只要有一个参数有问题,直接判定非法
            }
        }
        return true;
    }

    /**
     * 判断字符串是否包含数组中的元素,存在一项则返回true
     * @param str 待校验的值
     * @return
     */
    private boolean strContainBlackEle(String str){
        if (isNotBlank(str) && black != null){
            for (String s : black){
                if (str.indexOf(s) != -1){//字符串中存在黑名单中的字符,直接返回true
                    return true;
                }
            }
        }
        return false;
    }

    private boolean isNotBlank(String str) {
        if(null==str || "".equals(str.trim()) || "null".equals(str)||"undefined".equals(str)){
            return false;
        }
        return true;
    }

    /**
     * 重定向到错误页面
     * @param request
     * @param response
     * @throws Exception
     */
    private static void redirect2ErrorPage(HttpServletRequest request,HttpServletResponse response) throws Exception{
        String requestWith = request.getHeader("x-requested-with");  //ajax XMLHttpRequest
        String contentType = request.getHeader("x-content-type");  //除了xml，其他都返回josn格式
        contentType = (contentType == null)? "":contentType;
        if(requestWith==null){    //浏览器访问
            response.setHeader("Content-Type", "text/html;charset=UTF-8");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write("<html><head><title>错误</title></head><body><h2>您可能存在非法操作,请输入合法信息,如有疑问,请联系系统管理员!</h2></body></html>");
        }else{ //ajax返回
            if(contentType.equalsIgnoreCase("xml")){ //返回xml
                response.setHeader("Content-Type", "text/xml;charset=UTF-8");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().print("<root><data_id></data_id><data_desc></data_desc><code>-101</code><info>存在非法参数！</info><data></data><callback></callback></root>");
            }else{//返回josn
                response.setHeader("Content-Type", "text/json;charset=UTF-8");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().print("{\"data_id\":\"\",\"data_desc\":\"\",\"code\":-102,\"info\":\"存在非法参数！\",\"data\":[],\"callback\":\"\"}");
            }
        }
    }
}
