package com.yy.young.common.service.impl;

import com.yy.young.common.service.IAdministrativeService;
import com.yy.young.common.util.CommonUtil;
import com.yy.young.dal.service.IDataAccessService;
import com.yy.young.dal.util.Page;
import com.yy.young.common.model.Administrative;
import com.yy.young.common.util.CommonConstants;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.annotation.Resource;
import java.util.List;

/**
 * 行政区划服务实现
 * Created by rookie on 2017-10-27.
 */
@Service("administrativeService")
public class AdministrativeServiceImpl implements IAdministrativeService {

    @Resource(name = "dataAccessService")
    IDataAccessService dataAccessService;//数据层服务

    private static final Logger logger = LoggerFactory.getLogger(AdministrativeServiceImpl.class);

    //获取数据列表
    @Override
    public List<Administrative> getAdministrativeList(Administrative administrative) throws Exception {
        return dataAccessService.getList(CommonConstants.MAPPER.TB_COMMON_ADMINISTRATIVE + ".getAdministrativeList", administrative);
    }

    //获取数据列表(分页)
    @Override
    public List<Administrative> getAdministrativePage(Administrative administrative, Page page) throws Exception {
        return dataAccessService.getList(CommonConstants.MAPPER.TB_COMMON_ADMINISTRATIVE + ".getAdministrativeList", administrative, page);
    }

    //查询单条
    @Override
    public Administrative getAdministrative(String id) throws Exception {
        return (Administrative)dataAccessService.getObject(CommonConstants.MAPPER.TB_COMMON_ADMINISTRATIVE + ".getAdministrativeById", id);
    }

    //修改
    @Override
    public int updateAdministrative(Administrative administrative) throws Exception {
        return dataAccessService.update(CommonConstants.MAPPER.TB_COMMON_ADMINISTRATIVE + ".update", administrative);
    }

    //批量删除
    @Override
    public int deleteAdministrative(String[] idArr) throws Exception {
        int i = 0;
        for(String id : idArr){
            i += this.deleteAdministrative(id);
        }
        return i;
    }

    //删除单条
    @Override
    public int deleteAdministrative(String id) throws Exception {
        return dataAccessService.delete(CommonConstants.MAPPER.TB_COMMON_ADMINISTRATIVE + ".delete", id);
    }

    //插入
    @Override
    public int insertAdministrative(Administrative administrative) throws Exception {
        return dataAccessService.insert(CommonConstants.MAPPER.TB_COMMON_ADMINISTRATIVE + ".insert", administrative);
    }

    @Override
    public List<Administrative> getProvinceList() throws Exception {
        Administrative administrative = new Administrative();
        administrative.setLevel(CommonConstants.ADMINISTRATIVE_LEVEL.PROVINCE);
        return this.getAdministrativeList(administrative);
    }

    @Override
    public List<Administrative> getCityListByProvinceCode(String provinceCode) throws Exception {
        return this.getSubListByCode(provinceCode);
    }

    @Override
    public List<Administrative> getCountyListByCityCode(String cityCode) throws Exception {
        return this.getSubListByCode(cityCode);
    }

    @Override
    public Administrative getAdministrativeByCode(String code) throws Exception {
        return (Administrative)dataAccessService.getObject(CommonConstants.MAPPER.TB_COMMON_ADMINISTRATIVE + ".getAdministrativeByCode", code);
    }

    @Override
    public List<Administrative> getSubListByCode(String code) throws Exception {
        return dataAccessService.getList(CommonConstants.MAPPER.TB_COMMON_ADMINISTRATIVE + ".getSubAdministrativeListByCode", code);
    }

}