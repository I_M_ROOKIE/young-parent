package com.yy.young.common.web;

import com.yy.young.common.service.ICommonService;
import com.yy.young.common.util.CommonUtil;
import com.yy.young.common.util.SpringContextHolder;
import com.yy.young.common.util.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 公共服务Map版
 * 通过在客户端发起请求中添加_service参数,来指定后台执行的服务
 * 提供了统一的请求入口,省去了controller层代码
 * 内部调用ICommonService<Map<String, Object>>接口服务来实现增删改查/导入导出等功能,省去了这些常规操作的service接口代码
 * 通过使用Map<String, Object>来代替实体类,省去了实体类代码
 * Created by rookie on 2018/3/13.
 */
@Controller
@RequestMapping("/common/scc")
public class SimpleCommonController extends AbstractCommonController<Map<String, Object>> {

    @Override
    public void initService() {
        logger.info("SimpleCommonController init: service form request.");
    }

    @Override
    public ICommonService<Map<String, Object>> getServiceFromRequest(HttpServletRequest request) {
        String serviceName = request.getParameter("_service");
        if (StringUtils.isNotBlank(serviceName)){
            return SpringContextHolder.getBean(serviceName);
        }
        return null;
    }

    @Override
    public Map<String, Object> backFinalParameter(Map<String, Object> param, HttpServletRequest request) {
        if (param != null){
            //logger.debug("入参map不为null,将request中的参数put到传参中!");
            param.putAll(CommonUtil.getParameterFromRequest(request));
            return param;
        }else {
            logger.debug("入参map为空!");
            return CommonUtil.getParameterFromRequest(request);
        }
    }
}
