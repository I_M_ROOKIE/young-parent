package com.yy.young.common.web;

import com.yy.young.common.util.CommonUtil;
import com.yy.young.common.util.Result;
import com.yy.young.common.util.StringUtils;
import com.yy.young.dal.util.Page;
import com.yy.young.interfaces.log.annotation.Log;
import com.yy.young.common.model.OperLog;
import com.yy.young.common.service.IOperLogService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 操作日志服务
 * Created by rookie on 2017-12-11.
 */
@Controller
@RequestMapping("/common/operLog")
public class OperLogController {

    @Resource(name="operLogService")
    IOperLogService service;

    /**
     * 获取数据列表
     * @param operLog
     * @param request
     * @return
     * @throws Exception
     */
    @Log("查询操作日志列表")
    @RequestMapping("/getOperLogList")
    @ResponseBody
    public Object getOperLogList(OperLog operLog, HttpServletRequest request) throws Exception{
        List<OperLog> list = service.getOperLogList(operLog);
        return new Result(list);
    }

    /**
     * 获取分页数据
     * @param operLog
     * @param request
     * @return
     * @throws Exception
     */
    @Log("分页查询操作日志列表")
    @RequestMapping("/getOperLogPage")
    @ResponseBody
    public Object getOperLogPage(OperLog operLog, HttpServletRequest request) throws Exception{
        Page page = CommonUtil.getPageFromRequest(request);
        List<OperLog> list = service.getOperLogPage(operLog, page);
        page.setData(list);
        return page;
    }

    /**
     * 获取单条数据
     * @param id
     * @param request
     * @return
     * @throws Exception
     */
    @Log("查询操作日志")
    @RequestMapping("/getOperLog")
    @ResponseBody
    public Object getOperLog(String id, HttpServletRequest request) throws Exception{
        OperLog operLog = service.getOperLog(id);
        return new Result(operLog);
    }

    /**
     * 新增
     * @param operLog
     * @param request
     * @return
     * @throws Exception
     */
    @Log("新增操作日志")
    @RequestMapping("/insert")
    @ResponseBody
    public Object insert(OperLog operLog, HttpServletRequest request) throws Exception{
        int num = service.insertOperLog(operLog);
        return new Result();
    }

    /**
     * 修改
     * @param operLog
     * @param request
     * @return
     * @throws Exception
     */
    @Log("修改操作日志")
    @RequestMapping("/update")
    @ResponseBody
    public Object update(OperLog operLog, HttpServletRequest request) throws Exception{
        int num = service.updateOperLog(operLog);
        return new Result();
    }

    /**
     * 删除
     * @param ids
     * @param id
     * @param request
     * @return
     * @throws Exception
     */
    @Log("删除操作日志")
    @RequestMapping("/delete")
    @ResponseBody
    public Object delete(String ids, String id, HttpServletRequest request) throws Exception{
        Result result = new Result();
        if(StringUtils.isNotBlank(ids)) {
            String[] idArr = ids.split(",");
            int num = service.deleteOperLog(idArr);
        }else if(StringUtils.isNotBlank(id)){
            int num = service.deleteOperLog(id);
        }else{
            result.setCode(-1);
            result.setInfo("删除失败:待删除编号无效!");
        }
        return result;
    }

    /**
     * 清空日志
     * @param request
     * @return
     * @throws Exception
     */
    @Log("清空日志")
    @RequestMapping("/clear")
    @ResponseBody
    public Object clear(HttpServletRequest request) throws Exception{
        service.clear();
        return new Result();
    }

}