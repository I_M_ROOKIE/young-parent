package com.yy.young.common.service;

import com.yy.young.dal.util.Page;
import com.yy.young.common.model.OperLog;
import java.util.List;

/**
 * 操作日志服务接口
 * Created by rookie on 2017-12-11.
 */
public interface IOperLogService {

    /**
     * 获取数据列表
     * @param operLog
     * @return
     * @throws Exception
     */
    List<OperLog> getOperLogList(OperLog operLog) throws Exception;

    /**
     * 获取数据列表(分页)
     * @param operLog
     * @param page 分页参数
     * @return
     * @throws Exception
     */
    List<OperLog> getOperLogPage(OperLog operLog, Page page) throws Exception;

    /**
     * 根据id获取信息
     * @param id
     * @return
     * @throws Exception
     */
    OperLog getOperLog(String id) throws Exception;

    /**
     * 修改
     * @param operLog
     * @return
     * @throws Exception
     */
    int updateOperLog(OperLog operLog) throws Exception;

    /**
     * 批量删除
     * @param idArr id数组
     * @return
     * @throws Exception
     */
    int deleteOperLog(String[] idArr) throws Exception;

    /**
     * 删除单条
     * @param id
     * @return
     * @throws Exception
     */
    int deleteOperLog(String id) throws Exception;

    /**
     * 插入
     * @param operLog
     * @return
     * @throws Exception
     */
    int insertOperLog(OperLog operLog) throws Exception;

    /**
     * 清空日志
     * @return
     * @throws Exception
     */
    int clear() throws Exception;

}