package com.yy.young.common.web;

import com.yy.young.common.util.CommonUtil;
import com.yy.young.common.util.Result;
import com.yy.young.common.util.StringUtils;
import com.yy.young.dal.util.Page;
import com.yy.young.common.model.Administrative;
import com.yy.young.common.service.IAdministrativeService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 行政区划服务实现
 * Created by rookie on 2017-10-27.
 */
@Controller
@RequestMapping("/common/administrative")
public class AdministrativeController {

    @Resource(name="administrativeService")
    IAdministrativeService service;

    /**
     * 获取数据列表
     * @param administrative
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/getAdministrativeList")
    @ResponseBody
    public Object getAdministrativeList(Administrative administrative, HttpServletRequest request) throws Exception{
        List<Administrative> list = service.getAdministrativeList(administrative);
        return new Result(list);
    }

    /**
     * 获取分页数据
     * @param administrative
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/getAdministrativePage")
    @ResponseBody
    public Object getAdministrativePage(Administrative administrative, HttpServletRequest request) throws Exception{
        Page page = CommonUtil.getPageFromRequest(request);
        List<Administrative> list = service.getAdministrativePage(administrative, page);
        page.setData(list);
        return page;
    }

    /**
     * 获取单条数据
     * @param id
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/getAdministrative")
    @ResponseBody
    public Object getAdministrative(String id, HttpServletRequest request) throws Exception{
        Administrative administrative = service.getAdministrative(id);
        return new Result(administrative);
    }

    /**
     * 新增
     * @param administrative
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/insert")
    @ResponseBody
    public Object insert(Administrative administrative, HttpServletRequest request) throws Exception{
        int num = service.insertAdministrative(administrative);
        return new Result();
    }

    /**
     * 修改
     * @param administrative
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/update")
    @ResponseBody
    public Object update(Administrative administrative, HttpServletRequest request) throws Exception{
        int num = service.updateAdministrative(administrative);
        return new Result();
    }

    /**
     * 删除
     * @param ids
     * @param id
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/delete")
    @ResponseBody
    public Object delete(String ids, String id, HttpServletRequest request) throws Exception{
        Result result = new Result();
        if(StringUtils.isNotBlank(ids)) {
            String[] idArr = ids.split(",");
            int num = service.deleteAdministrative(idArr);
        }else if(StringUtils.isNotBlank(id)){
            int num = service.deleteAdministrative(id);
        }else{
            result.setCode(-1);
            result.setInfo("删除失败:待删除编号无效!");
        }
        return result;
    }

    /**
     * 查询省级行政区
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/getProvinceList")
    @ResponseBody
    public Object getProvinceList(HttpServletRequest request) throws Exception{
        List<Administrative> list = service.getProvinceList();
        return new Result(list);
    }

    /**
     * 根据省编码查询市
     * @param provinceCode
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/getCityListByProvinceCode")
    @ResponseBody
    public Object getCityListByProvinceCode(String provinceCode, HttpServletRequest request) throws Exception{
        List<Administrative> list = service.getCityListByProvinceCode(provinceCode);
        return new Result(list);
    }

    /**
     * 根据市编码查询县
     * @param cityCode
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/getCountyListByCityCode")
    @ResponseBody
    public Object getCountyListByCityCode(String cityCode, HttpServletRequest request) throws Exception{
        List<Administrative> list = service.getCountyListByCityCode(cityCode);
        return new Result(list);
    }

    /**
     * 根据编码查询下级行政区
     * @param code
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/getSubListByCode")
    @ResponseBody
    public Object getSubListByCode(String code, HttpServletRequest request) throws Exception{
        List<Administrative> list = service.getSubListByCode(code);
        return new Result(list);
    }

    /**
     * 根据行政编码查询行政区信息
     * @param code
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/getAdministrativeByCode")
    @ResponseBody
    public Object getAdministrativeByCode(String code, HttpServletRequest request) throws Exception{
        Administrative one = service.getAdministrativeByCode(code);
        return new Result(one);
    }

}