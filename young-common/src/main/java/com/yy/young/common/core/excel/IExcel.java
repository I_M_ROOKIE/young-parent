package com.yy.young.common.core.excel;

import org.apache.poi.ss.usermodel.Sheet;

/**
 * excel顶级接口
 * Created by rookie on 2017/8/17.
 */
public interface IExcel {
    /**
     * 返回当前excel对象是否为空
     * @return
     */
    boolean isEmpty();

    /**
     * 返回当前excel的sheet页数
     * @return
     */
    int getSheetNum();


}
