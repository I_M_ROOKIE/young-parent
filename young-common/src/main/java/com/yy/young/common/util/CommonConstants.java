package com.yy.young.common.util;

/**
 * Created by Administrator on 2017/5/8.
 */
public class CommonConstants {

    //行政区级别
    public static interface ADMINISTRATIVE_LEVEL{
        String COUNTRY = "COUNTRY";//国家
        String PROVINCE = "PROVINCE";//省份
        String CITY = "CITY";//城市
        String COUNTY = "COUNTY";//县
    }
    //common包下的mapper映射
    public static interface MAPPER{
        String PLATFORM_CONFIG = "com.yy.young.common.mapper.platformConfig";//平台配置
        String TB_COMMON_ADMINISTRATIVE = "com.yy.young.common.mapper.administrative";//行政区划
        String TB_COMMON_OPER_LOG = "com.yy.young.common.mapper.operLog";//操作日志
    }

}
