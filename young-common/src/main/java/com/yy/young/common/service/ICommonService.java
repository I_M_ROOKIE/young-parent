package com.yy.young.common.service;

import com.yy.young.dal.util.Page;

import java.util.List;

/**
 * 公共的服务,用于快速开发小型业务
 * Created by rookie on 2018/3/12.
 */
public interface ICommonService<T> {
    /**
     * 查询
     * @param obj
     * @return
     * @throws Exception
     */
    List<T> getList(T obj) throws Exception;

    /**
     * 分页查询
     * @param obj
     * @param page
     * @return
     * @throws Exception
     */
    List<T> getPage(T obj, Page page) throws Exception;

    /**
     * 查询单条
     * @param obj
     * @return
     * @throws Exception
     */
    T get(T obj) throws Exception;

    /**
     * 修改
     * @param obj
     * @return
     * @throws Exception
     */
    int update(T obj) throws Exception;

    /**
     * 删除
     * @param id
     * @return
     * @throws Exception
     */
    int delete(String id) throws Exception;

    /**
     * 批量删除
     * @param idArr
     * @return
     * @throws Exception
     */
    int delete(String[] idArr) throws Exception;

    /**
     * 插入
     * @param obj
     * @return
     * @throws Exception
     */
    int insert(T obj) throws Exception;

    /**
     * 批量插入
     * @param list
     * @return
     * @throws Exception
     */
    int batchInsert(List<T> list) throws Exception;
}
