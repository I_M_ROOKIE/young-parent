package com.yy.young.common.web;

import com.yy.young.common.config.DomainHandler;
import com.yy.young.common.service.IPlatformConfigService;
import com.yy.young.common.util.CommonJsonUtil;
import com.yy.young.common.util.CommonUtil;
import com.yy.young.common.util.Result;
import com.yy.young.common.util.StringUtils;
import com.yy.young.dal.util.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/5/8.
 */
@Controller
@RequestMapping("/common/platformConfig")
public class PlatformConfigController {

    @Resource(name="platformConfigService")
    IPlatformConfigService platformConfigService;

    /**
     * 获取平台配置列表
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/getConfigList")
    @ResponseBody
    public Object getConfigList(HttpServletRequest request) throws Exception{
        Map<String,Object> parameter = CommonUtil.getParameterFromRequest(request);
        List<Map<String,Object>> list = platformConfigService.getConfigList(parameter);
        return new Result(list);
    }

    /**
     * 获取平台配置列表(分页)
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/getConfigPage")
    @ResponseBody
    public Object getConfigPage(HttpServletRequest request) throws Exception{
        Map<String,Object> parameter = CommonUtil.getParameterFromRequest(request);
        Page page = CommonUtil.getPageFromRequest(request);
        List<Map<String,Object>> list = platformConfigService.getConfigPage(parameter,page);
        page.setData(list);
        return page;
    }

    /**
     * 添加平台配置
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/insertConfig")
    @ResponseBody
    public Object insertConfig(HttpServletRequest request) throws Exception{
        Map<String,Object> parameter = CommonUtil.getParameterFromRequest(request);
        int num = platformConfigService.insertConfig(parameter);
        Result result = new Result();
        if(num < 0){
            result.setCode(-1);
            result.setInfo("添加平台配置失败!");
        }else{
            if (platformConfigService.isDomainConfig(parameter.get("CKEY")+"")){
                //更新域名
                DomainHandler.put(parameter.get("CKEY")+"", parameter.get("CVALUE")+"");
            }
        }
        return result;
    }

    /**
     * 修改平台配置信息
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/updateConfig")
    @ResponseBody
    public Object updateConfig(HttpServletRequest request) throws Exception{
        Map<String,Object> parameter = CommonUtil.getParameterFromRequest(request);
        int num = platformConfigService.updateConfig(parameter);
        Result result = new Result();
        if(num < 0){
            result.setCode(-1);
            result.setInfo("修改平台配置信息失败!");
        }else{
            if (platformConfigService.isDomainConfig(parameter.get("CKEY")+"")){
                //更新域名
                DomainHandler.put(parameter.get("CKEY")+"", parameter.get("CVALUE")+"");
            }
        }
        return result;
    }

    /**
     * 删除平台配置信息
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/deleteConfig")
    @ResponseBody
    public Object deleteConfig(HttpServletRequest request) throws Exception{
        Map<String,Object> parameter = CommonUtil.getParameterFromRequest(request);
        int num = platformConfigService.deleteConfig(parameter);
        Result result = new Result();
        if(num < 0){
            result.setCode(-1);
            result.setInfo("删除平台配置信息失败!");
        }
        return result;
    }

    /**
     * 获取平台域名配置列表
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/getDomainConfigList")
    @ResponseBody
    public Object getDomainConfigList(HttpServletRequest request) throws Exception{
        List<Map<String,Object>> list = platformConfigService.getDomainConfigList();
        return new Result(list);
    }

    /**
     * 根据分组查询配置信息
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/getConfigByCategoryAndKeys")
    @ResponseBody
    public Object getConfigByCategoryAndKeys(HttpServletRequest request) throws Exception{
        String category = request.getParameter("category");
        String keys = request.getParameter("keys");
        Result result = new Result();
        if(StringUtils.isNotBlank(category) && StringUtils.isNotBlank(keys)){
            result.setData(platformConfigService.getConfigByCategoryAndKeys(category, keys.split(",")));
        }else{
            result.setCode(-1);
            result.setInfo("分组信息或配置KEY为空!");
        }
        return result;
    }

    /**
     * 批量修改
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/batchUpdateOrInsertConfig")
    @ResponseBody
    public Object batchUpdateOrInsertConfig(String configs, HttpServletRequest request) throws Exception{
        Result result = new Result();
        if (StringUtils.isNotBlank(configs)){
            List<Map<String, Object>> list = CommonJsonUtil.jsonStrArrToMapList(configs);
            platformConfigService.batchUpdateOrInsertConfig(list);

            //将域名配置项同步到DomainHandler
            for (Map<String, Object> map : list) {
                if (platformConfigService.isDomainConfig(map.get("CKEY")+"")){
                    //更新域名
                    DomainHandler.put(map.get("CKEY")+"", map.get("CVALUE")+"");
                }
            }

        }else{
            result.setCode(-1);
            result.setInfo("待修改配置参数无效!");
        }
        return result;
    }
}
