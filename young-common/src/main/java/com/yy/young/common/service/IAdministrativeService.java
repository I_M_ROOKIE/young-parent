package com.yy.young.common.service;

import com.yy.young.dal.util.Page;
import com.yy.young.common.model.Administrative;
import java.util.List;

/**
 * 行政区划服务接口
 * Created by rookie on 2017-10-27.
 */
public interface IAdministrativeService {

    /**
     * 获取数据列表
     * @param administrative
     * @return
     * @throws Exception
     */
    List<Administrative> getAdministrativeList(Administrative administrative) throws Exception;

    /**
     * 获取数据列表(分页)
     * @param administrative
     * @param page 分页参数
     * @return
     * @throws Exception
     */
    List<Administrative> getAdministrativePage(Administrative administrative, Page page) throws Exception;

    /**
     * 根据id获取信息
     * @param id
     * @return
     * @throws Exception
     */
    Administrative getAdministrative(String id) throws Exception;

    /**
     * 修改
     * @param administrative
     * @return
     * @throws Exception
     */
    int updateAdministrative(Administrative administrative) throws Exception;

    /**
     * 批量删除
     * @param idArr id数组
     * @return
     * @throws Exception
     */
    int deleteAdministrative(String[] idArr) throws Exception;

    /**
     * 删除单条
     * @param id
     * @return
     * @throws Exception
     */
    int deleteAdministrative(String id) throws Exception;

    /**
     * 插入
     * @param administrative
     * @return
     * @throws Exception
     */
    int insertAdministrative(Administrative administrative) throws Exception;

    /**
     * 获取省级行政区
     * @return
     * @throws Exception
     */
    List<Administrative> getProvinceList() throws Exception;

    /**
     * 根据省份编码查询市级行政区
     * @return
     * @throws Exception
     */
    List<Administrative> getCityListByProvinceCode(String provinceCode) throws Exception;

    /**
     * 根据市编码查询县
     * @return
     * @throws Exception
     */
    List<Administrative> getCountyListByCityCode(String cityCode) throws Exception;

    /**
     * 根据编码查询行政区信息
     * @param code
     * @return
     * @throws Exception
     */
    Administrative getAdministrativeByCode(String code) throws Exception;

    /**
     * 根据编码查询下级行政区
     * @param code
     * @return
     * @throws Exception
     */
    List<Administrative> getSubListByCode(String code) throws Exception;
}