package com.yy.young.common.service.impl;

import com.yy.young.common.service.ICommonService;

import java.util.List;

/**
 * 公共服务抽象类
 * 提供了普通查询,批量删除,批量插入实现
 * Created by rookie on 2018/3/13.
 */
public abstract class AbstractCommonService<T> implements ICommonService<T> {

    @Override
    public List<T> getList(T obj) throws Exception {
        return getPage(obj, null);
    }

    @Override
    public int delete(String[] idArr) throws Exception {
        int i = 0;
        for(String id : idArr){
            i += this.delete(id);
        }
        return i;
    }

    @Override
    public int batchInsert(List<T> list) throws Exception {
        int count = 0;
        for (T obj : list){
            if (obj != null){
                count += this.insert(obj);
            }
        }
        return count;
    }
}
