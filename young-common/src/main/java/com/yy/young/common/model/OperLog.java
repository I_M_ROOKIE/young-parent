package com.yy.young.common.model;

import com.yy.young.common.util.CommonJsonUtil;
import com.yy.young.common.util.Result;
import com.yy.young.interfaces.log.model.LogDTO;

import java.util.Date;

/**
 * 操作日志实体类
 * Created by rookie on 2017-12-11.
 */
public class OperLog{

    private String id;//主键
    private String oper;//操作
    private Date startTime;//请求时间
    private Date endTime;//请求完成时间
    private String userId;//用户编号
    private String userName;//用户名称
    private String clientBrowser;//浏览器信息
    private String clientIp;//客户端IP
    private String serverIp;//服务器IP
    private String param;//请求参数
    private String result;//返回结果
    private String remark;//备注
    private String exceptionInfo;//异常信息
    private Long timeConsuming;//耗时
    private String url;//耗时
    private String hasException;//发生异常,有值时表示存在异常信息

    /**
     * 根据日志传输对象生成操作日志对象
     * @param logDTO
     * @return
     */
    public static OperLog fromLogDTO(LogDTO logDTO){
        OperLog log = new OperLog();
        log.oper = logDTO.getLog().value();
        log.startTime = logDTO.getBeginTime();
        log.endTime = logDTO.getEndTime();
        if(logDTO.getLoginUser() != null){
            log.userId = logDTO.getLoginUser().getId();
            log.userName = logDTO.getLoginUser().getName();
        }
        log.clientBrowser = logDTO.getBrowser();
        log.clientIp = logDTO.getClientIp();
        log.serverIp = logDTO.getServerIp();
        log.param = CommonJsonUtil.bean2JsonStr(logDTO.getRequestParam());
        log.url = logDTO.getUri();
        if(logDTO.getResult() != null && logDTO.getResult() instanceof Result){
            //结果代码不为0时,记录返回信息
            if (((Result) logDTO.getResult()).getCode() != 0){
                log.result = CommonJsonUtil.bean2JsonStr(logDTO.getResult());
            }else {
                log.result = "成功";
            }
        }else{
            if (logDTO.getException() != null){
                log.setExceptionInfo(logDTO.getException().toString());
                log.result = "发生异常";
            }else{
                log.result = "成功";
            }
        }

        if (log.endTime != null && log.startTime != null){
            log.timeConsuming = log.endTime.getTime() - log.startTime.getTime();
        }
        return log;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getOper() {
        return oper;
    }
    public void setOper(String oper) {
        this.oper = oper;
    }
    public Date getStartTime() {
        return startTime;
    }
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }
    public Date getEndTime() {
        return endTime;
    }
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
    public String getUserId() {
        return userId;
    }
    public void setUserId(String userId) {
        this.userId = userId;
    }
    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public String getClientBrowser() {
        return clientBrowser;
    }
    public void setClientBrowser(String clientBrowser) {
        this.clientBrowser = clientBrowser;
    }
    public String getClientIp() {
        return clientIp;
    }
    public void setClientIp(String clientIp) {
        this.clientIp = clientIp;
    }
    public String getServerIp() {
        return serverIp;
    }
    public void setServerIp(String serverIp) {
        this.serverIp = serverIp;
    }
    public String getParam() {
        return param;
    }
    public void setParam(String param) {
        this.param = param;
    }
    public String getResult() {
        return result;
    }
    public void setResult(String result) {
        this.result = result;
    }
    public String getRemark() {
        return remark;
    }
    public void setRemark(String remark) {
        this.remark = remark;
    }
    public String getExceptionInfo() {
        return exceptionInfo;
    }
    public void setExceptionInfo(String exceptionInfo) {
        this.exceptionInfo = exceptionInfo;
    }

    public Long getTimeConsuming() {
        return timeConsuming;
    }

    public void setTimeConsuming(Long timeConsuming) {
        this.timeConsuming = timeConsuming;
    }

    public String getHasException() {
        return hasException;
    }

    public void setHasException(String hasException) {
        this.hasException = hasException;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}