package com.yy.young.common.util;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.*;

import javax.servlet.http.HttpServletRequest;

import com.yy.young.interfaces.model.User;
import com.yy.young.base.util.GlobalConstants;
import com.yy.young.dal.util.Page;
import org.springframework.util.MultiValueMap;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.DefaultMultipartHttpServletRequest;

/**
 * 公共工具类
* @ClassName: CommonUtil 
* @Description: 公共工具类
* @author rookie
* @date 2016-03-29 
*
 */
public class CommonUtil {

    //本机ip缓存,linux获取网卡配置缓慢(5S左右)
    private static String LOCAL_IP = null;

	/**
	 * 将request中的参数封装为map对象返回
	* @Title: getParameterFromRequest 
	* @Description: 将request中的参数封装为map对象返回
	* @param @param request
	* @param @return    设定文件 
	* @return Map<String,Object>    返回类型 
	* @throws
	 */
	public static Map<String,Object> getParameterFromRequest(HttpServletRequest request){
		Map<String,Object> result = new HashMap<String,Object>();
		String  tmp = request.getParameter("parameter");
        if(StringUtils.isNotBlank(tmp)){
            Map map = CommonJsonUtil.jsonStr2Bean(tmp, Map.class);
            result.putAll(map);
        }
        /*request.removeAttribute("parameter");
        request.removeAttribute("currentPage");
        request.removeAttribute("pageSize");*/
        
		Enumeration<String> enums = request.getParameterNames();
		while (enums.hasMoreElements()) {
		    String key = enums.nextElement();
            String val = request.getParameter(key);
            result.put(key, val);
		}
		result.remove("parameter");
		result.remove("currentPage");
		result.remove("pageSize");
		result.put(GlobalConstants.SESSION.KEY_LOGINUSER, getLoginUser(request));
		return result;
	}

    /**
     * 获取上传文件的集合
     * @param request
     * @return
     */
    public static List<MultipartFile> getMultipartFileFromRequest(DefaultMultipartHttpServletRequest request){
        List<MultipartFile> list = new ArrayList<MultipartFile>();
        MultiValueMap<String, MultipartFile> map = request.getMultiFileMap();
        if (map != null){
            Set<String> keys = map.keySet();
            if (keys != null && keys.size() > 0){
                for (String key : keys){
                    List<MultipartFile> files = map.get(key);//注意:这里的get得到的是一个list
                    for(MultipartFile one : files){
                        if (one != null && !one.isEmpty()){
                            list.add(one);
                        }
                    }
                }

            }
        }
        return list;
    }

	
	/**
	 * !
	 * // 分页信息
	 * @param request
	 * @return
	 */
	public static Page<Map<String,Object>> getPageFromRequest(HttpServletRequest request){
        Page<Map<String,Object>> page = new Page<Map<String,Object>>();
        String currentPage = request.getParameter("pageNumber");
        page.setPageNumber(StringUtils.isNotBlank(currentPage)?Integer.parseInt(currentPage):1);
        String pageSize = request.getParameter("pageSize");
        page.setPageSize(StringUtils.isNotBlank(pageSize)?Integer.parseInt(pageSize):10);
        page.setCallback(StringUtils.isNotBlank(request.getParameter("callback"))?request.getParameter("callback"):"");
        return page;
    }
	
	/**
	 * !
	 *  获取当前登录用户信息
	 * @param request
	 * @return
	 */
	public static User getLoginUser(HttpServletRequest request){
		return (User) request.getSession().getAttribute(GlobalConstants.SESSION.KEY_LOGINUSER);
	}


    /** 
    * 方法名称: setParam2Null 
    * 方法描述: TODO(这里用一句话描述这个方法的作用) 
    * @参数：
    *         @param parameter
    *         @param key
    * @返回值：
    *		  void    返回类型 
    * @作者    haoyl
    * @创建时间 2015-6-26上午9:06:26
    */ 
    public static void setParam2Null(Map<String, Object> parameter, String key) {
        parameter.put(key, StringUtils.isBlank(parameter.get(key)+"")?null:parameter.get(key));
    }
    
    /**
     * 获取UUID
     * @return
     * @author ccc
     * @date 2016年2月19日 下午3:45:58
     */
    public static String getUUID(){
    	return UUID.randomUUID().toString().replaceAll("-", "");
    }

    /**
     * 获取默认值
     * @param str1
     * @param str2
     * @return
     */
    public static String getDefaultValue(String str1,String str2){
        if(StringUtils.isNotBlank(str1)){//str1 is not null
            return str1;
        }else{
            return str2;
        }
    }

    /**
     * 获取本地IP地址
     * @return
     */
    public static String getLocalIp(){
        if(LOCAL_IP != null){
            return LOCAL_IP;
        }
        String ipAddress = "";
        InetAddress inet = null;
        try {
            inet = InetAddress.getLocalHost();
        } catch (UnknownHostException var4) {
            var4.printStackTrace();
        }
        if (inet != null){
            ipAddress = inet.getHostAddress();
            if(ipAddress != null && ipAddress.length() > 15 && ipAddress.indexOf(",") > 0) {
                ipAddress = ipAddress.substring(0, ipAddress.indexOf(","));
            }
            LOCAL_IP = ipAddress;//计入缓存,方便下次使用
        }
        return ipAddress;
    }


}
