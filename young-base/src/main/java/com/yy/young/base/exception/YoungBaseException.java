package com.yy.young.base.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class YoungBaseException extends Exception {

	private static final long serialVersionUID = 1L;
	
	Logger logger = LoggerFactory.getLogger(YoungBaseException.class);
	/*
	 * 错误代码
	 */
	public String errorCode;
	/*
	 * 错误信息
	 */
	public String errorInfo;
	/**
	 * 构造器
	 */
	public YoungBaseException(){
		super();
		this.errorCode = "-901";
		this.errorInfo = "操作失败";
	}
	/**
	 * 构造器
	 * @param msg
	 */
	public YoungBaseException(String msg){
		super(msg);
		this.errorCode = "-901";
		this.errorInfo = msg;
	}
	/**
	 * 构造器
	 * @param errorCode
	 * @param errorInfo
	 */
	public YoungBaseException(String errorCode,String errorInfo){
		super(errorInfo);
		this.errorCode = errorCode;
		this.errorInfo = errorInfo;
	}
	/**
	 * 打印错误信息
	 */
	public void printStackTrace(){
		logger.error("[Young平台异常],错误代码:"+this.errorCode+";"+"错误信息:"+this.errorInfo);
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorInfo() {
		return errorInfo;
	}
	public void setErrorInfo(String errorInfo) {
		this.errorInfo = errorInfo;
	}

}
