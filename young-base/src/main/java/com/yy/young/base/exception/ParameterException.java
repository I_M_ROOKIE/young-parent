package com.yy.young.base.exception;

/**
 * Created by rookie on 2017/6/25.
 */
public class ParameterException extends YoungBaseException {

    public ParameterException(){
        super("参数异常");
    }

    public ParameterException(String msg){
        super(msg);
    }

    public ParameterException(String code, String msg){
        super(code, msg);
    }

}
