package com.yy.young.base.core;

import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.format.Formatter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * 日期转换
 * Created by rookie on 2017/6/28.
 */
public class DateFormatter implements Formatter<Date>{

    Logger logger = LoggerFactory.getLogger(DateFormatter.class);

    @Override
    public Date parse(String text, Locale locale) throws ParseException {
        logger.debug("[时间参数绑定格式转换] 待转换时间字符串为 {}", text);
        Date date = null;
        if (text != null){
            SimpleDateFormat format = null;
            //根据时间文本格式匹配时间格式化对象
            if (text.length() > 10){
                format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            }else if (text.length() == 10){
                format = new SimpleDateFormat("yyyy-MM-dd");
            }

            //当格式化对象有效时,执行格式化
            if (format != null){
                try {
                    date = format.parse(text);
                } catch (Exception e) {
                    logger.error("[时间参数绑定格式转换] *异常* 格式转换异常,字符串为 {}", text);
                    e.printStackTrace();
                }
            }else{
                logger.warn("[时间参数绑定格式转换] *警告 没有找到对应的时间格式化对象,待格式化文本为{}", text);
            }
        }
        return date;
    }

    @Override
    public String print(Date object, Locale locale) {
        return null;
    }
}
