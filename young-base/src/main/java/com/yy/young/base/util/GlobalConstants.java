package com.yy.young.base.util;

/**
 * Created by Administrator on 2017/5/9.
 */
public class GlobalConstants {

    public static final String CHARSET = "UTF-8";

    //url配置项
    public interface URL_CONFIG_KEY{
        String URL_STATIC = "URL_STATIC";//静态资源服务器域名
        String URL_FS = "URL_FS";//文件服务器域名
        String URL_UMS = "URL_UMS";//ums服务器域名
        String URL_CMS = "URL_CMS";//cms服务器域名
        String URL_PORTAL = "URL_PORTAL";//门户域名
        String URL_INDEX = "URL_INDEX";//登录成功后跳转的首页地址
    }

    //结果常量
    public interface RETURN{
        String SUCCESS = "YES";//操作成功标识
        String FAIL = "NO";//失败操作标识
    }

    //session常量
    public interface SESSION{
        String KEY_LOGINUSER = "loginUser";//存入session的用户key
    }

    //重定向地址常量
    public interface REDIRECT{
        String EXCEPTION = "/common/error.jsp";//异常处理重定向的页面
    }
}
