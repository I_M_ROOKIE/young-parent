package com.yy.young.ums.service;

import com.yy.young.interfaces.model.User;
import com.yy.young.dal.util.Page;

import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/5/8.
 */
public interface IUmsUserService {
    /**
     * 获取用户列表
     * @param parameter
     * @return
     * @throws Exception
     */
    List<Map<String,Object>> getUserList(Map<String,Object> parameter) throws Exception;

    /**
     * 获取用户(分页)
     * @param parameter
     * @param page
     * @return
     * @throws Exception
     */
    List<Map<String,Object>> getUserPage(Map<String,Object> parameter,Page page) throws Exception;

    /**
     * 修改用户信息
     * @param parameter
     * @return
     * @throws Exception
     */
    int updateUser(Map<String,Object> parameter) throws Exception;

    /**
     * 更新用户信息
     * @param user
     * @return
     * @throws Exception
     */
    int updateUser(User user) throws Exception;

    /**
     * 删除用户信息
     * @param parameter
     * @return
     * @throws Exception
     */
    int deleteUser(Map<String,Object> parameter) throws Exception;

    /**
     * 删除用户
     * @param userId
     * @return
     * @throws Exception
     */
    int deleteUser(String userId) throws Exception;

    /**
     * 插入用户信息
     * @param parameter
     * @return
     * @throws Exception
     */
    int insertUser(Map<String,Object> parameter) throws Exception;

    /**
     * 插入用户
     * @param user
     * @return
     * @throws Exception
     */
    int insertUser(User user) throws Exception;

    /**
     * 根据用户名查找用户
     * @param account
     * @return
     * @throws Exception
     */
    User getUserByAccount(String account) throws Exception;

    /**
     * 重置密码
     * @param ids 用户id拼接成的字符串,逗号分隔
     * @return
     * @throws Exception
     */
    int resetPassword(String ids) throws Exception;

    /**
     * 修改密码
     * @param userId 用户id
     * @param newPwd 新密码
     * @return
     * @throws Exception
     */
    int updatePassword(String userId, String newPwd) throws Exception;

    /**
     * 根据用户id查询登录用户
     * @param userId
     * @return
     * @throws Exception
     */
    User getUserById(String userId) throws Exception;

    /**
     * 完善登录的用户信息
     * @param user
     * @throws Exception
     */
    void perfectLoginUserInfo(User user) throws Exception;

    /**
     * 根据单位查询用户
     * @param companyId
     * @return
     * @throws Exception
     */
    List<User> getUserListByCompanyId(String companyId) throws Exception;

    /**
     * 判断是否存在指定id/account的用户
     * 在创建用户之前做唯一性验证
     * @param userIdOrAccount 用户id或者登陆账号
     * @return
     * @throws Exception
     */
    boolean existUserIdOrAccount(String userIdOrAccount) throws Exception;
}
