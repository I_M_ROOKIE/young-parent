package com.yy.young.ums.util;

import com.yy.young.common.util.CommonUtil;
import com.yy.young.common.util.MD5Util;
import com.yy.young.common.util.StringUtils;
import org.apache.commons.codec.binary.Base64;

/**
 * ums安全相关处理类
 * Created by rookie on 2017/6/29.
 */
public class SecureHandler {

    /**
     * 加密
     * 用于用户密码加密等
     * @param str 待加密的明文
     * @return 返回加密后的密文
     */
    public static String encrypt(String str){
        return MD5Util.MD5(str);
    }

    /**
     * 生成单点token令牌
     * 规则:将用户ID和时间戳拼接后进行base64编码
     * @param userId
     * @return
     */
    public static String createToken(String userId, long time){
        String token = userId + CommonUtil.getDefaultValue(UmsConfigHelper.getConfigValue(UmsConstants.CONFIG_KEY.SIGN_TOKEN_CHAR), UmsConstants.SSO.SIGN_TOKEN_CHAR) + time;//明文
        byte[] bytes = Base64.encodeBase64(token.getBytes());
        return new String(bytes);
    }

    /**
     * token解码
     * @param token
     * @return
     */
    public static String decodeToken(String token){
        return new String(Base64.decodeBase64(token));
    }

    /**
     * 验证token有效性,是否被篡改/伪造/失效
     * @param token token密文
     * @return
     */
    public static boolean verifyToken(String token){
        String m = decodeToken(token);
        //通过判断解密后的明文中是否包含干扰符来判断是否有效
        String str = CommonUtil.getDefaultValue(UmsConfigHelper.getConfigValue(UmsConstants.CONFIG_KEY.SIGN_TOKEN_CHAR), UmsConstants.SSO.SIGN_TOKEN_CHAR);//干扰符
        if (StringUtils.isNotBlank(m) && m.indexOf(str) > -1 && m.split(str).length == 2){
            return true;
        }
        return false;
    }

    /**
     * 读取token中的用户id
     * @param token
     * @return
     */
    public static String getUserIdFromToken(String token){
        String m = decodeToken(token);
        if (verifyToken(token)){
            return m.split(CommonUtil.getDefaultValue(UmsConfigHelper.getConfigValue(UmsConstants.CONFIG_KEY.SIGN_TOKEN_CHAR), UmsConstants.SSO.SIGN_TOKEN_CHAR))[0];
        }
        return null;
    }

    /*public static void main(String[] args){
        String a = createToken("admin", System.currentTimeMillis());
        System.out.println("加密后的值为:"+a);
        System.out.println("解密后的值为:"+new String(Base64.decodeBase64(a)));
    }*/
}
