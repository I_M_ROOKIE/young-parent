package com.yy.young.ums.model;

/**
 * Created by rookie on 2017/5/11.
 * sso结果bean
 */
public class SsoInfo {
    private int code;//结果代码,1表示成功,-1表示失败
    private String info;//结果信息
    private String redirectTo;//重定向url,登录成功后跳转url
    private String token;//登录令牌

    public SsoInfo() {
        this(-1, "用户名或密码错误!", null);
    }

    public SsoInfo(int code, String info) {
        this(code, info, null, null);
    }

    public SsoInfo(int code, String info, String redirectTo) {
        this(code, info, redirectTo, null);
    }

    public SsoInfo(int code, String info, String redirectTo, String token) {
        this.code = code;
        this.info = info;
        this.redirectTo = redirectTo;
        this.token = token;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getRedirectTo() {
        return redirectTo;
    }

    public void setRedirectTo(String redirectTo) {
        this.redirectTo = redirectTo;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
