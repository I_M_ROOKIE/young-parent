package com.yy.young.ums.service.impl;

import com.yy.young.common.util.CommonUtil;
import com.yy.young.common.util.StringUtils;
import com.yy.young.dal.service.IDataAccessService;
import com.yy.young.dal.util.Page;
import com.yy.young.interfaces.ums.model.Dept;
import com.yy.young.ums.service.IUmsDeptService;
import com.yy.young.ums.service.IUmsDeptUserService;
import com.yy.young.ums.util.UmsConstants;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/5/8.
 */
@Service("umsDeptService")
public class UmsDeptServiceImpl implements IUmsDeptService {

    @Resource(name="dataAccessService")
    IDataAccessService dataService;

    @Resource(name = "umsDeptUserService")
    IUmsDeptUserService umsDeptUserService;

    @Override
    public List<Map<String, Object>> getDeptList(Map<String, Object> parameter) throws Exception {
        return dataService.getList(UmsConstants.MAPPER.UMS_DEPT+".getDept",parameter);
    }

    @Override
    public List<Map<String, Object>> getDeptPage(Map<String, Object> parameter,Page page) throws Exception {
        return dataService.getList(UmsConstants.MAPPER.UMS_DEPT+".getDept",parameter,page);
    }

    @Override
    public int updateDept(Map<String, Object> parameter) throws Exception {
        return dataService.update(UmsConstants.MAPPER.UMS_DEPT+".updateDept",parameter);
    }

    @Override
    public int deleteDept(Map<String, Object> parameter) throws Exception {
        String idsStr = parameter.get("ids")+"";
        if(StringUtils.isNotBlank(idsStr)){
            String[] idsArr = idsStr.split(",");
            parameter.put("ids",idsArr);
            if (idsArr != null && idsArr.length > 0){
                for (String did : idsArr){
                    //同时删除部门-用户关联
                    umsDeptUserService.deleteDeptUserByDeptId(did);
                }
            }
        }
        return dataService.delete(UmsConstants.MAPPER.UMS_DEPT+".deleteDept",parameter);
    }

    @Override
    public int insertDept(Map<String, Object> parameter) throws Exception {
        if(StringUtils.isBlank(parameter.get("ID") + "")){
            parameter.put("ID", CommonUtil.getUUID());//默认ID为UUID
        }
        if(StringUtils.isBlank(parameter.get("PARENT_ID") + "")){
            parameter.put("PARENT_ID", UmsConstants.DEFAULT.ROOT_DEPT);//默认上级部门为根节点
        }
        parameter.put("CREATE_TIME",new Date());//部门创建时间
        //查询父节点信息
        Dept parent = this.getDeptById(parameter.get("PARENT_ID")+"");
        parameter.put("WHOLE_ID", parent.getWholeId() + "/" + parameter.get("ID"));//设置id全路径
        parameter.put("WHOLE_NAME", parent.getWholeName() + "/" + parameter.get("NAME"));//设置name全路径
        return dataService.insert(UmsConstants.MAPPER.UMS_DEPT+".insertDept",parameter);
    }

    @Override
    public List<Map<String, Object>> getDeptListByUserId(String userId) throws Exception {
        return dataService.getList(UmsConstants.MAPPER.UMS_DEPT+".getDeptListByUserId",userId);
    }

    //TODO 从数组中取最大范围id,多个属于同级,则用字符串拼接,当下仅以层级比较,没有比对父节点
    @Override
    public String getMaxRangeId(String[] rangeArr) throws Exception {
        if (rangeArr != null && rangeArr.length > 0){
            if (rangeArr.length == 1){
                return rangeArr[0];
            }

            //去重
            List<String> list = new ArrayList<String>();//范围集合
            for(String range : rangeArr){
                if (range != null && !list.contains(range)){
                    list.add(range);
                }
            }

            if (list != null && list.size() > 0){
                return list.get(0);
            }

            //查询出对应的机构数据
            String rangeId = "";
            Dept dept = null;
            for (String range : list){
                Dept d = this.getDeptById(range);
                if (d != null){
                    if (dept != null){
                        if (d.getNum() > dept.getNum()){
                            dept = d;
                            rangeId = d.getId();
                        }else if (d.getNum() == dept.getNum()){
                            rangeId += "," + d.getId();
                        }
                    }else {
                        dept = d;
                        rangeId = d.getId();
                    }
                }
            }

            return rangeId;

        }
        return null;
    }

    @Override
    public Dept getDeptById(String id) throws Exception {
        return (Dept)dataService.getObject(UmsConstants.MAPPER.UMS_DEPT+".getDeptById", id);
    }
}
