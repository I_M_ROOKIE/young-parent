package com.yy.young.ums.web;

import com.yy.young.common.util.CommonUtil;
import com.yy.young.common.util.Result;
import com.yy.young.dal.util.Page;
import com.yy.young.interfaces.log.annotation.Log;
import com.yy.young.ums.service.IUmsDeptService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/5/8.
 */
@Controller
@RequestMapping("/ums/dept")
public class UmsDeptController {

    @Resource(name="umsDeptService")
    IUmsDeptService DeptService;

    /**
     * 获取部门列表
     * @param request
     * @return
     * @throws Exception
     */
    @Log("查询组织机构列表")
    @RequestMapping("/getDeptList")
    @ResponseBody
    public Object getDeptList(HttpServletRequest request) throws Exception{
        Map<String,Object> parameter = CommonUtil.getParameterFromRequest(request);
        List<Map<String,Object>> list = DeptService.getDeptList(parameter);
        return new Result(list);
    }

    /**
     * 获取部门列表(分页)
     * @param request
     * @return
     * @throws Exception
     */
    @Log("分页查询组织机构列表")
    @RequestMapping("/getDeptPage")
    @ResponseBody
    public Object getDeptPage(HttpServletRequest request) throws Exception{
        Map<String,Object> parameter = CommonUtil.getParameterFromRequest(request);
        Page page = CommonUtil.getPageFromRequest(request);
        List<Map<String,Object>> list = DeptService.getDeptPage(parameter,page);
        page.setData(list);
        return page;
    }

    /**
     * 添加部门
     * @param request
     * @return
     * @throws Exception
     */
    @Log("添加组织机构")
    @RequestMapping("/insertDept")
    @ResponseBody
    public Object insertDept(HttpServletRequest request) throws Exception{
        Map<String,Object> parameter = CommonUtil.getParameterFromRequest(request);
        int num = DeptService.insertDept(parameter);
        Result result = new Result();
        if(num < 0){
            result.setCode(-1);
            result.setInfo("添加部门失败!");
        }
        return result;
    }

    /**
     * 修改部门信息
     * @param request
     * @return
     * @throws Exception
     */
    @Log("修改组织机构")
    @RequestMapping("/updateDept")
    @ResponseBody
    public Object updateDept(HttpServletRequest request) throws Exception{
        Map<String,Object> parameter = CommonUtil.getParameterFromRequest(request);
        int num = DeptService.updateDept(parameter);
        Result result = new Result();
        if(num < 0){
            result.setCode(-1);
            result.setInfo("修改部门信息失败!");
        }
        return result;
    }

    /**
     * 删除部门信息
     * @param request
     * @return
     * @throws Exception
     */
    @Log("删除组织机构")
    @RequestMapping("/deleteDept")
    @ResponseBody
    public Object deleteDept(HttpServletRequest request) throws Exception{
        Map<String,Object> parameter = CommonUtil.getParameterFromRequest(request);
        int num = DeptService.deleteDept(parameter);
        Result result = new Result();
        if(num < 0){
            result.setCode(-1);
            result.setInfo("删除部门信息失败!");
        }
        return result;
    }


}
