package com.yy.young.ums.service;

import com.yy.young.dal.util.Page;

import java.util.List;
import java.util.Map;

/**
 * Created by rookie on 2017/6/29.
 */
public interface IUmsDeptUserService {
    /**
     * 获取部门用户信息
     * 参数有:关联关系主键:ID,部门id:DEPT_ID,用户id:USER_ID,用户性别:SEX,用户状态:STATE,用户名称:USER_NAME,全组织范围检索:ALLUSER
     * @param parameter
     * @return
     * @throws Exception
     */
    List<Map<String,Object>> getDeptUserList(Map<String,Object> parameter) throws Exception;

    /**
     * 获取部门用户信息(分页)
     * 参数有:关联关系主键:ID,部门id:DEPT_ID,用户id:USER_ID,用户性别:SEX,用户状态:STATE,用户名称:USER_NAME,全组织范围检索:ALLUSER
     * @param parameter
     * @param page
     * @return
     * @throws Exception
     */
    List<Map<String,Object>> getDeptUserPage(Map<String,Object> parameter,Page page) throws Exception;

    /**
     * 根据用户编号删除相关的部门用户关联信息
     * @param userId 用户编号
     * @return
     * @throws Exception
     */
    int deleteDeptUserByUserId(String userId) throws Exception;

    /**
     * 根据部门id删除关联
     * @param deptId
     * @return
     * @throws Exception
     */
    int deleteDeptUserByDeptId(String deptId) throws Exception;
}
