package com.yy.young.ums.service.impl;

import com.yy.young.common.util.StringUtils;
import com.yy.young.dal.service.IDataAccessService;
import com.yy.young.dal.util.Page;
import com.yy.young.ums.model.ApplicationSystem;
import com.yy.young.ums.service.IUmsAppService;
import com.yy.young.ums.util.UmsConstants;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/5/8.
 */
@Service("umsAppService")
public class UmsAppServiceImpl implements IUmsAppService {

    @Resource(name="dataAccessService")
    IDataAccessService dataService;

    @Override
    public List<ApplicationSystem> getAppList(ApplicationSystem parameter) throws Exception {
        return dataService.getList(UmsConstants.MAPPER.UMS_APP+".getApp",parameter);
    }

    @Override
    public List<ApplicationSystem> getAppPage(ApplicationSystem parameter,Page page) throws Exception {
        return dataService.getList(UmsConstants.MAPPER.UMS_APP+".getApp",parameter,page);
    }

    @Override
    public int updateApp(ApplicationSystem parameter) throws Exception {
        parameter.setUpdateTime(new Date());//默认修改时间为当前时间
        return dataService.update(UmsConstants.MAPPER.UMS_APP+".updateApp",parameter);
    }

    @Override
    public int deleteApp(Map<String, Object> parameter) throws Exception {
        String idsStr = parameter.get("ids")+"";
        if(StringUtils.isNotBlank(idsStr)){
            String[] idsArr = idsStr.split(",");
            parameter.put("ids",idsArr);
        }
        return dataService.delete(UmsConstants.MAPPER.UMS_APP+".deleteApp",parameter);
    }

    @Override
    public int insertApp(ApplicationSystem parameter) throws Exception {
        parameter.setCreateTime(new Date());//创建时间
        parameter.setUpdateTime(new Date());//默认修改时间为当前时间
        return dataService.insert(UmsConstants.MAPPER.UMS_APP+".insertApp",parameter);
    }

}
