package com.yy.young.ums.service;

import com.yy.young.dal.util.Page;

import java.util.List;
import java.util.Map;

/**
 * Created by rookie on 2017/6/29.
 */
public interface IUmsRoleUserService {
    /**
     * 获取角色用户信息
     * @param parameter
     * @return
     * @throws Exception
     */
    List<Map<String,Object>> getRoleUserList(Map<String, Object> parameter) throws Exception;

    /**
     * 获取角色用户信息(分页)
     * @param parameter
     * @param page
     * @return
     * @throws Exception
     */
    List<Map<String,Object>> getRoleUserPage(Map<String, Object> parameter, Page page) throws Exception;

    /**
     * 删除角色用户
     * @param idArr
     * @return
     * @throws Exception
     */
    int deleteRoleUser(String[] idArr) throws Exception;

    /**
     * 删除单个
     * @param id
     * @return
     * @throws Exception
     */
    int deleteRoleUser(String id) throws Exception;

    /**
     * 根据用户删除用户角色关联关系
     * @param userId 用户编号
     * @return
     * @throws Exception
     */
    int deleteRoleUserByUserId(String userId) throws Exception;
}
