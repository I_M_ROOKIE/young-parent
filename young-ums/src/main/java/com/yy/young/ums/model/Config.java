package com.yy.young.ums.model;

/**
 * 统一用户系统配置实体类
 * Created by rookie on 2018-01-27.
 */
public class Config{

    private String id;//主键
    private String key;//key
    private String value;//值
    private Integer num;//显示顺序
    private String state;//是否启用
    private String remark;//备注

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getKey() {
        return key;
    }
    public void setKey(String key) {
        this.key = key;
    }
    public String getValue() {
        return value;
    }
    public void setValue(String value) {
        this.value = value;
    }
    public Integer getNum() {
        return num;
    }
    public void setNum(Integer num) {
        this.num = num;
    }
    public String getState() {
        return state;
    }
    public void setState(String state) {
        this.state = state;
    }
    public String getRemark() {
        return remark;
    }
    public void setRemark(String remark) {
        this.remark = remark;
    }
}