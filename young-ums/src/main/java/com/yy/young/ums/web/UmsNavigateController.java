package com.yy.young.ums.web;

import com.yy.young.common.util.CommonUtil;
import com.yy.young.common.util.Result;
import com.yy.young.common.util.StringUtils;
import com.yy.young.dal.util.Page;
import com.yy.young.interfaces.log.annotation.Log;
import com.yy.young.ums.service.IUmsNavigateService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/5/8.
 */
@Controller
@RequestMapping("/ums/navigate")
public class UmsNavigateController {

    @Resource(name="umsNavigateService")
    IUmsNavigateService navigateService;

    /**
     * 获取资源列表
     * @param request
     * @return
     * @throws Exception
     */
    @Log("查询导航资源列表")
    @RequestMapping("/getNavigateList")
    @ResponseBody
    public Object getNavigateList(HttpServletRequest request) throws Exception{
        Map<String,Object> parameter = CommonUtil.getParameterFromRequest(request);
        List<Map<String,Object>> list = navigateService.getNavigateList(parameter);
        return new Result(list);
    }

    /**
     * 获取资源列表(分页)
     * @param request
     * @return
     * @throws Exception
     */
    @Log("分页查询导航资源列表")
    @RequestMapping("/getNavigatePage")
    @ResponseBody
    public Object getNavigatePage(HttpServletRequest request) throws Exception{
        Map<String,Object> parameter = CommonUtil.getParameterFromRequest(request);
        Page page = CommonUtil.getPageFromRequest(request);
        List<Map<String,Object>> list = navigateService.getNavigatePage(parameter, page);
        page.setData(list);
        return page;
    }

    /**
     * 添加资源
     * @param request
     * @return
     * @throws Exception
     */
    @Log("添加导航资源")
    @RequestMapping("/insertNavigate")
    @ResponseBody
    public Object insertNavigate(HttpServletRequest request) throws Exception{
        Map<String,Object> parameter = CommonUtil.getParameterFromRequest(request);
        int num = navigateService.insertNavigate(parameter);
        Result result = new Result();
        if(num < 0){
            result.setCode(-1);
            result.setInfo("添加资源失败!");
        }
        return result;
    }

    /**
     * 修改资源信息
     * @param request
     * @return
     * @throws Exception
     */
    @Log("修改导航资源")
    @RequestMapping("/updateNavigate")
    @ResponseBody
    public Object updateNavigate(HttpServletRequest request) throws Exception{
        Map<String,Object> parameter = CommonUtil.getParameterFromRequest(request);
        int num = navigateService.updateNavigate(parameter);
        Result result = new Result();
        if(num < 0){
            result.setCode(-1);
            result.setInfo("修改资源信息失败!");
        }
        return result;
    }

    /**
     * 删除资源信息
     * @param request
     * @return
     * @throws Exception
     */
    @Log("删除导航资源")
    @RequestMapping("/deleteNavigate")
    @ResponseBody
    public Object deleteNavigate(HttpServletRequest request) throws Exception{
        Map<String,Object> parameter = CommonUtil.getParameterFromRequest(request);
        int num = navigateService.deleteNavigate(parameter);
        Result result = new Result();
        if(num < 0){
            result.setCode(-1);
            result.setInfo("删除资源信息失败!");
        }
        return result;
    }

    /**
     * 查询角色拥有的资源权限情况(查询所有的资源信息并且标注该角色是否含有此权限)
     * @param request
     * @return
     * @throws Exception
     */
    @Log("查询所有资源并标注角色关联情况")
    @RequestMapping("/getNavigateByRole")
    @ResponseBody
    public Object getNavigateByRole(HttpServletRequest request) throws Exception{
        String roleId = request.getParameter("roleId");
        Result res = new Result();
        if(StringUtils.isBlank(roleId)){
            res.setCode(-1);
            res.setInfo("查询失败:角色ID为空!");
            return res;
        }
        List<Map<String,Object>> list = navigateService.getNavAuthByRole(roleId);
        res.setData(list);
        return res;
    }
}
