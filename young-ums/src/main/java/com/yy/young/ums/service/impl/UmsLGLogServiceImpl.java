package com.yy.young.ums.service.impl;

import com.yy.young.common.util.ClientInfoUtil;
import com.yy.young.common.util.CommonUtil;
import com.yy.young.common.util.StringUtils;
import com.yy.young.dal.service.IDataAccessService;
import com.yy.young.dal.util.Page;
import com.yy.young.interfaces.model.User;
import com.yy.young.ums.model.LGLog;
import com.yy.young.ums.service.IUmsLGLogService;
import com.yy.young.ums.service.IUmsUserService;
import com.yy.young.ums.util.UmsConstants;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

/**
 * 登录日志服务实现
 * Created by rookie on 2017/9/28.
 */
@Service("umsLGLogService")
public class UmsLGLogServiceImpl implements IUmsLGLogService {

    @Resource(name="dataAccessService")
    IDataAccessService dataService;

    @Resource(name="umsUserService")
    IUmsUserService umsUserService;

    @Override
    public List<LGLog> getLGLogPage(LGLog lgLog, Page page) throws Exception {
        return dataService.getList(UmsConstants.MAPPER.UMS_LGLOG + ".getLGLog", lgLog, page);
    }

    @Override
    public LGLog getLGLog(String id) throws Exception {
        return (LGLog)dataService.getObject(UmsConstants.MAPPER.UMS_LGLOG + ".getLGLogById", id);
    }

    @Override
    public int updateLGLog(LGLog lgLog) throws Exception {
        if (lgLog == null){
            return 0;
        }
        return dataService.update(UmsConstants.MAPPER.UMS_LGLOG + ".updateLGLog", lgLog);
    }

    @Override
    public int deleteLGLog(String[] idsArr) throws Exception {
        if (idsArr == null){
            return 0;
        }
        return dataService.delete(UmsConstants.MAPPER.UMS_LGLOG + ".deleteLGLog", idsArr);
    }

    @Override
    public int insertLGLog(LGLog lgLog) throws Exception {
        if (lgLog == null){
            return 0;
        }
        if (StringUtils.isBlank(lgLog.getId())){
            lgLog.setId(CommonUtil.getUUID());
        }
        if (lgLog.getLoginTime() == null){
            lgLog.setLoginTime(new Date());
        }
        return dataService.insert(UmsConstants.MAPPER.UMS_LGLOG + ".insertLGLog", lgLog);
    }

    @Override
    public List<LGLog> getLGLogByAccount(String account) throws Exception {
        return dataService.getList(UmsConstants.MAPPER.UMS_LGLOG + ".getLGLogByAccount", account);
    }

    @Override
    public List<LGLog> getLGLogByUserId(String userId) throws Exception {
        User user = umsUserService.getUserById(userId);
        return dataService.getList(UmsConstants.MAPPER.UMS_LGLOG + ".getLGLogByAccount", user.getAccount());
    }

    @Override
    public int clearLGLog() throws Exception {
        return dataService.delete(UmsConstants.MAPPER.UMS_LGLOG + ".clearLGLog", null);
    }

    @Override
    public void writeLGLog(String account, String result, String remark, HttpServletRequest request) {
        LGLog loLog = new LGLog();
        loLog.setId(CommonUtil.getUUID());
        loLog.setAccount(account);
        loLog.setLoginTime(new Date());
        loLog.setResult(result);
        if (request != null){
            loLog.setClientIp(ClientInfoUtil.getClientIpAddr(request));
        }
        loLog.setServerIp(CommonUtil.getLocalIp());
        loLog.setRemark(remark);
        try {
            this.insertLGLog(loLog);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int countUserLoginNum(User user) throws Exception {
        if (user != null && user.getAccount() != null){//账号有效则按照账号查找
            return (Integer)dataService.getObject(UmsConstants.MAPPER.UMS_LGLOG + ".countUserLoginNum", user.getAccount());
        }else if (user != null && user.getId() != null){//账号无效时先根据用户id查询账号,再根据账号查找
            User user2 = umsUserService.getUserById(user.getId());
            if (user2 != null){
                return (Integer)dataService.getObject(UmsConstants.MAPPER.UMS_LGLOG + ".countUserLoginNum", user2.getAccount());
            }
        }
        return 0;
    }

    @Override
    public Date getUserLastLoginTime(User user) throws Exception {
        LGLog log = this.getUserLastLoginLog(user);
        if (log != null){
            return log.getLoginTime();
        }
        return null;
    }

    @Override
    public LGLog getUserLastLoginLog(User user) throws Exception {
        LGLog log = new LGLog();
        //验证账号有效,账号无效则根据id查询用户信息
        if (user != null && user.getAccount() != null){
            log.setAccount(user.getAccount());
        }else if (user != null && user.getId() != null){
            User user2 = umsUserService.getUserById(user.getId());
            if (user2 != null){
                log.setAccount(user2.getAccount());
            }
        }
        if (log.getAccount() != null){
            List<LGLog> list = dataService.getList(UmsConstants.MAPPER.UMS_LGLOG + ".getLGLogByAccount", log.getAccount(), new Page(1,2));
            //查询前2条,因为是时间倒序,所以第一条就是最后一次登录的日志,第2条是上一次登录的日志
            if (list != null && list.size() > 1){
                return list.get(1);//返回第二条
            }
        }
        return null;
    }

    @Override
    public LGLog getUserFinalLoginLog(User user) throws Exception {
        LGLog log = new LGLog();
        //验证账号有效,账号无效则根据id查询用户信息
        if (user != null && user.getAccount() != null){
            log.setAccount(user.getAccount());
        }else if (user != null && user.getId() != null){
            User user2 = umsUserService.getUserById(user.getId());
            if (user2 != null){
                log.setAccount(user2.getAccount());
            }
        }
        if (log.getAccount() != null){
            List<LGLog> list = dataService.getList(UmsConstants.MAPPER.UMS_LGLOG + ".getLGLogByAccount", log.getAccount(), new Page(1,2));
            //查询前2条,因为是时间倒序,所以第一条就是最后一次登录的日志,第2条是上一次登录的日志
            if (list != null && list.size() > 0){
                return list.get(0);//返回第一条
            }
        }
        return null;
    }
}
