package com.yy.young.ums.service;

import com.yy.young.dal.util.Page;
import com.yy.young.interfaces.ums.model.Dept;

import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/5/8.
 */
public interface IUmsDeptService {
    /**
     * 获取部门列表
     * @param parameter
     * @return
     * @throws Exception
     */
    List<Map<String,Object>> getDeptList(Map<String, Object> parameter) throws Exception;

    /**
     * 获取部门(分页)
     * @param parameter
     * @param page
     * @return
     * @throws Exception
     */
    List<Map<String,Object>> getDeptPage(Map<String, Object> parameter, Page page) throws Exception;

    /**
     * 修改部门信息
     * @param parameter
     * @return
     * @throws Exception
     */
    int updateDept(Map<String, Object> parameter) throws Exception;

    /**
     * 删除部门信息
     * @param parameter
     * @return
     * @throws Exception
     */
    int deleteDept(Map<String, Object> parameter) throws Exception;

    /**
     * 插入部门信息
     * @param parameter
     * @return
     * @throws Exception
     */
    int insertDept(Map<String, Object> parameter) throws Exception;

    /**
     * 根据用户ID获取部门列表
     * @param userId
     * @return
     * @throws Exception
     */
    List<Map<String,Object>> getDeptListByUserId(String userId) throws Exception;

    /**
     * 从管理范围数组中获取最大管理范围
     * @param rangeArr
     * @return
     * @throws Exception
     */
    String getMaxRangeId(String[] rangeArr) throws Exception;

    /**
     * 根据编号查询部门信息
     * @param id
     * @return
     * @throws Exception
     */
    Dept getDeptById(String id) throws Exception;
}
