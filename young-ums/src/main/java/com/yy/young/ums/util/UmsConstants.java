package com.yy.young.ums.util;

import com.yy.young.common.config.DomainHandler;
import com.yy.young.common.util.StringUtils;

/**
 * 统一用户服务端常量
 * Created by Administrator on 2017/5/8.
 */
public class UmsConstants {

    /**
     * 与文件系统交互常量
     */
    public interface FS{
        String FS_TRANSFER_URI = "fs/file/transferFile.action";//文件传输地址
        String FS_FOLDERID_HEADER = "header";//头像文件夹id
    }

    /**
     * ums配置状态枚举项
     */
    public interface CONFIG_STATE{
        String YES = "1";//启用
        String NO = "0";//禁用
    }

    /**
     * ums配置项的key,根据key值可以取到对应配置
     */
    public interface CONFIG_KEY{
        String SSO_COOKIE_DOMAIN = "SSO_COOKIE_DOMAIN";//单点cookie域配置key值,优先根据此key取域配置项,取不到就使用下面的SSO.DEFAULT_SSO_COOKIE_DOMAIN
        String FS_TRANSFER_URL = "FS_TRANSFER_URL";//文件传输地址配置key
        String SIGN_TOKEN_CHAR = "SIGN_TOKEN_CHAR";//单点token生成时的干扰符
        String LOGIN_IP_BLACK_LIST = "LOGIN_IP_BLACK_LIST";//登录黑名单
    }


    /**
     * 字典值
     */
    public interface DICTIONARY{
        String DEPT_TYPE_COMPANY = "COMPANY";//部门类型为单位
        String DEPT_TYPE_DEPT = "DEPT";//部门类型为部门
        String DEPT_TYPE_OTHER = "OTHER";//部门类型为其他
        String ROLE_TYPE_ADMIN = "ADMIN";//管理员角色
        String ROLE_TYPE_NORMAL = "NORMAL";//普通角色
    }

    /**
     * 默认值
     */
    public interface DEFAULT{
        String ROOT_DEPT = "ROOT";//部门默认根节点
        String NOT_COMPANY = "NOT_COMPANY";//无单位用户默认使用的单位id
        String ROOT_NAVIGATE = "ROOT";//资源默认根节点
        String USER_PASSWORD = "123456";//默认初始密码
    }

    /**
     * sql映射
     */
    public interface MAPPER{
        String UMS_USER = "com.yy.young.ums.mapper.user";//用户
        String UMS_DEPT = "com.yy.young.ums.mapper.dept";//部门
        String UMS_DEPT_USER = "com.yy.young.ums.mapper.deptUser";//部门用户
        String UMS_ROLE = "com.yy.young.ums.mapper.role";//角色
        String UMS_ROLE_USER = "com.yy.young.ums.mapper.roleUser";//角色用户
        String UMS_NAVIGATE = "com.yy.young.ums.mapper.navigate";//资源
        String UMS_APP = "com.yy.young.ums.mapper.app";//应用系统
        String UMS_OUT = "com.yy.young.ums.mapper.out";//对外开放的服务
        String UMS_LGLOG = "com.yy.young.ums.mapper.lglog";//登录日志
        String UMS_CONFIG = "com.yy.young.ums.mapper.config";//登录日志
    }

    /**
     * 单点登录配置
     */
    public interface SSO{
        String DEFAULT_SSO_COOKIE_DOMAIN = "young.com";//token令牌cookie的域
        String SIGN_TOKEN_CHAR = "!";//生成token时的干扰符
        String TOKEN_KEY = "_token";//用户登录信任串的key
        String SSO_LOGIN = "/page/login.jsp";//平台单点登录页面地址,和重定向的差别在于多了个/
        String REDIRECT_LOGIN = "page/login.jsp";//重定向到的平台登录页
        String REDIRECT_INDEX_URI_DEF = "portal/";//重定向的默认平台门户首页,page/portal/index.jsp
    }

}
