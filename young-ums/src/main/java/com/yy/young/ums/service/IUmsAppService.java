package com.yy.young.ums.service;

import com.yy.young.dal.util.Page;
import com.yy.young.ums.model.ApplicationSystem;

import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/5/8.
 */
public interface IUmsAppService {
    /**
     * 获取应用系统列表
     * @param app
     * @return
     * @throws Exception
     */
    List<ApplicationSystem> getAppList(ApplicationSystem app) throws Exception;

    /**
     * 获取应用系统(分页)
     * @param app
     * @param page
     * @return
     * @throws Exception
     */
    List<ApplicationSystem> getAppPage(ApplicationSystem app, Page page) throws Exception;

    /**
     * 修改应用系统信息
     * @param app
     * @return
     * @throws Exception
     */
    int updateApp(ApplicationSystem app) throws Exception;

    /**
     * 删除应用系统信息
     * @param parameter
     * @return
     * @throws Exception
     */
    int deleteApp(Map<String, Object> parameter) throws Exception;

    /**
     * 插入应用系统信息
     * @param app
     * @return
     * @throws Exception
     */
    int insertApp(ApplicationSystem app) throws Exception;
    
}
