package com.yy.young.ums.web;

import com.yy.young.base.exception.YoungBaseException;
import com.yy.young.common.util.CommonUtil;
import com.yy.young.common.util.Result;
import com.yy.young.common.util.StringUtils;
import com.yy.young.dal.util.Page;
import com.yy.young.interfaces.log.annotation.Log;
import com.yy.young.interfaces.model.User;
import com.yy.young.ums.service.IUmsRoleService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/5/8.
 */
@Controller
@RequestMapping("/ums/role")
public class UmsRoleController {

    @Resource(name="umsRoleService")
    IUmsRoleService RoleService;

    /**
     * 获取角色列表
     * @param request
     * @return
     * @throws Exception
     */
    @Log("查询角色列表")
    @RequestMapping("/getRoleList")
    @ResponseBody
    public Object getRoleList(HttpServletRequest request) throws Exception{
        Map<String,Object> parameter = CommonUtil.getParameterFromRequest(request);
        List<Map<String,Object>> list = RoleService.getRoleList(parameter);
        return new Result(list);
    }

    /**
     * 获取角色列表(分页)
     * @param request
     * @return
     * @throws Exception
     */
    @Log("分页查询角色列表")
    @RequestMapping("/getRolePage")
    @ResponseBody
    public Object getRolePage(HttpServletRequest request) throws Exception{
        Map<String,Object> parameter = CommonUtil.getParameterFromRequest(request);
        Page page = CommonUtil.getPageFromRequest(request);
        List<Map<String,Object>> list = RoleService.getRolePage(parameter, page);
        page.setData(list);
        return page;
    }

    /**
     * 添加角色
     * @param request
     * @return
     * @throws Exception
     */
    @Log("添加角色")
    @RequestMapping("/insertRole")
    @ResponseBody
    public Object insertRole(HttpServletRequest request) throws Exception{
        Map<String,Object> parameter = CommonUtil.getParameterFromRequest(request);
        User user = CommonUtil.getLoginUser(request);
        Result result = new Result();
        if(user == null){
            result.setCode(-1);
            result.setInfo("登录信息获取失败,请重新登录后重试!");
        }else if(StringUtils.isBlank(parameter.get("COMPANY_ID")+"")){
            result.setCode(-1);
            result.setInfo("所属单位不允许为空!");
        }else{
            int num = RoleService.insertRole(parameter, user);
            if(num < 0){
                result.setCode(-1);
                result.setInfo("添加角色失败!");
            }
        }
        return result;
    }

    /**
     * 修改角色信息
     * @param request
     * @return
     * @throws Exception
     */
    @Log("修改角色")
    @RequestMapping("/updateRole")
    @ResponseBody
    public Object updateRole(HttpServletRequest request) throws Exception{
        Map<String,Object> parameter = CommonUtil.getParameterFromRequest(request);
        User user = CommonUtil.getLoginUser(request);
        Result result = new Result();
        if(user == null){
            result.setCode(-1);
            result.setInfo("登录信息获取失败,请重新登录后重试!");
        }else{
            int num = RoleService.updateRole(parameter, user);
            if(num < 0){
                result.setCode(-1);
                result.setInfo("修改角色信息失败!");
            }
        }
        return result;
    }

    /**
     * 删除角色信息
     * @param request
     * @return
     * @throws Exception
     */
    @Log("删除角色")
    @RequestMapping("/deleteRole")
    @ResponseBody
    public Object deleteRole(HttpServletRequest request) throws Exception{
        Map<String,Object> parameter = CommonUtil.getParameterFromRequest(request);
        Result result = new Result();
        int num = RoleService.deleteRole(parameter);
        if(num < 0){
            result.setCode(-1);
            result.setInfo("删除角色信息失败!");
        }
        return result;
    }

    /**
     * 批量关联用户到角色(1个角色添加多个用户关联)
     * @param request
     * @return
     * @throws Exception
     */
    @Log("批量关联用户(N)到角色(1)")
    @RequestMapping("/batchInsertUser2Role")
    @ResponseBody
    public Object batchInsertUser2Role(HttpServletRequest request) throws Exception{
        Map<String,Object> parameter = CommonUtil.getParameterFromRequest(request);
        Result result = new Result();
        if(!parameter.containsKey("roleId") || !parameter.containsKey("userId")){
            result.setCode(-1);
            result.setInfo("操作失败:角色id和用户id不允许为空!");
        }else{
            int num = RoleService.batchInsertUser2Role(parameter);
            if(num < 0){
                result.setCode(-1);
                result.setInfo("添加角色用户失败!");
            }
        }
        return result;
    }

    /**
     * 批量关联资源到角色
     * @param request
     * @return
     * @throws Exception
     */
    @Log("批量关联资源(N)到角色(1)")
    @RequestMapping("/batchInsertNavigate2Role")
    @ResponseBody
    public Object batchInsertNavigate2Role(HttpServletRequest request) throws Exception{
        Map<String,Object> parameter = CommonUtil.getParameterFromRequest(request);
        Result result = new Result();
        if(!parameter.containsKey("roleId")){
            result.setCode(-1);
            result.setInfo("操作失败:角色id不允许为空!");
        }else{
            int num = RoleService.batchInsertNavigate2Role(parameter);
            if(num < 0){
                result.setCode(-1);
                result.setInfo("添加角色资源失败!");
            }
        }
        return result;
    }

    /**
     * 批量关联用户和角色(N-N)
     * @param request
     * @return
     * @throws Exception
     */
    @Log("批量关联用户(N)和角色(N)")
    @RequestMapping("/batchMakeUserAndRole")
    @ResponseBody
    public Object batchMakeUserAndRole(HttpServletRequest request) throws Exception{
        Map<String,Object> parameter = CommonUtil.getParameterFromRequest(request);
        Result result = new Result();
        if(!parameter.containsKey("roleId") || !parameter.containsKey("userId")){
            result.setCode(-1);
            result.setInfo("操作失败:角色id和用户id不允许为空!");
        }else{
            int num = RoleService.batchMakeUserAndRole(parameter.get("userId").toString().split(","), parameter.get("roleId").toString().split(","));
            if(num < 0){
                result.setCode(-1);
                result.setInfo("添加角色用户失败!");
            }
        }
        return result;
    }
}
