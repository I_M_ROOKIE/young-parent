package com.yy.young.ums.service;

import com.yy.young.dal.util.Page;
import com.yy.young.ums.model.Config;
import java.util.List;

/**
 * 统一用户系统配置服务接口
 * Created by rookie on 2018-01-27.
 */
public interface IUmsConfigService {

    /**
     * 获取数据列表
     * @param config
     * @return
     * @throws Exception
     */
    List<Config> getConfigList(Config config) throws Exception;

    /**
     * 获取数据列表(分页)
     * @param config
     * @param page 分页参数
     * @return
     * @throws Exception
     */
    List<Config> getConfigPage(Config config, Page page) throws Exception;

    /**
     * 根据id获取信息
     * @param id
     * @return
     * @throws Exception
     */
    Config getConfig(String id) throws Exception;

    /**
     * 根据key获取信息
     * @param key
     * @return
     * @throws Exception
     */
    Config getConfigByKey(String key) throws Exception;

    /**
     * 根据key获取配置值(已启用的)
     * @param key
     * @return
     * @throws Exception
     */
    String getEnabledConfigValue(String key);

    /**
     * 修改
     * @param config
     * @return
     * @throws Exception
     */
    int updateConfig(Config config) throws Exception;

    /**
     * 批量删除
     * @param idArr id数组
     * @return
     * @throws Exception
     */
    int deleteConfig(String[] idArr) throws Exception;

    /**
     * 删除单条
     * @param id
     * @return
     * @throws Exception
     */
    int deleteConfig(String id) throws Exception;

    /**
     * 插入
     * @param config
     * @return
     * @throws Exception
     */
    int insertConfig(Config config) throws Exception;

}