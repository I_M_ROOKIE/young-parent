package com.yy.young.ums.model;


import java.util.Date;

/**
 * 登录日志
 * Created by rookie on 2017/9/28.
 */
public class LGLog implements java.io.Serializable {

    private String id;//主键
    private String account;//账号
    private String result;//登录结果
    private Date loginTime;//登录时间
    private String clientIp;//客户端ip
    private String serverIp;//服务器ip
    private String remark;//备注

    private Date loginTime1;//登录时间起始
    private Date loginTime2;//登录时间截止

    public LGLog() {
        super();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Date getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Date loginTime) {
        this.loginTime = loginTime;
    }

    public String getClientIp() {
        return clientIp;
    }

    public void setClientIp(String clientIp) {
        this.clientIp = clientIp;
    }

    public String getServerIp() {
        return serverIp;
    }

    public void setServerIp(String serverIp) {
        this.serverIp = serverIp;
    }

    public Date getLoginTime1() {
        return loginTime1;
    }

    public void setLoginTime1(Date loginTime1) {
        this.loginTime1 = loginTime1;
    }

    public Date getLoginTime2() {
        return loginTime2;
    }

    public void setLoginTime2(Date loginTime2) {
        this.loginTime2 = loginTime2;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
