package com.yy.young.ums.service;

import com.yy.young.dal.util.Page;
import com.yy.young.interfaces.model.User;
import com.yy.young.interfaces.ums.model.Role;

import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/5/8.
 */
public interface IUmsRoleService {
    /**
     * 获取角色列表
     * @param parameter
     * @return
     * @throws Exception
     */
    List<Map<String,Object>> getRoleList(Map<String, Object> parameter) throws Exception;

    /**
     * 获取角色(分页)
     * @param parameter
     * @param page
     * @return
     * @throws Exception
     */
    List<Map<String,Object>> getRolePage(Map<String, Object> parameter, Page page) throws Exception;

    /**
     * 根据单位id获取角色列表
     * @param companyId
     * @return
     * @throws Exception
     */
    List<Map<String,Object>> getRoleListByCompanyId(String companyId) throws Exception;

    /**
     * 修改角色信息
     * @param parameter
     * @return
     * @throws Exception
     */
    int updateRole(Map<String, Object> parameter, User user) throws Exception;

    /**
     * 删除角色信息
     * @param parameter
     * @return
     * @throws Exception
     */
    int deleteRole(Map<String, Object> parameter) throws Exception;

    /**
     * 插入角色信息
     * @param parameter
     * @return
     * @throws Exception
     */
    int insertRole(Map<String, Object> parameter, User user) throws Exception;

    /**
     * 批量关联角色用户
     * @param parameter
     * @return
     * @throws Exception
     */
    int batchInsertUser2Role(Map<String, Object> parameter) throws Exception;

    /**
     * 批量关联资源到角色(单角色多资源关联)
     * @param parameter
     * @return
     * @throws Exception
     */
    int batchInsertNavigate2Role(Map<String, Object> parameter) throws Exception;

    /**
     * 根据用户id获取角色列表
     * @param userId
     * @return
     * @throws Exception
     */
    List<Map<String,Object>> getRoleListByUserId(String userId) throws Exception;

    /**
     * 删除角色拥有的权限
     * @param roleId
     * @return
     * @throws Exception
     */
    int deleteAuthByRole(String roleId) throws Exception;

    /**
     * 根据角色删除用户角色关联关系
     * @param roleId
     * @return
     * @throws Exception
     */
    int deleteUserByRole(String roleId) throws Exception;

    /**
     * 批量关联用户和角色,N-N
     * @param userIds
     * @param roleIds
     * @return
     * @throws Exception
     */
    int batchMakeUserAndRole(String[] userIds, String[] roleIds) throws Exception;

    /**
     * 根据角色id获取角色信息
     * @param roleId
     * @return
     * @throws Exception
     */
    Role getRoleById(String roleId) throws Exception;
}
