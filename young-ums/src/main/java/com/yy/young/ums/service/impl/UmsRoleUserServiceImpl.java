package com.yy.young.ums.service.impl;

import com.yy.young.dal.service.IDataAccessService;
import com.yy.young.dal.util.Page;
import com.yy.young.ums.service.IUmsRoleUserService;
import com.yy.young.ums.util.UmsConstants;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * Created by rookie on 2017/6/29.
 */
@Service("umsRoleUserService")
public class UmsRoleUserServiceImpl implements IUmsRoleUserService {

    @Resource(name="dataAccessService")
    IDataAccessService dataService;

    @Override
    public List<Map<String, Object>> getRoleUserList(Map<String, Object> parameter) throws Exception {
        return dataService.getList(UmsConstants.MAPPER.UMS_ROLE_USER+".getRoleUser",parameter);
    }

    @Override
    public List<Map<String, Object>> getRoleUserPage(Map<String, Object> parameter, Page page) throws Exception {
        return dataService.getList(UmsConstants.MAPPER.UMS_ROLE_USER+".getRoleUser", parameter, page);
    }

    //批量删除
    @Override
    public int deleteRoleUser(String[] idArr) throws Exception {
        int i = 0;
        for(String id : idArr){
            i += this.deleteRoleUser(id);
        }
        return i;
    }

    //删除单条
    @Override
    public int deleteRoleUser(String id) throws Exception {
        return dataService.delete(UmsConstants.MAPPER.UMS_ROLE_USER + ".delete", id);
    }

    //删除用户相关的关联关系
    @Override
    public int deleteRoleUserByUserId(String userId) throws Exception {
        return dataService.delete(UmsConstants.MAPPER.UMS_ROLE_USER + ".deleteRoleUserByUserId", userId);
    }
}
