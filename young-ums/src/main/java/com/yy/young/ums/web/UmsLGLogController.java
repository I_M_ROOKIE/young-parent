package com.yy.young.ums.web;

import com.yy.young.common.util.CommonUtil;
import com.yy.young.common.util.Result;
import com.yy.young.common.util.StringUtils;
import com.yy.young.dal.util.Page;
import com.yy.young.interfaces.log.annotation.Log;
import com.yy.young.interfaces.model.User;
import com.yy.young.ums.model.LGLog;
import com.yy.young.ums.service.IUmsLGLogService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * 登录日志
 * Created by Administrator on 2017/5/8.
 */
@Controller
@RequestMapping("/ums/lgLog")
public class UmsLGLogController {

    @Resource(name="umsLGLogService")
    IUmsLGLogService umsLGLogService;

    /**
     * 获取登录日志列表(分页)
     * @param lgLog
     * @param request
     * @return
     * @throws Exception
     */
    @Log("查询登录日志")
    @RequestMapping("/getLGLogPage")
    @ResponseBody
    public Object getLGLogPage(LGLog lgLog, HttpServletRequest request) throws Exception{
        Page page = CommonUtil.getPageFromRequest(request);
        List<LGLog> list = umsLGLogService.getLGLogPage(lgLog, page);
        page.setData(list);
        return page;
    }

    /**
     * 根据id查询登录日志
     * @param lgLog
     * @param request
     * @return
     * @throws Exception
     */
    @Log("分页查询登录日志")
    @RequestMapping("/getLGLogById")
    @ResponseBody
    public Object getLGLogById(LGLog lgLog, HttpServletRequest request) throws Exception{

        return new Result(umsLGLogService.getLGLog(lgLog.getId()));
    }

    /**
     * 查询我的登录日志
     * @param lgLog
     * @param request
     * @return
     * @throws Exception
     */
    @Log("查询我的登录日志")
    @RequestMapping("/getMyLGLogPage")
    @ResponseBody
    public Object getMyLGLog(LGLog lgLog, HttpServletRequest request) throws Exception{
        Page page = CommonUtil.getPageFromRequest(request);
        User user = CommonUtil.getLoginUser(request);
        if (user != null && StringUtils.isNotBlank(user.getAccount())){
            lgLog.setAccount(user.getAccount());
            List<LGLog> list = umsLGLogService.getLGLogPage(lgLog, page);
            page.setData(list);
        }else{
            page.setCode(-1);
            page.setInfo("获取用户信息失败:请登录重试!");
        }

        return page;
    }

    /**
     * 添加登录日志
     * @param lgLog
     * @param request
     * @return
     * @throws Exception
     */
    @Log("添加登录日志")
    @RequestMapping("/insertLGLog")
    @ResponseBody
    public Object insertLGLog(LGLog lgLog, HttpServletRequest request) throws Exception{
        Result result = new Result();
        User user = CommonUtil.getLoginUser(request);
        if (user != null && StringUtils.isNotBlank(user.getAccount())){
            lgLog.setAccount(user.getAccount());
            umsLGLogService.insertLGLog(lgLog);
        }else{
            result.setCode(-1);
            result.setInfo("操作失败:用户信息无效!");
        }
        return result;
    }

    /**
     * 修改登录日志信息
     * @param lgLog
     * @param request
     * @return
     * @throws Exception
     */
    @Log("修改登录日志")
    @RequestMapping("/updateLGLog")
    @ResponseBody
    public Object updateLGLog(LGLog lgLog, HttpServletRequest request) throws Exception{
        int num = umsLGLogService.updateLGLog(lgLog);
        Result result = new Result();
        if(num < 0){
            result.setCode(-1);
            result.setInfo("修改登录日志信息失败!");
        }
        return result;
    }

    /**
     * 删除登录日志信息
     * @param request
     * @return
     * @throws Exception
     */
    @Log("删除登录日志")
    @RequestMapping("/deleteLGLog")
    @ResponseBody
    public Object deleteLGLog(HttpServletRequest request) throws Exception{
        Map<String,Object> parameter = CommonUtil.getParameterFromRequest(request);
        String idsStr = parameter.get("ids")+"";
        String[] idArr;
        Result result = new Result();
        if(StringUtils.isNotBlank(idsStr)) {
            idArr = idsStr.split(",");
            int num = umsLGLogService.deleteLGLog(idArr);
            if(num < 0){
                result.setCode(-1);
                result.setInfo("删除登录日志信息失败!");
            }
        }else{
            result.setCode(-1);
            result.setInfo("删除失败:待删除日志ID无效!");
        }
        return result;
    }


}
