package com.yy.young.ums.service.impl;

import com.yy.young.common.util.StringUtils;
import com.yy.young.interfaces.model.User;
import com.yy.young.interfaces.ums.model.Dept;
import com.yy.young.interfaces.ums.model.Navigate;
import com.yy.young.interfaces.ums.model.Role;
import com.yy.young.interfaces.ums.model.SsoVerifyDTO;
import com.yy.young.interfaces.ums.service.IUmsOutService;
import com.yy.young.ums.model.LGLog;
import com.yy.young.ums.service.*;
import com.yy.young.ums.util.SecureHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * Created by rookie on 2017/5/11.
 * 统一用户对外开放的服务
 */
public class UmsOutServiceImpl implements IUmsOutService {

    @Resource(name="umsNavigateService")
    private IUmsNavigateService navigateService;

    @Resource(name="umsUserService")
    IUmsUserService userService;

    @Resource(name="umsLGLogService")
    IUmsLGLogService umsLGLogService;

    @Resource(name="umsDeptService")
    IUmsDeptService DeptService;

    @Resource(name="umsRoleService")
    IUmsRoleService RoleService;

    private static final Logger logger = LoggerFactory.getLogger(UmsOutServiceImpl.class);

    @Override
    public List<Navigate> getNavigateByUserId(String userId) throws Exception {
        return navigateService.getNavigateByUserId(userId);
    }

    @Override
    public List<Navigate> getNavigateAsTreeByUserId(String userId) throws Exception {
        return navigateService.getNavigateAsTreeByUserId(userId);
    }

    @Override
    public User getLoginUserById(String userId) throws Exception {
        User user = userService.getUserById(userId);
        if(user != null){
            user.setPassword("");//置空密码
            userService.perfectLoginUserInfo(user);
        }
        return user;
    }

    @Override
    public SsoVerifyDTO verifySSO(String token) throws Exception {
        //令牌非空
        if (StringUtils.isBlank(token)){
            return SsoVerifyDTO.getFailInstance(token, "令牌无效");
        }
        //令牌有效性验证
        if (!SecureHandler.verifyToken(token)){
            return SsoVerifyDTO.getFailInstance(token, "令牌无效");
        }
        String userId = SecureHandler.getUserIdFromToken(token);
        return SsoVerifyDTO.getSuccessInstance(token, this.getLoginUserById(userId));
    }

    //查询用户上一次登录时间
    @Override
    public Date getLastLoginTime(String userId) throws Exception {
        if (StringUtils.isNotBlank(userId)){
            User user = new User();
            user.setId(userId);
            LGLog lgLog = umsLGLogService.getUserLastLoginLog(user);
            if (lgLog != null){
                return lgLog.getLoginTime();
            }
        }
        return null;
    }

    @Override
    public Dept getDeptById(String deptId) throws Exception {
        return DeptService.getDeptById(deptId);
    }

    @Override
    public Role getRoleById(String roleId) throws Exception {
        return RoleService.getRoleById(roleId);
    }

    //判断用户id或account是否被使用
    @Override
    public boolean existUserIdOrAccount(String userIdOrAccount) throws Exception {
        return userService.existUserIdOrAccount(userIdOrAccount);
    }

    //插入用户
    @Override
    public boolean insertUser(User user) throws Exception {
        //id/account/companyId不允许为空
        if (StringUtils.isBlank(user.getId()) || StringUtils.isBlank(user.getAccount()) || StringUtils.isBlank(user.getCompanyId())){
            logger.warn("[UMS对外服务--新增用户] 失败: 用户编号/账号/单位ID为空. {}", user);
            return false;
        }
        int i = userService.insertUser(user);
        if (i > 0){
            return true;
        }
        return false;
    }

    //修改用户
    @Override
    public int updateUser(User user) throws Exception {
        if (StringUtils.isBlank(user.getId())){
            logger.warn("[UMS对外服务--修改用户信息] 失败: 用户编号为空. {}", user);
            return -1;
        }
        return userService.updateUser(user);
    }

    //删除用户
    @Override
    public int deleteUser(String userId) throws Exception {
        if (StringUtils.isBlank(userId)){
            logger.warn("[UMS对外服务--删除用户] 失败: 用户编号为空. {}", userId);
            return -1;
        }
        return userService.deleteUser(userId);
    }
}
