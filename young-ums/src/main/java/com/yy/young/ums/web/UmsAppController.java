package com.yy.young.ums.web;

import com.yy.young.common.util.CommonUtil;
import com.yy.young.common.util.Result;
import com.yy.young.dal.util.Page;
import com.yy.young.interfaces.log.annotation.Log;
import com.yy.young.ums.model.ApplicationSystem;
import com.yy.young.ums.service.IUmsAppService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/5/8.
 */
@Controller
@RequestMapping("/ums/app")
public class UmsAppController {

    @Resource(name="umsAppService")
    IUmsAppService AppService;

    /**
     * 获取应用系统列表
     * @param app
     * @param request
     * @return
     * @throws Exception
     */
    @Log("查询应用系统列表")
    @RequestMapping("/getAppList")
    @ResponseBody
    public Object getAppList(ApplicationSystem app, HttpServletRequest request) throws Exception{
        List<ApplicationSystem> list = AppService.getAppList(app);
        return new Result(list);
    }

    /**
     * 获取应用系统列表(分页)
     * @param app
     * @param request
     * @return
     * @throws Exception
     */
    @Log("分页查询应用系统列表")
    @RequestMapping("/getAppPage")
    @ResponseBody
    public Object getAppPage(ApplicationSystem app, HttpServletRequest request) throws Exception{
        Page page = CommonUtil.getPageFromRequest(request);
        List<ApplicationSystem> list = AppService.getAppPage(app, page);
        page.setData(list);
        return page;
    }

    /**
     * 添加应用系统
     * @param app
     * @param request
     * @return
     * @throws Exception
     */
    @Log("添加应用系统")
    @RequestMapping("/insertApp")
    @ResponseBody
    public Object insertApp(ApplicationSystem app, HttpServletRequest request) throws Exception{
        int num = AppService.insertApp(app);
        Result result = new Result();
        if(num < 0){
            result.setCode(-1);
            result.setInfo("添加应用系统失败!");
        }
        return result;
    }

    /**
     * 修改应用系统信息
     * @param app
     * @param request
     * @return
     * @throws Exception
     */
    @Log("修改应用系统")
    @RequestMapping("/updateApp")
    @ResponseBody
    public Object updateApp(ApplicationSystem app, HttpServletRequest request) throws Exception{
        int num = AppService.updateApp(app);
        Result result = new Result();
        if(num < 0){
            result.setCode(-1);
            result.setInfo("修改应用系统信息失败!");
        }
        return result;
    }

    /**
     * 删除应用系统信息
     * @param request
     * @return
     * @throws Exception
     */
    @Log("删除应用系统")
    @RequestMapping("/deleteApp")
    @ResponseBody
    public Object deleteApp(HttpServletRequest request) throws Exception{
        Map<String,Object> parameter = CommonUtil.getParameterFromRequest(request);
        int num = AppService.deleteApp(parameter);
        Result result = new Result();
        if(num < 0){
            result.setCode(-1);
            result.setInfo("删除应用系统信息失败!");
        }
        return result;
    }


}
