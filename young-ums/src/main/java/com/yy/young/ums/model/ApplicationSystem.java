package com.yy.young.ums.model;

import java.util.Date;

/**
 * 应用系统实体类
 * Created by rookie on 2017/6/28.
 */
public class ApplicationSystem {

    private String id;//应用系统代号
    private String name;//应用系统名称
    private Date createTime;//应用系统接入时间
    private Date updateTime;//最新改动时间
    private String urlIndex;//应用系统首页地址
    private Integer num;//显示顺序
    private String remark;//备注

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUrlIndex() {
        return urlIndex;
    }

    public void setUrlIndex(String urlIndex) {
        this.urlIndex = urlIndex;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
