package com.yy.young.ums.service;

import com.yy.young.dal.util.Page;
import com.yy.young.interfaces.model.User;
import com.yy.young.ums.model.LGLog;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

/**
 * 登录日志服务
 * Created by rookie on 2017/9/28.
 */
public interface IUmsLGLogService {
    /**
     * 获取日志(分页)
     * @param lgLog
     * @param page
     * @return
     * @throws Exception
     */
    List<LGLog> getLGLogPage(LGLog lgLog, Page page) throws Exception;

    /**
     * 查询日志
     * @param id
     * @return
     * @throws Exception
     */
    LGLog getLGLog(String id) throws Exception;

    /**
     * 修改日志信息
     * @param lgLog
     * @return
     * @throws Exception
     */
    int updateLGLog(LGLog lgLog) throws Exception;

    /**
     * 删除日志信息
     * @param idsArr
     * @return
     * @throws Exception
     */
    int deleteLGLog(String[] idsArr) throws Exception;

    /**
     * 插入日志信息
     * @param lgLog
     * @return
     * @throws Exception
     */
    int insertLGLog(LGLog lgLog) throws Exception;

    /**
     * 根据账号查询所有登录日志
     * @param account
     * @return
     * @throws Exception
     */
    List<LGLog> getLGLogByAccount(String account) throws Exception;

    /**
     * 根据用户id查询所有登录日志
     * @param userId
     * @return
     * @throws Exception
     */
    List<LGLog> getLGLogByUserId(String userId) throws Exception;

    /**
     * 清空登录日志
     * @return
     * @throws Exception
     */
    int clearLGLog() throws Exception;

    /**
     * 写入登录日志
     * @param account
     * @param result
     * @param remark
     * @param request
     */
    void writeLGLog(String account, String result, String remark, HttpServletRequest request);

    /**
     * 计算用户登录次数
     * @param user
     * @return
     * @throws Exception
     */
    int countUserLoginNum(User user) throws Exception;

    /**
     * 获取用户最后一次登录系统的时间
     * @param user
     * @return
     * @throws Exception
     */
    Date getUserLastLoginTime(User user) throws Exception;

    /**
     * 获取用户上一次的登录日志
     * @param user
     * @return
     * @throws Exception
     */
    LGLog getUserLastLoginLog(User user) throws Exception;

    /**
     * 获取用户最后一次的登录日志
     * @param user
     * @return
     * @throws Exception
     */
    LGLog getUserFinalLoginLog(User user) throws Exception;
}
