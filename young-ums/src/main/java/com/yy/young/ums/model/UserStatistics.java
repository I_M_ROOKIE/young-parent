package com.yy.young.ums.model;

import com.yy.young.interfaces.model.User;

import java.util.Date;

/**
 * 用户统计信息实体类
 * Created by rookie on 2017/9/30.
 */
public class UserStatistics implements java.io.Serializable {

    private User user;//用户信息
    private int loginNum;//登录次数
    private int messageNum;//消息数
    private int logNum;//操作日志记录数
    private Date lastLoginTime;//最近登录时间
    private String lastLoginIp;//最近登录ip

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getLoginNum() {
        return loginNum;
    }

    public void setLoginNum(int loginNum) {
        this.loginNum = loginNum;
    }

    public int getMessageNum() {
        return messageNum;
    }

    public void setMessageNum(int messageNum) {
        this.messageNum = messageNum;
    }

    public int getLogNum() {
        return logNum;
    }

    public void setLogNum(int logNum) {
        this.logNum = logNum;
    }

    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public String getLastLoginIp() {
        return lastLoginIp;
    }

    public void setLastLoginIp(String lastLoginIp) {
        this.lastLoginIp = lastLoginIp;
    }
}
