package com.yy.young.ums.service.impl;

import com.yy.young.ums.service.IUmsConfigService;
import com.yy.young.dal.service.IDataAccessService;
import com.yy.young.dal.util.Page;
import com.yy.young.ums.model.Config;
import com.yy.young.ums.util.UmsConfigHelper;
import com.yy.young.ums.util.UmsConstants;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 统一用户系统配置服务实现
 * Created by rookie on 2018-01-27.
 */
@Service("UmsConfigService")
public class UmsConfigServiceImpl implements IUmsConfigService {

    @Resource(name = "dataAccessService")
    IDataAccessService dataAccessService;//数据层服务

    private static final Logger logger = LoggerFactory.getLogger(UmsConfigServiceImpl.class);
    
    @PostConstruct
    public void init() throws Exception {
        logger.info("[UMS配置初始化] 初始化开始...");
        List<Config> list = this.getConfigList(null);
        if (list != null && list.size() > 0){
            for (Config config : list){
                UmsConfigHelper.addOrUpdateCache(config);
            }
        }
        logger.info("[UMS配置初始化] 初始化结束!");
    }



    //获取数据列表
    @Override
    public List<Config> getConfigList(Config config) throws Exception {
        return dataAccessService.getList(UmsConstants.MAPPER.UMS_CONFIG + ".getConfigList", config);
    }

    //获取数据列表(分页)
    @Override
    public List<Config> getConfigPage(Config config, Page page) throws Exception {
        return dataAccessService.getList(UmsConstants.MAPPER.UMS_CONFIG + ".getConfigList", config, page);
    }

    //查询单条
    @Override
    public Config getConfig(String id) throws Exception {
        return (Config)dataAccessService.getObject(UmsConstants.MAPPER.UMS_CONFIG + ".getConfigById", id);
    }

    //根据key查询
    @Override
    public Config getConfigByKey(String key) throws Exception {
        if (UmsConfigHelper.isExist(key)){
            return UmsConfigHelper.getConfig(key);
        }
        return (Config)dataAccessService.getObject(UmsConstants.MAPPER.UMS_CONFIG + ".getConfigByKey", key);
    }

    //获取有效的配置值,目前直接从缓存配置中获取
    @Override
    public String getEnabledConfigValue(String key) {
        try {
            Config config = this.getConfigByKey(key);
            if (config != null && UmsConstants.CONFIG_STATE.YES.equals(config.getState())){
                return config.getValue();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    //修改
    @Override
    public int updateConfig(Config config) throws Exception {
        int num = dataAccessService.update(UmsConstants.MAPPER.UMS_CONFIG + ".update", config);
        UmsConfigHelper.addOrUpdateCache(config);//更新缓存
        return num;
    }

    //批量删除
    @Override
    public int deleteConfig(String[] idArr) throws Exception {
        int i = 0;
        for(String id : idArr){
            i += this.deleteConfig(id);
        }
        return i;
    }

    //删除单条
    @Override
    public int deleteConfig(String id) throws Exception {
        Config config = this.getConfig(id);
        int num = 0;
        if (config != null){
            num = dataAccessService.delete(UmsConstants.MAPPER.UMS_CONFIG + ".delete", id);
            UmsConfigHelper.remove(config.getKey());//删除缓存
        }
        return num;
    }

    //插入
    @Override
    public int insertConfig(Config config) throws Exception {
        int num = dataAccessService.insert(UmsConstants.MAPPER.UMS_CONFIG + ".insert", config);
        UmsConfigHelper.addOrUpdateCache(config);//更新缓存
        return num;
    }

}