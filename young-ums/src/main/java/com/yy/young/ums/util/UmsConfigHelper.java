package com.yy.young.ums.util;

import com.yy.young.ums.model.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * ums系统配置助手(仅有效配置),提供配置项缓存与获取功能
 * Created by rookie on 2018/1/28.
 */
public class UmsConfigHelper {

    //配置缓存,存储有效值
    //TODO 可升级使用redis作为分布式缓存
    private static final Map<String, Config> configCache = new HashMap<String, Config>();

    private static final Logger logger = LoggerFactory.getLogger(UmsConfigHelper.class);

    /**
     * 更新配置缓存(不存在就新增,存在就更新)
     * @param config
     */
    public static void addOrUpdateCache(Config config){
        if (config != null){
            if (UmsConstants.CONFIG_STATE.NO.equals(config.getState())){//若配置设置为'禁用',则移除此配置
                remove(config.getKey());
            }else if (config.getValue() != null){//值为空说明没有对值进行修改,所以不处理
                logger.info("[UMS配置更新] {}={}", config.getKey(), config.getValue());
                configCache.put(config.getKey(), config);
            }
        }
    }

    /**
     * 获取指定key的配置
     * @param key
     * @return
     */
    public static Config getConfig(String key){
        return configCache.get(key);
    }

    /**
     * 获取指定key的配置值
     * @param key
     * @return
     */
    public static String getConfigValue(String key){
        if (configCache.get(key) != null){
            logger.debug("[UMS配置获取] {}={}", key, configCache.get(key).getValue());
            return configCache.get(key).getValue();
        }
        return null;
    }

    /**
     * 移除指定配置
     * @param key
     */
    public static Config remove(String key){
        return configCache.remove(key);
    }

    /**
     * 指定key的配置是否存在
     * @param key
     * @return
     */
    public static boolean isExist(String key){
        return configCache.containsKey(key);
    }
}
