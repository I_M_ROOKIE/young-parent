package com.yy.young.ums.service;

import com.yy.young.dal.util.Page;
import com.yy.young.interfaces.ums.model.Navigate;

import javax.print.DocFlavor;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/5/8.
 */
public interface IUmsNavigateService {
    /**
     * 获取资源列表
     * @param parameter
     * @return
     * @throws Exception
     */
    List<Map<String,Object>> getNavigateList(Map<String, Object> parameter) throws Exception;

    /**
     * 获取资源(分页)
     * @param parameter
     * @param page
     * @return
     * @throws Exception
     */
    List<Map<String,Object>> getNavigatePage(Map<String, Object> parameter, Page page) throws Exception;

    /**
     * 根据id获取资源
     * @param id
     * @return
     * @throws Exception
     */
    Map<String,Object> getNavigateById(String id) throws Exception;

    /**
     * 修改资源信息
     * @param parameter
     * @return
     * @throws Exception
     */
    int updateNavigate(Map<String, Object> parameter) throws Exception;

    /**
     * 删除资源信息
     * @param parameter
     * @return
     * @throws Exception
     */
    int deleteNavigate(Map<String, Object> parameter) throws Exception;

    /**
     * 插入资源信息
     * @param parameter
     * @return
     * @throws Exception
     */
    int insertNavigate(Map<String, Object> parameter) throws Exception;

    /**
     * 根据用户ID获取用户包含的资源列表
     * @param userId
     * @return
     * @throws Exception
     */
    List<Navigate> getNavigateByUserId(String userId) throws Exception;

    /**
     * 查询角色拥有的资源权限情况(查询所有的资源信息并且标注该角色是否含有此权限)
     * @param roleId
     * @return
     * @throws Exception
     */
    List<Map<String,Object>> getNavAuthByRole(String roleId) throws Exception;

    /**
     * 根据用户id获取资源树(获取菜单)
     * @param userId
     * @return
     * @throws Exception
     */
    List<Navigate> getNavigateAsTreeByUserId(String userId) throws Exception;

    /**
     * 根据资源id删除角色权限关联数据
     * @param ids
     * @return
     * @throws Exception
     */
    int deleteAuthByNavigateIds(String[] ids) throws Exception;
}
