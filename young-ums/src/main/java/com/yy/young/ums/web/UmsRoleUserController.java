package com.yy.young.ums.web;

import com.yy.young.common.util.CommonUtil;
import com.yy.young.common.util.Result;
import com.yy.young.common.util.StringUtils;
import com.yy.young.dal.util.Page;
import com.yy.young.interfaces.log.annotation.Log;
import com.yy.young.ums.service.IUmsRoleUserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * Created by rookie on 2017/6/29.
 */
@Controller
@RequestMapping("/ums/roleUser")
public class UmsRoleUserController {

    @Resource(name = "umsRoleUserService")
    IUmsRoleUserService umsRoleUserService;

    /**
     * 获取角色用户信息
     * @param request
     * @return
     * @throws Exception
     */
    @Log("查询角色用户列表")
    @RequestMapping("/getRoleUserList")
    @ResponseBody
    public Object getRoleUserList(HttpServletRequest request) throws Exception{
        Map<String,Object> parameter = CommonUtil.getParameterFromRequest(request);
        List<Map<String,Object>> list = umsRoleUserService.getRoleUserList(parameter);
        return new Result(list);
    }

    /**
     * 获取角色用户信息(分页)
     * @param request
     * @return
     * @throws Exception
     */
    @Log("分页查询角色用户列表")
    @RequestMapping("/getRoleUserPage")
    @ResponseBody
    public Object getRoleUserPage(HttpServletRequest request) throws Exception{
        Map<String,Object> parameter = CommonUtil.getParameterFromRequest(request);
        Page page = CommonUtil.getPageFromRequest(request);
        List<Map<String,Object>> list = umsRoleUserService.getRoleUserPage(parameter, page);
        page.setData(list);
        return page;
    }

    /**
     * 删除角色用户关联
     * @param ids
     * @param id
     * @param request
     * @return
     * @throws Exception
     */
    @Log("删除角色用户关联")
    @RequestMapping("/delete")
    @ResponseBody
    public Object deleteRole(String ids, String id, HttpServletRequest request) throws Exception{
        Result result = new Result();
        if(StringUtils.isNotBlank(ids)) {
            String[] idArr = ids.split(",");
            int num = umsRoleUserService.deleteRoleUser(idArr);
        }else if(StringUtils.isNotBlank(id)){
            int num = umsRoleUserService.deleteRoleUser(id);
        }else{
            result.setCode(-1);
            result.setInfo("删除失败:待删除编号无效!");
        }
        return result;
    }
}
