package com.yy.young.ums.web;

import com.yy.young.common.util.CommonJsonUtil;
import com.yy.young.common.util.CommonUtil;
import com.yy.young.common.util.Result;
import com.yy.young.common.util.StringUtils;
import com.yy.young.interfaces.log.annotation.Log;
import com.yy.young.interfaces.model.User;
import com.yy.young.ums.service.IUmsDeptService;
import com.yy.young.ums.service.IUmsRoleService;
import com.yy.young.ums.util.UmsConstants;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by rookie on 2017/6/30.
 */
@Controller
@RequestMapping("/ums/tree")
public class UmsTreeController {

    @Resource(name="umsDeptService")
    IUmsDeptService deptService;

    @Resource(name="umsRoleService")
    IUmsRoleService roleService;

    /**
     * 获取单位树
     * 由于前端使用ztree树,后台不需要将数据整理为树形结构
     * @param request
     * @return
     * @throws Exception
     */
    @Log("获取单位树")
    @RequestMapping("/getCompanyTree")
    @ResponseBody
    public Object getCompanyTree(HttpServletRequest request) throws Exception{
        Map<String,Object> parameter = CommonUtil.getParameterFromRequest(request);
        parameter.put("TYPE", UmsConstants.DICTIONARY.DEPT_TYPE_COMPANY);//类型为单位
        //当前用户
        User user = CommonUtil.getLoginUser(request);
        //根节点
        String root = request.getParameter("root");
        if (StringUtils.isBlank(root)){//参数没有根节点信息时使用用户的管理范围
            root = user.getRangeId();
        }
        if (StringUtils.isBlank(root)){//用户没有管理权限时默认使用用户的当前单位
            root = user.getCompanyId();
        }
        parameter.put("RANGE_ID", root);
        List<Map<String,Object>> list = deptService.getDeptList(parameter);

        //当存在callback跨域参数时,返回跨域格式结果
        if(StringUtils.isNotBlank(request.getParameter("callback"))){
            return request.getParameter("callback")+"("+CommonJsonUtil.bean2JsonStr(new Result(list))+");";
        }
        return new Result(list);
    }

    /**
     * 获取部门树
     * 由于前端使用ztree树,后台不需要将数据整理为树形结构
     * @param request
     * @return
     * @throws Exception
     */
    @Log("获取部门树")
    @RequestMapping("/getDeptTree")
    @ResponseBody
    public Object getDeptTree(HttpServletRequest request) throws Exception{
        Map<String,Object> parameter = CommonUtil.getParameterFromRequest(request);
        parameter.put("TYPE", UmsConstants.DICTIONARY.DEPT_TYPE_DEPT);//类型为部门
        //当前用户
        User user = CommonUtil.getLoginUser(request);
        //根节点
        String root = request.getParameter("root");
        if (StringUtils.isBlank(root)){//参数没有根节点信息时使用用户的管理范围
            root = user.getRangeId();
        }
        if (StringUtils.isBlank(root)){//用户没有管理权限时默认使用用户的当前单位
            root = user.getCompanyId();
        }
        parameter.put("RANGE_ID", root);
        List<Map<String,Object>> list = deptService.getDeptList(parameter);
        return new Result(list);
    }

    /**
     * 获取组织机构树,不区分类别
     * @param request
     * @return
     * @throws Exception
     */
    @Log("获取组织机构树")
    @RequestMapping("/getOrgTree")
    @ResponseBody
    public Object getOrgTree(HttpServletRequest request) throws Exception{
        Map<String,Object> parameter = CommonUtil.getParameterFromRequest(request);
        if(StringUtils.isNotBlank(parameter.get("onlyDept")+"")){
            parameter.put("TYPE", UmsConstants.DICTIONARY.DEPT_TYPE_DEPT);//类型为部门
        }else if (StringUtils.isNotBlank(parameter.get("onlyCompany")+"")){
            parameter.put("TYPE", UmsConstants.DICTIONARY.DEPT_TYPE_COMPANY);//类型为单位
        }
        //当前用户
        User user = CommonUtil.getLoginUser(request);
        //根节点
        String root = request.getParameter("root");
        if (StringUtils.isBlank(root)){//参数没有根节点信息时使用用户的管理范围
            root = user.getRangeId();
        }
        if (StringUtils.isBlank(root)){//用户没有管理权限时默认使用用户的当前单位
            root = user.getCompanyId();
        }
        parameter.put("RANGE_ID", root);
        List<Map<String,Object>> list = deptService.getDeptList(parameter);
        return new Result(list);
    }

    /**
     * 获取角色树
     * @param request
     * @return
     * @throws Exception
     */
    @Log("获取单位角色树")
    @RequestMapping("/getRoleTree")
    @ResponseBody
    public Object getRoleTree(HttpServletRequest request) throws Exception{
        Map<String,Object> parameter = CommonUtil.getParameterFromRequest(request);
        //当前用户
        User user = CommonUtil.getLoginUser(request);
        //根节点
        String root = request.getParameter("root");
        if (StringUtils.isBlank(root)){//参数没有根节点信息时使用用户的管理范围
            root = user.getRangeId();
        }
        if (StringUtils.isBlank(root)){//用户没有管理权限时默认使用用户的当前单位
            root = user.getCompanyId();
        }
        parameter.put("RANGE_ID", root);//限定范围
        parameter.put("TYPE", UmsConstants.DICTIONARY.DEPT_TYPE_COMPANY);//类型为单位
        //获取单位树
        List<Map<String,Object>> deptList = deptService.getDeptList(parameter);
        Iterator<Map<String,Object>> it = deptList.iterator();
        //遍历获取单位下的角色信息
        while (it.hasNext()){
            Map<String,Object> item = it.next();
            item.put("nocheck", "true");
            item.put("nodeType", "COMPANY");//节点类型,用于区分角色节点和单位节点
            List<Map<String,Object>> list = roleService.getRoleListByCompanyId(item.get("ID") + "");
            item.put("children", list);
        }

        //当存在callback跨域参数时,返回跨域格式结果
        if(StringUtils.isNotBlank(request.getParameter("callback"))){
            return request.getParameter("callback")+"("+CommonJsonUtil.bean2JsonStr(new Result(deptList))+");";
        }
        return new Result(deptList);
    }
}
