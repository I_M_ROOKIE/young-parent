package com.yy.young.ums.service.impl;

import com.yy.young.dal.service.IDataAccessService;
import com.yy.young.dal.util.Page;
import com.yy.young.ums.service.IUmsDeptUserService;
import com.yy.young.ums.util.UmsConstants;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * Created by rookie on 2017/6/29.
 */
@Service("umsDeptUserService")
public class UmsDeptUserServiceImpl implements IUmsDeptUserService {

    @Resource(name="dataAccessService")
    IDataAccessService dataService;

    @Override
    public List<Map<String, Object>> getDeptUserList(Map<String, Object> parameter) throws Exception {
        return dataService.getList(UmsConstants.MAPPER.UMS_DEPT_USER+".getDeptUser",parameter);
    }

    @Override
    public List<Map<String, Object>> getDeptUserPage(Map<String, Object> parameter, Page page) throws Exception {
        return dataService.getList(UmsConstants.MAPPER.UMS_DEPT_USER+".getDeptUser", parameter, page);
    }

    //根据用户编号删除相关的部门用户关联信息
    @Override
    public int deleteDeptUserByUserId(String userId) throws Exception {
        return dataService.delete(UmsConstants.MAPPER.UMS_DEPT_USER+".deleteDeptUserByUserId", userId);
    }

    @Override
    public int deleteDeptUserByDeptId(String deptId) throws Exception {
        return dataService.delete(UmsConstants.MAPPER.UMS_DEPT_USER+".deleteDeptUserByDeptId", deptId);
    }
}
