package com.yy.young.ums.web;

import com.yy.young.common.util.CommonUtil;
import com.yy.young.common.util.Result;
import com.yy.young.dal.util.Page;
import com.yy.young.interfaces.log.annotation.Log;
import com.yy.young.ums.service.IUmsDeptUserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * Created by rookie on 2017/6/29.
 */
@Controller
@RequestMapping("/ums/deptUser")
public class UmsDeptUserController {

    @Resource(name = "umsDeptUserService")
    IUmsDeptUserService umsDeptUserService;

    /**
     * 获取部门用户信息
     * @param request
     * @return
     * @throws Exception
     */
    @Log("查询部门用户信息")
    @RequestMapping("/getDeptUserList")
    @ResponseBody
    public Object getDeptUserList(HttpServletRequest request) throws Exception{
        Map<String,Object> parameter = CommonUtil.getParameterFromRequest(request);
        List<Map<String,Object>> list = umsDeptUserService.getDeptUserList(parameter);
        return new Result(list);
    }

    /**
     * 获取部门用户信息(分页)
     * @param request
     * @return
     * @throws Exception
     */
    @Log("分页查询部门用户信息")
    @RequestMapping("/getDeptUserPage")
    @ResponseBody
    public Object getDeptUserPage(HttpServletRequest request) throws Exception{
        Map<String,Object> parameter = CommonUtil.getParameterFromRequest(request);
        Page page = CommonUtil.getPageFromRequest(request);
        List<Map<String,Object>> list = umsDeptUserService.getDeptUserPage(parameter, page);
        page.setData(list);
        return page;
    }
}
