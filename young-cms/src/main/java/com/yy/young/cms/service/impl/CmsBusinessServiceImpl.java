package com.yy.young.cms.service.impl;

import com.yy.young.cms.model.Business;
import com.yy.young.cms.service.ICmsBusinessService;
import com.yy.young.cms.util.CmsConstants;
import com.yy.young.dal.service.IDataAccessService;
import com.yy.young.dal.util.Page;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * 业务管理服务实现
 * Created by rookie on 2017/8/11.
 */
@Service("cmsBusinessService")
public class CmsBusinessServiceImpl implements ICmsBusinessService {

    @Resource(name = "dataAccessService")
    IDataAccessService dataAccessService;//数据层服务

    @Override
    public List<Business> getBusinessList(Business Business) throws Exception {
        return dataAccessService.getList(CmsConstants.MAPPER.CMS_BUSINESS + ".getBusiness", Business);
    }

    @Override
    public List<Business> getBusinessPage(Business Business, Page page) throws Exception {
        return dataAccessService.getList(CmsConstants.MAPPER.CMS_BUSINESS + ".getBusiness", Business, page);
    }

    @Override
    public int updateBusiness(Business Business) throws Exception {
        return dataAccessService.update(CmsConstants.MAPPER.CMS_BUSINESS + ".updateBusiness", Business);
    }

    @Override
    public int deleteBusiness(String[] idsArr) throws Exception {

        return dataAccessService.delete(CmsConstants.MAPPER.CMS_BUSINESS + ".deleteBusiness", idsArr);
    }

    @Override
    public int insertBusiness(Business Business) throws Exception {
        Business.setCreateTime(new Date());//创建时间

        return dataAccessService.insert(CmsConstants.MAPPER.CMS_BUSINESS + ".insertBusiness", Business);
    }
}
