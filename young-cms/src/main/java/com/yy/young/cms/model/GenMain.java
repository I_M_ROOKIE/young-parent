package com.yy.young.cms.model;

import com.yy.young.common.core.excel.ExcelColumn;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
* 代码生成主体信息实体类
* Created by rookie on 2018-7-11.
*/
public class GenMain{

    @ExcelColumn(value = "代码生成编号", order = 1)
    private String id;//代码生成编号

    @ExcelColumn(value = "代码生成名称", order = 2)
    private String name;//代码生成名称

    @ExcelColumn(value = "表名", order = 3)
    private String tableName;//表名

    @ExcelColumn(value = "实体类名", order = 4)
    private String className;//实体类名

    @ExcelColumn(value = "包名", order = 5)
    private String packageName;//包名

    @ExcelColumn(value = "模块名", order = 6)
    private String moduleName;//模块名

    @ExcelColumn(value = "表说明", order = 7)
    private String tableRemark;//表说明

    @ExcelColumn(value = "日志服务", order = 8)
    private String logService;//日志服务

    @ExcelColumn(value = "保存路径", order = 9)
    private String savePath;//保存路径

    @ExcelColumn(value = "作者", order = 10)
    private String author;//作者

    @ExcelColumn(value = "创建人编号", order = 11)
    private String createUserId;//创建人编号,关联用户表

    @ExcelColumn(value = "创建人姓名", order = 12)
    private String createUserName;//创建人姓名

    @ExcelColumn(value = "创建时间", order = 13)
    private Date createTime;//创建时间

    @ExcelColumn(value = "修改时间", order = 14)
    private Date updateTime;//修改时间

    private List<GenCol> colList;//字段配置

    public GenMain(){
        super();
    }

    /**
     * 添加字段配置
     * @param genCol
     */
    public void addCol(GenCol genCol){
        if (this.colList == null){
            this.colList = new ArrayList<GenCol>();
        }
        this.colList.add(genCol);
    }

    /**
     * 批量添加字段配置
     * @param genColList
     */
    public void addCol(List<GenCol> genColList){
        if (this.colList == null){
            this.colList = genColList;
        }else if (genColList != null){
            for(GenCol genCol : genColList){
                this.colList.add(genCol);
            }
        }
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getTableName() {
        return tableName;
    }
    public void setTableName(String tableName) {
        this.tableName = tableName;
    }
    public String getClassName() {
        return className;
    }
    public void setClassName(String className) {
        this.className = className;
    }
    public String getPackageName() {
        return packageName;
    }
    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }
    public String getModuleName() {
        return moduleName;
    }
    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }
    public String getTableRemark() {
        return tableRemark;
    }
    public void setTableRemark(String tableRemark) {
        this.tableRemark = tableRemark;
    }
    public String getLogService() {
        return logService;
    }
    public void setLogService(String logService) {
        this.logService = logService;
    }
    public String getSavePath() {
        return savePath;
    }
    public void setSavePath(String savePath) {
        this.savePath = savePath;
    }
    public String getAuthor() {
        return author;
    }
    public void setAuthor(String author) {
        this.author = author;
    }
    public String getCreateUserId() {
        return createUserId;
    }
    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }
    public String getCreateUserName() {
        return createUserName;
    }
    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }
    public Date getCreateTime() {
        return createTime;
    }
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    public Date getUpdateTime() {
        return updateTime;
    }
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public List<GenCol> getColList() {
        return colList;
    }

    public void setColList(List<GenCol> colList) {
        this.colList = colList;
    }
}