package com.yy.young.cms.service;

import com.yy.young.dal.util.Page;
import com.yy.young.cms.model.Comment;
import java.util.List;

/**
* 评论服务接口
* Created by rookie on 2018-04-03.
*/
public interface ICmsCommentService {

    /**
    * 查询
    * @param obj
    * @return
    * @throws Exception
    */
    List<Comment> getList(Comment obj) throws Exception;

    /**
     * 查询(完整字段)
     * @param obj
     * @return
     * @throws Exception
     */
    List<Comment> getListOfDetail(Comment obj) throws Exception;

    /**
    * 分页查询
    * @param obj
    * @param page
    * @return
    * @throws Exception
    */
    List<Comment> getPage(Comment obj, Page page) throws Exception;

    /**
     * 分页查询(完整字段)
     * @param obj
     * @param page
     * @return
     * @throws Exception
     */
    List<Comment> getPageOfDetail(Comment obj, Page page) throws Exception;

    /**
    * 查询单条
    * @param id
    * @return
    * @throws Exception
    */
    Comment get(String id) throws Exception;

    /**
    * 统计数量
    * @param obj
    * @return Integer 满足xx条件的数据有多少条
    * @throws Exception
    */
    int count(Comment obj) throws Exception;

    /**
    * 修改
    * @param obj
    * @return
    * @throws Exception
    */
    int update(Comment obj) throws Exception;

    /**
    * 删除
    * @param id
    * @return
    * @throws Exception
    */
    int delete(String id) throws Exception;

    /**
    * 批量删除
    * @param idArr
    * @return
    * @throws Exception
    */
    int delete(String[] idArr) throws Exception;

    /**
    * 插入
    * @param obj
    * @return
    * @throws Exception
    */
    int insert(Comment obj) throws Exception;

    /**
    * 批量插入
    * @param list
    * @return
    * @throws Exception
    */
    int batchInsert(List<Comment> list) throws Exception;

}