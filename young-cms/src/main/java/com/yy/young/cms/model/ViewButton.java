package com.yy.young.cms.model;

/**
 * 视图按钮实体类
 * Created by rookie on 2017/9/18.
 */
public class ViewButton implements java.io.Serializable {
    private String id;//唯一编号
    private String name;//按钮名称
    private String display;//显示条件
    private int num;//显示顺序
    private String viewId;//所属视图编号
    private String buttonId;//关联按钮编号
    private Button button;//关联按钮

    public ViewButton() {
        super();
    }

    public String getButtonId() {
        return buttonId;
    }

    public void setButtonId(String buttonId) {
        this.buttonId = buttonId;
    }

    public String getViewId() {
        return viewId;
    }

    public void setViewId(String viewId) {
        this.viewId = viewId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public Button getButton() {
        return button;
    }

    public void setButton(Button button) {
        this.button = button;
    }
}
