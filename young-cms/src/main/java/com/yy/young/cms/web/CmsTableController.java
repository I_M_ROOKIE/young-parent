package com.yy.young.cms.web;

import com.yy.young.base.exception.ParameterException;
import com.yy.young.base.exception.YoungBaseException;
import com.yy.young.base.util.GlobalConstants;
import com.yy.young.cms.model.Table;
import com.yy.young.cms.service.ICmsTableService;
import com.yy.young.common.util.CommonUtil;
import com.yy.young.common.util.Result;
import com.yy.young.common.util.StringUtils;
import com.yy.young.dal.util.Page;
import com.yy.young.interfaces.model.User;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

/**
 * cms业务表请求处理
 * Created by Administrator on 2017/5/8.
 */
@Controller
@RequestMapping("/cms/table")
public class CmsTableController {

    @Resource(name="cmsTableService")
    ICmsTableService tableService;

    /**
     * 获取业务表列表
     * @param table
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/getTableList")
    @ResponseBody
    public Object getTableList(Table table, HttpServletRequest request) throws Exception{
        List<Table> list = tableService.getTableList(table);
        return new Result(list);
    }

    /**
     * 获取业务表列表(分页)
     * @param table
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/getTablePage")
    @ResponseBody
    public Object getTablePage(Table table, HttpServletRequest request) throws Exception{
        Page page = CommonUtil.getPageFromRequest(request);
        List<Table> list = tableService.getTablePage(table, page);
        page.setData(list);
        return page;
    }

    /**
     * 添加业务表
     * @param table
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/insertTable")
    @ResponseBody
    public Object insertTable(Table table, HttpServletRequest request) throws Exception{
        User user = (User) request.getSession().getAttribute(GlobalConstants.SESSION.KEY_LOGINUSER);
        Result result = new Result();
        //添加当前用户信息
        table.setCreateUser(user.getId());
        table.setCompanyId(user.getCompanyId());
        int num = tableService.insertTable(table);
        if(num < 0){
            result.setCode(-1);
            result.setInfo("添加业务表失败!");
        }
        return result;
    }

    /**
     * 修改业务表信息
     * @param table
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/updateTable")
    @ResponseBody
    public Object updateTable(Table table, HttpServletRequest request) throws Exception{
        int num = tableService.updateTable(table);
        Result result = new Result();
        if(num < 0){
            result.setCode(-1);
            result.setInfo("修改业务表信息失败!");
        }
        return result;
    }

    /**
     * 删除业务表信息
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/deleteTable")
    @ResponseBody
    public Object deleteTable(HttpServletRequest request) throws Exception{
        Map<String,Object> parameter = CommonUtil.getParameterFromRequest(request);
        String idsStr = parameter.get("ids")+"";
        String[] idArr;
        Result result = new Result();
        if(StringUtils.isNotBlank(idsStr)) {
            idArr = idsStr.split(",");
            int num = tableService.deleteTable(idArr);
            if(num < 0){
                result.setCode(-1);
                result.setInfo("删除业务表信息失败!");
            }
        }else{
            result.setCode(-1);
            result.setInfo("删除失败:待删除业务表ID无效!");
        }
        return result;
    }

}
