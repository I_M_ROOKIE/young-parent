package com.yy.young.cms.service;

import com.yy.young.cms.model.View;
import com.yy.young.dal.util.Page;

import java.util.List;

/**
 * 视图服务接口
 * Created by Administrator on 2017/5/8.
 */
public interface ICmsViewService {
    /**
     * 获取视图列表
     * @param view
     * @return
     * @throws Exception
     */
    List<View> getViewList(View view) throws Exception;

    /**
     * 根据视图id查询视图信息
     * @param viewId
     * @return
     * @throws Exception
     */
    View getView(String viewId) throws Exception;

    /**
     * 获取视图(分页)
     * @param view
     * @param page
     * @return
     * @throws Exception
     */
    List<View> getViewPage(View view, Page page) throws Exception;

    /**
     * 修改视图信息
     * @param view
     * @return
     * @throws Exception
     */
    int updateView(View view) throws Exception;

    /**
     * 删除视图信息
     * @param idsArr 编号数组
     * @return
     * @throws Exception
     */
    int deleteView(String[] idsArr) throws Exception;

    /**
     * 插入视图信息
     * @param view
     * @return
     * @throws Exception
     */
    int insertView(View view) throws Exception;
    
}
