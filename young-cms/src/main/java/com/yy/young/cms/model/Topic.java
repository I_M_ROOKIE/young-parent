package com.yy.young.cms.model;

import com.yy.young.common.core.excel.ExcelColumn;
import java.util.Date;
/**
* 主题实体类
* Created by rookie on 2018-5-17.
*/
public class Topic{

    @ExcelColumn(value = "主题编号", order = 1)
    private String id;//主题编号

    @ExcelColumn(value = "标题", order = 2)
    private String title;//标题

    @ExcelColumn(value = "文本内容", order = 3)
    private String contentText;//文本内容

    @ExcelColumn(value = "带格式内容", order = 4)
    private String contentFormat;//带格式内容

    @ExcelColumn(value = "作者编号", order = 5)
    private String authorId;//作者编号

    @ExcelColumn(value = "作者名字", order = 6)
    private String authorName;//作者名字

    @ExcelColumn(value = "创建时间", order = 7)
    private Date createTime;//创建时间

    @ExcelColumn(value = "修改时间", order = 8)
    private Date updateTime;//修改时间

    @ExcelColumn(value = "应用编号", order = 9)
    private String appId;//应用编号

    @ExcelColumn(value = "应用中该主题对应地址", order = 10)
    private String url;//应用中该主题对应地址

    @ExcelColumn(value = "分类/频道", order = 11)
    private String category;//分类/频道

    @ExcelColumn(value = "来源ID,应用中的编号", order = 12)
    private String sourceId;//来源ID,应用中的编号

    public Topic(){
        super();
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getContentText() {
        return contentText;
    }
    public void setContentText(String contentText) {
        this.contentText = contentText;
    }
    public String getContentFormat() {
        return contentFormat;
    }
    public void setContentFormat(String contentFormat) {
        this.contentFormat = contentFormat;
    }
    public String getAuthorId() {
        return authorId;
    }
    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }
    public String getAuthorName() {
        return authorName;
    }
    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }
    public Date getCreateTime() {
        return createTime;
    }
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    public Date getUpdateTime() {
        return updateTime;
    }
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
    public String getAppId() {
        return appId;
    }
    public void setAppId(String appId) {
        this.appId = appId;
    }
    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    public String getCategory() {
        return category;
    }
    public void setCategory(String category) {
        this.category = category;
    }
    public String getSourceId() {
        return sourceId;
    }
    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

}