package com.yy.young.cms.service;

import com.yy.young.cms.model.Column;
import com.yy.young.cms.model.Table;

import java.util.List;

/**
 * 数据库操作服务
 * Created by rookie on 2017/9/12.
 */
public interface ICmsDBService {
    /**
     * 根据表名判断是否存在该表
     * @param tableId
     * @return
     * @throws Exception
     */
    boolean existTable(String tableId) throws Exception;
    /**
     * 建表
     * @param table
     * @return
     * @throws Exception
     */
    boolean createTable(Table table) throws Exception;

    /**
     * 删除表
     * @param table
     * @return
     * @throws Exception
     */
    boolean dropTable(Table table) throws Exception;

    /**
     * 根据表名删除表
     * @param tableId
     * @return
     * @throws Exception
     */
    boolean dropTable(String tableId) throws Exception;

    /**
     * 添加字段
     * @param column
     * @return
     * @throws Exception
     */
    boolean addColumn(Column column) throws Exception;

    /**
     * 修改字段
     * @param column
     * @return
     * @throws Exception
     */
    boolean modifyColumn(Column column) throws Exception;

    /**
     * 删除字段
     * @param column
     * @return
     * @throws Exception
     */
    boolean deleteColumn(Column column) throws Exception;

    /**
     * 获取数据库所有的表信息
     * (基础信息:表名,注释)
     * @return
     * @throws Exception
     */
    List<Table> getTableListOfSimpleInfo() throws Exception;

    /**
     * 获取某表的字段信息
     * @param tableName
     * @return
     * @throws Exception
     */
    List<Column> getColumsByTableName(String tableName) throws Exception;
}
