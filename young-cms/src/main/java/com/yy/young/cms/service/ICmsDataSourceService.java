package com.yy.young.cms.service;

import com.yy.young.cms.model.DataSource;
import com.yy.young.dal.util.Page;

import java.util.List;
import java.util.Map;

/**
 * 视图数据源服务接口
 * Created by rookie on 2017-11-01.
 */
public interface ICmsDataSourceService {

    /**
     * 获取数据列表
     * @param dataSource
     * @return
     * @throws Exception
     */
    List<DataSource> getDataSourceList(DataSource dataSource) throws Exception;

    /**
     * 获取数据列表(分页)
     * @param dataSource
     * @param page 分页参数
     * @return
     * @throws Exception
     */
    List<DataSource> getDataSourcePage(DataSource dataSource, Page page) throws Exception;

    /**
     * 根据id获取信息
     * @param id
     * @return
     * @throws Exception
     */
    DataSource getDataSource(String id) throws Exception;

    /**
     * 修改
     * @param dataSource
     * @return
     * @throws Exception
     */
    int updateDataSource(DataSource dataSource) throws Exception;

    /**
     * 批量删除
     * @param idArr id数组
     * @return
     * @throws Exception
     */
    int deleteDataSource(String[] idArr) throws Exception;

    /**
     * 删除单条
     * @param id
     * @return
     * @throws Exception
     */
    int deleteDataSource(String id) throws Exception;

    /**
     * 插入
     * @param dataSource
     * @return
     * @throws Exception
     */
    int insertDataSource(DataSource dataSource) throws Exception;

}
