package com.yy.young.cms.model;

import java.util.Date;

/**
 * 配置实体类
 * Created by rookie on 2018-01-04.
 */
public class Config{

    private String id;//主键
    private String key;//key
    private String value;//值
    private Float num;//显示顺序
    private String remark;//备注
    private String category;//分组
    private String parentId;//父节点编号
    private Date createTime;//创建时间
    private Date updateTime;//修改时间
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public Float getNum() {
        return num;
    }
    public void setNum(Float num) {
        this.num = num;
    }
    public String getRemark() {
        return remark;
    }
    public void setRemark(String remark) {
        this.remark = remark;
    }
    public String getCategory() {
        return category;
    }
    public void setCategory(String category) {
        this.category = category;
    }
    public String getParentId() {
        return parentId;
    }
    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}