package com.yy.young.cms.service;

import com.yy.young.cms.model.Business;
import com.yy.young.dal.util.Page;

import java.util.List;

/**
 * 业务服务接口
 * Created by Administrator on 2017/5/8.
 */
public interface ICmsBusinessService {
    /**
     * 获取业务列表
     * @param business
     * @return
     * @throws Exception
     */
    List<Business> getBusinessList(Business business) throws Exception;

    /**
     * 获取业务(分页)
     * @param business
     * @param page
     * @return
     * @throws Exception
     */
    List<Business> getBusinessPage(Business business, Page page) throws Exception;

    /**
     * 修改业务信息
     * @param business
     * @return
     * @throws Exception
     */
    int updateBusiness(Business business) throws Exception;

    /**
     * 删除业务信息
     * @param idsArr 编号数组
     * @return
     * @throws Exception
     */
    int deleteBusiness(String[] idsArr) throws Exception;

    /**
     * 插入业务信息
     * @param business
     * @return
     * @throws Exception
     */
    int insertBusiness(Business business) throws Exception;
    
}
