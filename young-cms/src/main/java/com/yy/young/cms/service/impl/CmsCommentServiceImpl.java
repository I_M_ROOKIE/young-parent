package com.yy.young.cms.service.impl;

import com.yy.young.base.exception.ParameterException;
import com.yy.young.cms.model.Topic;
import com.yy.young.cms.service.ICmsCommentService;
import com.yy.young.cms.service.ICmsTopicService;
import com.yy.young.cms.util.CmsConstants;
import com.yy.young.common.util.CommonUtil;
import com.yy.young.common.util.StringUtils;
import com.yy.young.dal.service.IDataAccessService;
import com.yy.young.dal.util.Page;
import com.yy.young.cms.model.Comment;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
* 评论服务实现
* Created by rookie on 2018-04-03.
*/
@Service("commentService")
public class CmsCommentServiceImpl implements ICmsCommentService {

    @Resource(name = "dataAccessService")
    IDataAccessService dataAccessService;//数据层服务

    @Resource(name="topicService")
    ICmsTopicService topicService;

    private static final Logger logger = LoggerFactory.getLogger(CmsCommentServiceImpl.class);

    private static final String MAPPER = CmsConstants.MAPPER.CMS_COMMENT;//mapper的namespace

    //获取数据列表
    @Override
    public List<Comment> getList(Comment obj) throws Exception {
        return dataAccessService.getList(MAPPER + ".getList", obj);
    }

    @Override
    public List<Comment> getListOfDetail(Comment obj) throws Exception {
        return dataAccessService.getList(MAPPER + ".getListOfDetail", obj);
    }

    //获取数据列表(分页)
    @Override
    public List<Comment> getPage(Comment obj, Page page) throws Exception {
        return dataAccessService.getList(MAPPER + ".getList", obj, page);
    }

    @Override
    public List<Comment> getPageOfDetail(Comment obj, Page page) throws Exception {
        return dataAccessService.getList(MAPPER + ".getListOfDetail", obj, page);
    }

    //查询单条
    @Override
    public Comment get(String id) throws Exception {
        return (Comment)dataAccessService.getObject(MAPPER + ".get", id);
    }

    //统计数量
    @Override
    public int count(Comment obj) throws Exception {
        return (Integer)dataAccessService.getObject(MAPPER + ".count", obj);
    }

    //修改
    @Override
    public int update(Comment obj) throws Exception {
        obj.setUpdateTime(new Date());
        return dataAccessService.update(MAPPER + ".update", obj);
    }

    //批量删除
    @Override
    public int delete(String[] idArr) throws Exception {
        int i = 0;
        for(String id : idArr){
            i += this.delete(id);
        }
        return i;
    }

    //删除单条
    @Override
    public int delete(String id) throws Exception {
        return dataAccessService.delete(MAPPER + ".delete", id);
    }

    //插入
    @Override
    public int insert(Comment obj) throws Exception {
        //评论中文本内容和格式内容全为空,则返回异常
        if (StringUtils.isBlank(obj.getContentText()) && StringUtils.isBlank(obj.getContentFormat())){
            throw new ParameterException("操作失败:评论内容不允许为空");
        }
        //评论的应用id和所属源id不允许为空
        if (StringUtils.isBlank(obj.getAppId())){
            throw new ParameterException("操作失败:应用编号不允许为空");
        }
        if (StringUtils.isBlank(obj.getSourceId())){
            throw new ParameterException("操作失败:源编号不允许为空");
        }

        //由于允许第三方应用自行保存主题,所以在这里不对主题是否存在做判断
        if (StringUtils.isBlank(obj.getId())){
            obj.setId(CommonUtil.getUUID());
        }
        //根据应用id和源id查询对应的主题,若主题存在,则设置该评论的主题编号
        Topic topic = topicService.get(obj.getAppId(), obj.getSourceId());
        if (topic != null){
            obj.setTopicId(topic.getId());
        }else{//若不存在对应topic,说明主题由应用自行管理(未托管到cms-topic),则默认主题编号为空,当该源主题创建时再关联
            obj.setTopicId(null);
        }
        obj.setCreateTime(new Date());
        obj.setUpdateTime(new Date());
        return dataAccessService.insert(MAPPER + ".insert", obj);
    }

    //批量插入
    @Override
    public int batchInsert(List<Comment> list) throws Exception {
        int i = 0;
        for(Comment item : list){
            i += this.insert(item);
        }
        return i;
    }

}