package com.yy.young.cms.service;

import com.yy.young.dal.util.Page;
import com.yy.young.cms.model.Topic;
import java.util.List;

/**
* 主题服务接口
* Created by rookie on 2018-04-03.
*/
public interface ICmsTopicService {

    /**
    * 查询
    * @param obj
    * @return
    * @throws Exception
    */
    List<Topic> getList(Topic obj) throws Exception;

    /**
     * 查询详细
     * @param obj
     * @return
     * @throws Exception
     */
    List<Topic> getListOfDetail(Topic obj) throws Exception;

    /**
    * 分页查询
    * @param obj
    * @param page
    * @return
    * @throws Exception
    */
    List<Topic> getPage(Topic obj, Page page) throws Exception;

    /**
     * 分页查询详细
     * @param obj
     * @param page
     * @return
     * @throws Exception
     */
    List<Topic> getPageOfDetail(Topic obj, Page page) throws Exception;

    /**
    * 查询单条
    * @param id
    * @return
    * @throws Exception
    */
    Topic get(String id) throws Exception;

    /**
     * 根据应用id和源id查询主题
     * @param appId
     * @param sourceId
     * @return
     * @throws Exception
     */
    Topic get(String appId, String sourceId) throws Exception;

    /**
    * 统计数量
    * @param obj
    * @return Integer 满足xx条件的数据有多少条
    * @throws Exception
    */
    int count(Topic obj) throws Exception;

    /**
    * 修改
    * @param obj
    * @return
    * @throws Exception
    */
    int update(Topic obj) throws Exception;

    /**
    * 删除
    * @param id
    * @return
    * @throws Exception
    */
    int delete(String id) throws Exception;

    /**
    * 批量删除
    * @param idArr
    * @return
    * @throws Exception
    */
    int delete(String[] idArr) throws Exception;

    /**
    * 插入
    * @param obj
    * @return
    * @throws Exception
    */
    int insert(Topic obj) throws Exception;

    /**
    * 批量插入
    * @param list
    * @return
    * @throws Exception
    */
    int batchInsert(List<Topic> list) throws Exception;

    /**
     * 判断是否存在主题
     * @param id 主题编号
     * @return
     * @throws Exception
     */
    boolean existTopic(String id) throws Exception;

    /**
     * 判断是否存在主题
     * @param appId 应用编号
     * @param sourceId 源id
     * @return
     * @throws Exception
     */
    boolean existTopic(String appId, String sourceId) throws Exception;

}