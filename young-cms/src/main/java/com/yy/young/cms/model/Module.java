package com.yy.young.cms.model;


/**
 * cms模块实体类
 * Created by rookie on 2017/8/11.
 */
public class Module extends CmsBase{
    private String id;//模块编号
    private String name;//模块名称
    private int num;//显示顺序
    private String remark;//备注

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }


    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
