package com.yy.young.cms.service.impl;

import com.yy.young.base.exception.ParameterException;
import com.yy.young.cms.util.CmsConstants;
import com.yy.young.common.util.CommonUtil;
import com.yy.young.common.util.StringUtils;
import com.yy.young.dal.service.IDataAccessService;
import com.yy.young.dal.util.Page;
import com.yy.young.cms.model.GenCol;
import com.yy.young.cms.service.ICmsGenColService;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.annotation.Resource;
import java.util.List;

/**
* 代码生成字段配置服务实现
* Created by rookie on 2018-04-03.
*/
@Service("genColService")
public class CmsGenColServiceImpl implements ICmsGenColService {

    @Resource(name = "dataAccessService")
    IDataAccessService dataAccessService;//数据层服务

    private static final Logger logger = LoggerFactory.getLogger(CmsGenColServiceImpl.class);

    private static final String MAPPER = CmsConstants.MAPPER.CMS_GEN_COL;//mapper的namespace

    //获取数据列表
    @Override
    public List<GenCol> getList(GenCol obj) throws Exception {
        return dataAccessService.getList(MAPPER + ".getList", obj);
    }

    //获取数据列表(分页)
    @Override
    public List<GenCol> getPage(GenCol obj, Page page) throws Exception {
        return dataAccessService.getList(MAPPER + ".getList", obj, page);
    }

    //查询单条
    @Override
    public GenCol get(String id) throws Exception {
        return (GenCol)dataAccessService.getObject(MAPPER + ".get", id);
    }

    //统计数量
    @Override
    public int count(GenCol obj) throws Exception {
        return (Integer)dataAccessService.getObject(MAPPER + ".count", obj);
    }

    //修改
    @Override
    public int update(GenCol obj) throws Exception {
        if (StringUtils.isBlank(obj.getId())){
            throw new ParameterException("字段配置编号不允许为空!");
        }
        if (StringUtils.isBlank(obj.getGenId())) {
            throw new ParameterException("代码生成编号不允许为空!");
        }
        System.out.println(obj.getAttrName()+" --- "+obj.getFillExplain());
        return dataAccessService.update(MAPPER + ".update", obj);
    }

    //批量删除
    @Override
    public int delete(String[] idArr) throws Exception {
        int i = 0;
        for(String id : idArr){
            i += this.delete(id);
        }
        return i;
    }

    //删除单条
    @Override
    public int delete(String id) throws Exception {
        return dataAccessService.delete(MAPPER + ".delete", id);
    }

    //插入
    @Override
    public int insert(GenCol obj) throws Exception {
        if (StringUtils.isBlank(obj.getGenId())) {
            throw new ParameterException("代码生成编号不允许为空!");
        }
        if (StringUtils.isBlank(obj.getId())) {
            obj.setId(CommonUtil.getUUID());
        }
        return dataAccessService.insert(MAPPER + ".insert", obj);
    }

    //批量插入
    @Override
    public int batchInsert(List<GenCol> list) throws Exception {
        int i = 0;
        for(GenCol item : list){
            i += this.insert(item);
        }
        return i;
    }

    @Override
    public int deleteByGenId(String genId) throws Exception {
        return dataAccessService.delete(MAPPER + ".deleteByGenId", genId);
    }

}