package com.yy.young.cms.model;

import com.yy.young.common.core.excel.ExcelColumn;
import java.util.Date;
/**
* 代码生成字段配置实体类
* Created by rookie on 2018-7-12.
*/
public class GenCol{

    @ExcelColumn(value = "主键", order = 1)
    private String id;//主键

    @ExcelColumn(value = "代码生成编号,关联tb_cms_gen_main", order = 2)
    private String genId;//代码生成编号,关联tb_cms_gen_main

    @ExcelColumn(value = "字段名", order = 3)
    private String fieldName;//字段名

    @ExcelColumn(value = "java属性名", order = 4)
    private String attrName;//java属性名

    @ExcelColumn(value = "字段描述", order = 5)
    private String fieldRemark;//字段描述

    @ExcelColumn(value = "字段类型", order = 6)
    private String fieldType;//字段类型

    @ExcelColumn(value = "字段长度", order = 7)
    private Integer fieldLength;//字段长度

    @ExcelColumn(value = "字段非空", order = 8)
    private String fieldNotNull;//字段非空

    @ExcelColumn(value = "是否在视图显示", order = 9)
    private String viewShow;//是否在视图显示

    @ExcelColumn(value = "在视图中的顺序", order = 10)
    private Integer viewNum;//在视图中的顺序

    @ExcelColumn(value = "是否作为查询条件", order = 11)
    private String asCondition;//是否作为查询条件

    @ExcelColumn(value = "是否支持排序", order = 12)
    private String supportSort;//是否支持排序

    @ExcelColumn(value = "在表单显示方式", order = 13)
    private String formShow;//在表单显示方式

    @ExcelColumn(value = "在表单中的顺序", order = 14)
    private Integer formNum;//在表单中的顺序

    @ExcelColumn(value = "控件类型,input/select..", order = 15)
    private String controlType;//控件类型,input/select..

    @ExcelColumn(value = "是否必填", order = 16)
    private String controlMust;//是否必填

    @ExcelColumn(value = "控件参数,例如url,取值等", order = 17)
    private String controlParam;//控件参数,例如url,取值等

    @ExcelColumn(value = "默认值", order = 18)
    private String controlDefault;//默认值

    @ExcelColumn(value = "只读属性", order = 19)
    private String controlReadonly;//只读属性

    @ExcelColumn(value = "控件样式", order = 20)
    private String controlClass;//控件样式

    @ExcelColumn(value = "控件表单验证", order = 21)
    private String controlCheck;//控件表单验证

    @ExcelColumn(value = "控件表单验证值", order = 22)
    private String controlCheckVal;//控件表单验证值

    @ExcelColumn(value = "填写说明", order = 23)
    private String fillExplain;//填写说明

    public GenCol(){
        super();
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getGenId() {
        return genId;
    }
    public void setGenId(String genId) {
        this.genId = genId;
    }
    public String getFieldName() {
        return fieldName;
    }
    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }
    public String getAttrName() {
        return attrName;
    }
    public void setAttrName(String attrName) {
        this.attrName = attrName;
    }
    public String getFieldRemark() {
        return fieldRemark;
    }
    public void setFieldRemark(String fieldRemark) {
        this.fieldRemark = fieldRemark;
    }
    public String getFieldType() {
        return fieldType;
    }
    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }
    public Integer getFieldLength() {
        return fieldLength;
    }
    public void setFieldLength(Integer fieldLength) {
        this.fieldLength = fieldLength;
    }
    public String getFieldNotNull() {
        return fieldNotNull;
    }
    public void setFieldNotNull(String fieldNotNull) {
        this.fieldNotNull = fieldNotNull;
    }
    public String getViewShow() {
        return viewShow;
    }
    public void setViewShow(String viewShow) {
        this.viewShow = viewShow;
    }
    public Integer getViewNum() {
        return viewNum;
    }
    public void setViewNum(Integer viewNum) {
        this.viewNum = viewNum;
    }
    public String getAsCondition() {
        return asCondition;
    }
    public void setAsCondition(String asCondition) {
        this.asCondition = asCondition;
    }
    public String getSupportSort() {
        return supportSort;
    }
    public void setSupportSort(String supportSort) {
        this.supportSort = supportSort;
    }
    public String getFormShow() {
        return formShow;
    }
    public void setFormShow(String formShow) {
        this.formShow = formShow;
    }
    public Integer getFormNum() {
        return formNum;
    }
    public void setFormNum(Integer formNum) {
        this.formNum = formNum;
    }
    public String getControlType() {
        return controlType;
    }
    public void setControlType(String controlType) {
        this.controlType = controlType;
    }
    public String getControlMust() {
        return controlMust;
    }
    public void setControlMust(String controlMust) {
        this.controlMust = controlMust;
    }
    public String getControlParam() {
        return controlParam;
    }
    public void setControlParam(String controlParam) {
        this.controlParam = controlParam;
    }
    public String getControlDefault() {
        return controlDefault;
    }
    public void setControlDefault(String controlDefault) {
        this.controlDefault = controlDefault;
    }
    public String getControlReadonly() {
        return controlReadonly;
    }
    public void setControlReadonly(String controlReadonly) {
        this.controlReadonly = controlReadonly;
    }
    public String getControlClass() {
        return controlClass;
    }
    public void setControlClass(String controlClass) {
        this.controlClass = controlClass;
    }

    public String getControlCheck() {
        return controlCheck;
    }

    public void setControlCheck(String controlCheck) {
        this.controlCheck = controlCheck;
    }

    public String getControlCheckVal() {
        return controlCheckVal;
    }

    public void setControlCheckVal(String controlCheckVal) {
        this.controlCheckVal = controlCheckVal;
    }

    public String getFillExplain() {
        return fillExplain;
    }

    public void setFillExplain(String fillExplain) {
        this.fillExplain = fillExplain;
    }
}