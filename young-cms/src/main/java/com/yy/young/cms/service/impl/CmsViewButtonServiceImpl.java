package com.yy.young.cms.service.impl;

import com.yy.young.cms.model.Button;
import com.yy.young.cms.model.ViewButton;
import com.yy.young.cms.service.ICmsButtonService;
import com.yy.young.cms.service.ICmsViewButtonService;
import com.yy.young.cms.util.CmsConstants;
import com.yy.young.common.util.CommonUtil;
import com.yy.young.dal.service.IDataAccessService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 视图按钮服务实现
 * Created by rookie on 2017/9/18.
 */
@Service("cmsViewButtonService")
public class CmsViewButtonServiceImpl implements ICmsViewButtonService {

    @Resource(name = "dataAccessService")
    IDataAccessService dataAccessService;//数据层服务

    @Resource(name="cmsButtonService")
    ICmsButtonService cmsButtonService;

    @Override
    public List<ViewButton> getViewButtonByViewId(String viewId) throws Exception {
        return dataAccessService.getList(CmsConstants.MAPPER.CMS_VIEW_BUTTON + ".getViewButtonByViewId", viewId);
    }

    @Override
    public ViewButton getViewButtonById(String id) throws Exception {
        return (ViewButton)dataAccessService.getObject(CmsConstants.MAPPER.CMS_VIEW_BUTTON + ".getViewButtonById", id);
    }

    @Override
    public int updateViewButton(ViewButton viewButton) throws Exception {
        return dataAccessService.update(CmsConstants.MAPPER.CMS_VIEW_BUTTON + ".updateViewButton", viewButton);
    }

    @Override
    public int deleteViewButton(String[] idArr) throws Exception {
        return dataAccessService.delete(CmsConstants.MAPPER.CMS_VIEW_BUTTON + ".deleteViewButton", idArr);
    }

    @Override
    public int batchAddButtonToView(String[] buttonIdArr, String viewId) throws Exception {
        int num = 0;
        for(String buttonId : buttonIdArr){
            //查询按钮信息
            Button button = cmsButtonService.getButton(buttonId);
            if (button != null){
                ViewButton viewButton = new ViewButton();
                viewButton.setId(CommonUtil.getUUID());//uuid
                viewButton.setName(button.getName());//按钮名称默认采用原始名称
                viewButton.setNum(button.getNum());//按钮显示顺序默认使用原始顺序
                viewButton.setButtonId(button.getId());//设置关联按钮编号
                viewButton.setViewId(viewId);//设置所属视图
                dataAccessService.insert(CmsConstants.MAPPER.CMS_VIEW_BUTTON + ".insertViewButton", viewButton);
                num++;
            }
        }
        return num;
    }
}
