package com.yy.young.cms.service;

import com.yy.young.cms.model.Column;
import com.yy.young.cms.model.Table;
import com.yy.young.dal.util.Page;

import java.util.List;

/**
 * 数据表字段服务接口
 * Created by Administrator on 2017/5/8.
 */
public interface ICmsTableColService {
    /**
     * 获取数据表字段列表
     * @param column
     * @return
     * @throws Exception
     */
    List<Column> getColList(Column column) throws Exception;

    /**
     * 根据id查询字段
     * @param id
     * @return
     * @throws Exception
     */
    Column getColumn(String id) throws Exception;

    /**
     * 获取数据表字段(分页)
     * @param column
     * @param page
     * @return
     * @throws Exception
     */
    List<Column> getColPage(Column column, Page page) throws Exception;

    /**
     * 查询某张表下的业务字段信息
     * @param tableId
     * @return
     * @throws Exception
     */
    List<Column> getColByTable(String tableId) throws Exception;

    /**
     * 修改数据表字段信息
     * @param column
     * @return
     * @throws Exception
     */
    int updateCol(Column column) throws Exception;

    /**
     * 删除数据表字段信息
     * @param idsArr 编号数组
     * @return
     * @throws Exception
     */
    int deleteCol(String[] idsArr) throws Exception;

    /**
     * 删除指定表的所有字段记录(删除业务记录,不负责删除实际库中的字段)
     * 删除表的时候调用此方法删除字段关联记录
     * @param tableId
     * @return
     * @throws Exception
     */
    int deleteAllColInfo(String tableId) throws Exception;

    /**
     * 插入数据表字段信息
     * @param column
     * @return
     * @throws Exception
     */
    int insertCol(Column column) throws Exception;

    /**
     * 批量保存字段
     * @param tableId 表名
     * @param list 字段集合
     * @return
     * @throws Exception
     */
    int batchSaveTableCol(String tableId, List<Column> list) throws Exception;

}
