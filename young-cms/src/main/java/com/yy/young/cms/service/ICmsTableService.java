package com.yy.young.cms.service;

import com.yy.young.cms.model.Table;
import com.yy.young.dal.util.Page;

import java.util.List;

/**
 * 数据表服务接口
 * Created by Administrator on 2017/5/8.
 */
public interface ICmsTableService {
    /**
     * 获取数据表列表
     * @param table
     * @return
     * @throws Exception
     */
    List<Table> getTableList(Table table) throws Exception;

    /**
     * 根据表名查询表信息
     * @param tableId
     * @return
     * @throws Exception
     */
    Table getTable(String tableId) throws Exception;

    /**
     * 获取数据表(分页)
     * @param table
     * @param page
     * @return
     * @throws Exception
     */
    List<Table> getTablePage(Table table, Page page) throws Exception;

    /**
     * 修改数据表信息
     * @param table
     * @return
     * @throws Exception
     */
    int updateTable(Table table) throws Exception;

    /**
     * 删除数据表信息
     * @param idsArr 编号数组
     * @return
     * @throws Exception
     */
    int deleteTable(String[] idsArr) throws Exception;

    /**
     * 插入数据表信息
     * @param table
     * @return
     * @throws Exception
     */
    int insertTable(Table table) throws Exception;
    
}
