package com.yy.young.cms.model.template;

import com.yy.young.cms.model.Column;

/**
 * 实体类成员变量实体类
 * Created by rookie on 2018/4/2.
 */
public class ModelAttribute {

    private Column column;//对应数据库字段
    private String type;//类型,Stirng/Date/Integer/Float/Double
    private String name;//名称
    private String remark;//注释

    public ModelAttribute(){
        super();
    }
    public ModelAttribute(Column column){
        super();
        if (column != null){
            this.column = column;
            //匹配类型
            if (column.getType() != null){
                if (column.getType().indexOf("char") > -1 || column.getType().indexOf("CHAR") > -1 || column.getType().indexOf("text") > -1 || column.getType().indexOf("TEXT") > -1){
                    this.type = "String";
                }else if (column.getType().indexOf("date") > -1 || column.getType().indexOf("DATE") > -1 || column.getType().indexOf("time") > -1 || column.getType().indexOf("TIME") > -1){
                    this.type = "Date";
                }else if (column.getType().indexOf("number") > -1 || column.getType().indexOf("NUMBER") > -1 || column.getType().indexOf("int") > -1 || column.getType().indexOf("INT") > -1){
                    this.type = "Integer";
                }else if (column.getType().indexOf("float") > -1 || column.getType().indexOf("FLOAT") > -1){
                    this.type = "Float";
                }else if (column.getType().indexOf("double") > -1 || column.getType().indexOf("DOUBLE") > -1){
                    this.type = "Double";
                }else{
                    this.type = "String";
                }
            }
            //生成变量名
            if (column.getField() != null && column.getField().indexOf("_") > -1){
                String[] arr = column.getField().split("_");
                StringBuilder sb = new StringBuilder();
                for (int i=0;i<arr.length;i++){
                    if (i == 0){
                        sb.append(arr[i].toLowerCase());//小写
                    }else{
                        sb.append(arr[i].substring(0, 1).toUpperCase()).append(arr[i].substring(1).toLowerCase());//从第二个起的单词首字母大写
                    }
                }
                this.name = sb.toString();
            }else{
                this.name = column.getField().toLowerCase();
            }
            //变量注释默认与字段注释一致
            this.remark = column.getRemark();
        }
    }

    public Column getColumn() {
        return column;
    }

    public void setColumn(Column column) {
        this.column = column;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
