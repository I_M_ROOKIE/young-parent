package com.yy.young.cms.service.impl;

import com.yy.young.cms.service.ICmsDataSourceService;
import com.yy.young.common.util.CommonUtil;
import com.yy.young.dal.service.IDataAccessService;
import com.yy.young.dal.util.Page;
import com.yy.young.cms.model.DataSource;
import com.yy.young.cms.util.CmsConstants;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * 视图数据源服务实现
 * Created by rookie on 2017-11-01.
 */
@Service("cmsDataSourceService")
public class CmsDataSourceServiceImpl implements ICmsDataSourceService {

    @Resource(name = "dataAccessService")
    IDataAccessService dataAccessService;//数据层服务

    private static final Logger logger = LoggerFactory.getLogger(CmsDataSourceServiceImpl.class);

    //获取数据列表
    @Override
    public List<DataSource> getDataSourceList(DataSource dataSource) throws Exception {
        return dataAccessService.getList(CmsConstants.MAPPER.TB_CMS_DATA_SOURCE + ".getDataSourceList", dataSource);
    }

    //获取数据列表(分页)
    @Override
    public List<DataSource> getDataSourcePage(DataSource dataSource, Page page) throws Exception {
        return dataAccessService.getList(CmsConstants.MAPPER.TB_CMS_DATA_SOURCE + ".getDataSourceList", dataSource, page);
    }

    //查询单条
    @Override
    public DataSource getDataSource(String id) throws Exception {
        return (DataSource)dataAccessService.getObject(CmsConstants.MAPPER.TB_CMS_DATA_SOURCE + ".getDataSourceById", id);
    }

    //修改
    @Override
    public int updateDataSource(DataSource dataSource) throws Exception {
        dataSource.setUpdateTime(new Date());//修改时间
        return dataAccessService.update(CmsConstants.MAPPER.TB_CMS_DATA_SOURCE + ".update", dataSource);
    }

    //批量删除
    @Override
    public int deleteDataSource(String[] idArr) throws Exception {
        int i = 0;
        for(String id : idArr){
            i += this.deleteDataSource(id);
        }
        return i;
    }

    //删除单条
    @Override
    public int deleteDataSource(String id) throws Exception {
        return dataAccessService.delete(CmsConstants.MAPPER.TB_CMS_DATA_SOURCE + ".delete", id);
    }

    //插入
    @Override
    public int insertDataSource(DataSource dataSource) throws Exception {
        if (dataSource.getId() == null){
            dataSource.setId(CommonUtil.getUUID());
        }
        dataSource.setCreateTime(new Date());//创建时间
        dataSource.setUpdateTime(new Date());//修改时间
        return dataAccessService.insert(CmsConstants.MAPPER.TB_CMS_DATA_SOURCE + ".insert", dataSource);
    }

}