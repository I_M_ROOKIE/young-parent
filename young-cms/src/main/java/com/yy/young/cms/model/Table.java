package com.yy.young.cms.model;


import java.util.List;

/**
 * cms数据库表实体类
 * Created by rookie on 2017/8/11.
 */
public class Table extends CmsBase {
    private String id;//表名 表名 表名,重要的事情说3遍
    private String name;//中文名
    private int num;//显示顺序
    private String remark;//备注
    private String businessId;//所属业务编号
    private String master;//主表标识 1:是,0:否
    private List<Column> columnList;//字段列表
    private String moduleId;//模块id

    @Override
    public String toString() {
        return "Table{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", num=" + num +
                ", remark='" + remark + '\'' +
                ", businessId='" + businessId + '\'' +
                ", master='" + master + '\'' +
                ", columnList=" + columnList +
                '}';
    }

    public String getModuleId() {
        return moduleId;
    }

    public void setModuleId(String moduleId) {
        this.moduleId = moduleId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }


    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    public String getMaster() {
        return master;
    }

    public void setMaster(String master) {
        this.master = master;
    }

    public List<Column> getColumnList() {
        return columnList;
    }

    public void setColumnList(List<Column> columnList) {
        this.columnList = columnList;
    }
}
