package com.yy.young.cms.model;

import java.util.Date;

/**
 * cms实体基类
 * Created by rookie on 2017/8/12.
 */
public class CmsBase implements java.io.Serializable {
    protected Date createTime;//创建时间
    protected Date updateTime;//修改时间
    protected String createUser;//创建人员id
    protected String companyId;//所属单位

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
