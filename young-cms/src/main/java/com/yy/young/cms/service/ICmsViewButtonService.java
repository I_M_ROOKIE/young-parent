package com.yy.young.cms.service;

import com.yy.young.cms.model.ViewButton;

import java.util.List;

/**
 * 视图按钮服务接口
 * Created by rookie on 2017/9/18.
 */
public interface ICmsViewButtonService {

    /**
     * 根据视图编号查询视图按钮按钮
     * @param viewId
     * @return
     * @throws Exception
     */
    List<ViewButton> getViewButtonByViewId(String viewId) throws Exception;

    /**
     * 根据编号查询视图按钮
     * @param id
     * @return
     * @throws Exception
     */
    ViewButton getViewButtonById(String id) throws Exception;

    /**
     * 修改视图按钮
     * @param viewButton
     * @return
     * @throws Exception
     */
    int updateViewButton(ViewButton viewButton) throws Exception;

    /**
     * 删除视图按钮
     * @param idArr 待删除视图按钮的编号数组
     * @return
     * @throws Exception
     */
    int deleteViewButton(String[] idArr) throws Exception;

    /**
     * 批量添加按钮到视图
     * @param buttonIdArr 按钮编号数组
     * @return
     */
    int batchAddButtonToView(String[] buttonIdArr, String viewId) throws Exception;
}
