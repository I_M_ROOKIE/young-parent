package com.yy.young.cms.web;

import com.yy.young.cms.model.Button;
import com.yy.young.cms.service.ICmsButtonService;
import com.yy.young.common.util.CommonUtil;
import com.yy.young.common.util.Result;
import com.yy.young.common.util.StringUtils;
import com.yy.young.dal.util.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * cms按钮请求处理
 * Created by Administrator on 2017/5/8.
 */
@Controller
@RequestMapping("/cms/button")
public class CmsButtonController {

    @Resource(name="cmsButtonService")
    ICmsButtonService cmsButtonService;

    /**
     * 获取按钮列表
     * @param button
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/getButtonList")
    @ResponseBody
    public Object getButtonList(Button button, HttpServletRequest request) throws Exception{
        List<Button> list = cmsButtonService.getButtonList(button);
        return new Result(list);
    }

    /**
     * 获取按钮列表
     * @param button
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/getButtonById")
    @ResponseBody
    public Object getButtonById(Button button, HttpServletRequest request) throws Exception{
        Result result = null;
        if(button != null && StringUtils.isNotBlank(button.getId())){
            button = cmsButtonService.getButton(button.getId());
            result = new Result(button);
        }else{
            result = new Result();
            result.setCode(-1);
            result.setInfo("查询按钮信息失败:按钮id为空");
        }
        return result;
    }

    /**
     * 获取按钮列表(分页)
     * @param button
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/getButtonPage")
    @ResponseBody
    public Object getButtonPage(Button button, HttpServletRequest request) throws Exception{
        Page page = CommonUtil.getPageFromRequest(request);
        List<Button> list = cmsButtonService.getButtonPage(button, page);
        page.setData(list);
        return page;
    }

    /**
     * 添加按钮
     * @param button
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/insertButton")
    @ResponseBody
    public Object insertButton(Button button, HttpServletRequest request) throws Exception{
        int num = cmsButtonService.insertButton(button);
        Result result = new Result();
        if(num < 0){
            result.setCode(-1);
            result.setInfo("添加按钮失败!");
        }
        return result;
    }

    /**
     * 修改按钮信息
     * @param button
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/updateButton")
    @ResponseBody
    public Object updateButton(Button button, HttpServletRequest request) throws Exception{
        int num = cmsButtonService.updateButton(button);
        Result result = new Result();
        if(num < 0){
            result.setCode(-1);
            result.setInfo("修改按钮信息失败!");
        }
        return result;
    }

    /**
     * 删除按钮信息
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/deleteButton")
    @ResponseBody
    public Object deleteButton(HttpServletRequest request) throws Exception{
        Map<String,Object> parameter = CommonUtil.getParameterFromRequest(request);
        String idsStr = parameter.get("ids")+"";
        String[] idArr;
        Result result = new Result();
        if(StringUtils.isNotBlank(idsStr)) {
            idArr = idsStr.split(",");
            int num = cmsButtonService.deleteButton(idArr);
            if(num < 0){
                result.setCode(-1);
                result.setInfo("删除按钮信息失败!");
            }
        }else{
            result.setCode(-1);
            result.setInfo("删除失败:待删除按钮ID无效!");
        }

        return result;
    }


}
