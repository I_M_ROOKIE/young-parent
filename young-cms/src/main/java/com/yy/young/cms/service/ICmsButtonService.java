package com.yy.young.cms.service;

import com.yy.young.cms.model.Button;
import com.yy.young.dal.util.Page;

import java.util.List;

/**
 * 按钮服务接口
 * Created by Administrator on 2017/5/8.
 */
public interface ICmsButtonService {
    /**
     * 获取按钮列表
     * @param button
     * @return
     * @throws Exception
     */
    List<Button> getButtonList(Button button) throws Exception;

    /**
     * 根据按钮id查询按钮信息
     * @param buttonId
     * @return
     * @throws Exception
     */
    Button getButton(String buttonId) throws Exception;

    /**
     * 获取按钮(分页)
     * @param button
     * @param page
     * @return
     * @throws Exception
     */
    List<Button> getButtonPage(Button button, Page page) throws Exception;

    /**
     * 修改按钮信息
     * @param button
     * @return
     * @throws Exception
     */
    int updateButton(Button button) throws Exception;

    /**
     * 删除按钮信息
     * @param idsArr 编号数组
     * @return
     * @throws Exception
     */
    int deleteButton(String[] idsArr) throws Exception;

    /**
     * 插入按钮信息
     * @param button
     * @return
     * @throws Exception
     */
    int insertButton(Button button) throws Exception;
    
}
