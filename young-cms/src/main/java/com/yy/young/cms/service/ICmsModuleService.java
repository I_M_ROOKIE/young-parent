package com.yy.young.cms.service;

import com.yy.young.cms.model.Module;
import com.yy.young.dal.util.Page;
import java.util.List;
import java.util.Map;

/**
 * 模块服务接口
 * Created by Administrator on 2017/5/8.
 */
public interface ICmsModuleService {
    /**
     * 获取模块列表
     * @param module
     * @return
     * @throws Exception
     */
    List<Module> getModuleList(Module module) throws Exception;

    /**
     * 获取模块(分页)
     * @param module
     * @param page
     * @return
     * @throws Exception
     */
    List<Module> getModulePage(Module module, Page page) throws Exception;

    /**
     * 修改模块信息
     * @param module
     * @return
     * @throws Exception
     */
    int updateModule(Module module) throws Exception;

    /**
     * 删除模块信息
     * @param idsArr
     * @return
     * @throws Exception
     */
    int deleteModule(String[] idsArr) throws Exception;

    /**
     * 插入模块信息
     * @param module
     * @return
     * @throws Exception
     */
    int insertModule(Module module) throws Exception;
    
}
