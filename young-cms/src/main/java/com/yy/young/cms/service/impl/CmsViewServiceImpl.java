package com.yy.young.cms.service.impl;

import com.yy.young.cms.model.View;
import com.yy.young.cms.service.ICmsViewService;
import com.yy.young.cms.util.CmsConstants;
import com.yy.young.common.util.CommonUtil;
import com.yy.young.common.util.StringUtils;
import com.yy.young.dal.service.IDataAccessService;
import com.yy.young.dal.util.Page;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * 视图服务实现
 * Created by rookie on 2017/8/11.
 */
@Service("cmsViewService")
public class CmsViewServiceImpl implements ICmsViewService {

    @Resource(name = "dataAccessService")
    IDataAccessService dataAccessService;//数据层服务

    @Override
    public List<View> getViewList(View view) throws Exception {
        return dataAccessService.getList(CmsConstants.MAPPER.CMS_VIEW + ".getView", view);
    }

    @Override
    public View getView(String viewId) throws Exception {
        return (View)dataAccessService.getObject(CmsConstants.MAPPER.CMS_VIEW + ".getViewById", viewId);
    }

    @Override
    public List<View> getViewPage(View view, Page page) throws Exception {
        return dataAccessService.getList(CmsConstants.MAPPER.CMS_VIEW + ".getView", view, page);
    }

    @Override
    public int updateView(View view) throws Exception {
        view.setUpdateTime(new Date());//修改时间
        return dataAccessService.update(CmsConstants.MAPPER.CMS_VIEW + ".updateView", view);
    }

    @Override
    public int deleteView(String[] idsArr) throws Exception {
        return dataAccessService.delete(CmsConstants.MAPPER.CMS_VIEW + ".deleteView", idsArr);
    }

    @Override
    public int insertView(View view) throws Exception {
        if (view.getId() == null){
            view.setId(CommonUtil.getUUID());
        }
        view.setCreateTime(new Date());//创建时间
        view.setUpdateTime(new Date());//修改时间
        return dataAccessService.insert(CmsConstants.MAPPER.CMS_VIEW + ".insertView", view);
    }

}
