package com.yy.young.cms.web;

import com.yy.young.cms.model.Comment;
import com.yy.young.cms.model.Topic;
import com.yy.young.cms.service.ICmsCommentService;
import com.yy.young.cms.service.ICmsTopicService;
import com.yy.young.common.util.CommonUtil;
import com.yy.young.common.util.Result;
import com.yy.young.common.util.StringUtils;
import com.yy.young.dal.util.Page;
import com.yy.young.interfaces.log.annotation.Log;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * cms第三方应用服务接口
 * Created by rookie on 2018/5/25.
 */
@Controller
@RequestMapping("/cms/api")
public class CmsApiController {

    @Resource(name="commentService")
    ICmsCommentService commentService;

    @Resource(name="topicService")
    ICmsTopicService topicService;

    /**
     * 根据应用编号查询主题
     * @param appId
     * @param request
     * @return
     * @throws Exception
     */
    @Log("根据应用编号查询主题")
    @RequestMapping("/getTopicList")
    @ResponseBody
    public Object getTopicList(String appId, HttpServletRequest request) throws Exception{
        Result result = new Result();
        if (StringUtils.isBlank(appId)){
            result.setCode(-1);
            result.setInfo("应用编号为空!");
        }else{
            Topic topic = new Topic();
            topic.setAppId(appId);
            result.setData(topicService.getListOfDetail(topic));
        }
        return result;
    }

    /**
     * 根据应用编号查询主题
     * @param appId 应用编号,非空
     * @param request
     * @return
     * @throws Exception
     */
    @Log("根据应用编号查询主题")
    @RequestMapping("/getTopicPage")
    @ResponseBody
    public Object getTopicPage(String appId, HttpServletRequest request) throws Exception{
        Page page = CommonUtil.getPageFromRequest(request);
        if (StringUtils.isBlank(appId)){
            page.setCode(-1);
            page.setInfo("应用编号为空!");
        }else{
            Topic topic = new Topic();
            topic.setAppId(appId);
            page.setData(topicService.getPageOfDetail(topic, page));
        }
        return page;
    }

    /**
     * 根据应用编号和源id查询主题
     * @param appId 应用编号,非空
     * @param sourceId 源编号,非空
     * @param request
     * @return
     * @throws Exception
     */
    @Log("根据应用编号和源id查询主题")
    @RequestMapping("/getTopic")
    @ResponseBody
    public Object getTopicPage(String appId, String sourceId, HttpServletRequest request) throws Exception{
        Result result = new Result();
        if (StringUtils.isBlank(appId)){
            result.setCode(-1);
            result.setInfo("应用编号为空!");
        }else if (StringUtils.isBlank(sourceId)){
            result.setCode(-1);
            result.setInfo("源编号为空!");
        }else{
            Topic topic = new Topic();
            topic.setAppId(appId);
            topic.setSourceId(sourceId);
            topic = topicService.get(appId, sourceId);
            if (topic != null){
                List list = new ArrayList();
                list.add(topic);
                result.setData(list);
            }
        }
        return result;
    }

    /**
     * 新增
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @Log("新增主题")
    @RequestMapping("/saveTopic")
    @ResponseBody
    public Object saveTopic(Topic obj, HttpServletRequest request) throws Exception{
        Result result = new Result();
        if (StringUtils.isBlank(obj.getAppId())){
            result.setCode(-1);
            result.setInfo("应用编号为空!");
        }else if (StringUtils.isBlank(obj.getSourceId())){
            result.setCode(-1);
            result.setInfo("源编号为空!");
        }else if (StringUtils.isBlank(obj.getContentText()) && StringUtils.isBlank(obj.getContentFormat())){
            result.setCode(-1);
            result.setInfo("内容为空!");
        }
        topicService.insert(obj);
        return result;
    }

    /**
     * 根据应用id和源id查询评论列表
     * @param appId 应用编号,非空
     * @param sourceId 源编号,非空
     * @param request
     * @return
     * @throws Exception
     */
    @Log("根据应用id和源id查询评论列表")
    @RequestMapping("/getCommentList")
    @ResponseBody
    public Object getCommentList(String appId, String sourceId, HttpServletRequest request) throws Exception{
        Result result = new Result();
        if (StringUtils.isBlank(appId)){
            result.setCode(-1);
            result.setInfo("应用编号为空!");
        }else if (StringUtils.isBlank(sourceId)){
            result.setCode(-1);
            result.setInfo("源编号为空!");
        }else{
            Comment comment = new Comment();
            comment.setAppId(appId);
            comment.setSourceId(sourceId);
            result.setData(commentService.getListOfDetail(comment));
        }
        return result;
    }

    /**
     * 根据应用id和源id查询评论列表
     * @param appId 应用编号,非空
     * @param sourceId 源编号,非空
     * @param request
     * @return
     * @throws Exception
     */
    @Log("根据应用id和源id查询评论列表")
    @RequestMapping("/getCommentPage")
    @ResponseBody
    public Object getCommentPage(String appId, String sourceId, HttpServletRequest request) throws Exception{
        Page page = CommonUtil.getPageFromRequest(request);
        if (StringUtils.isBlank(appId)){
            page.setCode(-1);
            page.setInfo("应用编号为空!");
        }else if (StringUtils.isBlank(sourceId)){
            page.setCode(-1);
            page.setInfo("源编号为空!");
        }else{
            Comment comment = new Comment();
            comment.setAppId(appId);
            comment.setSourceId(sourceId);
            page.setData(commentService.getPageOfDetail(comment, page));
        }
        return page;
    }

    /**
     * 新增评论
     * @param obj
     * @param request
     * @return
     * @throws Exception
     */
    @Log("新增评论")
    @RequestMapping("/saveComment")
    @ResponseBody
    public Object saveComment(Comment obj, HttpServletRequest request) throws Exception{
        Result result = new Result();
        if (StringUtils.isBlank(obj.getAppId())){
            result.setCode(-1);
            result.setInfo("应用编号为空!");
        }else if (StringUtils.isBlank(obj.getSourceId())){
            result.setCode(-1);
            result.setInfo("源编号为空!");
        }else if (StringUtils.isBlank(obj.getContentText()) && StringUtils.isBlank(obj.getContentFormat())){
            result.setCode(-1);
            result.setInfo("内容为空!");
        }
        commentService.insert(obj);
        return result;
    }
}
