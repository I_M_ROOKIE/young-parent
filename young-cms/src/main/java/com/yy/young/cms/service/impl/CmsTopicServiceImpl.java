package com.yy.young.cms.service.impl;

import com.yy.young.base.exception.ParameterException;
import com.yy.young.cms.util.CmsConstants;
import com.yy.young.common.util.CommonUtil;
import com.yy.young.common.util.StringUtils;
import com.yy.young.dal.service.IDataAccessService;
import com.yy.young.dal.util.Page;
import com.yy.young.cms.model.Topic;
import com.yy.young.cms.service.ICmsTopicService;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
* 主题服务实现
* Created by rookie on 2018-04-03.
*/
@Service("topicService")
public class CmsTopicServiceImpl implements ICmsTopicService {

    @Resource(name = "dataAccessService")
    IDataAccessService dataAccessService;//数据层服务

    private static final Logger logger = LoggerFactory.getLogger(CmsTopicServiceImpl.class);

    private static final String MAPPER = CmsConstants.MAPPER.CMS_TOPIC;//mapper的namespace

    //获取数据列表
    @Override
    public List<Topic> getList(Topic obj) throws Exception {
        return dataAccessService.getList(MAPPER + ".getList", obj);
    }

    @Override
    public List<Topic> getListOfDetail(Topic obj) throws Exception {
        return dataAccessService.getList(MAPPER + ".getListOfDetail", obj);
    }

    //获取数据列表(分页)
    @Override
    public List<Topic> getPage(Topic obj, Page page) throws Exception {
        return dataAccessService.getList(MAPPER + ".getList", obj, page);
    }

    @Override
    public List<Topic> getPageOfDetail(Topic obj, Page page) throws Exception {
        return dataAccessService.getList(MAPPER + ".getListOfDetail", obj, page);
    }

    //查询单条
    @Override
    public Topic get(String id) throws Exception {
        return (Topic)dataAccessService.getObject(MAPPER + ".get", id);
    }

    @Override
    public Topic get(String appId, String sourceId) throws Exception {
        Topic topic = new Topic();
        topic.setAppId(appId);
        topic.setSourceId(sourceId);
        return (Topic)dataAccessService.getObject(MAPPER + ".getByAppIdAndSourceId", topic);
    }

    //统计数量
    @Override
    public int count(Topic obj) throws Exception {
        return (Integer)dataAccessService.getObject(MAPPER + ".count", obj);
    }

    //修改
    @Override
    public int update(Topic obj) throws Exception {
        obj.setUpdateTime(new Date());
        return dataAccessService.update(MAPPER + ".update", obj);
    }

    //批量删除
    @Override
    public int delete(String[] idArr) throws Exception {
        int i = 0;
        for(String id : idArr){
            i += this.delete(id);
        }
        return i;
    }

    //删除单条
    @Override
    public int delete(String id) throws Exception {
        return dataAccessService.delete(MAPPER + ".delete", id);
    }

    //插入
    @Override
    public int insert(Topic obj) throws Exception {
        obj.setId(CommonUtil.getUUID());
        if (StringUtils.isBlank(obj.getSourceId())){//如果源id为空,则默认与id相同
            obj.setSourceId(obj.getId());
        }
        //判断是否存在该主题
        if (this.existTopic(obj.getAppId(), obj.getSourceId())){
            logger.error("[新增主题] 操作失败: 主题已存在.appId={},sourceId={}", obj.getAppId(), obj.getSourceId());
            throw new ParameterException("操作失败,该主题已存在");
        }
        obj.setCreateTime(new Date());
        obj.setUpdateTime(new Date());
        return dataAccessService.insert(MAPPER + ".insert", obj);
    }

    //批量插入
    @Override
    public int batchInsert(List<Topic> list) throws Exception {
        int i = 0;
        for(Topic item : list){
            i += this.insert(item);
        }
        return i;
    }

    //根据主题编号判断是否存在
    @Override
    public boolean existTopic(String id) throws Exception {
        Topic topic = new Topic();
        topic.setId(id);
        int i = this.count(topic);
        if (i > 0){
            return true;
        }
        return false;
    }

    //根据应用编号和源ID判断是否存在主题
    @Override
    public boolean existTopic(String appId, String sourceId) throws Exception {
        Topic topic = new Topic();
        topic.setAppId(appId);
        topic.setSourceId(sourceId);
        int i = (Integer)dataAccessService.getObject(MAPPER + ".countByAppIdAndSourceId", topic);
        if (i > 0){
            return true;
        }
        return false;
    }

}