package com.yy.young.cms.model.template;

import java.util.Date;
import java.util.List;

/**
 * 模型类模板实体类
 * Created by rookie on 2018/4/2.
 */
public class Model {
    private String packageName;//包名
    private String moduleName;//模块名
    private String className;//类名
    private String tableName;//表名
    private String pid;//主键字段名
    private String remark;//注释
    private String author;//作者
    private Date createTime;//创建时间
    private List<ModelAttribute> attributeList;//成员变量

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public List<ModelAttribute> getAttributeList() {
        return attributeList;
    }

    public void setAttributeList(List<ModelAttribute> attributeList) {
        this.attributeList = attributeList;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }
}
