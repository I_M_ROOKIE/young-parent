package com.yy.young.cms.service.impl;

import com.yy.young.cms.model.Column;
import com.yy.young.cms.service.ICmsDBService;
import com.yy.young.cms.service.ICmsTableColService;
import com.yy.young.cms.util.CmsConstants;
import com.yy.young.common.util.CommonUtil;
import com.yy.young.common.util.StringUtils;
import com.yy.young.dal.service.IDataAccessService;
import com.yy.young.dal.util.Page;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 业务表服务实现
 * Created by rookie on 2017/8/11.
 */
@Service("cmsTableColService")
public class CmsTableColServiceImpl implements ICmsTableColService {

    @Resource(name = "dataAccessService")
    IDataAccessService dataAccessService;//数据层服务

    @Resource(name = "cmsDBService")
    ICmsDBService cmsDBService;

    private static final Logger logger = LoggerFactory.getLogger(CmsTableColServiceImpl.class);


    @Override
    public List<Column> getColList(Column column) throws Exception {
        return dataAccessService.getList(CmsConstants.MAPPER.CMS_TABLE_COL + ".getCol", column);
    }

    @Override
    public Column getColumn(String id) throws Exception {
        return (Column)dataAccessService.getObject(CmsConstants.MAPPER.CMS_TABLE_COL + ".getColById", id);
    }

    @Override
    public List<Column> getColByTable(String tableId) throws Exception {
        return dataAccessService.getList(CmsConstants.MAPPER.CMS_TABLE_COL + ".getColByTable", tableId);
    }

    @Override
    public List<Column> getColPage(Column column, Page page) throws Exception {
        return dataAccessService.getList(CmsConstants.MAPPER.CMS_TABLE_COL + ".getCol", column, page);
    }

    @Override
    public int updateCol(Column column) throws Exception {
        //修改记录
        int num = dataAccessService.update(CmsConstants.MAPPER.CMS_TABLE_COL + ".updateCol", column);
        cmsDBService.modifyColumn(column);//修改字段
        return num;
    }

    @Override
    public int deleteCol(String[] idsArr) throws Exception {
        int num = 0;
        for (String id : idsArr){
            cmsDBService.deleteColumn(this.getColumn(id));//删除实际字段
            dataAccessService.delete(CmsConstants.MAPPER.CMS_TABLE_COL + ".deleteCol", new String[]{id});//删除字段信息
            num++;
        }
        return num;
    }

    @Override
    public int deleteAllColInfo(String tableId) throws Exception {
        return dataAccessService.delete(CmsConstants.MAPPER.CMS_TABLE_COL + ".deleteAllColInfo", tableId);
    }

    @Override
    public int insertCol(Column column) throws Exception {
        if(StringUtils.isBlank(column.getId())){
            column.setId(CommonUtil.getUUID());//赋默认值UUID
        }
        //插入字段信息
        int num = dataAccessService.insert(CmsConstants.MAPPER.CMS_TABLE_COL + ".insertCol", column);
        cmsDBService.addColumn(column);//新增字段
        return num;
    }

    @Override
    public int batchSaveTableCol(String tableId, List<Column> list) throws Exception {
        int num = 0;
        for(Column column : list){
            column.setTableId(tableId);//设置表名
            if (column.getId() != null && StringUtils.isNotBlank(column.getId())){//存在id,则修改
                logger.debug("[批量保存字段] 执行更新操作,字段信息:{}", column);
                this.updateCol(column);
            }else{//否则,新增
                column.setId(CommonUtil.getUUID());
                logger.debug("[批量保存字段] 执行新增操作,字段信息:{}", column);
                this.insertCol(column);
            }
            num++;
        }
        return num;
    }
}
