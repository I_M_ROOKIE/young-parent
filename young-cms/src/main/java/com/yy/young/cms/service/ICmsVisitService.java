package com.yy.young.cms.service;

import com.yy.young.dal.util.Page;

import java.util.List;
import java.util.Map;

/**
 * Created by rookie on 2018/4/9.
 */
public interface ICmsVisitService {
    /**
     * 查询数据
     * @param dataSourceId 数据源id
     * @param parameter 参数
     * @param page 分页参数
     * @return
     * @throws Exception
     */
    List<Map<String, Object>> query(String dataSourceId, Map<String, Object> parameter, Page page) throws Exception;
}
