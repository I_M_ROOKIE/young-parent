package com.yy.young.cms.web;

import com.yy.young.cms.service.ICmsVisitService;
import com.yy.young.common.util.CommonUtil;
import com.yy.young.common.util.Result;
import com.yy.young.common.util.StringUtils;
import com.yy.young.dal.util.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * cms访问控制器,cms客户端服务
 * 处理客户端关于数据源的请求
 * Created by rookie on 2018/4/9.
 */
@Controller
@RequestMapping("/cms")
public class CmsVisitController {

    @Resource(name="cmsVisitService")
    ICmsVisitService cmsVisitService;

    /**
     * 数据源访问入口
     * @param dataSourceId
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/query/{dataSourceId}")
    @ResponseBody
    public Object askDataSource(@PathVariable("dataSourceId")String dataSourceId, HttpServletRequest request) throws Exception{
        Result result = new Result();
        if (StringUtils.isNotBlank(dataSourceId)){
            Page page = null;
            if (StringUtils.isNotBlank(request.getParameter("pageNumber")) && StringUtils.isNotBlank(request.getParameter("pageSize"))){
                page = CommonUtil.getPageFromRequest(request);
            }
            result.setData(cmsVisitService.query(dataSourceId, CommonUtil.getParameterFromRequest(request), page));
        }
        return result;
    }
}
