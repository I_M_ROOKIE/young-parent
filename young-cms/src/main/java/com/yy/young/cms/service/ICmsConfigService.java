package com.yy.young.cms.service;

import com.yy.young.dal.util.Page;
import com.yy.young.cms.model.Config;
import java.util.List;

/**
 * 配置服务接口
 * Created by rookie on 2018-01-04.
 */
public interface ICmsConfigService {

    /**
     * 获取数据列表
     * @param config
     * @return
     * @throws Exception
     */
    List<Config> getConfigList(Config config) throws Exception;

    /**
     * 获取数据列表(分页)
     * @param config
     * @param page 分页参数
     * @return
     * @throws Exception
     */
    List<Config> getConfigPage(Config config, Page page) throws Exception;

    /**
     * 根据id获取信息
     * @param id
     * @return
     * @throws Exception
     */
    Config getConfig(String id) throws Exception;

    /**
     * 修改
     * @param config
     * @return
     * @throws Exception
     */
    int updateConfig(Config config) throws Exception;

    /**
     * 批量删除
     * @param idArr id数组
     * @return
     * @throws Exception
     */
    int deleteConfig(String[] idArr) throws Exception;

    /**
     * 删除单条
     * @param id
     * @return
     * @throws Exception
     */
    int deleteConfig(String id) throws Exception;

    /**
     * 插入
     * @param config
     * @return
     * @throws Exception
     */
    int insertConfig(Config config) throws Exception;

}