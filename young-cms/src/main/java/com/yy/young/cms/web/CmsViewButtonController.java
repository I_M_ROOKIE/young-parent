package com.yy.young.cms.web;

import com.yy.young.cms.model.Button;
import com.yy.young.cms.model.ViewButton;
import com.yy.young.cms.service.ICmsViewButtonService;
import com.yy.young.common.util.Result;
import com.yy.young.common.util.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 视图按钮web层
 * Created by rookie on 2017/9/18.
 */
@RequestMapping("/cms/viewButton")
@Controller
public class CmsViewButtonController {

    @Resource(name = "cmsViewButtonService")
    ICmsViewButtonService cmsViewButtonService;

    /**
     * 获取视图下的按钮
     * @param viewId
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/getViewButtonByViewId")
    @ResponseBody
    public Object getViewButtonByViewId(String viewId, HttpServletRequest request) throws Exception{
        List<ViewButton> list = cmsViewButtonService.getViewButtonByViewId(viewId);
        return new Result(list);
    }

    /**
     * 根据编号获取视图按钮
     * @param id
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/getViewButtonById")
    @ResponseBody
    public Object getViewButtonById(String id, HttpServletRequest request) throws Exception{
        ViewButton viewButton = cmsViewButtonService.getViewButtonById(id);
        return new Result(viewButton);
    }

    /**
     * 修改视图按钮
     * @param viewButton
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/updateViewButton")
    @ResponseBody
    public Object updateViewButton(ViewButton viewButton, HttpServletRequest request) throws Exception{
        cmsViewButtonService.updateViewButton(viewButton);
        return new Result();
    }

    /**
     * 删除视图按钮
     * @param ids
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/deleteViewButton")
    @ResponseBody
    public Object deleteViewButton(String ids, HttpServletRequest request) throws Exception{
        Result result = new Result();
        if (StringUtils.isNotBlank(ids)){
            cmsViewButtonService.deleteViewButton(ids.split(","));
        }else{
            result.setCode(-1);
            result.setInfo("删除失败:待删除编号无效!");
        }

        return result;
    }

    /**
     * 批量关联按钮和视图
     * @param buttonIds
     * @param viewId
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/batchAddButtonToView")
    @ResponseBody
    public Object batchAddButtonToView(String buttonIds, String viewId, HttpServletRequest request) throws Exception{
        Result result = new Result();
        if (StringUtils.isNotBlank(buttonIds) && StringUtils.isNotBlank(viewId)){
            cmsViewButtonService.batchAddButtonToView(buttonIds.split(","), viewId);
        }else{
            result.setCode(-1);
            result.setInfo("关联失败:视图编号或按钮编号无效!");
        }

        return result;
    }
}
