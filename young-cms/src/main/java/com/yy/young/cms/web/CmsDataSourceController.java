package com.yy.young.cms.web;

import com.yy.young.common.util.CommonUtil;
import com.yy.young.common.util.Result;
import com.yy.young.common.util.StringUtils;
import com.yy.young.dal.util.Page;
import com.yy.young.cms.model.DataSource;
import com.yy.young.cms.service.ICmsDataSourceService;
import com.yy.young.interfaces.model.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 视图数据源服务实现
 * Created by rookie on 2017-11-01.
 */
@Controller
@RequestMapping("/cms/dataSource")
public class CmsDataSourceController {

    @Resource(name="cmsDataSourceService")
    ICmsDataSourceService service;

    /**
     * 获取数据列表
     * @param dataSource
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/getDataSourceList")
    @ResponseBody
    public Object getDataSourceList(DataSource dataSource, HttpServletRequest request) throws Exception{
        List<DataSource> list = service.getDataSourceList(dataSource);
        return new Result(list);
    }

    /**
     * 获取分页数据
     * @param dataSource
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/getDataSourcePage")
    @ResponseBody
    public Object getDataSourcePage(DataSource dataSource, HttpServletRequest request) throws Exception{
        Page page = CommonUtil.getPageFromRequest(request);
        List<DataSource> list = service.getDataSourcePage(dataSource, page);
        page.setData(list);
        return page;
    }

    /**
     * 获取单条数据
     * @param id
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/getDataSource")
    @ResponseBody
    public Object getDataSource(String id, HttpServletRequest request) throws Exception{
        DataSource dataSource = service.getDataSource(id);
        return new Result(dataSource);
    }

    /**
     * 新增
     * @param dataSource
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/insert")
    @ResponseBody
    public Object insert(DataSource dataSource, HttpServletRequest request) throws Exception{
        User user = CommonUtil.getLoginUser(request);
        dataSource.setCreateUser(user.getId());//创建用户id
        dataSource.setCompanyId(user.getCompanyId());//创建用户单位
        int num = service.insertDataSource(dataSource);
        return new Result();
    }

    /**
     * 修改
     * @param dataSource
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/update")
    @ResponseBody
    public Object update(DataSource dataSource, HttpServletRequest request) throws Exception{
        int num = service.updateDataSource(dataSource);
        return new Result();
    }

    /**
     * 删除
     * @param ids
     * @param id
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/delete")
    @ResponseBody
    public Object delete(String ids, String id, HttpServletRequest request) throws Exception{
        Result result = new Result();
        if(StringUtils.isNotBlank(ids)) {
            String[] idArr = ids.split(",");
            int num = service.deleteDataSource(idArr);
        }else if(StringUtils.isNotBlank(id)){
            int num = service.deleteDataSource(id);
        }else{
            result.setCode(-1);
            result.setInfo("删除失败:待删除编号无效!");
        }
        return result;
    }



}