package com.yy.young.cms.web;

import com.yy.young.cms.model.Column;
import com.yy.young.cms.service.ICmsTableColService;
import com.yy.young.common.util.CommonJsonUtil;
import com.yy.young.common.util.CommonUtil;
import com.yy.young.common.util.Result;
import com.yy.young.common.util.StringUtils;
import com.yy.young.dal.util.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * cms业务表字段请求处理
 * Created by Administrator on 2017/5/8.
 */
@Controller
@RequestMapping("/cms/tableCol")
public class CmsTableColController {

    @Resource(name="cmsTableColService")
    ICmsTableColService tableColService;

    /**
     * 获取业务表字段列表
     * @param column
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/getTableColList")
    @ResponseBody
    public Object getTableColList(Column column, HttpServletRequest request) throws Exception{
        List<Column> list = tableColService.getColList(column);
        return new Result(list);
    }

    /**
     * 获取表下的业务字段信息
     * @param column
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/getColByTable")
    @ResponseBody
    public Object getColByTable(Column column, HttpServletRequest request) throws Exception{
        List<Column> list = tableColService.getColByTable(column.getTableId());
        return new Result(list);
    }

    /**
     * 获取业务表字段列表(分页)
     * @param column
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/getTableColPage")
    @ResponseBody
    public Object getTableColPage(Column column, HttpServletRequest request) throws Exception{
        Page page = CommonUtil.getPageFromRequest(request);
        List<Column> list = tableColService.getColPage(column, page);
        page.setData(list);
        return page;
    }

    /**
     * 添加业务表字段
     * @param column
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/insertTableCol")
    @ResponseBody
    public Object insertTableCol(Column column, HttpServletRequest request) throws Exception{
        Result result = new Result();
        int num = tableColService.insertCol(column);
        if(num < 0){
            result.setCode(-1);
            result.setInfo("添加字段失败!");
        }
        return result;
    }

    /**
     * 批量保存字段
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/batchSaveTableCol")
    @ResponseBody
    public Object batchSaveTableCol(HttpServletRequest request) throws Exception{
        Result result = new Result();
        String tableId = request.getParameter("tableId");//表名
        if(StringUtils.isBlank(tableId)){
            result.setCode(-1);
            result.setInfo("表名为空!");
            return result;
        }
        String arrStr = request.getParameter("cols");//json字符串形式的字段
        if(StringUtils.isBlank(arrStr)){
            result.setCode(-1);
            result.setInfo("待保存字段为空!");
            return result;
        }
        //转为list形式
        List<Column> columnList = CommonJsonUtil.jsonStrArrToBeanList(arrStr, Column.class);
        if (columnList != null && columnList.size() > 0){
            tableColService.batchSaveTableCol(tableId, columnList);
        }
        return result;
    }

    /**
     * 修改业务表字段信息
     * @param column
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/updateTableCol")
    @ResponseBody
    public Object updateTable(Column column, HttpServletRequest request) throws Exception{
        int num = tableColService.updateCol(column);
        Result result = new Result();
        if(num < 0){
            result.setCode(-1);
            result.setInfo("修改字段信息失败!");
        }
        return result;
    }

    /**
     * 删除业务表字段信息
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/deleteTableCol")
    @ResponseBody
    public Object deleteTableCol(HttpServletRequest request) throws Exception{
        Map<String,Object> parameter = CommonUtil.getParameterFromRequest(request);
        String idsStr = parameter.get("ids")+"";
        String[] idArr;
        Result result = new Result();
        if(StringUtils.isNotBlank(idsStr)) {
            idArr = idsStr.split(",");
            int num = tableColService.deleteCol(idArr);
            if(num < 0){
                result.setCode(-1);
                result.setInfo("删除字段信息失败!");
            }
        }else{
            result.setCode(-1);
            result.setInfo("删除失败:待删除字段ID无效!");
        }

        return result;
    }


}
