package com.yy.young.cms.service.impl;

import com.yy.young.cms.service.ICmsConfigService;
import com.yy.young.common.util.CommonUtil;
import com.yy.young.common.util.StringUtils;
import com.yy.young.dal.service.IDataAccessService;
import com.yy.young.dal.util.Page;
import com.yy.young.cms.model.Config;
import com.yy.young.cms.util.CmsConstants;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * 配置服务实现
 * Created by rookie on 2018-01-04.
 */
@Service("configService")
public class CmsConfigServiceImpl implements ICmsConfigService {

    @Resource(name = "dataAccessService")
    IDataAccessService dataAccessService;//数据层服务

    private static final Logger logger = LoggerFactory.getLogger(CmsConfigServiceImpl.class);

    //获取数据列表
    @Override
    public List<Config> getConfigList(Config config) throws Exception {
        return dataAccessService.getList(CmsConstants.MAPPER.TB_CMS_CONFIG + ".getConfigList", config);
    }

    //获取数据列表(分页)
    @Override
    public List<Config> getConfigPage(Config config, Page page) throws Exception {
        return dataAccessService.getList(CmsConstants.MAPPER.TB_CMS_CONFIG + ".getConfigList", config, page);
    }

    //查询单条
    @Override
    public Config getConfig(String id) throws Exception {
        return (Config)dataAccessService.getObject(CmsConstants.MAPPER.TB_CMS_CONFIG + ".getConfigById", id);
    }

    //修改
    @Override
    public int updateConfig(Config config) throws Exception {
        config.setUpdateTime(new Date());
        return dataAccessService.update(CmsConstants.MAPPER.TB_CMS_CONFIG + ".update", config);
    }

    //批量删除
    @Override
    public int deleteConfig(String[] idArr) throws Exception {
        int i = 0;
        for(String id : idArr){
            i += this.deleteConfig(id);
        }
        return i;
    }

    //删除单条
    @Override
    public int deleteConfig(String id) throws Exception {
        return dataAccessService.delete(CmsConstants.MAPPER.TB_CMS_CONFIG + ".delete", id);
    }

    //插入
    @Override
    public int insertConfig(Config config) throws Exception {
        if (StringUtils.isBlank(config.getId())){
            config.setId(CommonUtil.getUUID());
        }
        config.setCreateTime(new Date());
        config.setUpdateTime(new Date());
        return dataAccessService.insert(CmsConstants.MAPPER.TB_CMS_CONFIG + ".insert", config);
    }

}