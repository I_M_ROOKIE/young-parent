package com.yy.young.cms.service.impl;

import com.yy.young.cms.model.GenCol;
import com.yy.young.cms.service.ICmsGenColService;
import com.yy.young.cms.util.CmsConstants;
import com.yy.young.common.util.CommonUtil;
import com.yy.young.common.util.StringUtils;
import com.yy.young.dal.service.IDataAccessService;
import com.yy.young.dal.util.Page;
import com.yy.young.cms.model.GenMain;
import com.yy.young.cms.service.ICmsGenMainService;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
* 代码生成主体信息服务实现
* Created by rookie on 2018-04-03.
*/
@Service("genMainService")
public class CmsGenMainServiceImpl implements ICmsGenMainService {

    @Resource(name = "dataAccessService")
    IDataAccessService dataAccessService;//数据层服务

    @Resource(name="genColService")
    ICmsGenColService genColService;

    private static final Logger logger = LoggerFactory.getLogger(CmsGenMainServiceImpl.class);

    private static final String MAPPER = CmsConstants.MAPPER.CMS_GEN_MAIN;//mapper的namespace

    //获取数据列表
    @Override
    public List<GenMain> getList(GenMain obj) throws Exception {
        return dataAccessService.getList(MAPPER + ".getList", obj);
    }

    //获取数据列表(分页)
    @Override
    public List<GenMain> getPage(GenMain obj, Page page) throws Exception {
        return dataAccessService.getList(MAPPER + ".getList", obj, page);
    }

    //查询单条
    @Override
    public GenMain get(String id) throws Exception {
        return (GenMain)dataAccessService.getObject(MAPPER + ".get", id);
    }

    //统计数量
    @Override
    public int count(GenMain obj) throws Exception {
        return (Integer)dataAccessService.getObject(MAPPER + ".count", obj);
    }

    //修改
    @Override
    public int update(GenMain obj) throws Exception {
        obj.setUpdateTime(new Date());
        int i = dataAccessService.update(MAPPER + ".update", obj);
        //更新字段配置
        if (obj.getColList() != null){
            //设置字段配置所属的代码生成编号
            for(GenCol genCol : obj.getColList()){
                genCol.setGenId(obj.getId());
                genColService.update(genCol);
            }
        }
        return i;
    }

    //批量删除
    @Override
    public int delete(String[] idArr) throws Exception {
        int i = 0;
        for(String id : idArr){
            i += this.delete(id);
        }
        return i;
    }

    //删除单条
    @Override
    public int delete(String id) throws Exception {
        genColService.deleteByGenId(id);//删除子数据
        return dataAccessService.delete(MAPPER + ".delete", id);
    }

    //插入
    @Override
    public int insert(GenMain obj) throws Exception {
        if (StringUtils.isBlank(obj.getId())){
            obj.setId(CommonUtil.getUUID());
        }
        obj.setCreateTime(new Date());
        obj.setUpdateTime(new Date());
        int i = dataAccessService.insert(MAPPER + ".insert", obj);//插入代码生成主信息
        //插入字段配置
        if (obj.getColList() != null){
            //设置字段配置所属的代码生成编号
            for(GenCol genCol : obj.getColList()){
                genCol.setGenId(obj.getId());
            }
            //批量插入字段配置
            genColService.batchInsert(obj.getColList());
        }
        return i;
    }

    //批量插入
    @Override
    public int batchInsert(List<GenMain> list) throws Exception {
        int i = 0;
        for(GenMain item : list){
            i += this.insert(item);
        }
        return i;
    }

}