package com.yy.young.cms.web;

import com.yy.young.common.util.CommonUtil;
import com.yy.young.common.util.Result;
import com.yy.young.common.util.StringUtils;
import com.yy.young.dal.util.Page;
import com.yy.young.interfaces.log.annotation.Log;
import com.yy.young.cms.model.Config;
import com.yy.young.cms.service.ICmsConfigService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 配置服务
 * Created by rookie on 2018-01-04.
 */
@Controller
@RequestMapping("/cms/config")
public class CmsConfigController {

    @Resource(name="configService")
    ICmsConfigService service;

    /**
     * 获取数据列表
     * @param config
     * @param request
     * @return
     * @throws Exception
     */
    @Log("查询配置列表")
    @RequestMapping("/getConfigList")
    @ResponseBody
    public Object getConfigList(Config config, HttpServletRequest request) throws Exception{
        List<Config> list = service.getConfigList(config);
        return new Result(list);
    }

    /**
     * 获取分页数据
     * @param config
     * @param request
     * @return
     * @throws Exception
     */
    @Log("分页查询配置列表")
    @RequestMapping("/getConfigPage")
    @ResponseBody
    public Object getConfigPage(Config config, HttpServletRequest request) throws Exception{
        Page page = CommonUtil.getPageFromRequest(request);
        List<Config> list = service.getConfigPage(config, page);
        page.setData(list);
        return page;
    }

    /**
     * 获取单条数据
     * @param id
     * @param request
     * @return
     * @throws Exception
     */
    @Log("查询配置")
    @RequestMapping("/getConfig")
    @ResponseBody
    public Object getConfig(String id, HttpServletRequest request) throws Exception{
        Config config = service.getConfig(id);
        return new Result(config);
    }

    /**
     * 新增
     * @param config
     * @param request
     * @return
     * @throws Exception
     */
    @Log("新增配置")
    @RequestMapping("/insert")
    @ResponseBody
    public Object insert(Config config, HttpServletRequest request) throws Exception{
        int num = service.insertConfig(config);
        return new Result();
    }

    /**
     * 修改
     * @param config
     * @param request
     * @return
     * @throws Exception
     */
    @Log("修改配置")
    @RequestMapping("/update")
    @ResponseBody
    public Object update(Config config, HttpServletRequest request) throws Exception{
        int num = service.updateConfig(config);
        return new Result();
    }

    /**
     * 删除
     * @param ids
     * @param id
     * @param request
     * @return
     * @throws Exception
     */
    @Log("删除配置")
    @RequestMapping("/delete")
    @ResponseBody
    public Object delete(String ids, String id, HttpServletRequest request) throws Exception{
        Result result = new Result();
        if(StringUtils.isNotBlank(ids)) {
            String[] idArr = ids.split(",");
            int num = service.deleteConfig(idArr);
        }else if(StringUtils.isNotBlank(id)){
            int num = service.deleteConfig(id);
        }else{
            result.setCode(-1);
            result.setInfo("删除失败:待删除编号无效!");
        }
        return result;
    }

}