package com.yy.young.cms.model;

/**
 * 数据表字段实体类
 * Created by rookie on 2017/8/26.
 */
public class Column implements java.io.Serializable {

    private String id;  //主键uuid
    private String tableId; //表名
    private String field;   //字段名
    private String type;    //字段类型,用于字段记录
    private String proType; //实际数据库中的类型,不同类型的数据库类型表示不同
    private Integer length; //字段长度
    private Integer floatLength;    //浮点类型的小数位长度
    private String notNull; //非空
    private String remark;  //备注
    private int num;    //显示顺序

    public Column(){
        super();
    }

    public Column(String field, String type, Integer length, Integer floatLength, String notNull){
        super();
        this.field = field;
        this.type = type;
        this.length = length;
        this.floatLength = floatLength;
        this.notNull = notNull;
    }

    @Override
    public String toString() {
        return "Column{" +
                "id='" + id + '\'' +
                ", tableId='" + tableId + '\'' +
                ", field='" + field + '\'' +
                ", type='" + type + '\'' +
                ", length=" + length +
                ", notNull='" + notNull + '\'' +
                ", remark='" + remark + '\'' +
                ", num=" + num +
                '}';
    }

    public String getProType() {
        return proType;
    }

    public void setProType(String proType) {
        this.proType = proType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getType() {
        return type;
    }

    public Integer getFloatLength() {
        return floatLength;
    }

    public void setFloatLength(Integer floatLength) {
        this.floatLength = floatLength;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public String getNotNull() {
        return notNull;
    }

    public void setNotNull(String notNull) {
        this.notNull = notNull;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }
}
