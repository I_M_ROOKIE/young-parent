package com.yy.young.cms.util;

import com.yy.young.dal.EDBType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 数据库sql关于时间格式化工具类
 * Created by rookie on 2018/4/9.
 */
public class SqlUtil {

    private static Logger logger = LoggerFactory.getLogger(SqlUtil.class);

    /**
     * 将日期转化成数据库中的日期格式
     * @param date 日期字符串
     * @param edbType 数据库类型
     * @return String TO_DATE('2005-01-01','yyyy-mm-dd')
     */
    public static String formatDateStr(String date, EDBType edbType) {
        if (date == null)
            return null;
        StringBuffer result = new StringBuffer();
        if (edbType == EDBType.ORACLE){
            result.append("TO_DATE('");
            if (date.indexOf(' ') > 0) {
                result.append(date.substring(0, date.indexOf(' ')));
            } else {
                result.append(date);
            }
            result.append("','yyyy-mm-dd')");
        }else if (edbType == EDBType.MYSQL){
            result.append("STR_TO_DATE('");
            if (date.indexOf(' ') > 0) {
                result.append(date.substring(0, date.indexOf(' ')));
            } else {
                result.append(date);
            }
            result.append("','%Y-%m-%d')");
        }else if (edbType == EDBType.POSTGRESQL){
            result.append("to_date('");
            if (date.indexOf(' ') > 0) {
                result.append(date.substring(0, date.indexOf(' ')));
            } else {
                result.append(date);
            }
            result.append("','YYYY-MM-DD')");
        }else{//默认使用oracle方式
            logger.warn("[sql日期格式化] 未知的数据库类型: {}", edbType);
            result.append("TO_DATE('");
            if (date.indexOf(' ') > 0) {
                result.append(date.substring(0, date.indexOf(' ')));
            } else {
                result.append(date);
            }
            result.append("','yyyy-mm-dd')");
        }
        return result.toString();
    }

    /**
     * 格式化年-月类型的时间字符串
     * @param date
     * @param edbType
     * @return
     */
    public static String formatYearMonthStr(String date, EDBType edbType) {
        if (date == null)
            return null;
        StringBuffer result = new StringBuffer();
        if (edbType == EDBType.ORACLE){
            result.append("TO_DATE('");
            if (date.indexOf(' ') > 0) {
                result.append(date.substring(0, date.indexOf(' ')));
            } else {
                result.append(date);
            }
            result.append("','yyyy-mm')");
        }else if (edbType == EDBType.MYSQL){
            result.append("STR_TO_DATE('");
            if (date.indexOf(' ') > 0) {
                result.append(date.substring(0, date.indexOf(' ')));
            } else {
                result.append(date);
            }
            result.append("','%Y-%m')");
        }else if (edbType == EDBType.POSTGRESQL){
            result.append("to_date('");
            if (date.indexOf(' ') > 0) {
                result.append(date.substring(0, date.indexOf(' ')));
            } else {
                result.append(date);
            }
            result.append("','YYYY-MM')");
        }else{//默认使用oracle方式
            logger.warn("[sql年月格式化] 未知的数据库类型: {}", edbType);
            result.append("TO_DATE('");
            if (date.indexOf(' ') > 0) {
                result.append(date.substring(0, date.indexOf(' ')));
            } else {
                result.append(date);
            }
            result.append("','yyyy-mm')");
        }
        return result.toString();
    }

    /**
     * 格式化年时间字符串
     * @param date
     * @param edbType
     * @return
     */
    public static String formatYearStr(String date, EDBType edbType) {
        if (date == null)
            return null;
        StringBuffer result = new StringBuffer();
        if (edbType == EDBType.ORACLE){
            result.append("TO_DATE('");
            if (date.indexOf(' ') > 0) {
                result.append(date.substring(0, date.indexOf(' ')));
            } else {
                result.append(date);
            }
            result.append("','yyyy')");
        }else if (edbType == EDBType.MYSQL){
            result.append("STR_TO_DATE('");
            if (date.indexOf(' ') > 0) {
                result.append(date.substring(0, date.indexOf(' ')));
            } else {
                result.append(date);
            }
            result.append("','%Y')");
        }else if (edbType == EDBType.POSTGRESQL){
            result.append("to_date('");
            if (date.indexOf(' ') > 0) {
                result.append(date.substring(0, date.indexOf(' ')));
            } else {
                result.append(date);
            }
            result.append("','YYYY')");
        }else{//默认使用oracle方式
            logger.warn("[sql年格式化] 未知的数据库类型: {}", edbType);
            result.append("TO_DATE('");
            if (date.indexOf(' ') > 0) {
                result.append(date.substring(0, date.indexOf(' ')));
            } else {
                result.append(date);
            }
            result.append("','yyyy')");
        }
        return result.toString();
    }

    /**
     * 格式化时间字符串
     * @param dateTime 时间字符串
     * @param edbType 数据库类型
     * @return String TO_DATE('2005-01-01 12:10:11','yyyy-mm-dd hh24:mi:ss')
     */
    public static String formatTimeStr(String dateTime, EDBType edbType) {
        if (dateTime == null)
            return null;
        StringBuffer result = new StringBuffer();
        if (edbType == EDBType.ORACLE){
            result.append("TO_DATE('");
            if (dateTime.indexOf('.') > 0) {
                result.append(dateTime.substring(0, dateTime.indexOf('.')));
            } else {
                result.append(dateTime);
            }
            result.append("','yyyy-mm-dd hh24:mi:ss')");
        }else if (edbType == EDBType.MYSQL){
            result.append("STR_TO_DATE('");
            if (dateTime.indexOf('.') > 0) {
                result.append(dateTime.substring(0, dateTime.indexOf('.')));
            } else {
                result.append(dateTime);
            }
            result.append("','%Y-%m-%d %H:%i:%s')");
        }else if (edbType == EDBType.POSTGRESQL){
            result.append("to_timestamp('");
            if (dateTime.indexOf('.') > 0) {
                result.append(dateTime.substring(0, dateTime.indexOf('.')));
            } else {
                result.append(dateTime);
            }
            result.append("','YYYY-MM-DD HH24:MI:SS')");
        }else{//默认使用oracle方式
            logger.warn("[sql时间格式化] 未知的数据库类型: {}", edbType);
            result.append("TO_DATE('");
            if (dateTime.indexOf('.') > 0) {
                result.append(dateTime.substring(0, dateTime.indexOf('.')));
            } else {
                result.append(dateTime);
            }
            result.append("','yyyy-mm-dd hh24:mi:ss')");
        }
        return result.toString();
    }
}
