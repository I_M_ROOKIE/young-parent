package com.yy.young.cms.web;

import com.yy.young.base.util.GlobalConstants;
import com.yy.young.cms.model.Business;
import com.yy.young.cms.service.ICmsBusinessService;
import com.yy.young.common.util.CommonUtil;
import com.yy.young.common.util.Result;
import com.yy.young.common.util.StringUtils;
import com.yy.young.dal.util.Page;
import com.yy.young.interfaces.model.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * cms业务请求处理
 * Created by Administrator on 2017/5/8.
 */
@Controller
@RequestMapping("/cms/business")
public class CmsBusinessController {

    @Resource(name="cmsBusinessService")
    ICmsBusinessService businessService;

    /**
     * 获取业务列表
     * @param business
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/getBusinessList")
    @ResponseBody
    public Object getBusinessList(Business business, HttpServletRequest request) throws Exception{
        List<Business> list = businessService.getBusinessList(business);
        return new Result(list);
    }

    /**
     * 获取业务列表(分页)
     * @param business
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/getBusinessPage")
    @ResponseBody
    public Object getBusinessPage(Business business, HttpServletRequest request) throws Exception{
        Page page = CommonUtil.getPageFromRequest(request);
        List<Business> list = businessService.getBusinessPage(business, page);
        page.setData(list);
        return page;
    }

    /**
     * 添加业务
     * @param business
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/insertBusiness")
    @ResponseBody
    public Object insertBusiness(Business business, HttpServletRequest request) throws Exception{
        User user = (User) request.getSession().getAttribute(GlobalConstants.SESSION.KEY_LOGINUSER);
        Result result = new Result();
        //添加当前用户信息
        business.setCreateUser(user.getId());
        business.setCompanyId(user.getCompanyId());
        int num = businessService.insertBusiness(business);
        if(num < 0){
            result.setCode(-1);
            result.setInfo("添加业务失败!");
        }
        return result;
    }

    /**
     * 修改业务信息
     * @param business
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/updateBusiness")
    @ResponseBody
    public Object updateBusiness(Business business, HttpServletRequest request) throws Exception{
        int num = businessService.updateBusiness(business);
        Result result = new Result();
        if(num < 0){
            result.setCode(-1);
            result.setInfo("修改业务信息失败!");
        }
        return result;
    }

    /**
     * 删除业务信息
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/deleteBusiness")
    @ResponseBody
    public Object deleteBusiness(HttpServletRequest request) throws Exception{
        Map<String,Object> parameter = CommonUtil.getParameterFromRequest(request);
        String idsStr = parameter.get("ids")+"";
        String[] idArr;
        Result result = new Result();
        if(StringUtils.isNotBlank(idsStr)) {
            idArr = idsStr.split(",");
            int num = businessService.deleteBusiness(idArr);
            if(num < 0){
                result.setCode(-1);
                result.setInfo("删除业务信息失败!");
            }
        }else{
            result.setCode(-1);
            result.setInfo("删除失败:待删除业务ID无效!");
        }

        return result;
    }


}
