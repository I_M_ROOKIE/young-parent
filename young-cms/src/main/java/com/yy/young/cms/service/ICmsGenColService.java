package com.yy.young.cms.service;

import com.yy.young.dal.util.Page;
import com.yy.young.cms.model.GenCol;
import java.util.List;

/**
* 代码生成字段配置服务接口
* Created by rookie on 2018-04-03.
*/
public interface ICmsGenColService {

    /**
    * 查询
    * @param obj
    * @return
    * @throws Exception
    */
    List<GenCol> getList(GenCol obj) throws Exception;

    /**
    * 分页查询
    * @param obj
    * @param page
    * @return
    * @throws Exception
    */
    List<GenCol> getPage(GenCol obj, Page page) throws Exception;

    /**
    * 查询单条
    * @param id
    * @return
    * @throws Exception
    */
    GenCol get(String id) throws Exception;

    /**
    * 统计数量
    * @param obj
    * @return Integer 满足xx条件的数据有多少条
    * @throws Exception
    */
    int count(GenCol obj) throws Exception;

    /**
    * 修改
    * @param obj
    * @return
    * @throws Exception
    */
    int update(GenCol obj) throws Exception;

    /**
    * 删除
    * @param id
    * @return
    * @throws Exception
    */
    int delete(String id) throws Exception;

    /**
    * 批量删除
    * @param idArr
    * @return
    * @throws Exception
    */
    int delete(String[] idArr) throws Exception;

    /**
    * 插入
    * @param obj
    * @return
    * @throws Exception
    */
    int insert(GenCol obj) throws Exception;

    /**
    * 批量插入
    * @param list
    * @return
    * @throws Exception
    */
    int batchInsert(List<GenCol> list) throws Exception;

    /**
     * 根据代码生成编号删除字段配置
     * @param genId
     * @return
     * @throws Exception
     */
    int deleteByGenId(String genId) throws Exception;

}