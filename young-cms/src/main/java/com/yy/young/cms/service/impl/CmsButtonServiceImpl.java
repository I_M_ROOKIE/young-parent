package com.yy.young.cms.service.impl;

import com.yy.young.cms.model.Button;
import com.yy.young.cms.service.ICmsButtonService;
import com.yy.young.cms.util.CmsConstants;
import com.yy.young.common.util.StringUtils;
import com.yy.young.dal.service.IDataAccessService;
import com.yy.young.dal.util.Page;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 按钮服务实现
 * Created by rookie on 2017/8/11.
 */
@Service("cmsButtonService")
public class CmsButtonServiceImpl implements ICmsButtonService {

    @Resource(name = "dataAccessService")
    IDataAccessService dataAccessService;//数据层服务

    @Override
    public List<Button> getButtonList(Button button) throws Exception {
        return dataAccessService.getList(CmsConstants.MAPPER.CMS_BUTTON + ".getButton", button);
    }

    @Override
    public Button getButton(String buttonId) throws Exception {
        return (Button)dataAccessService.getObject(CmsConstants.MAPPER.CMS_BUTTON + ".getButtonById", buttonId);
    }

    @Override
    public List<Button> getButtonPage(Button button, Page page) throws Exception {
        return dataAccessService.getList(CmsConstants.MAPPER.CMS_BUTTON + ".getButton", button, page);
    }

    @Override
    public int updateButton(Button button) throws Exception {
        return dataAccessService.update(CmsConstants.MAPPER.CMS_BUTTON + ".updateButton", button);
    }

    @Override
    public int deleteButton(String[] idsArr) throws Exception {
        return dataAccessService.delete(CmsConstants.MAPPER.CMS_BUTTON + ".deleteButton", idsArr);
    }

    @Override
    public int insertButton(Button button) throws Exception {
        if (button != null && StringUtils.isBlank(button.getModuleId()) ){
            button.setModuleId(CmsConstants.COMMON_BUTTON_MODULE_ID);//公共按钮虚拟模块编号
        }
        return dataAccessService.insert(CmsConstants.MAPPER.CMS_BUTTON + ".insertButton", button);
    }

}
