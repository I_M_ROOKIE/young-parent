package com.yy.young.cms.service.impl;

import com.yy.young.cms.model.Module;
import com.yy.young.cms.service.ICmsModuleService;
import com.yy.young.cms.util.CmsConstants;
import com.yy.young.common.util.StringUtils;
import com.yy.young.dal.service.IDataAccessService;
import com.yy.young.dal.util.Page;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 模块服务实现
 * Created by rookie on 2017/8/11.
 */
@Service("cmsModuleService")
public class CmsModuleServiceImpl implements ICmsModuleService {

    @Resource(name = "dataAccessService")
    IDataAccessService dataAccessService;//数据层服务

    @Override
    public List<Module> getModuleList(Module module) throws Exception {
        return dataAccessService.getList(CmsConstants.MAPPER.CMS_MODULE + ".getModule", module);
    }

    @Override
    public List<Module> getModulePage(Module module, Page page) throws Exception {
        return dataAccessService.getList(CmsConstants.MAPPER.CMS_MODULE + ".getModule", module, page);
    }

    @Override
    public int updateModule(Module module) throws Exception {
        return dataAccessService.update(CmsConstants.MAPPER.CMS_MODULE + ".updateModule", module);
    }

    @Override
    public int deleteModule(String[] idsArr) throws Exception {

        return dataAccessService.delete(CmsConstants.MAPPER.CMS_MODULE + ".deleteModule", idsArr);
    }

    @Override
    public int insertModule(Module module) throws Exception {
        module.setCreateTime(new Date());//创建时间

        return dataAccessService.insert(CmsConstants.MAPPER.CMS_MODULE + ".insertModule", module);
    }
}
