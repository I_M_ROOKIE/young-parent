package com.yy.young.cms.web;

import com.yy.young.base.util.GlobalConstants;
import com.yy.young.cms.model.Module;
import com.yy.young.cms.service.ICmsModuleService;
import com.yy.young.common.util.CommonUtil;
import com.yy.young.common.util.Result;
import com.yy.young.common.util.StringUtils;
import com.yy.young.dal.util.Page;
import com.yy.young.interfaces.model.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * cms模块请求处理
 * Created by Administrator on 2017/5/8.
 */
@Controller
@RequestMapping("/cms/module")
public class CmsModuleController {

    @Resource(name="cmsModuleService")
    ICmsModuleService moduleService;

    /**
     * 获取模块列表
     * @param module
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/getModuleList")
    @ResponseBody
    public Object getModuleList(Module module, HttpServletRequest request) throws Exception{
        List<Module> list = moduleService.getModuleList(module);
        return new Result(list);
    }

    /**
     * 获取模块列表(分页)
     * @param module
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/getModulePage")
    @ResponseBody
    public Object getModulePage(Module module, HttpServletRequest request) throws Exception{
        Page page = CommonUtil.getPageFromRequest(request);
        List<Module> list = moduleService.getModulePage(module, page);
        page.setData(list);
        return page;
    }

    /**
     * 添加模块
     * @param module
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/insertModule")
    @ResponseBody
    public Object insertModule(Module module, HttpServletRequest request) throws Exception{
        User user = (User) request.getSession().getAttribute(GlobalConstants.SESSION.KEY_LOGINUSER);
        //添加当前用户信息
        module.setCreateUser(user.getId());
        module.setCompanyId(user.getCompanyId());
        int num = moduleService.insertModule(module);
        Result result = new Result();
        if(num < 0){
            result.setCode(-1);
            result.setInfo("添加模块失败!");
        }
        return result;
    }

    /**
     * 修改模块信息
     * @param module
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/updateModule")
    @ResponseBody
    public Object updateModule(Module module, HttpServletRequest request) throws Exception{
        int num = moduleService.updateModule(module);
        Result result = new Result();
        if(num < 0){
            result.setCode(-1);
            result.setInfo("修改模块信息失败!");
        }
        return result;
    }

    /**
     * 删除模块信息
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/deleteModule")
    @ResponseBody
    public Object deleteModule(HttpServletRequest request) throws Exception{
        Map<String,Object> parameter = CommonUtil.getParameterFromRequest(request);
        String idsStr = parameter.get("ids")+"";
        String[] idArr;
        Result result = new Result();
        if(StringUtils.isNotBlank(idsStr)) {
            idArr = idsStr.split(",");
            int num = moduleService.deleteModule(idArr);
            if(num < 0){
                result.setCode(-1);
                result.setInfo("删除模块信息失败!");
            }
        }else{
            result.setCode(-1);
            result.setInfo("删除失败:待删除模块ID无效!");
        }

        return result;
    }


}
