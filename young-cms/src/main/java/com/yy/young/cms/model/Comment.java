package com.yy.young.cms.model;

import com.yy.young.common.core.excel.ExcelColumn;
import java.util.Date;
/**
* 评论实体类
* Created by rookie on 2018-5-17.
*/
public class Comment{

    @ExcelColumn(value = "评论编号", order = 1)
    private String id;//评论编号

    @ExcelColumn(value = "主题编号", order = 2)
    private String topicId;//主题编号

    @ExcelColumn(value = "文本内容", order = 3)
    private String contentText;//文本内容

    @ExcelColumn(value = "带格式内容", order = 4)
    private String contentFormat;//带格式内容

    @ExcelColumn(value = "作者编号", order = 5)
    private String authorId;//作者编号

    @ExcelColumn(value = "作者名称", order = 6)
    private String authorName;//作者名称

    @ExcelColumn(value = "创建时间", order = 7)
    private Date createTime;//创建时间

    @ExcelColumn(value = "修改时间", order = 8)
    private Date updateTime;//修改时间

    @ExcelColumn(value = "应用编号", order = 9)
    private String appId;//应用编号

    @ExcelColumn(value = "回复的评论编号", order = 10)
    private String commentId;//回复的评论编号

    @ExcelColumn(value = "应用中的主题编号", order = 11)
    private String sourceId;//应用中的主题编号

    public Comment(){
        super();
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getTopicId() {
        return topicId;
    }
    public void setTopicId(String topicId) {
        this.topicId = topicId;
    }
    public String getContentText() {
        return contentText;
    }
    public void setContentText(String contentText) {
        this.contentText = contentText;
    }
    public String getContentFormat() {
        return contentFormat;
    }
    public void setContentFormat(String contentFormat) {
        this.contentFormat = contentFormat;
    }
    public String getAuthorId() {
        return authorId;
    }
    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }
    public String getAuthorName() {
        return authorName;
    }
    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }
    public Date getCreateTime() {
        return createTime;
    }
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    public Date getUpdateTime() {
        return updateTime;
    }
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
    public String getAppId() {
        return appId;
    }
    public void setAppId(String appId) {
        this.appId = appId;
    }
    public String getCommentId() {
        return commentId;
    }
    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }
    public String getSourceId() {
        return sourceId;
    }
    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

}