package com.yy.young.cms.web;

import com.yy.young.cms.model.Column;
import com.yy.young.cms.model.GenCol;
import com.yy.young.cms.model.template.Model;
import com.yy.young.cms.model.template.ModelAttribute;
import com.yy.young.cms.service.ICmsGenColService;
import com.yy.young.common.core.excel.ExcelExport;
import com.yy.young.common.core.excel.ExcelImport;
import com.yy.young.common.core.excel.IExcelExport;
import com.yy.young.common.core.excel.IExcelImport;
import com.yy.young.common.util.CommonJsonUtil;
import com.yy.young.common.util.CommonUtil;
import com.yy.young.common.util.Result;
import com.yy.young.common.util.StringUtils;
import com.yy.young.dal.EDBType;
import com.yy.young.dal.service.IDataAccessService;
import com.yy.young.dal.util.Page;
import com.yy.young.interfaces.log.annotation.Log;
import com.yy.young.cms.model.GenMain;
import com.yy.young.cms.service.ICmsGenMainService;
import com.yy.young.interfaces.model.User;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.*;

/**
* 代码生成主体信息服务
* Created by rookie on 2018-04-03.
*/
@Controller
@RequestMapping("/cms/genMain")
public class CmsGenMainController {

    @Resource(name="genMainService")
    ICmsGenMainService service;

    @Resource(name="genColService")
    ICmsGenColService genColService;

    @Resource(name = "dataAccessService")
    IDataAccessService dataAccessService;//数据层服务

    private final static Logger logger = LoggerFactory.getLogger(CmsGenMainController.class);

    /**
    * 获取数据列表
    * @param obj
    * @param request
    * @return
    * @throws Exception
    */
    @Log("查询代码生成主体信息列表")
    @RequestMapping("/getList")
    @ResponseBody
    public Object getList(GenMain obj, HttpServletRequest request) throws Exception{
        List<GenMain> list = service.getList(obj);
        return new Result(list);
    }

    /**
    * 获取分页数据
    * @param obj
    * @param request
    * @return
    * @throws Exception
    */
    @Log("分页查询代码生成主体信息列表")
    @RequestMapping("/getPage")
    @ResponseBody
    public Object getPage(GenMain obj, HttpServletRequest request) throws Exception{
        Page page = CommonUtil.getPageFromRequest(request);
        List<GenMain> list = service.getPage(obj, page);
        page.setData(list);
        return page;
    }

    /**
    * 获取单条数据
    * @param id
    * @param request
    * @return
    * @throws Exception
    */
    @Log("查询代码生成主体信息")
    @RequestMapping("/get")
    @ResponseBody
    public Object get(String id, HttpServletRequest request) throws Exception{
        GenMain obj = service.get(id);
        return new Result(obj);
    }

    /**
    * 统计数量
    * @param obj
    * @param request
    * @return
    * @throws Exception
    */
    @Log("统计代码生成主体信息数量")
    @RequestMapping("/count")
    @ResponseBody
    public Object count(GenMain obj, HttpServletRequest request) throws Exception{
        return new Result(service.count(obj));
    }

    /**
    * 新增
    * @param obj
    * @param request
    * @return
    * @throws Exception
    */
    @Log("新增代码生成主体信息")
    @RequestMapping("/insert")
    @ResponseBody
    public Object insert(GenMain obj, HttpServletRequest request) throws Exception{
        User user = CommonUtil.getLoginUser(request);
        if (user != null){
            obj.setCreateUserId(user.getId());
            obj.setCreateUserName(user.getName());
        }
        //获取字段配置信息
        String cols = request.getParameter("cols");
        if(StringUtils.isNotBlank(cols)){
            //将字符串格式的信息转为list
            List<GenCol> colList = CommonJsonUtil.jsonStrArrToBeanList(cols, GenCol.class);
            obj.setColList(colList);
        }
        service.insert(obj);
        return new Result();
    }

    /**
    * 修改
    * @param obj
    * @param request
    * @return
    * @throws Exception
    */
    @Log("修改代码生成主体信息")
    @RequestMapping("/update")
    @ResponseBody
    public Object update(GenMain obj, HttpServletRequest request) throws Exception{
        //获取字段配置信息
        String cols = request.getParameter("cols");
        if(StringUtils.isNotBlank(cols)){
            //将字符串格式的信息转为list
            List<GenCol> colList = CommonJsonUtil.jsonStrArrToBeanList(cols, GenCol.class);
            obj.setColList(colList);
        }
        service.update(obj);
        return new Result();
    }

    /**
    * 删除
    * @param ids
    * @param id
    * @param request
    * @return
    * @throws Exception
    */
    @Log("删除代码生成主体信息")
    @RequestMapping("/delete")
    @ResponseBody
    public Object delete(String ids, String id, HttpServletRequest request) throws Exception{
        Result result = new Result();
        if(StringUtils.isNotBlank(ids)) {
            String[] idArr = ids.split(",");
            service.delete(idArr);
        }else if(StringUtils.isNotBlank(id)){
            service.delete(id);
        }else{
            result.setCode(-1);
            result.setInfo("删除失败:待删除编号无效!");
        }
        return result;
    }

    /**
    * 导入
    * @param file
    * @param request
    * @return
    * @throws Exception
    */
    @Log("导入代码生成主体信息")
    @RequestMapping("/import")
    @ResponseBody
    public Object importExcel(MultipartFile file, HttpServletRequest request) throws Exception{
        Result result = new Result();
        try {
            if (file != null && !file.isEmpty()) {
                //excel导入处理,返回excel中的数据集合
                IExcelImport ei = new ExcelImport(file);//将文件转为ExcelImport对象
                //从excel读取数据
                List<GenMain> list = ei.getImportDataAsBean(GenMain.class);
                if (list != null && list.size() > 0){
                    int num = service.batchInsert(list);//批量插入
                    result.setInfo("成功导入数据" + num + "条!");
                }else {
                    result.setCode(-1);
                    result.setInfo("导入失败:excel解析后结果为空!");
                }
            } else {
                result.setCode(-1);
                result.setInfo("导入失败:文件为空!");
            }
        } catch (IOException e) {
            e.printStackTrace();
            result.setCode(-1);
            result.setInfo("导入失败!");
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            result.setCode(-1);
            result.setInfo("导入失败!");
        } catch (Exception e) {
            e.printStackTrace();
            result.setCode(-1);
            result.setInfo("导入失败!");
        }
        return result;
    }

    /**
    * 导出
    * @param obj 查询的参数
    * @param request
    * @param response
    * @throws Exception
    */
    @Log("导出代码生成主体信息")
    @RequestMapping("/export")
    public void exportExcel(GenMain obj, HttpServletRequest request, HttpServletResponse response) throws Exception {
        List<GenMain> list = service.getList(obj);
        if (list != null && list.size() > 0){
            IExcelExport ee = new ExcelExport();
            ee.insertBeanList(list, GenMain.class);
            ee.write2Response(response, "excel_" + System.currentTimeMillis() + ".xls");
        }else{
            response.getWriter().write("数据为空!");
        }
    }

    /**
     * 代码生成
     * @param genId 代码生成编号
     * @param request
     * @return
     * @throws Exception
     */
    @Log("代码生成")
    @RequestMapping("/generate/{genId}")
    @ResponseBody
    public Object generate(@PathVariable("genId")String genId, HttpServletRequest request) throws Exception{
        Result result = new Result();
        if (StringUtils.isBlank(genId)){
            result.setCode(-1);
            result.setInfo("代码生成编号不允许为空");
            return result;
        }
        //查询相关配置
        GenMain genMain = service.get(genId);
        if (genMain == null){
            result.setCode(-1);
            result.setInfo("未找到代码生成配置");
        }else if (StringUtils.isBlank(genMain.getPackageName()) || StringUtils.isBlank(genMain.getModuleName())){
            //包名和模块名不允许为空
            result.setCode(-1);
            result.setInfo("包名和模块名不允许为空");
        }else if (StringUtils.isBlank(genMain.getTableName())){
            result.setCode(-1);
            result.setInfo("表名不允许为空");
        }else if (StringUtils.isBlank(genMain.getSavePath())){
            result.setCode(-1);
            result.setInfo("代码保存路径不允许为空");
        }else{
            //查询字段配置
            GenCol genCol = new GenCol();
            genCol.setGenId(genMain.getId());
            List colList = genColService.getList(genCol);
            genMain.setColList(colList);
            if (colList != null && colList.size() > 0){
                //格式化字段类型,改为java中的类型
                for(GenCol col : genMain.getColList()){
                    if (col.getFieldType().indexOf("char") > -1 || col.getFieldType().indexOf("CHAR") > -1 || col.getFieldType().indexOf("text") > -1 || col.getFieldType().indexOf("TEXT") > -1){
                        col.setFieldType("String");
                    }else if (col.getFieldType().indexOf("date") > -1 || col.getFieldType().indexOf("DATE") > -1 || col.getFieldType().indexOf("time") > -1 || col.getFieldType().indexOf("TIME") > -1){
                        col.setFieldType("Date");
                    }else if (col.getFieldType().indexOf("number") > -1 || col.getFieldType().indexOf("NUMBER") > -1 || col.getFieldType().indexOf("int") > -1 || col.getFieldType().indexOf("INT") > -1){
                        col.setFieldType("Integer");
                    }else if (col.getFieldType().indexOf("float") > -1 || col.getFieldType().indexOf("FLOAT") > -1){
                        col.setFieldType("Float");
                    }else if (col.getFieldType().indexOf("double") > -1 || col.getFieldType().indexOf("DOUBLE") > -1){
                        col.setFieldType("Double");
                    }else{
                        col.setFieldType("String");
                    }
                }
                if (StringUtils.isBlank(genMain.getClassName())){//类名为空时,自动生成类名
                    String[] arr = genMain.getTableName().split("_");
                    StringBuilder sb = new StringBuilder();
                    for (int i=0;i<arr.length;i++){
                        sb.append(arr[i].substring(0, 1).toUpperCase()).append(arr[i].substring(1).toLowerCase());//单词首字母大写
                    }
                    genMain.setClassName(sb.toString());
                }

                //生成文件目录
                String rootPath = genMain.getSavePath();//"C:\\Users\\Administrator\\Desktop\\generate";//根目录
                String packagePath = rootPath + File.separator + "src" + File.separator + "main" + File.separator + "java" + File.separator + genMain.getPackageName().replace(".", File.separator) + File.separator + genMain.getModuleName();
                //资源目录,用来放配置文件和mapper文件
                String resourcePath = rootPath + File.separator + "src" + File.separator + "main" + File.separator + "resources" + File.separator + genMain.getPackageName().replace(".", File.separator) + File.separator + genMain.getModuleName();
                String pagePath = rootPath + File.separator + "src" + File.separator + "main" + File.separator + "webapp" + File.separator + "page" + File.separator + genMain.getModuleName();//jsp存放目录
                String path_model = packagePath + File.separator + "model";
                String path_service = packagePath + File.separator + "service";
                String path_service_impl = packagePath + File.separator + "service" + File.separator + "impl";
                String path_util = packagePath + File.separator + "util";
                String path_web = packagePath + File.separator + "web";
                String path_mapper;
                if (dataAccessService.getDBType() == EDBType.MYSQL){
                    path_mapper = resourcePath + File.separator + "mapper" + File.separator + "mysql";
                }else if (dataAccessService.getDBType() == EDBType.ORACLE){
                    path_mapper = resourcePath + File.separator + "mapper" + File.separator + "oracle";
                }else if (dataAccessService.getDBType() == EDBType.POSTGRESQL){
                    path_mapper = resourcePath + File.separator + "mapper" + File.separator + "postgres";
                }else {
                    result.setCode(-1);
                    result.setInfo("数据库类型不匹配");
                    return result;
                }
                String[] dirs = {path_model, path_service, path_service_impl, path_util, path_web, path_mapper, pagePath};
                //创建各目录
                for(String dir : dirs){
                    File f = new File(dir);
                    if (!f.exists()){//不存在此目录,则创建
                        f.mkdirs();
                        logger.info("[代码生成] 成功创建目录: {}", f.getAbsolutePath());
                    }
                }

                //将配置信息放入map,在ftl中使用
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("model", genMain);

                //生成实体类
                Configuration configuration = new Configuration();
                configuration.setClassForTemplateLoading(this.getClass(), "/template/gen/");
                try {
                    Template template = configuration.getTemplate("Model.ftl", "UTF-8");//Locale.CHINA,
                    try {
                        //FileWriter fw = new FileWriter(new File(path_model + File.separator + model.getClassName()+".java"));
                        Writer fw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(path_model + File.separator + genMain.getClassName()+".java"), "UTF-8"));
                        template.process(map, fw);
                        fw.flush();
                        fw.close();
                        logger.info("[代码生成] 成功创建实体类: {}", genMain.getClassName() + ".java");
                    } catch (TemplateException e) {
                        e.printStackTrace();
                        result.setCode(-1);
                        result.setInfo("写入文件失败");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    result.setCode(-1);
                    result.setInfo("获取模板失败");
                }

                //生成Mapper.xml映射文件
                try {
                    Template template = configuration.getTemplate("Mapper.ftl", "UTF-8");//Locale.CHINA,
                    try {
                        //FileWriter fw = new FileWriter(new File(path_mapper + File.separator + model.getClassName()+"Mapper.xml"));
                        Writer fw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(path_mapper + File.separator + genMain.getClassName()+"Mapper.xml"), "UTF-8"));
                        template.process(map, fw);
                        fw.flush();
                        fw.close();
                        logger.info("[代码生成] 成功创建Mapper.xml: {}", genMain.getClassName()+"Mapper.xml");
                    } catch (TemplateException e) {
                        e.printStackTrace();
                        result.setCode(-1);
                        result.setInfo("写入文件失败");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    result.setCode(-1);
                    result.setInfo("获取模板失败");
                }

                //生成service接口
                try {
                    Template template = configuration.getTemplate("Service.ftl", "UTF-8");//Locale.CHINA,
                    try {
                        //FileWriter fw = new FileWriter(new File(path_service + File.separator + "I" + model.getClassName()+"Service.java"));
                        Writer fw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(path_service + File.separator + "I" + genMain.getClassName()+"Service.java"), "UTF-8"));
                        template.process(map, fw);
                        fw.flush();
                        fw.close();
                        logger.info("[代码生成] 成功创建service接口: {}", "I" + genMain.getClassName()+"Service.java");
                    } catch (TemplateException e) {
                        e.printStackTrace();
                        result.setCode(-1);
                        result.setInfo("写入文件失败");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    result.setCode(-1);
                    result.setInfo("获取模板失败");
                }

                //生成serviceImpl服务实现类
                try {
                    Template template = configuration.getTemplate("ServiceImpl.ftl", "UTF-8");//Locale.CHINA,
                    try {
                        //FileWriter fw = new FileWriter(new File(path_service_impl + File.separator + model.getClassName()+"ServiceImpl.java"));
                        Writer fw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(path_service_impl + File.separator + genMain.getClassName()+"ServiceImpl.java"), "UTF-8"));
                        template.process(map, fw);
                        fw.flush();
                        fw.close();
                        logger.info("[代码生成] 成功创建service实现类: {}", genMain.getClassName()+"ServiceImpl.java");
                    } catch (TemplateException e) {
                        e.printStackTrace();
                        result.setCode(-1);
                        result.setInfo("写入文件失败");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    result.setCode(-1);
                    result.setInfo("获取模板失败");
                }

                //生成controller
                try {
                    Template template = configuration.getTemplate("Controller.ftl", "UTF-8");//Locale.CHINA,
                    try {
                        //FileWriter fw = new FileWriter(new File(path_web + File.separator + model.getClassName()+"Controller.java"));
                        Writer fw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(path_web + File.separator + genMain.getClassName()+"Controller.java"), "UTF-8"));
                        template.process(map, fw);
                        fw.flush();
                        fw.close();
                        logger.info("[代码生成] 成功创建Controller类: {}", genMain.getClassName()+"Controller.java");
                    } catch (TemplateException e) {
                        e.printStackTrace();
                        result.setCode(-1);
                        result.setInfo("写入文件失败");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    result.setCode(-1);
                    result.setInfo("获取模板失败");
                }

                //生成视图页
                //对字段配置按照视图显示顺序排序
                Collections.sort(genMain.getColList(), new Comparator<GenCol>() {
                    @Override
                    public int compare(GenCol o1, GenCol o2) {
                        if (o1.getViewNum() != null && o2.getViewNum() != null){
                            if (o1.getViewNum() > o2.getViewNum()){
                                return 1;
                            }else if (o1.getViewNum() < o2.getViewNum()){
                                return -1;
                            }
                        }
                        return 0;
                    }
                });
                try {
                    Template template = configuration.getTemplate("List.ftl", "UTF-8");//Locale.CHINA,
                    try {
                        //FileWriter fw = new FileWriter(new File(pagePath + File.separator + model.getClassName().substring(0,1).toLowerCase()+model.getClassName().substring(1) + "List.jsp"));
                        Writer fw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(pagePath + File.separator + genMain.getClassName().substring(0,1).toLowerCase()+genMain.getClassName().substring(1) + "List.jsp"), "UTF-8"));
                        template.process(map, fw);
                        fw.flush();
                        fw.close();
                        logger.info("[代码生成] 成功创建视图页: {}", genMain.getClassName().substring(0,1).toLowerCase()+genMain.getClassName().substring(1) + "List.jsp");
                    } catch (TemplateException e) {
                        e.printStackTrace();
                        result.setCode(-1);
                        result.setInfo("写入文件失败");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    result.setCode(-1);
                    result.setInfo("获取模板失败");
                }

                //生成表单页
                //将在表单显示的字段筛选出来
                List<GenCol> list2 = new ArrayList<GenCol>();
                for(GenCol genCol1 : genMain.getColList()){
                    if ("1".equals(genCol1.getFormShow())){
                        list2.add(genCol1);
                    }
                }
                genMain.setColList(list2);//将在表单显示的字段放进去
                //对字段配置按照表单显示顺序排序
                Collections.sort(genMain.getColList(), new Comparator<GenCol>() {
                    @Override
                    public int compare(GenCol o1, GenCol o2) {
                        if (o1.getFormNum() != null && o2.getFormNum() != null){
                            if (o1.getFormNum() > o2.getFormNum()){
                                return 1;
                            }else if (o1.getFormNum() < o2.getFormNum()){
                                return -1;
                            }
                        }
                        return 0;
                    }
                });
                try {
                    Template template = configuration.getTemplate("Form.ftl", "UTF-8");//Locale.CHINA,
                    try {
                        //FileWriter fw = new FileWriter(new File(pagePath + File.separator + model.getClassName().substring(0,1).toLowerCase()+model.getClassName().substring(1) + "Form.jsp"));
                        Writer fw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(pagePath + File.separator + genMain.getClassName().substring(0,1).toLowerCase()+genMain.getClassName().substring(1) + "Form.jsp"), "UTF-8"));
                        template.process(map, fw);
                        fw.flush();
                        fw.close();
                        logger.info("[代码生成] 成功创建表单页: {}", genMain.getClassName().substring(0,1).toLowerCase()+genMain.getClassName().substring(1) + "Form.jsp");
                    } catch (TemplateException e) {
                        e.printStackTrace();
                        result.setCode(-1);
                        result.setInfo("写入文件失败");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    result.setCode(-1);
                    result.setInfo("获取模板失败");
                }
            }else{
                result.setCode(-1);
                result.setInfo("未找到字段信息!");
            }
        }
        return new Result();
    }

}