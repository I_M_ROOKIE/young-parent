package com.yy.young.cms.web;

import com.yy.young.cms.model.View;
import com.yy.young.cms.model.ViewButton;
import com.yy.young.cms.service.ICmsViewButtonService;
import com.yy.young.cms.service.ICmsViewService;
import com.yy.young.common.util.CommonUtil;
import com.yy.young.common.util.Result;
import com.yy.young.common.util.StringUtils;
import com.yy.young.dal.util.Page;
import com.yy.young.interfaces.model.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * cms视图请求处理
 * Created by Administrator on 2017/5/8.
 */
@Controller
@RequestMapping("/cms/view")
public class CmsViewController {

    @Resource(name="cmsViewService")
    ICmsViewService cmsViewService;

    @Resource(name = "cmsViewButtonService")
    ICmsViewButtonService cmsViewButtonService;

    /**
     * 获取视图列表
     * @param view
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/getViewList")
    @ResponseBody
    public Object getViewList(View view, HttpServletRequest request) throws Exception{
        List<View> list = cmsViewService.getViewList(view);
        return new Result(list);
    }

    /**
     * 获取视图列表
     * @param view
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/getViewById")
    @ResponseBody
    public Object getViewById(View view, HttpServletRequest request) throws Exception{
        Result result = null;
        if(view != null && StringUtils.isNotBlank(view.getId())){
            view = cmsViewService.getView(view.getId());
            result = new Result(view);
        }else{
            result = new Result();
            result.setCode(-1);
            result.setInfo("查询视图信息失败:视图id为空");
        }
        return result;
    }

    /**
     * 获取视图列表(分页)
     * @param view
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/getViewPage")
    @ResponseBody
    public Object getViewPage(View view, HttpServletRequest request) throws Exception{
        Page page = CommonUtil.getPageFromRequest(request);
        List<View> list = cmsViewService.getViewPage(view, page);
        page.setData(list);
        return page;
    }

    /**
     * 添加视图
     * @param view
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/insertView")
    @ResponseBody
    public Object insertView(View view, HttpServletRequest request) throws Exception{
        User user = CommonUtil.getLoginUser(request);
        view.setCreateUser(user.getId());//创建用户id
        view.setCompanyId(user.getCompanyId());//创建用户单位
        int num = cmsViewService.insertView(view);
        Result result = new Result();
        if(num < 0){
            result.setCode(-1);
            result.setInfo("添加视图失败!");
        }
        return result;
    }

    /**
     * 修改视图信息
     * @param view
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/updateView")
    @ResponseBody
    public Object updateView(View view, HttpServletRequest request) throws Exception{
        int num = cmsViewService.updateView(view);
        Result result = new Result();
        if(num < 0){
            result.setCode(-1);
            result.setInfo("修改视图信息失败!");
        }
        return result;
    }

    /**
     * 删除视图信息
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/deleteView")
    @ResponseBody
    public Object deleteView(HttpServletRequest request) throws Exception{
        Map<String,Object> parameter = CommonUtil.getParameterFromRequest(request);
        String idsStr = parameter.get("ids")+"";
        String[] idArr;
        Result result = new Result();
        if(StringUtils.isNotBlank(idsStr)) {
            idArr = idsStr.split(",");
            int num = cmsViewService.deleteView(idArr);
            if(num < 0){
                result.setCode(-1);
                result.setInfo("删除视图信息失败!");
            }
        }else{
            result.setCode(-1);
            result.setInfo("删除失败:待删除视图ID无效!");
        }

        return result;
    }

    /**
     * 统一视图入口
     * @param viewId
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping("/{viewId}")
    public Object askView(@PathVariable("viewId")String viewId, HttpServletRequest request, Model model) throws Exception{
        if (StringUtils.isNotBlank(viewId)){
            View view = cmsViewService.getView(viewId);
            if (view != null){
                model.addAttribute("view", view);
                //查询视图下的按钮信息
                List<ViewButton> list = cmsViewButtonService.getViewButtonByViewId(viewId);
                model.addAttribute("buttonList", list);
                return "/page/cms/view.jsp";
            }
        }
        return "/common/404.jsp";
    }


}
