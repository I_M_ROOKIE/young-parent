package com.yy.young.cms.model;

import java.util.Date;

/**
 * 视图数据源实体类
 * Created by rookie on 2017-11-01.
 */
public class DataSource extends CmsBase {

    private String id;//数据源编号
    private String name;//数据源名称
    private String cols;//查询字段
    private String tableId;//关联表
    private String joinSql;//关联语句
    private String whereSql;//查询条件语句
    private String orderSql;//排序语句
    private String moduleId;//所属模块
    private int num;//显示顺序
    private String remark;//备注

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getCols() {
        return cols;
    }
    public void setCols(String cols) {
        this.cols = cols;
    }
    public String getTableId() {
        return tableId;
    }
    public void setTableId(String tableId) {
        this.tableId = tableId;
    }
    public String getJoinSql() {
        return joinSql;
    }
    public void setJoinSql(String joinSql) {
        this.joinSql = joinSql;
    }
    public String getWhereSql() {
        return whereSql;
    }
    public void setWhereSql(String whereSql) {
        this.whereSql = whereSql;
    }
    public String getOrderSql() {
        return orderSql;
    }
    public void setOrderSql(String orderSql) {
        this.orderSql = orderSql;
    }
    public String getModuleId() {
        return moduleId;
    }
    public void setModuleId(String moduleId) {
        this.moduleId = moduleId;
    }
    public Date getCreateTime() {
        return createTime;
    }
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    public Date getUpdateTime() {
        return updateTime;
    }
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
    public String getCreateUser() {
        return createUser;
    }
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }
    public String getCompanyId() {
        return companyId;
    }
    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }
    public int getNum() {
        return num;
    }
    public void setNum(int num) {
        this.num = num;
    }
    public String getRemark() {
        return remark;
    }
    public void setRemark(String remark) {
        this.remark = remark;
    }
}