package com.yy.young.cms.util;

import com.yy.young.cms.model.Column;

import java.util.*;

/**
 * cms常量
 * Created by rookie on 2017/8/11.
 */
public class CmsConstants {

    public static final String COMMON_BUTTON_MODULE_ID = "COMMON";//公共按钮虚拟模块ID
    /**
     * sql映射
     */
    public interface MAPPER{
        String CMS_MODULE = "com.yy.young.cms.mapper.module";//模块
        String CMS_BUSINESS = "com.yy.young.cms.mapper.business";//业务
        String CMS_TABLE = "com.yy.young.cms.mapper.table";//业务表
        String CMS_TABLE_COL = "com.yy.young.cms.mapper.tableCol";//业务表字段
        String CMS_DB = "com.yy.young.cms.mapper.db";//数据库操作
        String CMS_BUTTON = "com.yy.young.cms.mapper.button";//按钮
        String CMS_VIEW = "com.yy.young.cms.mapper.view";//视图
        String CMS_VIEW_BUTTON = "com.yy.young.cms.mapper.viewButton";//视图按钮
        String TB_CMS_DATA_SOURCE = "com.yy.young.cms.mapper.dataSource";//视图数据源
        String TB_CMS_CONFIG = "com.yy.young.cms.mapper.config";//配置
        String CMS_VISIT = "com.yy.young.cms.mapper.visit";//cms访问
        String CMS_TOPIC = "com.yy.young.cms.mapper.topic";//主题
        String CMS_COMMENT = "com.yy.young.cms.mapper.comment";//评论
        String CMS_GEN_MAIN = "com.yy.young.cms.mapper.genMain";//自动生成
        String CMS_GEN_COL = "com.yy.young.cms.mapper.genCol";//自动生成
    }

    /**
     * 字段类型
     */
    public interface ColumnType{
        String VARCHAR2 = "VARCHAR2";//字符串型
        String CHAR = "CHAR";//字符型
        String NUMBER = "NUMBER";//数字
        String DATE = "DATE";//时间
        String CLOB = "CLOB";//大文本
        String FLOAT = "FLOAT";//浮点类型
    }

    //CMS系统默认的字段集合
    public static final List<Column> DEFAULT_CMS_COLUMNS = new ArrayList<Column>() {
        {
            this.add(new Column("ID", ColumnType.VARCHAR2, 32, null, "1"));//UUID主键
            this.add(new Column("C_MODULEID", ColumnType.VARCHAR2, 32, null, "0"));//模块ID
            this.add(new Column("C_MODULENAME", ColumnType.VARCHAR2, 64, null, "0"));//模块名称
            this.add(new Column("C_BIZID", ColumnType.VARCHAR2, 32, null, "0"));//业务ID
            this.add(new Column("C_BIZNAME", ColumnType.VARCHAR2, 64, null, "0"));//业务名称
            this.add(new Column("C_SUBJECT", ColumnType.VARCHAR2, 256, null, "0"));//标题
            this.add(new Column("C_CREATERID", ColumnType.VARCHAR2, 32, null, "0"));//创建者ID
            this.add(new Column("C_CREATERNAME", ColumnType.VARCHAR2, 64, null, "0"));//创建者名称
            this.add(new Column("C_CREATETIME", ColumnType.VARCHAR2, 32, null, "0"));//创建时间
            this.add(new Column("C_UPDATETIME", ColumnType.VARCHAR2, 32, null, "0"));//修改时间
            this.add(new Column("C_COMPANYID", ColumnType.VARCHAR2, 32, null, "0"));//单位ID
            this.add(new Column("C_COMPANYNAME", ColumnType.VARCHAR2, 64, null, "0"));//单位名称
            this.add(new Column("C_DEPTID", ColumnType.VARCHAR2, 32, null, "0"));//部门ID
            this.add(new Column("C_DEPTNAME", ColumnType.VARCHAR2, 64, null, "0"));//部门名称
            this.add(new Column("C_DEPTIDWHOLE", ColumnType.VARCHAR2, 512, null, "0"));//组织全路径ID
            this.add(new Column("C_TRASHFLAG", ColumnType.VARCHAR2, 2, null, "0"));//删除标识位,1:删除,0:未删除
            this.add(new Column("C_FORMID", ColumnType.VARCHAR2, 32, null, "0"));//表单ID
        }
    };

    /**
     * 数据源常量标识
     */
    public interface DataSourceConst{
        String D_TODAY = "_d_today";//今天(时间类型)
        String S_TODAY = "_s_today";//今天(字符串类型)
        String D_MONTH = "_d_month";//当月(时间类型)
        String S_MONTH = "_s_month";//当月(字符串类型)
        String D_LAST_YEAR = "_d_lastYear";//去年(时间类型)
        String S_LAST_YEAR = "_s_lastYear";//去年(字符串类型)
        String D_THIS_YEAR = "_d_thisYear";//今年(时间类型)
        String S_THIS_YEAR = "_s_thisYear";//今年(字符串类型)
        String D_NEXT_YEAR = "_d_nextYear";//明年(时间类型)
        String S_NEXT_YEAR = "_s_nextYear";//明年(字符串类型)
        String USER_ID = "_userId";//当前用户id
        String USER_NAME = "_userName";//当前用户名称
        String USER_ROLE_ID = "_userRoleId";//当前用户角色id
        String USER_ROLE_NAME = "_userRoleName";//当前用户角色名称
        String USER_COMPANY_ID = "_userCompanyId";//当前用户单位ID
        String USER_COMPANY_NAME = "_userCompanyName";//当前用户单位名称
        String EQUAL = ";=;";//相等
        String NOT_EQUAL = ";!=;";//不相等
        String GT = ";>;";//大于
        String LT = ";<;";//小于
        String BETWEEN = ";between;";//between
        String LIKE = ";like;";//LIKE
        String IN = ";in;";//IN
        String AND = ";and;";//AND
        String OR = ";or;";//OR
        String BETWEEN_D_TODAY = BETWEEN + D_TODAY;
        String BETWEEN_S_TODAY = BETWEEN + S_TODAY;
        String BETWEEN_D_MONTH = BETWEEN + D_MONTH;
        String BETWEEN_S_MONTH = BETWEEN + S_MONTH;
        String BETWEEN_D_LAST_YEAR = BETWEEN + D_LAST_YEAR;
        String BETWEEN_S_LAST_YEAR = BETWEEN + S_LAST_YEAR;
        String BETWEEN_D_THIS_YEAR = BETWEEN + D_THIS_YEAR;
        String BETWEEN_S_THIS_YEAR = BETWEEN + S_THIS_YEAR;
        String BETWEEN_D_NEXT_YEAR = BETWEEN + D_NEXT_YEAR;
        String BETWEEN_S_NEXT_YEAR = BETWEEN + S_NEXT_YEAR;
    }
}
