<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
    <%--常量--%>
    <%@ include file="/common/constHead.jsp"%>
    <%--jQuery--%>
    <%@ include file="/common/jqueryHead.jsp"%>
    <%--jo--%>
    <%@ include file="/common/joHead.jsp"%>
    <%--bootstrap和字体--%>
    <%@ include file="/common/bootstrapHead.jsp"%>
    <%--layer--%>
    <%@ include file="/common/layerHead.jsp"%>
    <%--公共--%>
    <%@ include file="/common/commonHead.jsp"%>
    <%--日期选择--%>
    <%@ include file="/common/dateHead.jsp"%>
    <title>${model.remark!}表单</title>
    <script type="text/javascript">
        $(function(){
            var jParams = {
                "PKName" : "id",//主键属性名
                "saveAfter" : "toEdit",
                "addUrl" : "${model.moduleName}/${model.className?uncap_first}/insert.action",//新增
                "deleteUrl" : "${model.moduleName}/${model.className?uncap_first}/delete.action",//删除
                "updateUrl" : "${model.moduleName}/${model.className?uncap_first}/update.action",//修改
                "formDataUrl" : "${model.moduleName}/${model.className?uncap_first}/get.action"	//查询
            };
            joForm.initFormPage(jParams);//初始化
        });
        //初始化表单值前处理
        joForm.initFormValueBefore = function(oJson){
        <#list model.attributeList as item>
            <#if item.type=="Date">
            oJson.${item.name} = jo.formatDate(oJson.${item.name});
            </#if>
        </#list>
        };
    </script>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">
            <!--按钮栏-->
            <div class="form-group button-bar">
                <button type="button" isShow="joForm.isAdd" class="btn btn-sm btn-outline btn-default" onclick="joForm.save()">
                    <i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;新增
                </button>
                <button type="button" isShow="!joForm.isAdd" class="btn btn-sm btn-outline btn-default" onclick="joForm.update()">
                    <i class="fa fa-pencil-square" aria-hidden="true"></i>&nbsp;修改
                </button>
                <button type="button" isShow="!joForm.isAdd" class="btn btn-sm btn-outline btn-default" onclick="joForm.del()">
                    <i class="fa fa-trash" aria-hidden="true"></i>&nbsp;删除
                </button>
            </div>
            <!--按钮栏-->
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <form id="pageForm" name="pageForm" action="" method="post">
                <table class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
                <#list model.attributeList as item>
                <#--当元素为奇数个且存在下一个元素时,生成前半部分-->
                    <#if item_index%2==0 && item_has_next>
                    <tr>
                        <td width="15%" class="active <#if item.column.notNull=="1">must</#if>" align="">${item.remark!item.name}</td>
                        <td class="tnt">
                            <input type="text" <#if item.type=="Date">onclick="laydate()"</#if>  name="${item.name}" class="form-control input-sm <#if item.name=="id">readonly-edit</#if>" <#if item.column.notNull=="1">ErrEmpty</#if> />
                        </td>
                    <#--当元素为偶数个,生成后半部分-->
                    <#elseif item_index%2==1>
                        <td width="15%" class="active <#if item.column.notNull=="1">must</#if>" align="">${item.remark!item.name}</td>
                        <td class="tnt">
                            <input type="text" <#if item.type=="Date">onclick="laydate()"</#if>  name="${item.name}" class="form-control input-sm <#if item.name=="id">readonly-edit</#if>" <#if item.column.notNull=="1">ErrEmpty</#if> />
                        </td>
                    </tr>
                    <#else>
                        <tr>
                            <td width="15%" class="active <#if item.column.notNull=="1">must</#if>" align="">${item.remark!item.name}</td>
                            <td class="tnt" colspan="3">
                                <input type="text" <#if item.type=="Date">onclick="laydate()"</#if>  name="${item.name}" class="form-control input-sm <#if item.name=="id">readonly-edit</#if>" <#if item.column.notNull=="1">ErrEmpty</#if> />
                            </td>
                        </tr>
                    </#if>
                </#list>
                </table>
            </form>
        </div>
    </div>
</div>

<script src="<%=URL_STATIC%>static/plugin/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
<script type="text/javascript">
    jo.formatUI();//格式化jo组件
</script>
</body>
</html>
