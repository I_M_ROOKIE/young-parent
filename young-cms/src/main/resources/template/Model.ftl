package ${model.packageName}.${model.moduleName}.model;

import com.yy.young.common.core.excel.ExcelColumn;
import java.util.Date;
/**
* ${model.remark!}实体类
* Created by ${model.author!} on ${model.createTime?date}.
*/
public class ${model.className}{

    <#--变量-->
    <#list model.attributeList as item>
    @ExcelColumn(value = "${item.remark!item.name}", order = ${item_index+1})
    private ${item.type} ${item.name};//${item.remark!item.name}

    </#list>
    <#--无参构造器-->
    public ${model.className}(){
        super();
    }

    <#--get/set方法-->
    <#list model.attributeList as item>
    public ${item.type} get${item.name?cap_first}() {
        return ${item.name};
    }
    public void set${item.name?cap_first}(${item.type} ${item.name}) {
        this.${item.name} = ${item.name};
    }
    </#list>

}