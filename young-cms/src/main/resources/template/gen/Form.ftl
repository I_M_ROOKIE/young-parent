<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
    <%--常量--%>
    <%@ include file="/common/constHead.jsp"%>
    <%--jQuery--%>
    <%@ include file="/common/jqueryHead.jsp"%>
    <%--jo--%>
    <%@ include file="/common/joHead.jsp"%>
    <%--bootstrap和字体--%>
    <%@ include file="/common/bootstrapHead.jsp"%>
    <%--layer--%>
    <%@ include file="/common/layerHead.jsp"%>
    <%--公共--%>
    <%@ include file="/common/commonHead.jsp"%>
    <%--日期选择--%>
    <%@ include file="/common/dateHead.jsp"%>
    <title>${model.tableRemark!}表单</title>
    <script type="text/javascript">
        $(function(){
            var jParams = {
                "PKName" : "id",//主键属性名
                "saveAfter" : "toEdit",
                "addUrl" : "${model.moduleName}/${model.className?uncap_first}/insert",//新增
                "deleteUrl" : "${model.moduleName}/${model.className?uncap_first}/delete",//删除
                "updateUrl" : "${model.moduleName}/${model.className?uncap_first}/update",//修改
                "formDataUrl" : "${model.moduleName}/${model.className?uncap_first}/get",//查询
                "readonly2Label" : true //只读转文本
            };
            joForm.initFormPage(jParams);//初始化
        });
        //初始化新增页面回调函数(在初始化按钮前发生)
        joForm.initFormPageOfAdd = function(){

        };
        //初始化表单值前处理
        joForm.initFormValueBefore = function(oJson){
        <#list model.colList as item>
            <#if item.fieldType=="Date">
            oJson.${item.attrName} = jo.formatDate(oJson.${item.attrName});
            </#if>
        </#list>
        };
        //初始化表单值后处理
        joForm.initFormValueAfter = function(oJson){

        };
        //新增请求提交前回调函数
        joForm.dealDataAtSaveBefore = function(oJson){

        };
        //修改请求提交前回调函数
        joForm.dealDataAtUpdateBefore = function(oJson){

        };
        /* 关于joForm更多回调函数和配置参数可查看jo-page-form.js */
    </script>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">
            <!--按钮栏-->
            <div class="form-group button-bar">
                <button type="button" isShow="joForm.isAdd" class="btn btn-sm btn-outline btn-default" onclick="joForm.save()">
                    <i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;新增
                </button>
                <button type="button" isShow="!joForm.isAdd" class="btn btn-sm btn-outline btn-default" onclick="joForm.update()">
                    <i class="fa fa-pencil-square" aria-hidden="true"></i>&nbsp;修改
                </button>
                <button type="button" isShow="!joForm.isAdd" class="btn btn-sm btn-outline btn-default" onclick="joForm.del()">
                    <i class="fa fa-trash" aria-hidden="true"></i>&nbsp;删除
                </button>
            </div>
            <!--按钮栏-->
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <form id="pageForm" name="pageForm" action="" method="post">
                <table class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
                <#list model.colList as item>
                <#--当元素为奇数个且存在下一个元素时,生成前半部分-->
                    <#if item_index%2==0 && item_has_next>
                    <tr>
                        <td width="15%" class="active <#if item.controlMust=="1">must</#if>" align="">${item.fieldRemark!item.attrName}<#if item.fillExplain?? && item.fillExplain != ""><span class="fa fa-info-circle" style="padding: 0px 3px;color: #57C7D4;" onmouseover="window.tipsIdx=jo.tips('${item.fillExplain}',this,{tips:[2, '#3595CC'], time:10000});" onmouseout="jo.close(window.tipsIdx);"></span></#if></td>
                        <td class="tnt">
                        <#if item.controlType == "input">
                            <input type="text"   name="${item.attrName}" value="${item.controlDefault!}" class="form-control input-sm <#if item.controlReadonly=="add">readonly-add<#elseif item.controlReadonly=="edit">readonly-edit</#if>" <#if item.controlReadonly=="all">readonly</#if> <#if item.controlCheck?? && item.controlCheck != "">${item.controlCheck}="${item.controlCheckVal!}"</#if> />
                        <#elseif item.controlType == "date">
                            <input type="text" onclick="laydate()"  value="${item.controlDefault!}" name="${item.attrName}" class="form-control input-sm <#if item.controlReadonly=="add">readonly-add<#elseif item.controlReadonly=="edit">readonly-edit</#if>" <#if item.controlReadonly=="all">readonly</#if> <#if item.controlCheck?? && item.controlCheck != "">${item.controlCheck}="${item.controlCheckVal!}"</#if> />
                        <#elseif item.controlType == "select">
                            <select name="${item.attrName}" value="${item.controlDefault!}" class="form-control input-sm" <#if item.controlCheck?? && item.controlCheck != "">${item.controlCheck}="${item.controlCheckVal!}"</#if> >
                            <option value=""></option>
                            </select>
                        <#elseif item.controlType == "joSelect">
                            <select name="${item.attrName}" value="${item.controlDefault!}" class="joSelect form-control input-sm" data="" dataurl="${item.controlParam}" keyfield="id" valuefield="name" firstitem='{"id":"","name":""}' <#if item.controlCheck?? && item.controlCheck != "">${item.controlCheck}="${item.controlCheckVal!}"</#if> ></select>
                        <#elseif item.controlType == "checkbox">
                            <input type="text"   name="${item.attrName}" value="${item.controlDefault!}" class="form-control input-sm <#if item.controlReadonly=="add">readonly-add<#elseif item.controlReadonly=="edit">readonly-edit</#if>" <#if item.controlReadonly=="all">readonly</#if> <#if item.controlCheck?? && item.controlCheck != "">${item.controlCheck}="${item.controlCheckVal!}"</#if> />
                        <#elseif item.controlType == "radio">
                            <input type="text"   name="${item.attrName}" value="${item.controlDefault!}" class="form-control input-sm <#if item.controlReadonly=="add">readonly-add<#elseif item.controlReadonly=="edit">readonly-edit</#if>" <#if item.controlReadonly=="all">readonly</#if> <#if item.controlCheck?? && item.controlCheck != "">${item.controlCheck}="${item.controlCheckVal!}"</#if> />
                        <#elseif item.controlType == "textarea">
                            <textarea  name="${item.attrName}" value="" class="form-control input-sm <#if item.controlReadonly=="add">readonly-add<#elseif item.controlReadonly=="edit">readonly-edit</#if>" <#if item.controlReadonly=="all">readonly</#if> <#if item.controlCheck?? && item.controlCheck != "">${item.controlCheck}="${item.controlCheckVal!}"</#if> >${item.controlDefault!}</textarea>
                        <#elseif item.controlType == "tree">
                            <input type="text"  name="${item.attrName}" value="${item.controlDefault!}" class="form-control input-sm <#if item.controlReadonly=="add">readonly-add<#elseif item.controlReadonly=="edit">readonly-edit</#if>" <#if item.controlReadonly=="all">readonly</#if> <#if item.controlCheck?? && item.controlCheck != "">${item.controlCheck}="${item.controlCheckVal!}"</#if> />
                        <#elseif item.controlType == "trees">
                            <input type="text"  name="${item.attrName}" value="${item.controlDefault!}" class="form-control input-sm <#if item.controlReadonly=="add">readonly-add<#elseif item.controlReadonly=="edit">readonly-edit</#if>" <#if item.controlReadonly=="all">readonly</#if> <#if item.controlCheck?? && item.controlCheck != "">${item.controlCheck}="${item.controlCheckVal!}"</#if> />
                        <#elseif item.controlType == "list">
                            <input type="text"  name="${item.attrName}" value="${item.controlDefault!}" class="form-control input-sm <#if item.controlReadonly=="add">readonly-add<#elseif item.controlReadonly=="edit">readonly-edit</#if>" <#if item.controlReadonly=="all">readonly</#if> <#if item.controlCheck?? && item.controlCheck != "">${item.controlCheck}="${item.controlCheckVal!}"</#if> />
                        <#elseif item.controlType == "lists">
                            <input type="text"  name="${item.attrName}" value="${item.controlDefault!}" class="form-control input-sm <#if item.controlReadonly=="add">readonly-add<#elseif item.controlReadonly=="edit">readonly-edit</#if>" <#if item.controlReadonly=="all">readonly</#if> <#if item.controlCheck?? && item.controlCheck != "">${item.controlCheck}="${item.controlCheckVal!}"</#if> />
                        </#if>
                        </td>
                    <#--当元素为偶数个,生成后半部分-->
                    <#elseif item_index%2==1>
                        <td width="15%" class="active <#if item.controlMust=="1">must</#if>" align="">${item.fieldRemark!item.attrName}<#if item.fillExplain?? && item.fillExplain != ""><span class="fa fa-info-circle" style="padding: 0px 3px;color: #57C7D4;" onmouseover="window.tipsIdx=jo.tips('${item.fillExplain}',this,{tips:[2, '#3595CC'], time:10000});" onmouseout="jo.close(window.tipsIdx);"></span></#if></td>
                        <td class="tnt">
                        <#if item.controlType == "input">
                            <input type="text"   name="${item.attrName}" value="${item.controlDefault!}" class="form-control input-sm <#if item.controlReadonly=="add">readonly-add<#elseif item.controlReadonly=="edit">readonly-edit</#if>" <#if item.controlReadonly=="all">readonly</#if> <#if item.controlCheck?? && item.controlCheck != "">${item.controlCheck}="${item.controlCheckVal!}"</#if> />
                        <#elseif item.controlType == "date">
                            <input type="text" onclick="laydate()"  value="${item.controlDefault!}" name="${item.attrName}" class="form-control input-sm <#if item.controlReadonly=="add">readonly-add<#elseif item.controlReadonly=="edit">readonly-edit</#if>" <#if item.controlReadonly=="all">readonly</#if> <#if item.controlCheck?? && item.controlCheck != "">${item.controlCheck}="${item.controlCheckVal!}"</#if> />
                        <#elseif item.controlType == "select">
                            <select name="${item.attrName}" value="${item.controlDefault!}" class="form-control input-sm" <#if item.controlCheck?? && item.controlCheck != "">${item.controlCheck}="${item.controlCheckVal!}"</#if> >
                            <option value=""></option>
                            </select>
                        <#elseif item.controlType == "joSelect">
                            <select name="${item.attrName}" value="${item.controlDefault!}" class="joSelect form-control input-sm" data="" dataurl="${item.controlParam}" keyfield="id" valuefield="name" firstitem='{"id":"","name":""}' <#if item.controlCheck?? && item.controlCheck != "">${item.controlCheck}="${item.controlCheckVal!}"</#if> ></select>
                        <#elseif item.controlType == "checkbox">
                            <input type="text"   name="${item.attrName}" value="${item.controlDefault!}" class="form-control input-sm <#if item.controlReadonly=="add">readonly-add<#elseif item.controlReadonly=="edit">readonly-edit</#if>" <#if item.controlReadonly=="all">readonly</#if> <#if item.controlCheck?? && item.controlCheck != "">${item.controlCheck}="${item.controlCheckVal!}"</#if> />
                        <#elseif item.controlType == "radio">
                            <input type="text"   name="${item.attrName}" value="${item.controlDefault!}" class="form-control input-sm <#if item.controlReadonly=="add">readonly-add<#elseif item.controlReadonly=="edit">readonly-edit</#if>" <#if item.controlReadonly=="all">readonly</#if> <#if item.controlCheck?? && item.controlCheck != "">${item.controlCheck}="${item.controlCheckVal!}"</#if> />
                        <#elseif item.controlType == "textarea">
                            <textarea  name="${item.attrName}" value="" class="form-control input-sm <#if item.controlReadonly=="add">readonly-add<#elseif item.controlReadonly=="edit">readonly-edit</#if>" <#if item.controlReadonly=="all">readonly</#if> <#if item.controlCheck?? && item.controlCheck != "">${item.controlCheck}="${item.controlCheckVal!}"</#if> >${item.controlDefault!}</textarea>
                        <#elseif item.controlType == "tree">
                            <input type="text"  name="${item.attrName}" value="${item.controlDefault!}" class="form-control input-sm <#if item.controlReadonly=="add">readonly-add<#elseif item.controlReadonly=="edit">readonly-edit</#if>" <#if item.controlReadonly=="all">readonly</#if> <#if item.controlCheck?? && item.controlCheck != "">${item.controlCheck}="${item.controlCheckVal!}"</#if> />
                        <#elseif item.controlType == "trees">
                            <input type="text"  name="${item.attrName}" value="${item.controlDefault!}" class="form-control input-sm <#if item.controlReadonly=="add">readonly-add<#elseif item.controlReadonly=="edit">readonly-edit</#if>" <#if item.controlReadonly=="all">readonly</#if> <#if item.controlCheck?? && item.controlCheck != "">${item.controlCheck}="${item.controlCheckVal!}"</#if> />
                        <#elseif item.controlType == "list">
                            <input type="text"  name="${item.attrName}" value="${item.controlDefault!}" class="form-control input-sm <#if item.controlReadonly=="add">readonly-add<#elseif item.controlReadonly=="edit">readonly-edit</#if>" <#if item.controlReadonly=="all">readonly</#if> <#if item.controlCheck?? && item.controlCheck != "">${item.controlCheck}="${item.controlCheckVal!}"</#if> />
                        <#elseif item.controlType == "lists">
                            <input type="text"  name="${item.attrName}" value="${item.controlDefault!}" class="form-control input-sm <#if item.controlReadonly=="add">readonly-add<#elseif item.controlReadonly=="edit">readonly-edit</#if>" <#if item.controlReadonly=="all">readonly</#if> <#if item.controlCheck?? && item.controlCheck != "">${item.controlCheck}="${item.controlCheckVal!}"</#if> />
                        </#if>
                        </td>
                    </tr>
                    <#else>
                        <tr>
                            <td width="15%" class="active <#if item.controlMust=="1">must</#if>" align="">${item.fieldRemark!item.attrName}<#if item.fillExplain?? && item.fillExplain != ""><span class="fa fa-info-circle" style="padding: 0px 3px;color: #57C7D4;" onmouseover="window.tipsIdx=jo.tips('${item.fillExplain}',this,{tips:[2, '#3595CC'], time:10000});" onmouseout="jo.close(window.tipsIdx);"></span></#if></td>
                            <td class="tnt" colspan="3">
                            <#if item.controlType == "input">
                                <input type="text"   name="${item.attrName}" value="${item.controlDefault!}" class="form-control input-sm <#if item.controlReadonly=="add">readonly-add<#elseif item.controlReadonly=="edit">readonly-edit</#if>" <#if item.controlReadonly=="all">readonly</#if> <#if item.controlCheck?? && item.controlCheck != "">${item.controlCheck}="${item.controlCheckVal!}"</#if> />
                            <#elseif item.controlType == "date">
                                <input type="text" onclick="laydate()"  value="${item.controlDefault!}" name="${item.attrName}" class="form-control input-sm <#if item.controlReadonly=="add">readonly-add<#elseif item.controlReadonly=="edit">readonly-edit</#if>" <#if item.controlReadonly=="all">readonly</#if> <#if item.controlCheck?? && item.controlCheck != "">${item.controlCheck}="${item.controlCheckVal!}"</#if> />
                            <#elseif item.controlType == "select">
                                <select name="${item.attrName}" value="${item.controlDefault!}" class="form-control input-sm" <#if item.controlCheck?? && item.controlCheck != "">${item.controlCheck}="${item.controlCheckVal!}"</#if> >
                                <option value=""></option>
                                </select>
                            <#elseif item.controlType == "joSelect">
                                <select name="${item.attrName}" value="${item.controlDefault!}" class="joSelect form-control input-sm" data="" dataurl="${item.controlParam}" keyfield="id" valuefield="name" firstitem='{"id":"","name":""}' <#if item.controlCheck?? && item.controlCheck != "">${item.controlCheck}="${item.controlCheckVal!}"</#if> ></select>
                            <#elseif item.controlType == "checkbox">
                                <input type="text"   name="${item.attrName}" value="${item.controlDefault!}" class="form-control input-sm <#if item.controlReadonly=="add">readonly-add<#elseif item.controlReadonly=="edit">readonly-edit</#if>" <#if item.controlReadonly=="all">readonly</#if> <#if item.controlCheck?? && item.controlCheck != "">${item.controlCheck}="${item.controlCheckVal!}"</#if> />
                            <#elseif item.controlType == "radio">
                                <input type="text"   name="${item.attrName}" value="${item.controlDefault!}" class="form-control input-sm <#if item.controlReadonly=="add">readonly-add<#elseif item.controlReadonly=="edit">readonly-edit</#if>" <#if item.controlReadonly=="all">readonly</#if> <#if item.controlCheck?? && item.controlCheck != "">${item.controlCheck}="${item.controlCheckVal!}"</#if> />
                            <#elseif item.controlType == "textarea">
                                <textarea  name="${item.attrName}" value="" class="form-control input-sm <#if item.controlReadonly=="add">readonly-add<#elseif item.controlReadonly=="edit">readonly-edit</#if>" <#if item.controlReadonly=="all">readonly</#if> <#if item.controlCheck?? && item.controlCheck != "">${item.controlCheck}="${item.controlCheckVal!}"</#if> >${item.controlDefault!}</textarea>
                            <#elseif item.controlType == "tree">
                                <input type="text"  name="${item.attrName}" value="${item.controlDefault!}" class="form-control input-sm <#if item.controlReadonly=="add">readonly-add<#elseif item.controlReadonly=="edit">readonly-edit</#if>" <#if item.controlReadonly=="all">readonly</#if> <#if item.controlCheck?? && item.controlCheck != "">${item.controlCheck}="${item.controlCheckVal!}"</#if> />
                            <#elseif item.controlType == "trees">
                                <input type="text"  name="${item.attrName}" value="${item.controlDefault!}" class="form-control input-sm <#if item.controlReadonly=="add">readonly-add<#elseif item.controlReadonly=="edit">readonly-edit</#if>" <#if item.controlReadonly=="all">readonly</#if> <#if item.controlCheck?? && item.controlCheck != "">${item.controlCheck}="${item.controlCheckVal!}"</#if> />
                            <#elseif item.controlType == "list">
                                <input type="text"  name="${item.attrName}" value="${item.controlDefault!}" class="form-control input-sm <#if item.controlReadonly=="add">readonly-add<#elseif item.controlReadonly=="edit">readonly-edit</#if>" <#if item.controlReadonly=="all">readonly</#if> <#if item.controlCheck?? && item.controlCheck != "">${item.controlCheck}="${item.controlCheckVal!}"</#if> />
                            <#elseif item.controlType == "lists">
                                <input type="text"  name="${item.attrName}" value="${item.controlDefault!}" class="form-control input-sm <#if item.controlReadonly=="add">readonly-add<#elseif item.controlReadonly=="edit">readonly-edit</#if>" <#if item.controlReadonly=="all">readonly</#if> <#if item.controlCheck?? && item.controlCheck != "">${item.controlCheck}="${item.controlCheckVal!}"</#if> />
                            </#if>
                            </td>
                        </tr>
                    </#if>
                </#list>
                </table>
            </form>
        </div>
    </div>
</div>

<script src="<%=URL_STATIC%>static/plugin/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
<script type="text/javascript">
    jo.formatUI();//格式化jo组件
</script>
</body>
</html>
