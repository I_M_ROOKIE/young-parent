<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <%--常量--%>
    <%@ include file="/common/constHead.jsp"%>
    <%--jQuery--%>
    <%@ include file="/common/jqueryHead.jsp"%>
    <%--jo--%>
    <%@ include file="/common/joHead.jsp"%>
    <%--bootstrap和字体--%>
    <%@ include file="/common/bootstrapHead.jsp"%>
    <%--layer--%>
    <%@ include file="/common/layerHead.jsp"%>
    <%--zTree树--%>
    <%@ include file="/common/zTreeHead.jsp"%>
    <%--公共--%>
    <%@ include file="/common/commonHead.jsp"%>
    <%--日期选择--%>
    <%@ include file="/common/dateHead.jsp"%>
    <title>${model.tableRemark!}视图</title>
    <script type="text/javascript">
        $(function(){
            joView.init({grid:$("#mainList"),PKName:"id"});//初始化页面
        });
        //表格渲染时行处理,参数1为当前行的数据对象,参数2为当前行的索引值(最小为0)
        joView.handleItem = function(oItem,iIndex){

        };
        //表格渲染完成后的回调,参数为表格数据(数组对象)
        joView.setGridDataAfter = function(oList){

        };
        /* 关于joView更多回调函数和配置参数可查看jo-page-view.js */
    </script>
</head>
<body>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12">
            <%--检索栏--%>
            <form class="form-inline search-bar" id="pageForm">
            <#list model.colList as item>
                <#if item.asCondition != "1">
                <div class="form-group">
                    <label class="control-label">${item.fieldRemark}：&nbsp;</label>
                    <#if item.controlType == "input">
                    <input type="text" name="${item.attrName}" class="form-control input-sm">
                    <#elseif item.controlType == "date">
                    <input type="text" name="${item.attrName}" onclick="laydate()" class="form-control input-sm">
                    &nbsp;~&nbsp;<input type="text" name="${item.attrName}2" onclick="laydate()" class="form-control input-sm">
                    <#elseif item.controlType == "select">
                    <select name="${item.attrName}" class="form-control input-sm">
                        <option value=""></option>
                    </select>
                    <#elseif item.controlType == "joSelect">
                    <select name="${item.attrName}" class="joSelect form-control input-sm" data="" dataurl="${item.controlParam}" keyfield="id" valuefield="name" firstitem='{"id":"","name":""}'></select>
                    <#elseif item.controlType == "checkbox">
                    <input type="text" name="${item.attrName}" class="form-control input-sm">
                    <#elseif item.controlType == "radio">
                    <input type="text" name="${item.attrName}" class="form-control input-sm">
                    <#elseif item.controlType == "textarea">
                    <textarea name="${item.attrName}" class="form-control input-sm"></textarea>
                    <#elseif item.controlType == "tree">
                    <input type="text" name="${item.attrName}" class="form-control input-sm">
                    <#elseif item.controlType == "trees">
                    <input type="text" name="${item.attrName}" class="form-control input-sm">
                    <#elseif item.controlType == "list">
                    <input type="text" name="${item.attrName}" class="form-control input-sm">
                    <#elseif item.controlType == "lists">
                    <input type="text" name="${item.attrName}" class="form-control input-sm">
                    </#if>
                </div>
                </#if>
            </#list>
                <button type="button" class="btn btn-primary btn-sm" onclick="joView.select()"> <i class="fa fa-search" aria-hidden="true"></i>&nbsp;查询</button>
            </form>
            <%--/检索栏--%>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12">
            <%--按钮栏--%>
            <div class="form-group button-bar">
                <button isShow="" type="button" class="btn btn-sm btn-outline btn-default" onclick="joView.add()">
                    <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;新增
                </button>
                <button type="button" class="btn btn-sm btn-outline btn-default" onclick="joView.del()">
                    <i class="fa fa-trash-o" aria-hidden="true"></i>&nbsp;删除
                </button>
                <button type="button" class="btn btn-sm btn-outline btn-default" onclick="window.location.reload()">
                    <i class="fa fa-refresh" aria-hidden="true"></i>&nbsp;刷新
                </button>
            </div>
            <%--/按钮栏--%>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12">
            <%--grid--%>
            <div class="table-responsive">
                <table class="table table-bordered table-hover table-striped table-condensed" id="mainList" dataUrl="${model.moduleName}/${model.className?uncap_first}/getPage" deleteUrl="${model.moduleName}/${model.className?uncap_first}/delete" formUrl="page/${model.moduleName}/${model.className?uncap_first}Form.jsp">
                <#--定义临时变量temp_temp,用来表示第N个viewShow=1的字段配置,方便添加click事件-->
        <#assign temp_idx = 0>
        <#list model.colList as item>
            <#if item.viewShow == "1">
                <#assign temp_idx = temp_idx + 1>
                <#if item.fieldType=="Date">
                    <col field="[=jo.formatTime({${item.attrName}})]" title="${item.fieldRemark!item.attrName}" width="15%" align="" <#if temp_idx == 1>event="click"</#if> <#if item.supportSort == "1">order="${item.attrName}"</#if>/>
                <#else>
                    <col field="${item.attrName}" title="${item.fieldRemark!item.attrName}" width="15%" align="" <#if temp_idx == 1>event="click"</#if> <#if item.supportSort == "1">order="${item.attrName}"</#if>/>
                </#if>
            </#if>
        </#list>
                </table>
            </div>
            <%--/grid--%>

            <%--分页条--%>
            <div class="page-bar" gridId="mainList">

            </div>
            <%--/分页条--%>
        </div>
    </div>
</div>

<script src="<%=URL_STATIC%>static/plugin/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
<script type="text/javascript">
    jo.formatUI();//格式化jo组件
</script>
</body>
</html>
