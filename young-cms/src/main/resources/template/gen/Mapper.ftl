<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="${model.packageName}.${model.moduleName}.mapper.${model.className?uncap_first}">
    <!--查询字段信息-->
    <sql id="searchCol">
    <#list model.colList as item>
        <#if item_has_next>
        O.${item.fieldName} AS "${item.attrName}",
        <#else>
        O.${item.fieldName} AS "${item.attrName}"
        </#if>
    </#list>
    </sql>
    <!--关联查询相关sql-->
    <sql id="joinSql">
    </sql>
    <!-- 查询 -->
    <select id="getList" parameterType="${model.packageName}.${model.moduleName}.model.${model.className}" resultType="${model.packageName}.${model.moduleName}.model.${model.className}">
        SELECT
        <include refid="searchCol"></include>
        FROM ${model.tableName} O
        <include refid="joinSql"></include>
        <where>
        <#list model.colList as item>
        <#if item.asCondition?? && item.asCondition != "1" && item.fieldType == "Date">
            <if test="${item.attrName} != null and ${item.attrName} != ''">
                AND O.${item.fieldName} &gt;= ${"#"}{${item.attrName}}
            </if>
            <if test="${item.attrName}2 != null and ${item.attrName}2 != ''">
                AND O.${item.fieldName} &lt;= ${"#"}{${item.attrName}2}
            </if>
        <#elseif item.asCondition == "2">
            <if test="${item.attrName} != null and ${item.attrName} != ''">
                AND O.${item.fieldName} = ${"#"}{${item.attrName}}
            </if>
        <#elseif item.asCondition == "3">
            <if test="${item.attrName} != null and ${item.attrName} != ''">
                AND O.${item.fieldName} LIKE '%${"$"}{${item.attrName}}%'
            </if>
        </#if>
        </#list>
        </where>
        <choose>
        <#list model.colList as item>
        <#if item.supportSort == "1">
            <when test="orderBy != null and orderBy == '${item.attrName}'">
                ORDER BY O.${item.fieldName} ${"$"}{orderType}
            </when>
        <#else>
        </#if>
        </#list>
            <otherwise></otherwise>
        </choose>
    </select>
    <!-- 统计数量 -->
    <select id="count" parameterType="${model.packageName}.${model.moduleName}.model.${model.className}" resultType="int">
        SELECT
            count(*)
        FROM ${model.tableName} O
        <where>
        <#list model.colList as item>
        <#if item.asCondition?? && item.asCondition != "1" && item.fieldType == "Date">
            <if test="${item.attrName} != null and ${item.attrName} != ''">
                AND O.${item.fieldName} &gt; ${"#"}{${item.attrName}}
            </if>
            <if test="${item.attrName}2 != null and ${item.attrName}2 != ''">
                AND O.${item.fieldName} &lt; ${"#"}{${item.attrName}2}
            </if>
        <#elseif item.asCondition == "2">
            <if test="${item.attrName} != null and ${item.attrName} != ''">
                AND O.${item.fieldName} = ${"#"}{${item.attrName}}
            </if>
        <#elseif item.asCondition == "3">
            <if test="${item.attrName} != null and ${item.attrName} != ''">
                AND O.${item.fieldName} LIKE '%${"$"}{${item.attrName}}%'
            </if>
        </#if>
        </#list>
        </where>
    </select>
    <!--根据编号查询-->
    <select id="get" resultType="${model.packageName}.${model.moduleName}.model.${model.className}">
        SELECT
        <include refid="searchCol"></include>
        FROM ${model.tableName} O
        <include refid="joinSql"></include>
        WHERE O.ID=${"#"}{id}
    </select>
    <!-- 插入 -->
    <insert id="insert" parameterType="${model.packageName}.${model.moduleName}.model.${model.className}">
        INSERT INTO ${model.tableName}(
        <#list model.colList as item>
            <#if item_has_next>
            ${item.fieldName},
            <#else>
            ${item.fieldName}
            </#if>
        </#list>
        ) VALUES(
        <#list model.colList as item>
            <#if item_has_next>
            ${"#"}{${item.attrName}},
            <#else>
            ${"#"}{${item.attrName}}
            </#if>
        </#list>
        )
    </insert>
    <!-- 修改 -->
    <update id="update" parameterType="${model.packageName}.${model.moduleName}.model.${model.className}">
        UPDATE ${model.tableName}
        <set>
        <#list model.colList as item>
        <#if item.attrName != "id">
            <if test="${item.attrName} != null">
            ${item.fieldName} = ${"#"}{${item.attrName}},
            </if>
        </#if>
        </#list>
        </set>
        WHERE ID=${"#"}{id}
    </update>
    <!-- 删除 -->
    <delete id="delete">
        DELETE FROM ${model.tableName} WHERE ID = ${"#"}{id}
    </delete>
</mapper>