package ${model.packageName}.${model.moduleName}.model;

import com.yy.young.common.core.excel.ExcelColumn;
import java.util.Date;
/**
* ${model.tableRemark!}实体类
* Created by ${model.author!} on ${model.createTime?date}.
*/
public class ${model.className}{

    <#--变量-->
    <#list model.colList as item>
    <#if item.asCondition?? && item.asCondition != "1" && item.fieldType == "Date">
    @ExcelColumn(value = "${item.fieldRemark!item.fieldName}", order = ${item_index+1})
    private ${item.fieldType} ${item.attrName};//${item.fieldRemark!item.fieldName}
    private ${item.fieldType} ${item.attrName}2;//${item.fieldRemark!item.fieldName},按时间检索时作为结束时间使用
    <#else>
    @ExcelColumn(value = "${item.fieldRemark!item.fieldName}", order = ${item_index+1})
    private ${item.fieldType} ${item.attrName};//${item.fieldRemark!item.fieldName}
    </#if>
    </#list>

    private String orderBy;//排序字段
    private String orderType;//排序类型,升序asc/降序desc

    <#--无参构造器-->
    public ${model.className}(){
        super();
    }

    <#--get/set方法-->
    <#list model.colList as item>
    <#if item.asCondition?? && item.asCondition != "1" && item.fieldType == "Date">
    public ${item.fieldType} get${item.attrName?cap_first}() {
    return ${item.attrName};
    }
    public void set${item.attrName?cap_first}(${item.fieldType} ${item.attrName}) {
    this.${item.attrName} = ${item.attrName};
    }
    public ${item.fieldType} get${item.attrName?cap_first}2() {
    return ${item.attrName}2;
    }
    public void set${item.attrName?cap_first}2(${item.fieldType} ${item.attrName}2) {
    this.${item.attrName}2 = ${item.attrName}2;
    }
    <#else>
    public ${item.fieldType} get${item.attrName?cap_first}() {
    return ${item.attrName};
    }
    public void set${item.attrName?cap_first}(${item.fieldType} ${item.attrName}) {
    this.${item.attrName} = ${item.attrName};
    }
    </#if>
    </#list>

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

}