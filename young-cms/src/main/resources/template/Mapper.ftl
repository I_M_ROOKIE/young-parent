<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="${model.packageName}.${model.moduleName}.mapper.${model.className?uncap_first}">
    <!--查询字段信息-->
    <sql id="searchCol">
    <#list model.attributeList as item>
        <#if item_has_next>
        O.${item.column.field} AS "${item.name}",
        <#else>
        O.${item.column.field} AS "${item.name}"
        </#if>
    </#list>
    </sql>
    <!--关联查询相关sql-->
    <sql id="joinSql">
    </sql>
    <!-- 查询 -->
    <select id="getList" parameterType="${model.packageName}.${model.moduleName}.model.${model.className}" resultType="${model.packageName}.${model.moduleName}.model.${model.className}">
        SELECT
        <include refid="searchCol"></include>
        FROM ${model.tableName} O
        <include refid="joinSql"></include>
        <where>
            <if test="id != null and id != ''">
                AND O.ID = ${"#"}{id}
            </if>
        </where>
    </select>
    <!-- 统计数量 -->
    <select id="count" parameterType="${model.packageName}.${model.moduleName}.model.${model.className}" resultType="int">
        SELECT
            count(*)
        FROM ${model.tableName} O
        <where>
            <if test="id != null and id != ''">
                AND O.ID = ${"#"}{id}
            </if>
        </where>
    </select>
    <!--根据编号查询-->
    <select id="get" resultType="${model.packageName}.${model.moduleName}.model.${model.className}">
        SELECT
        <include refid="searchCol"></include>
        FROM ${model.tableName} O
        <include refid="joinSql"></include>
        WHERE O.ID=${"#"}{id}
    </select>
    <!-- 插入 -->
    <insert id="insert" parameterType="${model.packageName}.${model.moduleName}.model.${model.className}">
        INSERT INTO ${model.tableName}(
        <#list model.attributeList as item>
            <#if item_has_next>
            ${item.column.field},
            <#else>
            ${item.column.field}
            </#if>
        </#list>
        ) VALUES(
        <#list model.attributeList as item>
            <#if item_has_next>
            ${"#"}{${item.name}},
            <#else>
            ${"#"}{${item.name}}
            </#if>
        </#list>
        )
    </insert>
    <!-- 修改 -->
    <update id="update" parameterType="${model.packageName}.${model.moduleName}.model.${model.className}">
        UPDATE ${model.tableName}
        <set>
        <#list model.attributeList as item>
            <if test="${item.name} != null">
            ${item.column.field} = ${"#"}{${item.name}},
            </if>
        </#list>
        </set>
        WHERE ID=${"#"}{id}
    </update>
    <!-- 删除 -->
    <delete id="delete">
        DELETE FROM ${model.tableName} WHERE ID = ${"#"}{id}
    </delete>
</mapper>