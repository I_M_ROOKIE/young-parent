package ${model.packageName}.${model.moduleName}.service.impl;

import com.yy.young.common.util.CommonUtil;
import com.yy.young.dal.service.IDataAccessService;
import com.yy.young.dal.util.Page;
import ${model.packageName}.${model.moduleName}.model.${model.className};
import ${model.packageName}.${model.moduleName}.service.I${model.className}Service;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.annotation.Resource;
import java.util.List;

/**
* ${model.remark!}服务实现
* Created by rookie on 2018-04-03.
*/
@Service("${model.className?uncap_first}Service")
public class ${model.className}ServiceImpl implements I${model.className}Service {

    @Resource(name = "dataAccessService")
    IDataAccessService dataAccessService;//数据层服务

    private static final Logger logger = LoggerFactory.getLogger(${model.className}ServiceImpl.class);

    private static final String MAPPER = "${model.packageName}.${model.moduleName}.mapper.${model.className?uncap_first}";//mapper的namespace

    //获取数据列表
    @Override
    public List<${model.className}> getList(${model.className} obj) throws Exception {
        return dataAccessService.getList(MAPPER + ".getList", obj);
    }

    //获取数据列表(分页)
    @Override
    public List<${model.className}> getPage(${model.className} obj, Page page) throws Exception {
        return dataAccessService.getList(MAPPER + ".getList", obj, page);
    }

    //查询单条
    @Override
    public ${model.className} get(String id) throws Exception {
        return (${model.className})dataAccessService.getObject(MAPPER + ".get", id);
    }

    //统计数量
    @Override
    public int count(${model.className} obj) throws Exception {
        return (Integer)dataAccessService.getObject(MAPPER + ".count", obj);
    }

    //修改
    @Override
    public int update(${model.className} obj) throws Exception {
        return dataAccessService.update(MAPPER + ".update", obj);
    }

    //批量删除
    @Override
    public int delete(String[] idArr) throws Exception {
        int i = 0;
        for(String id : idArr){
            i += this.delete(id);
        }
        return i;
    }

    //删除单条
    @Override
    public int delete(String id) throws Exception {
        return dataAccessService.delete(MAPPER + ".delete", id);
    }

    //插入
    @Override
    public int insert(${model.className} obj) throws Exception {
        return dataAccessService.insert(MAPPER + ".insert", obj);
    }

    //批量插入
    @Override
    public int batchInsert(List<${model.className}> list) throws Exception {
        int i = 0;
        for(${model.className} item : list){
            i += this.insert(item);
        }
        return i;
    }

}